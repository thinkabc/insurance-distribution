
const urlPrefix = '/cxb/nol/';
//https://uat-lm.libertymutual.com.cn
const WEB_DOMAIN_FULL_PATH = window.location.protocol + "//" + window.location.hostname;
// const WEB_DOMAIN_FULL_PATH = window.location.protocol + "//" + window.location.hostname + ":" + window.location.port;
export const RequestUrl = {
    //续保车辆查询
    RENEWAL_QUERY: urlPrefix + "car/saleRenewalQuery",
    //投保车辆信息查询
    SALE_QUERY_VEHICLE: urlPrefix + "car/saleQueryVehicle",
    //Vin码查询
    SALE_QUERY_VIN: urlPrefix + "car/saleQueryVin",
    //车险投保初始化
    CAR_OFFER_INIT: urlPrefix + "carInit/getCarInit",
    //保费计算
    COMBINE_CALCULATE: urlPrefix + "car/combinecalculate",
    //提交核保
    PREMINUM_COMMIT: urlPrefix + "car/preminumCommit",
    //获得车辆初始化选择
    GET_CAR_SELE_INIT: urlPrefix + "carInit/getCarSeleInit",
    //车辆使用性质
    GET_USER_NATURE: urlPrefix + "carInit/getUserNature",
    //交叉销售计划查询
    CROSS_SALE_PLAN: urlPrefix + "car/crossSalePlan",
    //交叉销售计划保存
    CROSS_SALE_PLAN_SAVE: urlPrefix + "car/crossSalePlanSave",
    //保存数据信息
    SAVE_DATA: urlPrefix + "insure/saveData",
    //转保数据处理 
    VERIFY: urlPrefix + "car/verify",

    FIND_SAVE_DATA_ID: urlPrefix + "insure/findSaveDataId",

}
