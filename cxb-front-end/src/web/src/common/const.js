
//企业微信信息
export const WE_CHAT_ENTERPRISE = {
  CORPID: "wx09445574c762c28e",//企业号
  AGENTID_AYD: "1000008",//应用号 - 爱运动
}

//微信公众号信息
export const WE_CHAT = {
  ID: 'wxaf82fc366804d0e9', //公众号的应用程序ID,appID唯一标识（账户），正式公众号=wxf1583da9a8feb8d2,测试公众号=wxaf82fc366804d0e9
  NONCE_STR_STR: '6b22b788-2371-4ca3-96e1-155a53689e3f',//微信分享生成签名的随机串
  TIMESTAMP_STR: Date.parse(new Date()) / 1000,//微信分享生成签名的时间戳

  shareDesc_Indexs: '  买保险，查保单，查理赔……还有更多贴心服务，尽在恒华出行宝商城！', //微信分享 - 首页、列表页和投保页 - 摘要信息
  shareDesc_channel: '  邀请您加入恒华出行宝，移动投保，实时提现。', //微信分享 - 渠道注册 - 摘要信息

  OAUTH_TYPE_DEFAULT: "default",//授权类型 - 默认授权
  OAUTH_TYPE_CHANNEL: "channel",//授权类型 - 渠道注册授权
}

export const FooterIndex = {
  INDEX: 0,
  INSURANCE: 1,
  SERVICE: 2,
  MY: 3
}

export const PopState = {
  VUX_POP_IN: 'vux-pop-in',
  VUX_POP_OUT: 'vux-pop-out',
  VUX_POP_NULL: 'vux-pop-null'
}

export const FooterShow = {
  TRUE: true,
  FALSE: false
}
export const Car_Mutations = {
  CAR_RENEWAL_QUERY: "CAR_RENEWAL_QUERY",

  CAR_INSURE_SAVE: "CAR_INSURE_SAVE",

  CAR_SET_CAR_ADDR: "CAR_SET_CAR_ADDR",

  SAVE_DATA_CAR_INFO: "SAVE_DATA_CAR_INFO",

  GET_SALE_QUERY_VEHICLE: "GET_SALE_QUERY_VEHICLE",

  GET_SALE_QUERY_VIN_NO: "GET_SALE_QUERY_VIN_NO",

  CAR_SELECT_SAVE: "CAR_SELECT_SAVE",

  ADD_CAR_INFO_SAVE: "ADD_CAR_INFO_SAVE",

  CAR_CONFIG_SAVE: "CAR_CONFIG_SAVE",

  CAR_OFFER_INIT: "CAR_OFFER_INIT",

  //报价页面保存数据
  CAR_OFFER_SAVE: "CAR_OFFER_SAVE",

  CAR_OFFER: "CAR_OFFER",

  SET_OFFER_RESULT: "SET_OFFER_RESULT",

  SET_SPECIAL_AGREEMENT: "SET_SPECIAL_AGREEMENT",

  SAVE_SPECIAL_AGREEMENT: "SAVE_SPECIAL_AGREEMENT",

  SAVE_OFFER_RESULT: "SAVE_OFFER_RESULT",

  DOWN_LIST: "DOWN_LIST",

  SET_QUERY_STATE: "SET_QUERY_STATE",

  SET_INIT_SELE_CAR: "SET_INIT_SELE_CAR",

  GET_USER_NATURE_LIST: "GET_USER_NATURE_LIST",

  SAVE_DISTRIBUTION: "SAVE_DISTRIBUTION",

  COMMIT_DISTRIBUTION: "COMMIT_DISTRIBUTION",
  //查询交叉销售计划
  CROSS_SALE_PLAN: "CROSS_SALE_PLAN",
  //交叉销售计划保存
  CROSS_SALE_PLAN_SAVE: "CROSS_SALE_PLAN_SAVE",

  SET_CAR_AGR_INFO: "SET_CAR_AGR_INFO",

  SET_PRESERVATION: "SET_PRESERVATION",

  VERIFY: "VERIFY",

  SET_CAR_OFFER_DATA: "SET_CAR_OFFER_DATA",

  CAR_GET_SAVE_FIND_ID: "CAR_GET_SAVE_FIND_ID"
}
export const Mutations = {
  CAR_SET_CAR_ADDR: "CAR_SET_CAR_ADDR",
  SET_OLD_VERSION: "SET_OLD_VERSION",//设置旧版本号为最新版本号
  ADD_TEST: "ADD_TEST",
  MY_ADD_TEST: "MY_ADD_TEST",
  FOOTER_STATE: "FOOTER_STATE",
  POP_STATE: "POP_STATE",
  UPDATE_LOADING_STATUS: "UPDATE_LOADING_STATUS",
  FOOTER_SHOW: "FOOTER_SHOW",

  // 投保页面其他数据保存


  // 保存首页基本信息
  INDEX_QUERY_DATA: "INDEX_QUERY_DATA",

  // -----------------用户信息-保存-初始化
  SAVE_USER_INFO_INIT: "SAVE_USER_INFO_INIT",
  // -----------------用户信息-保存
  SAVE_USER_INFO: "SAVE_USER_INFO",
  // -----------------保存渠道用户usercode
  SAVE_CHANNEL_USER_CODE: "SAVE_CHANNEL_USER_CODE",
  // -----------------读取渠道用户
  READ_CHANNEL_USER_CODE: "READ_CHANNEL_USER_CODE",
  // -----------------零时的渠道用户信息-保存
  SAVE_USER_CHANNEL_INFO: "SAVE_USER_CHANNEL_INFO",
  // -----------------用户信息-清除
  REMOVE_USER_INFO: "REMOVE_USER_INFO",
  //刷新银行卡列表
  REFRESH_BANK_LIST: "REFRESH_BANK_LIST",
  //查询银行卡列表
  FIND_BANK_LIST: "FIND_BANK_LIST",

  //保存保险的基本信息
  INSURANCE_OPERATION: "INSURANCE_OPERATION",


  //设置地区
  INSURE_SET_REGION: "INSURE_SET_REGION",

  ORDER_OPERATION: "ORDER_OPERATION",

  SET_FIND_DATA: "SET_FIND_DATA",

  SET_IFRAME_DATA: "SET_IFRAME_DATA",

  //查询电子发票
  ORDER_QUERY_EINV: "ORDER_QUERY_EINV",

  ORDER_SET_SHOW_EINV: "ORDER_SET_SHOW_EINV",

  PAY_SET_DATA: "PAY_SET_DATA",


  //投保回调后的数据保存
  INSURE_RES_DATA: "INSURE_RES_DATA",

  INSURE_CURPRICE: "INSURE_CURPRICE",

  SEND_POLICY_EMIAL: "SEND_POLICY_EMIAL",

  INSURE_DATA_INDEX_DATA: "INSURE_DATA_INDEX_DATA",

  SAVE_INSURE_DEFAULT_SAVE: "SAVE_INSURE_DEFAULT_SAVE",
  SAVE_INSURE_DATA: "SAVE_INSURE_DATA",

  SAVE_INSURE_SET_DATA: "SAVE_INSURE_SET_DATA",

  SET_COMMIT_INSURE_DATA: "SET_COMMIT_INSURE_DATA",

  SAVE_HOT_AREA: "SAVE_HOT_AREA",

  SHARE_URL_SET: "SHARE_URL_SET",

  ORDER_SET_EMAIL: "ORDER_SET_EMAIL",

  AD_ACTIVITY: "AD_ACTIVITY",

  SET_SHOW_ELE: "SET_SHOW_ELE",

  //判断是否为微信浏览器
  SET_IS_WE_CHAR: "SET_IS_WE_CHAR",

  //是否显示历史赔案
  SHOW_HISTORY_CASE: "SHOW_HISTORY_CASE",

  //service马上报案-查询保单
  QUERY_POLICYS_SERVICE: "QUERY_POLICYS_SERVICE",

  //service进度查询-查询报案列表
  QUERY_REPORTS_SERVICE: "QUERY_REPORTS_SERVICE",

  //设置是否显示国家
  SHOW_SELE_COUNTRY: "SHOW_SELE_COUNTRY",

  //是否显示马上报案
  IS_SHOW_REPORT_IM: "IS_SHOW_REPORT_IM",

  SET_MENU: "SET_MENU",

  SET_IS_GET_INDEX: "SET_IS_GET_INDEX",

  //理赔类型
  CLIAIMS_TYPE: "CLIAIMS_TYPE",

  //保存产品
  SET_PRODUCT_LIST: "SET_PRODUCT_LIST",

  CLIAIMS_DATA: "CLIAIMS_DATA",

  SET_MSG_DATA: "SET_MSG_DATA",

  CUR_CLAUSE_TITLE_ONE: "CUR_CLAUSE_TITLE_ONE",

  CUR_CLAUSE_TITLE_LIST: "CUR_CLAUSE_TITLE_LIST",

  GET_CLAUSE_STORE_ITEM: "GET_CLAUSE_STORE_ITEM",

  SET_REFEREE_DATA: "SET_REFEREE_DATA",

  SET_SHARE_UUID: "SET_SHARE_UUID",

  ORDER_POLICYS: "ORDER_POLICYS",

  CLEAN_PRP_CMAIN_LIST: "CLEAN_PRP_CMAIN_LIST",

  OCCUPATION_GET: "OCCUPATION_GET",

  SET_INSURE_OCCUPATION_VALUE: "SET_INSURE_OCCUPATION_VALUE",

  GET_INSURE_OCCUPATION_DATA: "GET_INSURE_OCCUPATION_DATA",

  SET_INSURE_OCCUPATION_VALUE: "SET_INSURE_OCCUPATION_VALUE",

  SET_IS_WE_CHAR_STATE: "SET_IS_WE_CHAR_STATE",

  IS_SHOW_MY: "IS_SHOW_MY",

  SET_CAR_ADDR: "SET_CAR_ADDR",

  AXTX_GET_DATA: "AXTX_GET_DATA",

  AXTX_SAVE_DATA: "AXTX_SAVE_DATA",

  SET_AXTX_AGR: "SET_AXTX_AGR",

  INSURE_RES_DATA_AXTX: "INSURE_RES_DATA_AXTX",

  INSURANCE_LIST_TOP: "INSURANCE_LIST_TOP",

  VUE_SCROLLER_BTN: "VUE_SCROLLER_BTN",

  VUE_SCROLLER_NO_MORE_DATA: "VUE_SCROLLER_NO_MORE_DATA",

  GET_DETAILS_DAY: "GET_DETAILS_DAY",

  GET_DETAILS_MONTH: "GET_DETAILS_MONTH",

  ORDER_SELE_STATE: "ORDER_SELE_STATE",

  PRODUCT_MANAGE: "PRODUCT_MANAGE",

  PRODUCT_MANAGE_SAVE: "PRODUCT_MANAGE_SAVE",

  GET_ORDER_DETAIL: "GET_ORDER_DETAIL",
  //软文列表
  SHOP_SOFT_PAPER_LIST: "SHOP_SOFT_PAPER_LIST",
  //海报列表
  SHOP_POSTER_LIST: "SHOP_POSTER_LIST",
  //查询单个海报
  SET_POSTER: "SET_POSTER",

  SET_REFEREE_PRO: "SET_REFEREE_PRO",

  SET_SHOW_PAY_DATA: "SET_SHOW_PAY_DATA",

  SET_SHOW_PAY_DEFAULT: "SET_SHOW_PAY_DEFAULT",

  GET_DETAILS_YEAR: "GET_DETAILS_YEAR",

  PERFORMANCE_DETAILS: "PERFORMANCE_DETAILS",

  SET_ISPERDE_END: "SET_ISPERDE_END",

  SET_PARAMETERS: "SET_PARAMETERS",

  GET_PAY_DATA: "GET_PAY_DATA",

  //查询所有常驻地区
  GET_ALL_HOT_AREA: "GET_ALL_HOT_AREA",

  SET_IS_SHOP: "SET_IS_SHOP",

  SET_SHOP_HEADER_STATE: "SET_SHOP_HEADER_STATE",

  SET_IS_SHOW_DETL: "SET_IS_SHOW_DETL",
  SET_IS_SHOW_HOT: "SET_IS_SHOW_HOT",

  SET_CUR_SHOWINSURANCE: "SET_CUR_SHOWINSURANCE",

  REF_SHARE_U_UID: "REF_SHARE_U_UID",

  SET_SOFT_PAPER: "SET_SOFT_PAPER",

  SHOP_INFO_SAVE: "SHOP_INFO_SAVE",

  GET_IP_BAIDU_REGION: "GET_IP_BAIDU_REGION",
  INIT: "INIT",

  YMD_PERFORMANCE_DETAILS: "YMD_PERFORMANCE_DETAILS",
  SET_POSTER_LIST: "SET_POSTER_LIST",

  SET_SHARE_DATA: "SET_SHARE_DATA",

  GET_NEW_MONTH: "GET_NEW_MONTH",

  PER_MONTH_COUNT: "PER_MONTH_COUNT",

  IS_QUERY_HEADER: "IS_QUERY_HEADER",

  SAVE_SHARE_AGGER_DATA: "SAVE_SHARE_AGGER_DATA",

  IS_SHOW_VUE_SCROLLER_RE: "IS_SHOW_VUE_SCROLLER_RE",

  PERFORMANCE_DETAILS_PER_DE_END: "PERFORMANCE_DETAILS_PER_DE_END",

  SAVE_SHARE_OTHER_INFO: "SAVE_SHARE_OTHER_INFO",

  GET_CARLIMIT_NO: "GET_CARLIMIT_NO",

  //我的积分
  MY_INTEGRAL: "MY_INTEGRAL",
  //积分收支记录
  INTEGRAL_IN_OR_OUT_LIST: "INTEGRAL_IN_OR_OUT_LIST",
  //查询积分详情
  INTEGRAL_DETAIL: "INTEGRAL_DETAIL",
  //积分兑换记录
  INTEGRAL_EXCHANGE_LIST: "INTEGRAL_EXCHANGE_LIST",

  GET_PROMOTION_FEE: "GET_PROMOTION_FEE",

  // IS_SHOW_RATE: "IS_SHOW_RATE",

  LIST_TYPE: "LIST_TYPE",

  GET_QRCODE: "GET_QRCODE",

  SET_VAL_DATA: "SET_VAL_DATA",

  RULE_NUM: "RULE_NUM",

  QUERY_DRAW_TIMES: "QUERY_DRAW_TIMES",

  ACTIVITY_DRAW: "ACTIVITY_DRAW",

  IS_SHOW_RED: "IS_SHOW_RED",

  IS_SHOW_ROUTE_RED: "IS_SHOW_ROUTE_RED",

  SET_TRIGGER: "SET_TRIGGER",

  RED_TRIGGER: "RED_TRIGGER",

  //根据身份证查询用户信息
  APPLI_FIND_ID_CAR: "APPLI_FIND_ID_CAR",

  SET_DATA_INTEGRAL: "SET_DATA_INTEGRAL",
  //补全车辆信息保存
  ADD_CAR_INFO_SAVE: "ADD_CAR_INFO_SAVE",

  //AXTX查询车架号
  AXTX_GET_VIN: "AXTX_GET_VIN",

  SET_QUERY_STATE: "SET_QUERY_STATE",

  FIND_BY_QUE_ID: "FIND_BY_QUE_ID",

  FIND_BY_INPUT_ID: "FIND_BY_INPUT_ID",

  SET_CAR_AGR: "SET_CAR_AGR",

  GET_REGISTER_QRCODE: "GET_REGISTER_QRCODE",//邀请好友注册的二维码接口

  GET_FIND_ALL_FRIEND: "GET_FIND_ALL_FRIEND",

  SET_PINYIN_DETAIL: "SET_PINYIN_DETAIL",

  IS_SHOW_MSG_COM_DATA: "IS_SHOW_MSG_COM_DATA",

  IS_SHOW_MSG_COM_DATA_STATE: "IS_SHOW_MSG_COM_DATA_STATE",

  //初始化险种信息
  QUERY_INIT_RISK_DATA: "QUERY_INIT_RISK_DATA",

  //初始化菜单信息
  QUERY_MENU_DATA: "QUERY_MENU_DATA",
  //初始化广告信息
  ADVERT_INIT: "ADVERT_INIT",
  GET_SAVE_DATA: "GET_SAVE_DATA",

  GET_SAVE_FIND_ID: "GET_SAVE_FIND_ID"

}


//路由前缀-服务器需求
const rootName = "/sticcxb";
//路由列表
export const RouteUrl = {

  //========================================企业微信运动
  ADD_SPORT: rootName + "/addsport",
  SPORT_RANKING: rootName + "/sportranking",

  //========================================智通
  INSURE_ONE: rootName + "/insureone",
  INSURE_TWO: rootName + "/insuretwo",

  //========================================自销营平台
  INDEX: rootName + "/index",
  INSURESUCCESS: rootName + "/insuresuccess",
  INSURANCE: rootName + "/insurance",
  SERVICE: rootName + "/service",
  MY: rootName + "/my",
  MESSAGE: rootName + "/message",
  ACTIVITY: rootName + "/activity",
  CUSTOMER: rootName + "/customer",
  SHOP: rootName + "/shop",
  ORDER: rootName + "/order",
  HISTORICAL_RC: rootName + "/historicalRC",
  REPORT: rootName + "/report",
  INCOME: rootName + "/income",
  FRIENDS: rootName + "/friends",
  CLASSROOM: rootName + "/classroom",
  ABOUT: rootName + "/about",
  FEEDBACK: rootName + "/feedback",
  ACCOUNT: rootName + "/account",
  MGT_PASSWORD: rootName + "/mgtpassword",
  // MGT_USER_NAME: rootName + "/mgtusermame",
  MGT_MOBILE: rootName + "/mgtmobile",
  MGT_IDNUMBER: rootName + "/mgtidnumber",
  MGT_BANKS: rootName + "/mgtbanks",
  CODE_AUTHENTICATION: rootName + "/codeauthentication",
  INSURE: rootName + "/insure",
  DETAILS: rootName + "/details",
  LOGIN: rootName + "/login",
  REGISTER: rootName + "/register",
  REGISTER_HENGHUA: rootName + "/registerhenghua",
  POLICY_INQUIRY: rootName + "/policy",
  IFRAME: rootName + "/myPage",
  INSURE_CONFIRM: rootName + "/insureconfirm",
  AD_ACTIVITY: rootName + "/adActivity",
  ID_CAR_AUTHENTICATION: rootName + "/idCarAuthentication",
  FOLLOW_UP: rootName + "/followup",
  POLICY_LIST: rootName + "/policyList",
  EINV: rootName + "/Einv",
  REPORT_IMMEDIATELY: rootName + "/reportImmediately",
  REPORT_UPLOAD_FILES: rootName + "/reportUploadFiles",
  MSG: rootName + "/msg",
  CLAUSE_READ: rootName + "/clauseRead",
  INSURE_AXTX: rootName + "/insureAxtx",
  INSURE_CONFIRM_AXTX: rootName + "/insureconfirmAxtx",
  SET_SHOP: rootName + "/setshop",
  SHOP_INDEX: rootName + "/shopIndex",
  SHOP_MANAGE: rootName + "/shopManage",
  PRODUCT_MANAGE: rootName + "/productManage",
  PERFORMANCE_STATISTICS: rootName + "/performanceStatistics",
  PERFORMANCE_DETAILS: rootName + "/performanceDetails",
  MARKETING_SOFT_PAPER: rootName + "/marketingSoftPaper",
  MARKETING_POSTER: rootName + "/marketingPoster",
  ORDER_DETAILS: rootName + "/OrderDetails",
  APPLY_AGR_NO: rootName + "/applyagrno",
  PAY_DETAILS: rootName + "/payDetails",
  PAY_ORDER_LIST: rootName + "/PayOrderList",
  SOFT_PAPER: rootName + "/softPaper",
  SHOW_POSTER: rootName + "/showposter",
  SHOW_POSTER: rootName + "/showposter",
  CONFIG_URL: rootName + "/configUrl",
  MY_INTEGRAL: rootName + "/myintegral",
  INTEGRAL_IN_OR_OUT_LIST: rootName + "/integralinoroutlist",
  INTEGRAL_DETAIL: rootName + "/integraldetail",
  INTEGRAL_EXCHANGE: rootName + "/integralexchange",
  INTEGRAL_EXCHANGE_LIST: rootName + "/integralexchangelist",
  CAR_INSURE: rootName + "/carInsure",
  CAR_INFO: rootName + "/carInfo",
  CAR_INSURE_INFO: rootName + "/carInsureInfo",
  ADD_CAR_INFO: rootName + "/addCarInfo",
  CAR_SELECT: rootName + "/CarSelect",
  JUMP: rootName + "/jump",
  BATCH_PROCESS_QRCODE: rootName + "/batch_process_qrcode",
  SHOP_QRCODE: rootName + "/shopQrcode",
  ACTIVITY_GREAT_BENEFIT: rootName + "/activitygreatbenefit",
  CAR_OFFER: rootName + "/carOffer",
  OFFER_RESULT: rootName + "/offerResult",
  INSURE_CAR_INFO: rootName + "/insureCarInfo",
  INSURE_INFO: rootName + "/insureInfo",
  SPECIAL_AGREEMENT: rootName + "/specialAgreement",
  QUESTIONNAIRE: rootName + "/questionnaire",
  DISTRIBUTION: rootName + "/distribution",
  DEMO: rootName + "/demo",
  //五期 - 渠道/层级
  MY_SUPERIOR: rootName + "/mysuperior",//上级
  MY_FRIENDS: rootName + "/myfriends",//好友
  REGISTER_POSTER: rootName + "/registerposter",//邀请注册海报
  REGISTER_INVITE: rootName + "/registerinvite",//邀请好友注册
  REGISTER_ACCEPT: rootName + "/registeraccept",//接受邀请注册
  REGISTER_CHANNEL: rootName + "/registerchannel",//邀请注册

  // Detailssss: rootName + "/details/",
  // Insuree: rootName + "/insure/",
  // TEST1: "/test1",
  // TEST2: "/test2",
}
// 投保人与被保险人关系
export const policyHolderType = {
  PERSONAL: 1,
  ENTERPRISE: 2
}

//车主性质
export const OWNER_TYPE = {
  PERSONAL: 1,
  GROUP: 2
}
export const CAR_INSURED_RELATION = {
  NATURAL_PERSON: 3,
  NO_NATURAL_PERSON: 4,
}

export const Gender = {
  MALE: '1',
  WOMEN: '2',
  UNKNOWN: '9'
}

export const PerIdentifyType = { // 个人证件类型
  ID_CAR: '01', // 身份证
  OTHER_ID_CAR: "03", // 外国人永久居留身份证
  PASSPORT: '03', // 护照
  CERTIFICATE_OFFICERS: '04', // 军官证
  OTHER: '99'
}

export const CAR_EntIdentifyType = { // 团体/企业 证件类型
  ORGANIZATION: '51', // 组织机构代码登记号
  THREE_IN_ONE: '54', // 三证合一
  OTHER: '99' // 其他
}


export const EntIdentifyType = { // 团体/企业 证件类型
  BUSINESS: '51', // 工商登记号
  TAX: '52', // 税务登记号
  ORGANIZATION: '53', // 组织机构代码登记号
  THREE_IN_ONE: '54', // 三证合一
  OTHER: '99' // 其他
}

export const benefType = { // 受益方式
  LEGAL: '0', // 法定受益
  ORDER: '1', // 指定顺序受益
  SHARE: '2' // 指定份额受益
}

export const insuredidentity = { // 与投保人关系
  ONESELF: '01', // 本人
  SPOUSE: '10', // 配偶
  HUSBAND: '11', // 丈夫
  WIFE: '12', // 妻子
  SON: '20', // 儿子
  DAUGHTER: '30', // 女儿
  CHILDREN: '40', // 儿女
  PARENT: '50', // 父母
  FATHER: '51', // 父亲
  MOTHER: '52', // 父亲
  HIRE: '60', // 雇佣
  AGENT: '61', // 代理
}

export const intervalTime = { // 间隔时间
  YEAR: 100
}

export const SaveName = {
  //企业微信
  LOCAL_STORAGE_USER_ENTERPRISE: "LOCAL_STORAGE_USER_ENTERPRISE",

  //自营平台
  LOCAL_STORAGE_USER: "LOCAL_STORAGE_USER",
  IS_ORDER_FIND: "IS_ORDER_FIND",
  WECHAT_OPEN_ID: "WECHAT_OPEN_ID",
  //存储请求标识码名称
  JWT_TOKEN_NAME: "authorization",

  //渠道注册时保存用户信息到sessionStorage时的操作类型名称
  SESSION_STORAGE_CHANNEL_USER: "SESSION_STORAGE_CHANNEL_USER",
  //渠道注册时保存当前渠道账户的usercode到localStorage的名称
  CUR_CHANNEL_USER_CODE: "CUR_CHANNEL_USER_CODE",
}
//用户类型
export const USER_TYPE = ["", "游客", "普通客户", "专业代理", "商户", "销售人员"]

export const SERVICE_CODE = {
  SERVICE: "1",         //首页服务编码
  SHOP: "2",            //店铺
}


//字符串默认长度
export const StrLenth = {
  img: 4, //图形码
  sms: 6, //短信验证码
  mobile: 11, //手机号
  pwdMin: 6, //密码
  pwdMax: 18,
  nickNameMin: 3, //昵称
  nickNameMax: 20,
  userNameMin: 2, //真实姓名
  userNameMax: 32,
  eCodeMin: 2, //员工编码
  eCodeMax: 20,
  enterpriseNameMin: 5, //企业名称
  enterpriseNameMax: 80,//目前最长的企业名为65个汉字
  idNumberMin: 15, //身份证
  idNumberMax: 18,
  agrMin: 8, //业务关系代码
  agrMax: 10,
  bankNoMin: 19, //银行卡号,包含3个空格
  bankNoMax: 23//包含4个空格
}
export const TO_TYPE = {
  INSIDE_JUMP: '0', //内部地址跳转
  EXTERNAL_JUMP: '1', //外部地址跳转
  PHONE_NUMBER: '2', //电话号码
  INT_RENDER_DISPLAY: "3", //内部渲染展示
  PRODUCT: "4",         //跳转产品
  POP: "5",             //弹出活动
  POP_ALL: "6",         //弹出全屏显示活动
  ACTIVITY: "7",        //活动 ACTIVITY
}

export const BaseService = {
  POLICY: "policy", // 保单查询
  INVOICE: "invoice",  //电子发票
  FNOL: "fnol",    //马上报案
  CLAIM_QUERY: "claim_query",   //理赔查询
  CLAIM_UPLOAD: "claim_upload",  //上传照片
  PAY: "pay"
}
//防止IOS系统在vux的selector组件中选择时第一项选择无效的情况,key在组件中表示选项值,value表示显示的值
export const SelectorTag = {
  keyNull: "null",//选项值
  valNull: "-请选择-",//选项显示内容
}

export const INPUT_CONFIG_TYPE = {
  INSURE: "0",
  APPLI: "1",
  ADDINFO: "2",
  UPLOAD: "3",
}

export const JWT_EXPIRED_ERROR_CODE = 5403;//jwt的token过期标识码


export const DEFAULT_VEHICLE_CATEGORY_CODE = "K33";//车险交管车辆种类 默认值  k33轿车


export const Url_Key = {
  SHARE_UUID: "shareId",       //分享id
  PRODUCT_ID: "pro",           //产品id
  SOFT_PAPER: 'soft',          //软文
  REDIRECT_URL: 'reUrl',       //回调地址
  RECORDED_ID: "recId",        //销售人员编码  
  SHOW_POSTER: 'poster',        //海报id
  SUCC_URL: 'suceUrl',            //成功回跳地址

  AGREEMENT_CODE: 'agr',            //业务关系代码
  SALER_NAME: "salename",        //销售人员
  SALER_NUMBER: "salenum",    //销售人员 编码

  CHANNEL_NAME: "channelName",   //渠道名称
  UID: "uid",                     //标记来源    
  DEAL_UUID: "dealUuid",                     //唯一id标记每笔交易来源
  JUMP_URL: "jumpurl",                     //跳转地址   
  AD_ID: "advert",             //活动id   

  BASE_RATE: "baserate",            //下级佣金比例
}


