import {
  Mutations
} from 'src/common/const';
export default {
  addTest({
    commit,
    state
  }, id) {
    // state 逻辑等等...
    commit(Mutations.ADD_TEST, id);
  },
  myAddTest({
    commit
  }) {
    commit(Mutations.MY_ADD_TEST);
  },
  updatedFooterState({
    commit
  }, id) {
    commit(Mutations.FOOTER_STATE, id);
  },
  updatedPopState({
    commit
  }, state) {
    commit(Mutations.POP_STATE, state);
  },
  updateLoadingStatus({
    commit
  }, state) {
    commit(Mutations.UPDATE_LOADING_STATUS, state);
  },
  gotoInsure({
    commit
  }, state) {
    commit(Mutations.UPDATE_LOADING_STATUS, state);
  }
}
