import { RouteUrl, Mutations, SaveName, WE_CHAT } from 'src/common/const';
import OrderConfig from 'src/common/config/orderConfig';
import PinYin from 'src/common/util/pinyin';
import {
    RequestUrl
} from 'src/common/url';
export default {
    state: {
        //零时存储的上级渠道账户信息
        superChannelUserDto: {
            userCode: "",//上级渠道账户userCode
            nickName: "恒华出行宝",//昵称
            headUrl: "upload/userHeadImg/default/headImgDefault.jpg",//头像地址
            userType: "2",//用户类型:1游客,2普通客户,3专业代理,4商户,5销售人员
            registerType: "0",//注册类型：0=wx自主注册(个人用户)，1=后台注册(渠道用户)，2=分享注册(渠道用户)
            state: "0",//账户状态，0=已注销，1=有效，2=挂失
        },

        qrCodeImgBase64: {},//邀请注册的二维码图片base64码

        curShowPinYin: {
            'A': [],
            'B': [], 'C': [], 'D': [], 'F': [], 'G': [], 'H': [], 'I': [], 'J': [], 'K': [], 'L': [], 'M': [], 'N': [], 'O': [], 'P': [], 'Q': [], 'R': [], 'S': [], 'T': [], 'U': [], 'V': [], 'W': [], 'X': [], 'Y': [], 'Z': []
        },
        initData: {
            'A': [],
            'B': [], 'C': [], 'D': [], 'F': [], 'G': [], 'H': [], 'I': [], 'J': [], 'K': [], 'L': [], 'M': [], 'N': [], 'O': [], 'P': [], 'Q': [], 'R': [], 'S': [], 'T': [], 'U': [], 'V': [], 'W': [], 'X': [], 'Y': [], 'Z': []
        },
        showData: [],
        saveData: {},

        pinYinInit: ['A', 'B', 'C', 'D', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    },
    mutations: {

        //零时保存渠道用户信息
        [Mutations.SAVE_USER_CHANNEL_INFO](state, userInfo) {
            state.superChannelUserDto = userInfo;
        },

        //获取所有好友
        [Mutations.GET_FIND_ALL_FRIEND](state, data) {
            let _this = data._this;
            let userCode = data.userCode;
            let serch = data.serch;
            let type = data.type;
            let query = {
                TYPE: "FORM",
                type: type,
                sort: data.sort,
                serch: serch
            }
            //清空当前
            state.curShowPinYin = state.initData;
            state.showData = [];
            let saveName = "";
            if (type == '') {
                saveName = 'pinYin' + userCode + data.sort + serch;
            } else {
                saveName = data.type + userCode + data.sort + serch;
            }
            if (typeof (state.saveData[saveName]) == "undefined") {
                _this.$http.post(RequestUrl.FIND_ALL_FRIEND, query)
                    .then(function (res) {
                        if (res.success) {
                            let resData = res.result;
                            for (let i = 0; i < resData.length; i++) {
                                if (resData[i].headUrl == '' || resData[i].headUrl == null) {
                                    resData[i].headUrl = RequestUrl.WEB_PATH + "sticcxb/upload/assets/x3/logo.jpg";
                                }
                                if (resData[i].orderAmout == '' || resData[i].orderAmout == null) {
                                    resData[i].orderAmout = 0;
                                }
                                resData[i].showDetail = false;
                                if (type == "resDate") {
                                    resData[i].showMsg = resData[i].resDate;
                                } else if (type == "orderCount") {
                                    resData[i].showMsg = resData[i].orderCount + " 单";
                                } else if (type == "orderAmout") {
                                    resData[i].showMsg = "¥ " + resData[i].orderAmout;
                                }
                            }
                            if (type == '') {
                                let pinYinList = {};
                                for (let i = 0; i < state.pinYinInit.length; i++) {
                                    pinYinList[state.pinYinInit[i]] = [];
                                }
                                for (let i = 0; i < resData.length; i++) {
                                    let frist = PinYin.getFristChar(resData[i].userName.charAt(0))[0].toUpperCase();
                                    pinYinList[frist].push(resData[i]);
                                }
                                state.saveData[saveName] = pinYinList;
                                state.curShowPinYin = pinYinList;
                            } else {
                                state.saveData[saveName] = resData;
                                state.showData = resData;
                            }
                        }

                    })
            } else {
                if (type == '') {
                    state.curShowPinYin = state.saveData[saveName];
                } else {
                    state.showData = state.saveData[saveName];
                }
            }
        },
        //获取邀请好友注册的带logo二维码
        [Mutations.GET_REGISTER_QRCODE](state, params) {
            let _this = params._this;
            let data = { TYPE: "FORM", shareUrl: params.shareUrl, userHeadUrl: params.headUrl, fontSize: 30, fileType: "jpg", };
            _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, true);//loading
            _this.$http.post(RequestUrl.REGISTER_ZXING_QR_CODE, data).then(function (res) {
                _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, false);//loading
                if (res.state != 0) {
                    _this.$common.showMsg(_this, res.result, "red;", false); //显示错误信息
                    return;
                }
                state.qrCodeImgBase64[params.userCode] = res.result;
                _this.isDisabled = false;//可点击按钮
                _this.isReturnImg = true;//切换为接口返回的二维码图片
            });
        },
        //保存渠道用户usercode
        [Mutations.SAVE_CHANNEL_USER_CODE](state, userCode) {
            if (typeof (userCode) != 'undefined' && userCode != undefined && userCode != null) {
                //当前渠道账户的usercode
                localStorage[SaveName.CUR_CHANNEL_USER_CODE] = userCode;
                //零时保持当前操作类型为渠道授权注册
                localStorage[SaveName.SESSION_STORAGE_CHANNEL_USER] = WE_CHAT.OAUTH_TYPE_CHANNEL;
            }
        },
        //根据授权结果判断是否需要查询并保存渠道账户
        [Mutations.READ_CHANNEL_USER_CODE](state, _this) {
            //获取保存用户信息时的操作类型
            let saveType = localStorage[SaveName.SESSION_STORAGE_CHANNEL_USER];
            //获取当前渠道账户的usercode
            let curChannelUserCode = localStorage[SaveName.CUR_CHANNEL_USER_CODE]
            // alert("授权回调后判断=====：saveType=" + saveType + ",用户code==" + curChannelUserCode);

            //保存用户的操作类型为渠道，且已保存用户信息，但没有保存登录状态的渠道账户  跳转到“我的”栏目
            if (_this.$common.isNotEmpty(saveType) && saveType == WE_CHAT.OAUTH_TYPE_CHANNEL
                && _this.$common.isNotEmpty(curChannelUserCode)) {

                //清空零时变量
                localStorage[SaveName.SESSION_STORAGE_CHANNEL_USER] = "";
                localStorage[SaveName.CUR_CHANNEL_USER_CODE] = "";

                //查询渠道账户
                let data = { TYPE: "FORM", userCode: curChannelUserCode };
                _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, true);
                _this.$http.post(RequestUrl.FIND_USER_BY_USER_CODE, data).then(function (res) {
                    _this.$common.storeCommit(_this, Mutations.UPDATE_LOADING_STATUS, false);
                    if (res.state == 0) {
                        //保存USER_INFO信息
                        _this.$common.storeCommit(_this, Mutations.SAVE_USER_INFO, res.result);
                        // alert("成功注册并登录");

                        _this.$common.goUrl(_this, RouteUrl.MY);
                        return;
                    }
                    // _this.$common.showMsg(_this, res.result, "red;", false); //显示错误信息
                    // return;
                });
            }
        },
    },
    actions: {

    },
    getters: {

    }
}