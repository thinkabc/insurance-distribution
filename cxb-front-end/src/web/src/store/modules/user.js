import { Mutations, SaveName, WE_CHAT } from 'src/common/const'
import { RequestUrl } from "src/common/url";;
import DateUtil from "src/common/util/dateUtil";
export default {
  state: {
    //永久存储的注册账户
    userDto: null,
    //永久存储的注册账户是否已登录
    isLogin: false,
  },
  mutations: {
    //初始化保存用户信息
    [Mutations.SAVE_USER_INFO_INIT](state, data) {
      let _this = data._this;

      let lastDate = data.userInfo.lastloginDate;//最后登录时间戳
      let lastTime = DateUtil.getDateStrYmdByTs(lastDate);//最后登录时间yymmdd
      let todayTime = DateUtil.getNowDateStrYmd();//当前时间yymmdd

      // console.log("lastLoginTime==" + lastTime);
      // console.log("todayTime==" + todayTime);

      //不是当前登录过 或者是旧版本号 则需要重新读取数据
      if (lastTime < todayTime || _this.$store.state.isOldVersion) {
        let query = { TYPE: "FORM", id: data.userInfo.id }
        _this.$http.post(RequestUrl.FIND_USER_BY_ID, query).then(function (res) {
          if (res.state != 0) return;//查询失败
          state.userDto = res.result;
          sessionStorage["ID_CAR"] = state.userDto.idNumber;
          localStorage[SaveName.LOCAL_STORAGE_USER] = JSON.stringify(state.userDto);
          state.isLogin = true;
        });
      } else {
        state.userDto = data.userInfo;
        sessionStorage["ID_CAR"] = state.userDto.idNumber;
        localStorage[SaveName.LOCAL_STORAGE_USER] = JSON.stringify(state.userDto);
        state.isLogin = true;
      }
      // console.log('--------------------------user');
      // console.log(state.userDto);
    },
    //保存用户信息
    [Mutations.SAVE_USER_INFO](state, userInfo) {
      if (typeof (userInfo) != 'undefined' && userInfo != undefined && userInfo != null) {
        state.userDto = userInfo;
        localStorage[SaveName.LOCAL_STORAGE_USER] = JSON.stringify(state.userDto);
        // sessionStorage[SaveName.LOCAL_STORAGE_USER] = JSON.stringify(state.userDto);
        state.isLogin = true;
      }
      // console.log('--------------------------user');
      // console.log(state.userDto);
    },
    //清除用户信息
    [Mutations.REMOVE_USER_INFO](state) {
      localStorage.removeItem(SaveName.LOCAL_STORAGE_USER);
      // sessionStorage.removeItem(SaveName.LOCAL_STORAGE_USER);
      state.userDto = null;
      state.isLogin = false;
    },
    //刷新银行卡列表
    [Mutations.REFRESH_BANK_LIST](state, lists) {
      state.userDto.banks = lists;
    },
    //查询银行卡列表
    [Mutations.FIND_BANK_LIST](state, data) {
      let _this = data._this;
      let queryData = { TYPE: "FORM" };
      _this.$http.post(RequestUrl.FIND_BANK_LIST_ALL, queryData).then(function (res) {
        if (res.state == 0) {
          state.userDto.banks = res.result;
          return;
        }
      });
    }
  },
  actions: {

  },
  getters: {

  }
}
