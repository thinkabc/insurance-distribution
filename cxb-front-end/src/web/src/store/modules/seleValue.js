import { Mutations, SaveName, Url_Key, RouteUrl, INPUT_CONFIG_TYPE } from 'src/common/const';
import OrderConfig from 'src/common/config/orderConfig';
import { RequestUrl } from 'src/common/url';
import DateUtil from 'src/common/util/dateUtil';
export default {
    state: {
        cliaimsType: {},

        productList: {},

        cliaimsData: {
            autoPolNo: '',
            incidentTime: '',
            claimType: '',
            province: '',
            city: '',
            incidentStreetAddress: '',
            incidentNotes: '',
            userName: '',
            phoneNo: '',
            riskCode: '',
            email: ''
        },
        saveOccupation: {},
        isOccpaQuery: false,


        limitCarInfo: [],
        isQueryCarNo: false,

        //问卷内容
        queList: {},

        curQue: {
            title: "",
            introduction: "",
            question: [],
            statement: "",
            statementTitle: "",
            introductionTitle: "",
            limitItem: {
                min: 0,
                max: 0
            },
        },

        isSetQue: false,

        curUpload: {
            title: "",
            introduction: "",
            question: [],
            statement: "",
            statementTitle: "",
            introductionTitle: "",
            limitItem: {
                min: 0,
                max: 0
            },
        },
        isSetUpload: false,

        inputConfigList: {},

        insureInputConfig: {},  //投保人输入附加信息   type:1

        appliInputConfig: {},   //被保险人附加信息   type:2

        addInfoInputConfig: {}, //补充信息附加信息  type:3

        uploadInputConfig: {},  //文件上传附加信息  type:4

        saveDataList: {},

        showCarSaveData: [],


        saveDataIdList: {},
    },
    mutations: {
        [Mutations.GET_SAVE_FIND_ID](state, data) {
            let _this = data._this;
            let id = data.id;
            if (typeof (state.saveDataIdList[id]) == "undefined") {
                let fm = new FormData();
                fm.append('id', id);
                _this.$http.post(RequestUrl.FIND_SAVE_DATA_ID, fm).then(function (res) {
                    if (res.success) {

                    } else {

                    }
                });
            } else {

            }
        },
        [Mutations.GET_SAVE_DATA](state, data) {
            let riskCode = data.riskCode;
            let _this = data._this;
            if (_this.user.isLogin) {
                let userCode = _this.user.userDto.userCode;
                if (typeof (state.saveDataList[riskCode + userCode]) == "undefined") {
                    let base = {
                        data: [],
                        query: {
                            pageNumber: 0,
                            pageSize: 10,
                            isEnd: false,
                            TYPE: "FORM",
                            riskCode: "",
                        }
                    }
                    state.saveDataList[riskCode + userCode] = base;
                }
                let query = state.saveDataList[riskCode + userCode].query;
                if (!query.isEnd) {
                    query.pageNumber++;
                    query.riskCode = riskCode;

                    _this.$http.post(RequestUrl.FIND_SAVE_DATA, query).then(function (res) {
                        let resData = res.result;
                        let queryData = state.saveDataList[riskCode + userCode].query;
                        if (resData.length < queryData.pageSize) {
                            queryData.isEnd = true;
                            state.saveDataList[riskCode + userCode].query = queryData;
                        }
                        let saveDataList = state.saveDataList[riskCode + userCode].data;
                        if (riskCode = "0502") {

                            for (let i = 0; i < resData.length; i++) {
                                saveDataList.push(resData[i]);
                            }
                            state.saveDataList[riskCode + userCode].data = saveDataList;
                        }

                        state.showCarSaveData = saveDataList;
                    });
                } else {
                    state.showCarSaveData = state.saveDataList[riskCode + userCode].data;
                }
            }
        },
        [Mutations.FIND_BY_INPUT_ID](state, data) {
            let inputId = data.id;
            let proId = data.proId;
            let _this = data._this;
            let query = {
                TYPE: "FORM",
                id: inputId,
            }
            if (typeof (state.inputConfigList[inputId]) == "undefined") {
                _this.$http.post(RequestUrl.GET_CONFIG_DATA, query).then(function (res) {
                    if (res.success) {
                        let result = res.result;
                        state.inputConfigList[inputId] = "";
                        let configList = JSON.parse(res.result.configContent);
                        let insureInputConfig = {};
                        let appliInputConfig = {};
                        let addInfoInputConfig = {};
                        let uploadInputConfig = {};
                        for (let i = 0; i < 4; i++) {
                            let mark = i;
                            insureInputConfig[mark] = [];
                            appliInputConfig[mark] = [];
                            addInfoInputConfig[mark] = [];
                            uploadInputConfig[mark] = [];
                        }
                        for (let i = 0; i < configList.length; i++) {
                            let config = configList[i];
                            let input = {
                                // type: config.inputType,   // 1.输入框
                                inputType: config.inputType,
                                key: config.key,         //显示名字
                                value: config.default,  // 用户默认输入
                                isMust: config.isMust,   //是否为必输项
                                imgUrl: config.imgUrl,    //图片地址
                                regExp: config.regExp,     //校验正则 没有就不输入
                                seleList: config.seleList, //选项值
                                alert: config.alert,
                                min: config.min,
                                max: config.max
                            }
                            if (config.type == INPUT_CONFIG_TYPE.INSURE) {

                                insureInputConfig[config.inputType].push(input);

                            } else if (config.type == INPUT_CONFIG_TYPE.APPLI) {

                                appliInputConfig[config.inputType].push(input);
                            } else if (config.type == INPUT_CONFIG_TYPE.ADDINFO) {

                                addInfoInputConfig[config.inputType].push(input);
                            } else if (config.type == INPUT_CONFIG_TYPE.UPLOAD) {

                                uploadInputConfig[config.inputType].push(input);
                            }

                        }
                        // debugger
                        state.insureInputConfig[proId] = insureInputConfig;
                        state.appliInputConfig[proId] = appliInputConfig;
                        state.addInfoInputConfig[proId] = addInfoInputConfig;
                        state.uploadInputConfig[proId] = uploadInputConfig;
                        // debugger
                        // debugger
                    }
                });
            } else {

            }
        },
        [Mutations.FIND_BY_QUE_ID](state, data) {
            // debugger
            let _this = data._this;
            let query = {
                TYPE: "FORM",
                id: data.id,
            }
            if (typeof (state.queList[data.id]) == "undefined") {
                _this.$http.post(RequestUrl.FIND_BY_QUE_ID, query).then(function (res) {
                    if (res.success) {
                        let queList = res.result.question.split("~");
                        res.result.question = queList;
                        state.queList[data.id] = res.result;
                        if (data.type == 1) {
                            state.curQue = state.queList[data.id];
                            state.isSetQue = true;
                        } else {
                            state.isSetUpload = true;
                            let limitItem = JSON.parse(res.result.limitItem);
                            if (typeof (limitItem.min) == "undefined") {
                                limitItem.min = 0;
                            }
                            if (typeof (limitItem.max) == "undefined") {
                                limitItem.max = 0;
                            }
                            state.queList[data.id].limitItem = limitItem;
                            state.curUpload = state.queList[data.id];
                        }
                    } else {
                        if (data.type == 1) {
                            state.isSetQue = false;
                        } else {
                            state.isSetUpload = false;
                        }
                    }
                });
            } else {
                if (data.type == 1) {
                    state.curQue = state.queList[data.id];
                    state.isSetQue = true;
                } else {
                    state.isSetUpload = true;
                    state.curUpload = state.queList[data.id];
                }
            }
        },
        [Mutations.GET_CARLIMIT_NO](state, data) {
            if (!state.isQueryCarNo) {
                let _this = data._this;
                state.isQueryCarNo = true;
                let query = {};
                _this.$http.post(RequestUrl.GET_CARLIMIT_NO, query).then(function (res) {
                    state.limitCarInfo = res.result;
                });

            }
        },
        [Mutations.OCCUPATION_GET](state, occupation) {
            let level = occupation.level;
            let codeCode = occupation.codeCode;
            let _this = occupation._this;
            let query = {
                level: level,
                codeCode: codeCode,
                levelMin: occupation.levelMin,
                levelMax: occupation.levelMax,
                TYPE: "FORM",
                type: occupation.type
            }
            let seleData = [];
            if (typeof (state.saveOccupation[level + codeCode]) == "undefined") {
                state.saveOccupation[level + codeCode] = {}
                // if (level == 1) {
                state.isOccpaQuery = true;
                // }
                _this.isMove = false;
                _this.$http.post(RequestUrl.PRPD_QUERY, query).then(function (res) {
                    // if (level == 1) {
                    setTimeout(function () {
                        state.isOccpaQuery = false;
                    }, 300);
                    // } else {
                    //     _this.isMove = true;
                    //     state.isOccpaQuery = false;
                    // }
                    state.saveOccupation[level + codeCode] = res.result;
                    seleData[0] = state.saveOccupation[level + codeCode];
                    if (level == "1") {
                        _this.occupationSeleOne = seleData;
                    } else if (level == "2") {
                        _this.occupationSeleSub = seleData;
                    } else {
                        _this.occupationSeleThe = seleData;
                    }
                });
            } else {
                seleData[0] = state.saveOccupation[level + codeCode];
                if (level == "1") {
                    _this.occupationSeleOne = seleData;
                } else if (level == "2") {
                    _this.occupationSeleSub = seleData;
                } else {
                    _this.occupationSeleThe = seleData;
                }
            }
        },
        //理赔类型
        [Mutations.CLIAIMS_TYPE](state, queryData) {
            let _this = queryData._this;
            if (typeof (state.cliaimsType[queryData.code]) == "undefined") {
                state.cliaimsType[queryData.code] = {};
                let query = {
                    TYPE: "FORM",
                    code: queryData.code,
                    language: "中文"
                }
                _this.$http.post(RequestUrl.GET_CLIAIMS_TYPE, query).then(function (res) {
                    _this.cliaimsType = res.result;
                    state.cliaimsType[queryData.code] = res.result;
                });
            } else {
                _this.cliaimsType = state.cliaimsType[queryData.code];
            }

        },
        [Mutations.CLIAIMS_DATA](state, queryData) {
            state.cliaimsData = queryData;
        },
        [Mutations.SET_PRODUCT_LIST](state, data) {
            // debugger
            let type = data.type;
            let id = data.id;
            if (type == "add") {
                state.productList[id] = data.product;
            } else {
                let _this = data._this;
                let _state = data._state;
                let refereeId = data.refereeId + "";
                if (typeof (state.productList[id]) == "undefined") {
                    let querydata = { proId: id, userCode: data.userCode, refereeId: refereeId };
                    //本地token
                    let localToken = localStorage[SaveName.JWT_TOKEN_NAME];
                    $.ajax({
                        type: "POST",
                        url: RequestUrl.SHARE_FIND_PLAN,
                        data: querydata,
                        async: false,
                        headers: {
                            "authorization": localToken,
                            // "Accept": "application/json; charset=utf-8",
                            // "Content-Type": "application/x-www-form-urlencoded",
                        },
                        success: function (data, status, xhr) {
                            _this.$common.reJwtVal(data);
                            if (data.success) {
                                state.productList[id] = data.result.pro;
                                _state.isGetIndex = true;
                                _state.indexData = data.result.pro;
                            } else {
                                _state.isGetIndex = false;
                                _this.$common.goUrl(_this, RouteUrl.INDEX);
                                return;
                            }
                            // if (data.result.agr != false) {
                            //     _this.$common.storeCommit(_this, Mutations.SET_REFEREE_DATA, data.result.agr);
                            // }
                            // let uuid = _this.shareUuid;
                            // let parmList = [];
                            // let urlParm = {
                            //     key: Url_Key.SHARE_UUID,
                            //     value: uuid
                            // }
                            // let urlPro = {
                            //     key: Url_Key.PRODUCT_ID,
                            //     value: id
                            // }
                            // parmList.push(urlPro);
                            // parmList.push(urlParm);

                            // let shareUrl = RequestUrl.BASE_URL + RouteUrl.DETAILS + this.$common.setShareUrl(parmList);
                            //组合URI并跳转
                            // _this.$common.goUrl(_this, RouteUrl.DETAILS, _this.$common.setShareUrl(parmList));
                            // let urlParm = {
                            //     id: id,
                            //     shareId: refereeId
                            // };
                            // let addUrl = _this.$common.getShareUrl(_this, urlParm);
                            // //组合URI并跳转
                            // _this.$common.goUrl(_this, RouteUrl.DETAILS, addUrl);
                        }
                    });
                } else {
                    _state.isGetIndex = true;
                    _state.indexData = state.productList[id];
                }
            }

        },
    },
    actions: {

    },
    getters: {

    }
}