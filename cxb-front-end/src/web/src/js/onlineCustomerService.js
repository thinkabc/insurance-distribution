
import Validation from "src/common/util/validation";
export default {
    //在线客服
    onlineClickOfMobile(_this, mobile) {
        window.location.href = "tel:" + mobile;
    },
    //去掉字符串中的chart符号
    replaceChart(str, chart) {
        let strs = str.split(chart);
        let data = "";
        for (let i = 0; i < strs.length; i++) {
            data += strs[i];
        }
        return data;
    },
    //加载在线客服js并根据参数调用在线客服实例
    initJsFile(_this, dto) {
        // console.log("dto===>>>");
        // console.log(dto);

        $("#zhichiBtnBox").remove();
        $("#ZCPanel").remove();

        if ($("#zhichiBtnBox").length <= 0 && $("#ZCPanel").length <= 0) {
            //元素不存在时执行的代码

            // let srcUrl = "https://www.sobot.com/chat/frame/js/entrance.js?sysNum=a4b875b78fd74921a0a2fa873cdaea37";//测试
            let srcUrl = "https://www.sobot.com/chat/frame/js/entrance.js?sysNum=63bc6179d64b4daeb52b613a1435de14";//正式
            //移除重复异步加载的JavaScript文件
            $("script[src='" + srcUrl + "']").remove();

            let script = document.createElement('script');
            script.type = "text/javascript";
            script.src = srcUrl;
            script.id = "zhichiScript";//因为远程js需要，所以必须有ID属性
            script.className = "zhiCustomBtn";
            script.charset = "utf-8";

            //待js文件加载完毕即可初始化实例
            script.onload = function () {
                try {
                    //初始化实例
                    var zhiManager = (getzhiSDKInstance());
                    zhiManager.set('customBtn', 'true');//true 自定义咨询按钮 废弃系统默认按钮
                    zhiManager.set('moduleType', 3);// 1 : 仅机器人客服模式，2 : 仅人工客服模式，3 : 机器人客服优先模式，4 : 人工客服优先模式
                    zhiManager.set('anchor', true);//true 新窗口打开咨询页,false 打开悬浮窗咨询页（默认值）
                    zhiManager.set("color", 'C8161D');  //取值为0-9a-f共六位16进制字符[主题色]    | 默认取后台设置的颜色
                    zhiManager.set("powered", ' true'); //true[显示悬浮聊天窗体下方公司标识信息]   false[不显示]   | 默认显示
                    zhiManager.set('manTrace', true);   //true[开启用户访问轨迹收集]  false[不开启]  |默认不开启用户访问轨迹收集
                    // zhiManager.set('isInviteFlag', 'true'); //true[开启主动邀请功能]  false[不开启] |默认不开启
                    // zhiManager.set('invite', 1); // 是否开启自动邀请  1 开启 0 关闭  | 默认关闭
                    zhiManager.set('isMessageFlag', true); //true[开启输入框留言功能] false[不开启] | 默认开启
                    zhiManager.set('isFeedBackFlag', true); //true[开启输入框满意度评价功能] false[不开启] | 默认开启

                    if (Validation.isNotEmpty(dto.user)) {
                        zhiManager.set('userinfo', { //设置用户信息
                            partnerId: dto.user.userCode,   //用户对接ID，用户唯一标示，通常是企业自有用户系统中的userId
                            tel: dto.user.mobile,   //电话或手机
                            email: dto.user.email,   //邮箱
                            uname: dto.user.nickName,   //昵称
                            visitTitle: '',   //对话页标题，若不传入系统将获取用户打开咨询组件时所在页面的标题
                            visitUrl: '',   //对话页URL，若不传入系统将获取用户打开咨询组件时所在页面的URL
                            face: dto.user.headUrl,   //头像URL
                            realname: dto.user.userName,   //真实姓名
                            weibo: '',   //微博账号
                            weixin: dto.user.wxOpenId,   //微信账号
                            qq: '',   //QQ号
                            sex: dto.user.wxSex == 2 ? 0 : 1, //0 女 1 男
                            birthday: '',   //生日，如“1990-01-01”
                            remark: JSON.stringify(dto.user),   //用户的备注信息
                            params: '{"等级":"' + dto.user.level + '","客户状态":"' + dto.user.userType + '"}'   //自定义用户信息字段
                        });
                    }
                    if (Validation.isNotEmpty(dto.product)) {
                        zhiManager.set('groupId', 'e7606641580d423ca9f2888684aa025e');//指定组ID(在线技术组-微门店-产品)

                        //通过参数设置H5聊天页面显示商品信息内容
                        zhiManager.set("title_info", decodeURIComponent(dto.product.title));  //标题 必传 需编码
                        zhiManager.set("url_info", decodeURIComponent(dto.product.url)); //链接 必传 需编码
                        zhiManager.set("abstract_info", dto.product.desc); //摘要 需编码
                        zhiManager.set("label_info", dto.product.label); //标签 需编码
                        zhiManager.set("thumbnail_info", decodeURIComponent(dto.product.imgUrl)); //图片 需编码
                    } else {
                        zhiManager.set('groupId', '0af3bcac698c443ea46fe786d60bd7b4');//指定组ID(在线技术组-微门店)
                    }

                    //自定义按钮时 通过该方法获取客服发送的未读内容 & 消息数
                    zhiManager.on("receivemessage", function (ret) {
                        /**
                         * 返回格式：[{content:'您好',msgId:'615b0a3801804f14be1d456e11b329af',customName:'智齿科技'}]
                         */
                        // console.log("未读ret======");
                        // console.log(ret);
                    });
                    //自定义按钮时 通过该方法获取客服发送的离线消息数
                    zhiManager.on("unread.count", function (data) {
                        // console.log("离线data======");
                        // console.log(data);
                    });
                    //再调用load方法
                    zhiManager.on("load", function () {
                        zhiManager.initBtnDOM();
                    });
                } catch (error) {
                    console.log(error);
                }
            };
            //动态加载js组件
            let obj = document.body.appendChild(script);
        }
    }
}