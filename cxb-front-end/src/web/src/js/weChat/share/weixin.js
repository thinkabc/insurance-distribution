import { WE_CHAT } from 'src/common/const';//微信公众号等信息
import Sha1js from 'src/js/weChat/share/sha1';//引入sha1加密js
import wx from 'weixin-js-sdk'; //引入微信官方js-sdk 引入方式或 let wx = require('weixin-js-sdk');
import { RequestUrl } from 'src/common/url';
import { Mutations, RouteUrl, Url_Key, SaveName, TO_TYPE } from 'src/common/const';
let share_url = "";
let share_title = "";
let share_desc = "";
let share_imgUrl = "";
let type = "";
export default {
    // 需要引入sha1.js
    signature(jsapi_ticket) {
        // parse url with parameters
        var url = location.href.split('#')[0].toString();
        var newUrl = "";
        if (url.indexOf('?') == -1) {
            newUrl = url;
        } else {
            var rootUrl = url.split('?')[0].toString();
            // console.info('rootUrl = ' + rootUrl);
            var parameterUrl = "";
            var arr = url.split('?')[1].split('&');
            for (var i = 0; i < arr.length; i++) {
                var tmpParameter = arr[i];
                var encodeParameter = encodeURIComponent(tmpParameter);//参数解码
                if (parameterUrl == "") {
                    parameterUrl = encodeParameter;
                } else {
                    parameterUrl += '&' + encodeParameter;
                }
                // console.info('tmpParameter[' + tmpParameter + ']' + ', encodeParameter[' + encodeParameter + ']');
            }
            newUrl = rootUrl + '?' + parameterUrl;
        }
        newUrl = url;
        // console.log('newUrl = ' + newUrl);
        var signature_tmp = 'jsapi_ticket=' + jsapi_ticket + '&noncestr=' + WE_CHAT.NONCE_STR_STR + '&timestamp=' + WE_CHAT.TIMESTAMP_STR + '&url=' + newUrl;
        // console.log('signature_tmp = ' + signature_tmp);
        var signature_str = Sha1js.hex_sha1(signature_tmp);
        // console.log('signature_str = ' + signature_str);
        return signature_str;

    },
    // https必须引入 http://res.wx.qq.com/open/js/jweixin-1.2.0.js 文件
    share(jsapi_ticket, shareData) {
        let _this = shareData._this;
        type = shareData.type;
        share_title = shareData.title; //标题
        share_desc = shareData.desc; //描述
        share_imgUrl = shareData.imgUrl; //图片链接
        share_url = shareData.url; //分享地址
        var signature_str = this.signature(jsapi_ticket);

        //Vue中的hash模式（在路由中带有#号），微信都会在#的前边给加一个‘？’并带上自己的分享类型参数，并在#后的参数会截断，所以需要在#前再加?连接自定义参数
        var url_front = share_url.substring(0, share_url.indexOf('#'));
        var url_latter = share_url.substring(share_url.indexOf('#'), share_url.length);
        share_url = url_front + '?' + url_latter;

        // console.log('share_url = ' + share_url);
        // console.log('share_title = ' + share_title);
        // console.log('share_desc = ' + share_desc);
        // console.log('share_imgUrl = ' + share_imgUrl);
        // console.log('signature_str = ' + signature_str);
        wx.config({
            debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
            appId: WE_CHAT.ID, // 必填，公众号的唯一标识
            timestamp: WE_CHAT.TIMESTAMP_STR, // 必填，生成签名的时间戳
            nonceStr: WE_CHAT.NONCE_STR_STR, // 必填，生成签名的随机串
            signature: signature_str, // 必填，签名，见附录1
            jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo', 'onMenuShareQZone'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
        });
        // https必须引入 http://res.wx.qq.com/open/js/jweixin-1.2.0.js 文件
        wx.ready(function () {
            // 分享给朋友
            wx.onMenuShareAppMessage({
                title: share_title, // 分享标题
                desc: share_desc, // 分享描述
                link: share_url, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                imgUrl: share_imgUrl, // 分享图标
                //type: "",           // 分享类型,music、video或link，不填默认为link
                //dataUrl: "",        // 如果type是music或video，则要提供数据链接，默认为空
                success: function () {
                    _this.$common.saveShare("微信-朋友", _this, type);

                    // 用户确认分享后执行的回调函数
                    // alert("分享给朋友--成功share_url=" + share_url);
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });
            // 分享到朋友圈
            wx.onMenuShareTimeline({
                title: share_title, // 分享标题
                link: share_url, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                imgUrl: share_imgUrl, // 分享图标
                success: function () {
                    let proId = ""
                    if (type == 1) {
                        proId = "首页";
                    } else if (type == 2) {
                        proId = "更多";
                    } else if (type == 3) {
                        proId = "商店";
                    } else if (type == 4) {
                        proId = "软文";
                    } else if (type == 5) {
                        proId = "产品";
                    } else if (type == 6) {
                        proId = "海报";
                    } else if (type == 7) {
                        proId = "新春大聚惠";
                    }
                    let qyeryData = {
                        userCode: "",
                        shareType: "微信-朋友圈",
                        productId: proId
                    }
                    let localToken = localStorage[SaveName.JWT_TOKEN_NAME];
                    $.ajax({
                        type: "POST",
                        url: RequestUrl.ACT_SAVE_SHARE,
                        data: qyeryData,
                        async: true,
                        headers: {
                            "authorization": localToken,
                        },
                        success: function (res, status, xhr) {
                            _this.$common.reJwtVal(res);
                            _this.$common.storeCommit(_this, Mutations.SET_TRIGGER, true);
                        }
                    });

                    _this.$common.saveShare("微信-朋友圈", _this, type);
                    // 用户确认分享后执行的回调函数
                    // alert("分享到朋友圈--成功share_url=" + share_url);
                },
                cancel: function () {
                    // _this.$common.saveShare("微信-朋友圈", _this, type);
                    // 用户取消分享后执行的回调函数
                }
            });
            // 分享到QQ
            wx.onMenuShareQQ({
                title: share_title, // 分享标题
                desc: share_desc, // 分享描述
                link: share_url, // 分享链接
                imgUrl: share_imgUrl, // 分享图标
                success: function () {
                    _this.$common.saveShare("微信-QQ", _this, type);
                    // 用户确认分享后执行的回调函数
                    // alert("分享到QQ--成功share_url=" + share_url);
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });
            // 分享到腾讯微博
            wx.onMenuShareWeibo({
                title: share_title, // 分享标题
                desc: share_desc, // 分享描述
                link: share_url, // 分享链接
                imgUrl: share_imgUrl, // 分享图标
                success: function () {
                    _this.$common.saveShare("微信-腾讯微博", _this, type);
                    // 用户确认分享后执行的回调函数
                    // alert("分享到腾讯微博--成功share_url=" + share_url);
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });

            // 分享到QQ空间
            wx.onMenuShareQZone({
                title: share_title, // 分享标题
                desc: share_desc, // 分享描述
                link: share_url, // 分享链接
                imgUrl: share_imgUrl, // 分享图标
                success: function () {
                    _this.$common.saveShare("微信-QQ空间", _this, type);
                    // 用户确认分享后执行的回调函数
                    // alert("分享到QQ空间--成功share_url=" + share_url);
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });
        });
    }
}