package cn.com.libertymutual.production.dbconfig;

import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestTemplateConfig {
	
//	@Autowired
//	@Qualifier("restTemplateProperties2")
	private RestTemplateProperties restTemplateProperties = new RestTemplateProperties();
	
	@Bean(name ="singleRestTemplate")
	public RestTemplate singleRestTemplate( ) {
		
		PoolingHttpClientConnectionManager pollingConnectionManager = new PoolingHttpClientConnectionManager();
		pollingConnectionManager.setMaxTotal( restTemplateProperties.getPollingConnection().getMaxTotal() );
		pollingConnectionManager.setDefaultMaxPerRoute( restTemplateProperties.getPollingConnection().getDefaultMaxPerRoute() );
		
		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		httpClientBuilder.setConnectionManager(pollingConnectionManager);
		
		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory( httpClientBuilder.build() );
		
		clientHttpRequestFactory.setConnectTimeout( restTemplateProperties.getConnectTimeout() );
		clientHttpRequestFactory.setReadTimeout( restTemplateProperties.getReadTimeout() );
		
		clientHttpRequestFactory.setBufferRequestBody( restTemplateProperties.isBufferRequestBody() );
		
		RestTemplate restTemplate = new RestTemplate( clientHttpRequestFactory );
		HttpMessageConverter<?> httpMessageConverter = null;
		
		List<HttpMessageConverter<?>> converterList = restTemplate.getMessageConverters();
		HttpMessageConverter<?> converterTarget = null;
		for (HttpMessageConverter<?> item : converterList) {
			if (item.getClass() == StringHttpMessageConverter.class) {	//UTF-8
				converterTarget = item;
				//  break;
			}
			
			if (item.getClass() == AllEncompassingFormHttpMessageConverter.class) {	//UTF-8
				httpMessageConverter = item;
				//  break;
			}
			
		}
		
		
		if (converterTarget != null) {
			converterList.remove(converterTarget);
		}
		
		if( httpMessageConverter!= null ) {
			converterList.remove( httpMessageConverter );
		}
		
		HttpMessageConverter<?> converter = new StringHttpMessageConverter(StandardCharsets.UTF_8);
		converterList.add(converter);
		
		AllEncompassingFormHttpMessageConverter formHttpMessageConverter = new AllEncompassingFormHttpMessageConverter();
		formHttpMessageConverter.setMultipartCharset(StandardCharsets.UTF_8);
		
		converterList.add(formHttpMessageConverter);
		
		
		return restTemplate;
		
	}
}
