package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "t_sp_article")
public class TbSpArticle implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3179274832054046642L;
	private Integer id;
	private String name;
	private String type;
	private String riskCode;
	private Integer productId;
	private String productName;
	private String summaryInfo;
	private String content;
	private String imgUrl;
	private Integer views;
	private String userId;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createDate;
	private String modifyUser;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date modifyTime;
	private String approveStatus;
	private String status = "0";
	private Double rate;
	private TbSpApprove tbSpApprove;
	private String desc;
	private Double minPrice;
	private String approveAuth;
	@Transient
	public String getApproveAuth() {
		return approveAuth;
	}

	public void setApproveAuth(String approveAuth) {
		this.approveAuth = approveAuth;
	}
	@Transient
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Transient
	public Double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}

	@Transient
	public TbSpApprove getTbSpApprove() {
		return tbSpApprove;
	}

	public void setTbSpApprove(TbSpApprove tbSpApprove) {
		this.tbSpApprove = tbSpApprove;
	}

	@Transient
	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "NAME", length = 100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "TYPE", length = 10)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "RISKCODE", length = 10)
	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	@Column(name = "PRODUCT_ID", length = 11)
	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 100)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "SUMMARY_INFO")
	public String getSummaryInfo() {
		return summaryInfo;
	}

	public void setSummaryInfo(String summaryInfo) {
		this.summaryInfo = summaryInfo;
	}

	@Column(name = "CONTENT")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "IMG_URL", length = 100)
	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Column(name = "VIEWS", length = 10)
	public Integer getViews() {
		return views;
	}

	public void setViews(Integer views) {
		this.views = views;
	}

	@Column(name = "USER_ID", length = 10)
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userCode) {
		this.userId = userCode;
	}

	@Column(name = "CREATE_DATE")
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "MODIFY_USER", length = 10)
	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	@Column(name = "MODIFY_TIME")
	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	@Column(name = "APPROVE_STATUS", length = 1)
	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	@Column(name = "STATUS", length = 1)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean differ(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TbSpArticle paramObj = (TbSpArticle) obj;
		if (summaryInfo == null) {
			if (paramObj.summaryInfo != null)
				return false;
		} else if (!summaryInfo.equals(paramObj.summaryInfo))
			return false;
		if (content == null) {
			if (paramObj.content != null)
				return false;
		} else if (!content.equals(paramObj.content))
			return false;
		if (status == null) {
			if (paramObj.status != null)
				return false;
		} else if (!status.equals(paramObj.status))
			return false;
		if (imgUrl == null) {
			if (paramObj.imgUrl != null)
				return false;
		} else if (!imgUrl.equals(paramObj.imgUrl))
			return false;
		if (name == null) {
			if (paramObj.name != null)
				return false;
		} else if (!name.equals(paramObj.name))
			return false;
		if (productId == null) {
			if (paramObj.productId != null)
				return false;
		} else if (!productId.equals(paramObj.productId))
			return false;
		if (productName == null) {
			if (paramObj.productName != null)
				return false;
		} else if (!productName.equals(paramObj.productName))
			return false;
		if (riskCode == null) {
			if (paramObj.riskCode != null)
				return false;
		} else if (!riskCode.equals(paramObj.riskCode))
			return false;
		if (type == null) {
			if (paramObj.type != null)
				return false;
		} else if (!type.equals(paramObj.type))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TbSpArticle [id=" + id + ", name=" + name + ", type=" + type + ", riskCode=" + riskCode + ", productId=" + productId
				+ ", productName=" + productName + ", summaryInfo=" + summaryInfo + ", content=" + content + ", imgUrl=" + imgUrl + ", views=" + views
				+ ", userId=" + userId + ", createDate=" + createDate + ", modifyUser=" + modifyUser + ", modifyTime=" + modifyTime
				+ ", approveStatus=" + approveStatus + ", status=" + status + ", tbSpApprove=" + tbSpApprove + "]";
	}
}
