package cn.com.libertymutual.sp.service.api;

import java.util.Map;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpActivity;
import cn.com.libertymutual.sp.bean.TbSpOrder;
import cn.com.libertymutual.sp.req.DrawReq;

public interface ActivityService {

	ServiceResult activityList(String activityName, String planStartTime, String planEndTime,String activityCode,String status, int pageNumber, int pageSize);

	ServiceResult issueActivity(TbSpActivity activity);

	// ServiceResult activityApproval(Integer id, String status);

	// ServiceResult addBudget(Integer id, Double budget);

	ServiceResult stopActivity(Integer id);

	/**
	 *活动积分发放
	 * @param branchNo
	 * @param userCode
	 * @param spOrder
	 * @return
	 */
	public Map<String, Object> grantScore(String branchNo, String type, String userCode, TbSpOrder spOrder);

	public ServiceResult approveHistory(String budgetApprove, Integer id, Integer pageNumber, Integer pageSize);

	/**
	 * 根据用户查询抽奖次数
	 * @param userCode
	 * @return
	 */
	public ServiceResult queryDrawTimes(String userCode);

	/**
	 * 抽奖
	 * @param userCode
	 * @return
	 */
	public ServiceResult draw(String userCode);

	public ServiceResult task();

	ServiceResult addBudget(DrawReq drawReq);

	ServiceResult getActivityGreatBenefit();

}
