package cn.com.libertymutual.sp.dto.savePlan.base;
/**
 * 销售平台用基础保险期限Dto
 */
public class PeriodBaseDto {

	//dto成员变量首字大写，反序列化有问题
	private String startDate;//保险起期
	private String endDate;//保险止期
	private String startHour;
	private String endHour;
	
	public String getStartHour() {
		return startHour;
	}
	public void setStartHour(String startHour) {
		this.startHour = startHour;
	}
	public String getEndHour() {
		return endHour;
	}
	public void setEndHour(String endHour) {
		this.endHour = endHour;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	@Override
	public String toString() {
		return "PeriodBaseDto [startDate=" + startDate + ", endDate=" + endDate + ", startHour=" + startHour
				+ ", endHour=" + endHour + "]";
	}
	public PeriodBaseDto(String startDate, String endDate, String startHour, String endHour) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.startHour = startHour;
		this.endHour = endHour;
	}
	public PeriodBaseDto() {
		super();
	}
	

}
