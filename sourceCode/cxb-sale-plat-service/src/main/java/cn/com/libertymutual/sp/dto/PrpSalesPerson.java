package cn.com.libertymutual.sp.dto;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_prp_sales_person",  catalog = "")
public class PrpSalesPerson  implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1158426265421124833L;
	private int id;
    private String comCode;
    private String saleName;
    private String saleVocationCardNo;
    private String makeComCode;
    private String createrCode;
    private Date createTime;
    private String agreementNo;

    @Id    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "COM_CODE", nullable = true, length = 13)
    public String getComCode() {
        return comCode;
    }

    public void setComCode(String comCode) {
        this.comCode = comCode;
    }

    @Basic
    @Column(name = "SALE_NAME", nullable = false, length = 25)
    public String getSaleName() {
        return saleName;
    }

    public void setSaleName(String saleName) {
        this.saleName = saleName;
    }

    @Basic
    @Column(name = "SALE_VOCATION_CARD_NO", nullable = true, length = 50)
    public String getSaleVocationCardNo() {
        return saleVocationCardNo;
    }

    public void setSaleVocationCardNo(String saleVocationCardNo) {
        this.saleVocationCardNo = saleVocationCardNo;
    }

    @Basic
    @Column(name = "MAKE_COM_CODE", nullable = true, length = 8)
    public String getMakeComCode() {
        return makeComCode;
    }

    public void setMakeComCode(String makeComCode) {
        this.makeComCode = makeComCode;
    }

    @Basic
    @Column(name = "CREATER_CODE", nullable = true, length = 10)
    public String getCreaterCode() {
        return createrCode;
    }

    public void setCreaterCode(String createrCode) {
        this.createrCode = createrCode;
    }

    @Basic
    @Column(name = "CREATE_TIME", nullable = true)
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "agreement_No", nullable = true, length = 20)
    public String getAgreementNo() {
        return agreementNo;
    }

    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrpSalesPerson that = (PrpSalesPerson) o;

        if (id != that.id) return false;
        if (comCode != null ? !comCode.equals(that.comCode) : that.comCode != null) return false;
        if (saleName != null ? !saleName.equals(that.saleName) : that.saleName != null) return false;
        if (saleVocationCardNo != null ? !saleVocationCardNo.equals(that.saleVocationCardNo) : that.saleVocationCardNo != null)
            return false;
        if (makeComCode != null ? !makeComCode.equals(that.makeComCode) : that.makeComCode != null) return false;
        if (createrCode != null ? !createrCode.equals(that.createrCode) : that.createrCode != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (agreementNo != null ? !agreementNo.equals(that.agreementNo) : that.agreementNo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (comCode != null ? comCode.hashCode() : 0);
        result = 31 * result + (saleName != null ? saleName.hashCode() : 0);
        result = 31 * result + (saleVocationCardNo != null ? saleVocationCardNo.hashCode() : 0);
        result = 31 * result + (makeComCode != null ? makeComCode.hashCode() : 0);
        result = 31 * result + (createrCode != null ? createrCode.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (agreementNo != null ? agreementNo.hashCode() : 0);
        return result;
    }

	@Override
	public String toString() {
		return "PrpSalesPerson [id=" + id + ", comCode=" + comCode
				+ ", saleName=" + saleName + ", saleVocationCardNo="
				+ saleVocationCardNo + ", makeComCode=" + makeComCode
				+ ", createrCode=" + createrCode + ", createTime=" + createTime
				+ ", agreementNo=" + agreementNo + "]";
	}
}

