package cn.com.libertymutual.wx.message.requestdto;

import java.util.Calendar;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import cn.com.libertymutual.wx.common.MessageType;
import cn.com.libertymutual.wx.message.responsedto.ResponseBaseMessage;

public class VideoMessage extends RequestBaseMessage {
	@XStreamAlias("MediaId")
	private String mediaId;
	@XStreamAlias("ThumbMediaId")
	private String thumbMediaId;
	@XStreamAlias("Title")
	private String title;
	@XStreamAlias("Description")
	private String description;

	public VideoMessage(ResponseBaseMessage rbm) {
		long ct = Calendar.getInstance().getTimeInMillis();
		setCreateTime(ct);
		setFromUserName(rbm.getFromUserName());
		setToUserName(rbm.getToUserName());
		setMsgType(MessageType.REQ_MESSAGE_TYPE_VIDEO);
	}

	public String getMediaId() {
		return this.mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getThumbMediaId() {
		return this.thumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
