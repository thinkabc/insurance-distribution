package cn.com.libertymutual.sp.action;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Strings;

import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.ShareService;

@RestController
@RequestMapping(value = "/nol/share")
public class ShareController {
	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private ShareService shareService;

	@RequestMapping(value = "/saveShare")
	public ServiceResult saveShare(String userCode, String productId, String shareId, String shareType, HttpServletRequest request, String agrementNo,
			String saleCode, String saleName, String channelName, boolean isHotArea, String areaCode, String refUuid) {
		ServiceResult sr = new ServiceResult("分享失败", ServiceResult.STATE_APP_EXCEPTION);
		String userCodeTmp = Current.userCode.get();
		if (!Strings.isNullOrEmpty(userCodeTmp) && !userCodeTmp.startsWith("WX") && !userCodeTmp.startsWith("BS") && !userCodeTmp.startsWith("wx")
				&& !userCodeTmp.startsWith("bs")) {
			userCodeTmp = userCode;
		}

		return shareService.saveShare(sr, userCodeTmp, productId, shareId, shareType, request, agrementNo, saleCode, saleName, channelName, isHotArea,
				areaCode, refUuid);
	}

	@RequestMapping(value = "/actSaveShare")
	public ServiceResult saveShare(String userCode, String shareType, String productId) {
		ServiceResult sr = new ServiceResult("抽奖失败", ServiceResult.STATE_APP_EXCEPTION);
		String userCodeTmp = Current.userCode.get();
		if (!Strings.isNullOrEmpty(userCodeTmp) && !userCodeTmp.startsWith("WX") && !userCodeTmp.startsWith("BS") && !userCodeTmp.startsWith("wx")
				&& !userCodeTmp.startsWith("bs")) {
			userCodeTmp = userCode;
		} else {
			sr = shareService.saveActivity(sr, userCodeTmp, shareType, productId);
		}
		return sr;
	}

}
