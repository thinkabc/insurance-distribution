package cn.com.libertymutual.sp.sftp;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.util.RequestUtils;

//@Component
//@RefreshScope // 刷新配置无需重启服务
public class SFTP {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Value("${ftp.ports}")
	private String ports;

	@Value("${ftp.hosts}")
	private String hosts;

	@Value("${ftp.usernames}")
	private String usernames;

	@Value("${ftp.passwords}")
	private String passwords;

	@Value("${ftp.dsts}")
	private String dsts;

	public void SFTPupload(String src) {

		String thisIP = RequestUtils.getLocalHostLANAddress()[0];
		log.info("------thisIP-----------" + thisIP);
		log.info("------ports-----------" + ports);

		Map<String, String> sftpDetails = new HashMap<String, String>();

		String[] portss = ports.split(",");
		String[] hostss = hosts.split(",");
		String[] usernamess = usernames.split(",");
		String[] passwordss = passwords.split(",");
		String[] dstss = dsts.split(",");

		/*log.info("------hostss-----------" + hostss);
		log.info("------usernamess-----------" + usernamess);
		log.info("------passwordss-----------" + passwordss);
		log.info("------portss-----------" + portss);*/
		for (int i = 0; i < hostss.length; i++) {
			log.info("------hostss-----------" + hostss[i]);
			log.info("------usernamess-----------" + usernamess[i]);
			log.info("------passwordss-----------" + passwordss[i]);
			log.info("------portss-----------" + portss[i]);
			if (!hostss[i].equals(thisIP)) {
				sftpDetails.put(Constants.SFTP_REQ_HOST, hostss[i]);
				sftpDetails.put(Constants.SFTP_REQ_USERNAME, usernamess[i]);
				sftpDetails.put(Constants.SFTP_REQ_PASSWORD, passwordss[i]);
				sftpDetails.put(Constants.SFTP_REQ_PORT, portss[i]);
				SFTPChannel channel = new SFTPChannel();
				ChannelSftp chSftp;
				try {
					chSftp = channel.getChannel(sftpDetails, 60000);
					log.info("----------方法一-----------");
					try {
//						chSftp.put(src, dstss[i], ChannelSftp.OVERWRITE);
						chSftp.put(new FileInputStream(src), dstss[i], ChannelSftp.OVERWRITE);
						chSftp.quit();
						try {
							channel.closeChannel();
						} catch (Exception e) {
							e.printStackTrace();
						}
					} catch (SftpException e) {
						e.printStackTrace();
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				} catch (JSchException e) {
					e.printStackTrace();
				}
			}
		}

		// OutputStream out = chSftp.put(dst, ChannelSftp.OVERWRITE); //
		// 使用OVERWRITE模式
		// byte[] buff = new byte[1024 * 256]; // 设定每次传输的数据块大小为256KB
		// int read;
		// if (out != null) {
		// System.out.println("Start to read input stream");
		// InputStream is = new FileInputStream(src);
		// do {
		// read = is.read(buff, 0, buff.length);
		// if (read > 0) {
		// out.write(buff, 0, read);
		// }
		// out.flush();
		// } while (read >= 0);
		// System.out.println("input stream read done.");
		// }

		// chSftp.put(new FileInputStream(src), dst, ChannelSftp.OVERWRITE); //
		// 代码段3

	}
}