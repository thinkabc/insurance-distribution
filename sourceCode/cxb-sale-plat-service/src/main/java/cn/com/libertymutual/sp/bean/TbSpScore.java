package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;

@ApiModel
@Entity
@Table(name = "tb_sp_score")
public class TbSpScore implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7470444255718746419L;

	private Integer id;
	private String userCode;
	private String phoneNo;
	private String openid;
	private Double sumScore;
	private Double effctiveScore;// 有效积分
	private Double noeffctiveScore;// 待生效积分
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date statisticTime;
	private String version;

	public TbSpScore() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TbSpScore(String userCode, String phoneNo, String openid, Double sumScore, Double effctiveScore, Double noeffctiveScore,
			Date statisticTime, String version) {
		super();
		this.userCode = userCode;
		this.phoneNo = phoneNo;
		this.sumScore = sumScore;
		this.effctiveScore = effctiveScore;
		this.noeffctiveScore = noeffctiveScore;
		this.statisticTime = statisticTime;
		this.version = version;
		this.openid = openid;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "usercode")
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	@Column(name = "phoneno")
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	@Column(name = "Sumscore")
	public Double getSumScore() {
		return sumScore;
	}

	public void setSumScore(Double sumScore) {
		this.sumScore = sumScore;
	}

	@Column(name = "effctiveScore")
	public Double getEffctiveScore() {
		return effctiveScore;
	}

	public void setEffctiveScore(Double effctiveScore) {
		this.effctiveScore = effctiveScore;
	}

	@Column(name = "noeffctiveScore")
	public Double getNoeffctiveScore() {
		return noeffctiveScore;
	}

	public void setNoeffctiveScore(Double noeffctiveScore) {
		this.noeffctiveScore = noeffctiveScore;
	}

	@Column(name = "statisticTime")
	public Date getStatisticTime() {
		return statisticTime;
	}

	public void setStatisticTime(Date statisticTime) {
		this.statisticTime = statisticTime;
	}

	@Column(name = "version")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Column(name = "openid")
	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

}
