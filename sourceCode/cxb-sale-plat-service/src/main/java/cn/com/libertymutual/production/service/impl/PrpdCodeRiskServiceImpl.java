package cn.com.libertymutual.production.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdcoderiskMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdcoderiskExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdcoderiskKey;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdcoderiskExample.Criteria;
import cn.com.libertymutual.production.service.api.PrpdCodeRiskService;

/** 
 * @author GuoYue
 * @date 2017年9月12日
 *  
 */
@Service
public class PrpdCodeRiskServiceImpl implements PrpdCodeRiskService {

	@Autowired
	private PrpdcoderiskMapper prpdcoderiskMapper;
	
	@Override
	public void insert(PrpdcoderiskKey record) {
		prpdcoderiskMapper.insertSelective(record);
	}

	@Override
	public List<PrpdcoderiskKey> findCodeRiskByCondition(String codeType, String codeCode) {
		PrpdcoderiskExample example = new PrpdcoderiskExample();
		Criteria criteria = example.createCriteria();
		criteria.andCodetypeEqualTo(codeType);
		criteria.andCodecodeEqualTo(codeCode);
		return prpdcoderiskMapper.selectByExample(example);
	}

	@Override
	public void update(PrpdcoderiskKey record) {
		PrpdcoderiskExample example = new PrpdcoderiskExample();
		Criteria criteria = example.createCriteria();
		criteria.andCodetypeEqualTo(record.getCodetype());
		criteria.andCodecodeEqualTo(record.getCodecode());
		prpdcoderiskMapper.updateByExampleSelective(record, example);
	}

	@Override
	public void delete(PrpdcoderiskKey record) {
		PrpdcoderiskExample example = new PrpdcoderiskExample();
		Criteria criteria = example.createCriteria();
		criteria.andCodetypeEqualTo(record.getCodetype());
		criteria.andCodecodeEqualTo(record.getCodecode());
		prpdcoderiskMapper.deleteByExample(example);
	}

	@Override
	public List<PrpdcoderiskKey> findCodeRiskByClause(String codecode) {
		PrpdcoderiskExample example = new PrpdcoderiskExample();
		Criteria criteria = example.createCriteria();
		criteria.andCodecodeEqualTo(codecode);
		return prpdcoderiskMapper.selectByExample(example);
	}
	
}
