
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prptLimitDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptLimitDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="limitCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="limitCurrencyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="limitFee" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="limitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptLimitDto", propOrder = {
    "limitCurrencyCode",
    "limitCurrencyName",
    "limitFee",
    "limitType"
})
public class PrptLimitDto implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -4055308393039122646L;
	protected String limitCurrencyCode;
    protected String limitCurrencyName;
    protected double limitFee;
    protected String limitType;

    /**
     * Gets the value of the limitCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitCurrencyCode() {
        return limitCurrencyCode;
    }

    /**
     * Sets the value of the limitCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitCurrencyCode(String value) {
        this.limitCurrencyCode = value;
    }

    /**
     * Gets the value of the limitCurrencyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitCurrencyName() {
        return limitCurrencyName;
    }

    /**
     * Sets the value of the limitCurrencyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitCurrencyName(String value) {
        this.limitCurrencyName = value;
    }

    /**
     * Gets the value of the limitFee property.
     * 
     */
    public double getLimitFee() {
        return limitFee;
    }

    /**
     * Sets the value of the limitFee property.
     * 
     */
    public void setLimitFee(double value) {
        this.limitFee = value;
    }

    /**
     * Gets the value of the limitType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitType() {
        return limitType;
    }

    /**
     * Sets the value of the limitType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitType(String value) {
        this.limitType = value;
    }

}
