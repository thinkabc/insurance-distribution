package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpdItemRiskLibrary extends Prpditemlibrary {
	private String riskcode;

	private String riskversion;

	public String getRiskcode() {
		return riskcode;
	}

	public void setRiskcode(String riskcode) {
		this.riskcode = riskcode;
	}

	public String getRiskversion() {
		return riskversion;
	}

	public void setRiskversion(String riskversion) {
		this.riskversion = riskversion;
	}

}