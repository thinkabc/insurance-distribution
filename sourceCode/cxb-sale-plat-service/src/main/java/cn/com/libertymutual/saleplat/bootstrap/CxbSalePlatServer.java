package cn.com.libertymutual.saleplat.bootstrap;

/**
 * 出行宝主程序
 * 
 */
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RestController;

@ComponentScan(basePackages={"cn.com.libertymutual"})
@EnableAutoConfiguration
@EnableScheduling
@ServletComponentScan
@EnableTransactionManagement(order = 10) // 值越小，越优先执行 ,要优于事务的执行 ,在启动类中加上了@EnableTransactionManagement(order = 10)
@RestController
public class CxbSalePlatServer {
	
	public static void main(String[] args) {
		//new SpringApplicationBuilder(SalePlatServer.class).run(args);
		///SpringApplication.run(SalePlatServer.class, args);

		new SpringApplicationBuilder()
			.sources(CxbSalePlatServer.class)
			//.child(SalePlatServer.class)
			.bannerMode(Banner.Mode.CONSOLE)
			.run(args);
			
    	System.out.println("出行宝启动成功!!!!!!!!!!!!!!!!!!"); 
	}

}