package cn.com.libertymutual.production.pojo.request;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkind;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdrisk;

public class RiskKindRequest extends Request {

	private Prpdrisk risk;
	private List<Prpdkind> kinds;

	public Prpdrisk getRisk() {
		return risk;
	}

	public void setRisk(Prpdrisk risk) {
		this.risk = risk;
	}

	public List<Prpdkind> getKinds() {
		return kinds;
	}

	public void setKinds(List<Prpdkind> kinds) {
		this.kinds = kinds;
	}

}
