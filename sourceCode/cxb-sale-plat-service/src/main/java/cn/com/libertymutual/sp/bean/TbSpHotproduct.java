package cn.com.libertymutual.sp.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_sp_hotproduct")
public class TbSpHotproduct implements Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 9007469821417346231L;
	private Integer id;
	private Integer productId;
	private String branchCode;
	private String introduceUrl;
	private Integer serialNo;
	private String remark;
	private String status;

	// Constructors

	/** default constructor */
	public TbSpHotproduct() {
	}


	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "PRODUCT_ID", length = 10)
	public Integer getProductId() {
		return this.productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	@Column(name = "BRANCH_CODE", length = 10)
	public String getBranchCode() {
		return this.branchCode;
	}

	public void setBranchCode(String areaCode) {
		this.branchCode = areaCode;
	}

	@Column(name = "INTRODUCE_URL", length = 10)
	public String getIntroduceUrl() {
		return this.introduceUrl;
	}

	public void setIntroduceUrl(String introduceUrl) {
		this.introduceUrl = introduceUrl;
	}

	@Column(name = "SERIAL_NO", length = 50)
	public Integer getSerialNo() {
		return this.serialNo;
	}

	public void setSerialNo(Integer serialNo) {
		this.serialNo = serialNo;
	}

	@Column(name = "REMARK", length = 100)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "STATUS", length = 1)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}