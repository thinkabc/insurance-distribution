package cn.com.libertymutual.sp.dto.savePlan.base;
/**
 * 销售平台基础受益人Dto
 */
public class BenefBaseDto {

	private String insuredType;//受益人类型
	private String benefType;// 受益方式：2-指定份额受益
	private String benefName;// 名称
	private String identifyType;// 证件类型
	private String identifyNumber;// 证件号码
	private String benefitRate;// 受益比例
	private String email;
	private String phoneNumber;
	private String benefAddress;
	private String benefIdentity;

	
	public String getInsuredType() {
		return insuredType;
	}

	public void setInsuredType(String insuredType) {
		this.insuredType = insuredType;
	}
	
	public String getBenefType() {
		return benefType;
	}

	public void setBenefType(String benefType) {
		this.benefType = benefType;
	}

	public String getBenefName() {
		return benefName;
	}

	public void setBenefName(String benefName) {
		this.benefName = benefName;
	}

	public String getIdentifyType() {
		return identifyType;
	}

	public void setIdentifyType(String identifyType) {
		this.identifyType = identifyType;
	}

	public String getIdentifyNumber() {
		return identifyNumber;
	}

	public void setIdentifyNumber(String identifyNumber) {
		this.identifyNumber = identifyNumber;
	}

	public String getBenefitRate() {
		return benefitRate;
	}

	public void setBenefitRate(String benefitRate) {
		this.benefitRate = benefitRate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getBenefAddress() {
		return benefAddress;
	}

	public void setBenefAddress(String benefAddress) {
		this.benefAddress = benefAddress;
	}

	public String getBenefIdentity() {
		return benefIdentity;
	}

	public void setBenefIdentity(String benefIdentity) {
		this.benefIdentity = benefIdentity;
	}

	@Override
	public String toString() {
		return "BaseBenefDto [insuredType=" + insuredType + ", benefType=" + benefType + ", benefName=" + benefName
				+ ", identifyType=" + identifyType + ", identifyNumber=" + identifyNumber + ", benefitRate="
				+ benefitRate + ", email=" + email + ", phoneNumber=" + phoneNumber + ", benefAddress=" + benefAddress
				+ ", benefIdentity=" + benefIdentity + "]";
	}

	public BenefBaseDto(String insuredType, String benefType, String benefName, String identifyType,
			String identifyNumber, String benefitRate, String email, String phoneNumber, String benefAddress,
			String benefIdentity) {
		super();
		this.insuredType = insuredType;
		this.benefType = benefType;
		this.benefName = benefName;
		this.identifyType = identifyType;
		this.identifyNumber = identifyNumber;
		this.benefitRate = benefitRate;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.benefAddress = benefAddress;
		this.benefIdentity = benefIdentity;
	}

	public BenefBaseDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	 


}
