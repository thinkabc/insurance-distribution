package cn.com.libertymutual.sp.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.CarPlateService;
import cn.com.libertymutual.sys.bean.SysCodeNode;
import cn.com.libertymutual.sys.dao.ISysCodeNodeDao;
import cn.com.libertymutual.sys.service.api.IInitSharedMemory;

@Service("carPlateService")
public class CarPlateServiceImpl implements CarPlateService{

	@Autowired
	private ISysCodeNodeDao  codeNodeDao;
	@Autowired
	private IInitSharedMemory  initSharedMemory;
	@Autowired
	private RedisUtils redisUtils;
	@Override
	public ServiceResult plateList(Integer pageNumber, Integer pageSize) {
		ServiceResult sr = new ServiceResult();
		Sort sort = new Sort(Direction.ASC, "id");
		Pageable pageable = PageRequest.of(pageNumber - 1, pageSize, sort);
		if (StringUtils.isNotEmpty(pageNumber.toString())&&StringUtils.isNotEmpty(pageSize.toString())) {
			Page<SysCodeNode> plateList = codeNodeDao.findByCodeType(Constants.LIMIT_LICENSE_NO,pageable);
			sr.setResult(plateList);
		}
		
		return sr;
	}
	
	
	
	
	@Override
	public ServiceResult addPlate(String plateNo) {
		ServiceResult sr = new ServiceResult();
		SysCodeNode sysCoden = new SysCodeNode();
		sysCoden.setCodeType(Constants.LIMIT_LICENSE_NO);
		sysCoden.setCode(plateNo);
		sysCoden.setCodeCname(" ");
		sysCoden.setCodeEname(" ");
		sysCoden.setValidStatus(Constants.TRUE);
		sysCoden.setSerialNo(1);
		codeNodeDao.save(sysCoden);
		initSharedMemory.initCodeInto();
		return sr;
	}



	@Override
	public ServiceResult removePlate(String plateNo) {
		ServiceResult sr = new ServiceResult();
		HashMap<String, Map<String, SysCodeNode>> codeMap = (HashMap<String, Map<String, SysCodeNode>>) redisUtils.get(Constants.MAP_CODE);
		SysCodeNode branch = codeMap.get(Constants.LIMIT_LICENSE_NO).get(plateNo);
		codeNodeDao.delete(branch);
		initSharedMemory.initCodeInto();
		return sr;
	}

}
