package cn.com.libertymutual.production.service.api;


import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpditem;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpditemWithBLOBs;

public interface PrpdItemService {

	public void insert(PrpditemWithBLOBs record);
	
	public void update(PrpditemWithBLOBs record);
	
	public List<PrpditemWithBLOBs> select(Prpditem criteria);
	
}
