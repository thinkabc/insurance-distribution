package cn.com.libertymutual.sp.service.api;

import java.util.List;

import cn.com.libertymutual.sp.bean.TbSpProductConfig;

public interface ProductConfigService {
	List<TbSpProductConfig> findByBranchCode(String  branchCode);
}
