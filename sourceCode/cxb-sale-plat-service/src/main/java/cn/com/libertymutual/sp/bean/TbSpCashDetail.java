package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;

@ApiModel
@Entity
@Table(name = "tb_sp_cashdetail")
public class TbSpCashDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 61895689906697692L;
	private Integer id;
	private String businessNo;// 提现编号
	private Double score;
	private Double money;
	private Double rate;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;
	private String branchCode;
	private String scoreType;
	private String currency;
	private String policyNo;
	private String reasonNo;
	private String reason;
	private String remarks;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "business_No")
	public String getBusinessNo() {
		return businessNo;
	}

	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}

	@Column(name = "score")
	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	@Column(name = "money")
	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	@Column(name = "rate")
	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "SCORE_TYPE")
	public String getScoreType() {
		return scoreType;
	}

	public void setScoreType(String scoreType) {
		this.scoreType = scoreType;
	}

	@Column(name = "currency")
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Column(name = "policy_no")
	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	@Column(name = "reason_no")
	public String getReasonNo() {
		return reasonNo;
	}

	public void setReasonNo(String reasonNo) {
		this.reasonNo = reasonNo;
	}

	@Column(name = "reason")
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "remarks")
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public TbSpCashDetail() {
		super();
	}

	public TbSpCashDetail(String businessNo, Double score, Double money, Double rate, Date createTime, String scoreType, String currency,
			String policyNo, String reasonNo, String reason, String remarks, String branchCode) {
		super();
		this.businessNo = businessNo;
		this.score = score;
		this.money = money;
		this.rate = rate;
		this.createTime = createTime;
		this.scoreType = scoreType;
		this.currency = currency;
		this.policyNo = policyNo;
		this.reasonNo = reasonNo;
		this.reason = reason;
		this.remarks = remarks;
		this.branchCode = branchCode;
	}

	@Column(name = "BRANCH_CODE")
	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	private TbSpOrder order;

	@Transient
	public TbSpOrder getOrder() {
		return order;
	}

	public void setOrder(TbSpOrder order) {
		this.order = order;
	}

	private TbSpCashLog cashLog;

	@Transient
	public TbSpCashLog getCashLog() {
		return cashLog;
	}

	public void setCashLog(TbSpCashLog cashLog) {
		this.cashLog = cashLog;
	}

}
