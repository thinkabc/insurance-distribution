
package cn.com.libertymutual.sp.webService.allpolicy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prpTmainSubDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prpTmainSubDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="combineFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mainPolicyNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mainProposalNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proposalNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prpTmainSubDto", propOrder = {
    "combineFlag",
    "mainPolicyNo",
    "mainProposalNo",
    "proposalNo"
})
public class PrpTmainSubDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected String combineFlag;
    protected String mainPolicyNo;
    protected String mainProposalNo;
    protected String proposalNo;

    /**
     * Gets the value of the combineFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCombineFlag() {
        return combineFlag;
    }

    /**
     * Sets the value of the combineFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCombineFlag(String value) {
        this.combineFlag = value;
    }

    /**
     * Gets the value of the mainPolicyNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainPolicyNo() {
        return mainPolicyNo;
    }

    /**
     * Sets the value of the mainPolicyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainPolicyNo(String value) {
        this.mainPolicyNo = value;
    }

    /**
     * Gets the value of the mainProposalNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainProposalNo() {
        return mainProposalNo;
    }

    /**
     * Sets the value of the mainProposalNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainProposalNo(String value) {
        this.mainProposalNo = value;
    }

    /**
     * Gets the value of the proposalNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProposalNo() {
        return proposalNo;
    }

    /**
     * Sets the value of the proposalNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProposalNo(String value) {
        this.proposalNo = value;
    }

}
