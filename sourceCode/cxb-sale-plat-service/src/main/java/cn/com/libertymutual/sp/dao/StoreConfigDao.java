package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.com.libertymutual.sp.bean.TbSpStoreConfig;

public interface StoreConfigDao extends PagingAndSortingRepository<TbSpStoreConfig, Integer>, JpaSpecificationExecutor<TbSpStoreConfig> {

	@Query("select t from TbSpStoreConfig t where t.userCode = ?1")
	Page<TbSpStoreConfig> findByUserCode(String userCode, Pageable pageable);

	List<TbSpStoreConfig> findByUserCodeAndProductId(String userCode, Integer productId);

	@Query("select t from TbSpStoreConfig t where t.userCode = ?1")
	List<TbSpStoreConfig> findByUserCode(String userCode);

	@Query("select count(t.id) from TbSpStoreConfig t where t.userCode = ?1")
	int findEYongjCountByUserCode(String userCode);
}
