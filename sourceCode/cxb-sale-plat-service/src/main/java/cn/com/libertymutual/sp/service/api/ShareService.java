package cn.com.libertymutual.sp.service.api;

import javax.servlet.http.HttpServletRequest;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpShare;

public interface ShareService {

	ServiceResult saveActivity(ServiceResult sr, String userCodeTmp, String shareType, String productId);

	ServiceResult saveShare(ServiceResult sr, String userCode, String productId, String shareId, String shareType, HttpServletRequest request,
			String agrementNo, String saleCode, String saleName, String channelName, boolean isHotArea, String areaCode, String refUuid);

	ServiceResult shareList(String shareId, String userCode, String agrementNo, String startTime, String endTime, Integer pageNumber,
			Integer pageSize);

	ServiceResult addShare(TbSpShare tbSpShare);

	ServiceResult removeShare(Integer id);
}
