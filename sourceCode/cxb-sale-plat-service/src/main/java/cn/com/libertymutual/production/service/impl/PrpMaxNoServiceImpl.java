package cn.com.libertymutual.production.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpmaxnoMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpmaxno;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpmaxnoExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpmaxnoExample.Criteria;
import cn.com.libertymutual.production.service.api.PrpMaxNoService;

@Service
public class PrpMaxNoServiceImpl implements PrpMaxNoService {

	@Autowired
	private PrpmaxnoMapper prpmaxnoMapper;
	
	@Override
	public String findMaxNo(String tableName) {
		PrpmaxnoExample example = new PrpmaxnoExample();
		Criteria criteria = example.createCriteria();
		criteria.andTablenameEqualTo(tableName);
		List<Prpmaxno> list = prpmaxnoMapper.selectByExample(example);
		if(!list.isEmpty())  {
			return list.get(0).getMaxno();
		}
		return null;
	}

	@Override
	public void updateMaxNo(Prpmaxno record, String tableName) {
		PrpmaxnoExample example = new PrpmaxnoExample();
		Criteria criteria = example.createCriteria();
		criteria.andTablenameEqualTo(tableName);
		prpmaxnoMapper.updateByExampleSelective(record, example);
	}
	
}
