package cn.com.libertymutual.sp.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSON;

import cn.com.libertymutual.core.base.dto.AgreementCodeRequestDto;
import cn.com.libertymutual.core.base.dto.PrpLmAgent;
import cn.com.libertymutual.core.base.dto.TQueryLmAgentResponseDto;
import cn.com.libertymutual.core.email.IEmailService;
import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.security.encoder.Md5PwdEncoder;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.util.StringUtil;
import cn.com.libertymutual.core.util.enums.CoreServiceEnum;
import cn.com.libertymutual.core.util.uuid.UUID;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.core.web.util.RequestUtils;
import cn.com.libertymutual.sp.bean.TbSpAgreementNoConfig;
import cn.com.libertymutual.sp.bean.TbSpProductConfig;
import cn.com.libertymutual.sp.bean.TbSpSaleLog;
import cn.com.libertymutual.sp.bean.TbSpScoreConfig;
import cn.com.libertymutual.sp.bean.TbSpShare;
import cn.com.libertymutual.sp.bean.TbSpStoreConfig;
import cn.com.libertymutual.sp.bean.TbSpStoreProduct;
import cn.com.libertymutual.sp.bean.TbSpTopChannelAuthProduct;
import cn.com.libertymutual.sp.bean.TbSpUser;
import cn.com.libertymutual.sp.bean.TbSysHotarea;
import cn.com.libertymutual.sp.dao.AgreementNoConfigDao;
import cn.com.libertymutual.sp.dao.BankDao;
import cn.com.libertymutual.sp.dao.OrderDao;
import cn.com.libertymutual.sp.dao.ProductConfigDao;
import cn.com.libertymutual.sp.dao.ScoreConfigDao;
import cn.com.libertymutual.sp.dao.ShareDao;
import cn.com.libertymutual.sp.dao.SpSaleLogDao;
import cn.com.libertymutual.sp.dao.StoreConfigDao;
import cn.com.libertymutual.sp.dao.StoreProductDao;
import cn.com.libertymutual.sp.dao.TbSpExclusiveProductDao;
import cn.com.libertymutual.sp.dao.TbSysHotareaDao;
import cn.com.libertymutual.sp.dao.TopChannelAuthProductDao;
import cn.com.libertymutual.sp.dao.UserDao;
import cn.com.libertymutual.sp.dto.RateDto;
import cn.com.libertymutual.sp.dto.response.productManageDto;
import cn.com.libertymutual.sp.service.api.BranchSetService;
import cn.com.libertymutual.sp.service.api.InsureService;
import cn.com.libertymutual.sp.service.api.ShopService;
import cn.com.libertymutual.sp.service.api.SmsService;
import cn.com.libertymutual.sys.bean.SysCodeNode;
import cn.com.libertymutual.sys.bean.SysServiceInfo;
import cn.com.libertymutual.sys.dao.SysBranchDao;

@Service("ShopService")
public class ShopServiceImpl implements ShopService {
	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private StoreProductDao storeProductDao;
	@Autowired
	private ProductConfigDao productConfigDao;
	@Autowired
	private JdbcTemplate readJdbcTemplate;
	@Autowired
	private TbSysHotareaDao hotareaDao;
	@Autowired
	private SysBranchDao sysBranchDao;
	@Autowired
	private AgreementNoConfigDao agreementNoConfigDao;
	@Autowired
	private UserDao userDao;
	@Resource
	private RedisUtils redisUtils;
	@Autowired
	private ScoreConfigDao scoreConfigDao;
	@Autowired
	private OrderDao orderDao;
	@Autowired
	private TbSpExclusiveProductDao exclusiveProductDao;
	@Resource
	private SmsService smsService;// 短信
	@Autowired
	private BranchSetService branchSetService;
	@Resource
	private IEmailService emailService;
	@Resource
	private RedisUtils redis;
	@Resource
	private RestTemplate restTemplate;
	@Autowired
	private StoreConfigDao storeConfigDao;
	@Autowired
	private ShareDao shareDao;
	@Autowired
	private BankDao bankDao;
	@Autowired
	private SpSaleLogDao spSaleLogDao;

	@Autowired
	private InsureService insureService;

	@Autowired
	private TopChannelAuthProductDao topChannelAuthProductDao;

	@Override
	public TbSpUser mergeAccount(String userCode) {
		ServiceResult sr = new ServiceResult();
		sr.setSuccess();
		TbSpUser user = userDao.findByUserCodeAll(userCode);
		try {
			if (StringUtil.isNotEmpty(user.getUserCodeBs())) {
				TbSpUser userBs = userDao.findByUserCodeAll(user.getUserCodeBs());
				if (user.getPopularity() != null && userBs.getPopularity() != null) {
					user.setPopularity(userBs.getPopularity() + user.getPopularity());
				}
				if (user.getPopularity() == null && userBs.getPopularity() != null) {
					user.setPopularity(userBs.getPopularity());
				}
				if (user.getRegisteDate().getTime() > userBs.getRegisteDate().getTime()) {
					user.setRegisteDate(userBs.getRegisteDate());
				}
				if (StringUtil.isEmpty(user.getShopIntroduct()) && userBs.getShopIntroduct() != null) {
					user.setShopIntroduct(userBs.getShopIntroduct());
				}
				user = userDao.save(user);

				List<TbSpStoreProduct> userStoreConfig = storeProductDao.findUserCode(userCode);
				List<TbSpStoreProduct> userBsStoreConfig = storeProductDao.findUserCode(user.getUserCodeBs());
				if ((userStoreConfig == null && userBsStoreConfig != null) || (userStoreConfig != null && userBsStoreConfig != null)) {
					for (int i = 0; i < userBsStoreConfig.size(); i++) {
						boolean isSave = true;
						TbSpStoreProduct bsConfig = userBsStoreConfig.get(i);
						TbSpStoreProduct userConfig = new TbSpStoreProduct();
						userConfig.setUserCode(userCode);
						userConfig.setProductId(bsConfig.getProductId());
						userConfig.setSerialNo(bsConfig.getSerialNo());
						userConfig.setRiskSerialNo(bsConfig.getRiskSerialNo());
						userConfig.setIsShow(bsConfig.getIsShow());
						for (int j = 0; j < userStoreConfig.size(); j++) {
							TbSpStoreProduct userPro = userStoreConfig.get(j);
							if (userPro.getProductId().equals(bsConfig.getProductId())) {
								isSave = false;
								break;
							}
						}
						if (isSave) {
							storeProductDao.save(userConfig);
						}
					}
				}
			}
		} catch (Exception e) {
			sr.setFail();
		}
		return user;
	}

	@Override
	public Double getRate(Double baseRate, String userCode, String proId, boolean isBaseRate) {
		List<TbSpScoreConfig> scoreconfig = null;
		List<TbSpStoreConfig> stConfig = storeConfigDao.findByUserCodeAndProductId(userCode, Integer.parseInt(proId));
		if (stConfig != null && stConfig.size() > 0) {
			TbSpStoreConfig sp = stConfig.get(0);
			baseRate = sp.getRate();
		} else {
			TbSpUser user = userDao.findByUserCode(userCode);
			if (user != null && StringUtils.isNotBlank(user.getAgrementNo())) {
				List<TbSpAgreementNoConfig> dbagreelist = agreementNoConfigDao.findByAgreementNoAndProductId(user.getAgrementNo(),
						Integer.parseInt(proId));
				if (CollectionUtils.isNotEmpty(dbagreelist)) {
					baseRate = dbagreelist.get(0).getRate();
				}
			}
		}
		BigDecimal curRate = new BigDecimal(baseRate);
		if (isBaseRate) {
			BigDecimal rateBase = new BigDecimal(100);
			return curRate.multiply(rateBase).doubleValue();
		} else {
			scoreconfig = (List<TbSpScoreConfig>) redis.get(Constants.SCORE_CONFIG_INFO);
			if (CollectionUtils.isEmpty(scoreconfig)) {
				scoreconfig = scoreConfigDao.findAllConfig();
				redisUtils.set(Constants.SCORE_CONFIG_INFO, scoreconfig);
			}
			BigDecimal rate = new BigDecimal(scoreconfig.get(0).getFeeRate());
			BigDecimal base = new BigDecimal(1);
			curRate = curRate.divide(base.add(rate), 3, BigDecimal.ROUND_HALF_UP);
			return curRate.doubleValue();
		}

		// baseRate = baseRate * (1 - scoreconfig.get(0).getFeeRate());
	}

	@Override
	public Boolean isSetRate(String userCode) {
		Boolean isShowRate = true;
		TbSpUser user = userDao.findByUserCode(userCode);
		if (null != user && "1".equals(user.getRegisterType()) && StringUtils.isNotBlank(user.getComCode())) {
			TbSpUser userTopInfo = userDao.findByUserCode(user.getComCode());
			if (userTopInfo != null && StringUtils.isNotBlank(userTopInfo.getCanScore())) {
				if ("0".equals(userTopInfo.getCanScore())) {
					isShowRate = false;
				}
			}
		}
		return isShowRate;
	}

	@Override
	public ServiceResult productManage(String userCode, String branchCode, String riskCode) {
		ServiceResult sr = new ServiceResult();
		RateDto rateDto = insureService.getLevelRate(userCode);
		String userTop = rateDto.getUserCode();
		Double levenRate = rateDto.getRate();
		log.info("productManage userCode--{}", userCode);
		log.info("productManage branchCode--{}", branchCode);
		log.info("productManage riskCode--{}", riskCode);
		TbSysHotarea hot = hotareaDao.findByAreaCode(branchCode);
		Boolean isShowRate = isSetRate(userCode);
		if (hot != null && "all".equals(riskCode)) {
			String branCode = branchSetService.cutBranchCode(hot.getBranchCode());
			List<Object[]> proConfig = productConfigDao.findByBranchPro(branCode);
			List<productManageDto> dto = new ArrayList<productManageDto>();
			for (int i = 0; i < proConfig.size(); i++) {
				Object[] st = proConfig.get(i);
				productManageDto pm = new productManageDto();
				pm.setProductId(st[0].toString());
				pm.setRiskCode(st[1].toString());
				pm.setProductCname(st[2].toString());
				if (isShowRate) {
					pm.setRate(getRate(Double.parseDouble(st[3].toString()), userTop, pm.getProductId(), false) * levenRate);
				} else {
					pm.setRate(0.0);
				}
				pm.setRiskName(st[4].toString());
				dto.add(pm);
			}
			// 专属产品
			List<Object[]> exclusive = exclusiveProductDao.findProByUCode(userTop, branCode);
			if (null != exclusive && exclusive.size() != 0) {
				for (int i = 0; i < exclusive.size(); i++) {
					Object[] st = exclusive.get(i);
					productManageDto pm = new productManageDto();
					pm.setProductId(st[0].toString());
					pm.setRiskCode(st[1].toString());
					pm.setProductCname(st[2].toString());
					if (isShowRate) {
						pm.setRate(getRate(Double.parseDouble(st[3].toString()), userTop, pm.getProductId(), false) * levenRate);
					} else {
						pm.setRate(0.0);
					}
					pm.setRiskName(st[4].toString());
					dto.add(pm);
				}
			}
			Map<String, Object> proMap = new HashMap<String, Object>();
			proMap = setChannelPro(userCode, userTop, proMap);
			proMap.put("sp", storeProductDao.findUserCode(userCode));
			proMap.put("pc", dto);
			sr.setResult(proMap);
			return sr;
		} else if (hot != null) {
			String branCode = branchSetService.cutBranchCode(hot.getBranchCode());
			List<Object[]> proConfig = productConfigDao.findByBranchProRiskCode(branCode, riskCode);
			List<productManageDto> dto = new ArrayList<productManageDto>();
			for (int i = 0; i < proConfig.size(); i++) {
				Object[] st = proConfig.get(i);
				productManageDto pm = new productManageDto();
				pm.setProductId(st[0].toString());
				pm.setRiskCode(st[1].toString());
				pm.setProductCname(st[2].toString());
				if (isShowRate) {
					pm.setRate(getRate(Double.parseDouble(st[3].toString()), userTop, pm.getProductId(), false) * levenRate);
				} else {
					pm.setRate(0.0);
				}
				pm.setRiskName(st[4].toString());
				dto.add(pm);
			}
			// 专属产品
			List<Object[]> exclusive = exclusiveProductDao.findProByUCodeAndRiskCode(userTop, riskCode, branCode);
			if (null != exclusive && exclusive.size() != 0) {
				for (int i = 0; i < exclusive.size(); i++) {
					Object[] st = exclusive.get(i);
					productManageDto pm = new productManageDto();
					pm.setProductId(st[0].toString());
					pm.setRiskCode(st[1].toString());
					pm.setProductCname(st[2].toString());
					if (isShowRate) {
						pm.setRate(getRate(Double.parseDouble(st[3].toString()), userTop, pm.getProductId(), false) * levenRate);
					} else {
						pm.setRate(0.0);
					}
					pm.setRate(getRate(Double.parseDouble(st[3].toString()), userTop, pm.getProductId(), false) * levenRate);
					pm.setRiskName(st[4].toString());
					dto.add(pm);
				}
			}
			Map<String, Object> proMap = new HashMap<String, Object>();
			List<Object[]> spList = storeProductDao.findAndOrderProduct(riskCode, userCode);
			List<TbSpStoreProduct> proList = new ArrayList<TbSpStoreProduct>();
			for (int i = 0; i < spList.size(); i++) {
				Object[] st = spList.get(i);
				TbSpStoreProduct tbp = new TbSpStoreProduct();
				tbp.setId(Integer.parseInt(st[0].toString()));
				tbp.setProductId(Integer.parseInt(st[1].toString()));
				tbp.setSerialNo(Integer.parseInt(st[3].toString()));
				tbp.setRiskSerialNo(Integer.parseInt(st[2].toString()));
				// tbp.setProductName(st[4].toString());
				// tbp.setRiskSerialNo(Integer.parseInt(st[3].toString()));
				// tbp.setSerialNo(Integer.parseInt(st[3].toString()));
				proList.add(tbp);
			}
			proMap = setChannelPro(userCode, userTop, proMap);
			proMap.put("sp", proList);
			proMap.put("pc", dto);
			sr.setResult(proMap);
			return sr;

		} else {
			sr.setFail();
			return sr;
		}

	}

	public Map<String, Object> setChannelPro(String userCode, String userTop, Map<String, Object> proMap) {
		if (userCode == userTop) {
			proMap.put("ch", false);
		} else {
			List<TbSpTopChannelAuthProduct> channelList = topChannelAuthProductDao.findByUserCode(userTop);
			if (CollectionUtils.isNotEmpty(channelList)) {
				proMap.put("ch", channelList);
			} else {
				proMap.put("ch", false);
			}
		}
		return proMap;
	}

	@Override
	public ServiceResult saveStoreProductList(List<TbSpStoreProduct> storeList) {
		ServiceResult sr = new ServiceResult();
		sr.setSuccess();
		try {
			for (int i = 0; i < storeList.size(); i++) {
				if ("0".equals(storeList.get(i).getIsShow())) {
					storeProductDao.deleteById(storeList.get(i).getId());
				} else {
					storeProductDao.save(storeList.get(i));
				}
			}
		} catch (Exception e) {
			sr.setFail();
			sr.setResult("保存失败，请稍后再试");
		}
		return sr;
	}

	@Override
	public ServiceResult shopProductFind(String userCode, String type, Integer pageNumber, Integer pageSize) {
		ServiceResult sr = new ServiceResult();
		StringBuilder sql = new StringBuilder();
		List<Map<String, Object>> proList = null;
		Integer sqlPageNumber = (pageNumber - 1) * pageSize;
		sql.append(
				"select pro.id as id,pro.is_quota as isQuota,pro.Advance_day  as advanceDay,pro.MAX_DEADLINE as maxDeadLine, pro.MIN_DEADLINE as minDeadLine ,pro.PRODUCT_TYPE as productType,pro.ELEMENT as element,pro.RISK_CODE as riskCode,pro.PRODUCT_ENAME as productEname,pro.PRODUCT_CNAME as productCname,pro.IMG_URL as imgUrl,pro.IMG_THUMBNAIL_URL as imgThumbnailUrl,pro.DESCRITION as descrition,pro.MIN_PRICE as minPrice,pro.PRI_PRICE as priPrice,pro.DISCOUNT_PRICE as discountPrice,pro.QUANTITY as quantity,pro.DETAIL as detail,pro.INTRODUCE_URL as introduceUrl,pro.SCHEME_URL as schemeUrl,pro.DETAIL_URL as detailUrl,pro.REMARK as remark,pro.MIN_AGE as minAge,pro.MAX_AGE as maxAge   from tb_sp_storeproduct st,tb_sp_product pro where st.Product_id = pro.ID and  st.USER_CODE =? and st.IS_SHOW='1' and pro.STATUS in ('1','3')");
		if ("all".equals(type)) {
			sql.append("order by st.SERIAL_NO asc LIMIT " + sqlPageNumber.toString() + "," + pageSize.toString());
			proList = readJdbcTemplate.queryForList(sql.toString(), new Object[] { userCode });
		} else {
			sql.append("and pro.RISK_CODE=?  order by st.RISK_SERIAL_NO asc LIMIT " + sqlPageNumber.toString() + "," + pageSize.toString());
			proList = readJdbcTemplate.queryForList(sql.toString(), new Object[] { userCode, type });
		}
		// List<TbSpProduct> productList = new ArrayList<TbSpProduct>();
		// for(int i=0;i<proList.size();i++) {
		// TbSpProduct pro = new TbSpProduct();
		// Map<String,Object> proMap = proList.get(i);
		// pro.setAdvanceDay(proMap.get("Advance_day").toString());
		// pro.setDescrition(proMap.get("DESCRITION").toString());
		// pro.setDetail(proMap.get("DETAIL").toString());
		// pro.setDetailUrl(proMap.get("DETAIL_URL").toString());
		//// pro.setDiscountPrice(Double.parseDouble(proMap.get("DISCOUNT_PRICE").toString()));
		// pro.setElement(proMap.get("ELEMENT").toString());
		//// pro.setEndDate(new Date(proMap.get("ELEMENT").toString()));
		// pro.setImgThumbnailUrl(proMap.get("IMG_THUMBNAIL_URL").toString());
		// pro.setImgUrl(proMap.get("IMG_URL").toString());
		// pro.setIntroduceUrl(proMap.get("INTRODUCE_URL").toString());
		// pro.setIsQuota(proMap.get("is_quota").toString());
		// pro.setMaxAge(proMap.get("MAX_AGE").toString());
		// pro.setMaxDeadLine(proMap.get("MAX_DEADLINE").toString());
		// pro.setMinAge(proMap.get("MIN_AGE").toString());
		// pro.setMinDeadLine(proMap.get("MIN_DEADLINE").toString());
		// pro.setMinPrice(Double.parseDouble(proMap.get("MIN_PRICE").toString()));
		// pro.setProductCname(proMap.get("PRODUCT_CNAME").toString());
		// pro.setProductType(proMap.get("PRODUCT_TYPE").toString());
		// pro.setProductType(proMap.get("PRODUCT_TYPE").toString());
		//
		// }

		// System.out.println(proList.toString());

		sr.setSuccess();
		sr.setResult(proList);
		return sr;
	}

	@Override
	public ServiceResult setStateType(String userCode, String stateType) {
		userDao.updateTypeState(stateType, userCode);
		return null;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public ServiceResult updateAgreementNoByApply(HttpServletRequest request, ServiceResult sr, String userCode, String applyType, Integer areaId,
			String areaCode) throws IOException, ParseException, MessagingException {
		sr.setResult("申请失败");
		if (StringUtils.isBlank(userCode)) {
			sr.setResult("编码无效");
			return sr;
		}
		if (StringUtils.isBlank(applyType)) {
			sr.setResult("请选择申请类型");
			return sr;
		}
		// 所在地区，保险中介机构/代理人/非保险机构，姓名，联系电话。
		String applyName = "";
		switch (applyType) {
		case "1":
			applyName = "保险中介机构";
			break;
		case "2":
			applyName = "代理人";
			break;
		case "3":
			applyName = "非保险机构";
			break;
		default:
			sr.setResult("请选择有效的类型");
			return sr;
		}
		if (new Integer(0).compareTo(areaId) > 0) {
			sr.setResult("请选择常驻地区[ID]");
			return sr;
		}
		if (StringUtils.isBlank(areaCode)) {
			sr.setResult("请选择常驻地区[code]");
			return sr;
		}

		TbSpUser user = userDao.findByUserCode(userCode);
		if (null == user) {
			sr.setResult("用户不存在");
			return sr;
		}
		if (StringUtils.isBlank(user.getUserName())) {
			sr.setResult("您还没有实名");
			return sr;
		}
		if (StringUtils.isBlank(user.getIdNumber())) {
			sr.setResult("您还没有实名认证");
			return sr;
		}

		Optional<TbSysHotarea> ophotarea = hotareaDao.findById(areaId);
		if (null == ophotarea || !ophotarea.isPresent()) {
			sr.setResult("您选择的常驻地区已失效");
			return sr;
		}

		TbSysHotarea hotarea = ophotarea.get();
		if (!areaCode.equals(hotarea.getAreaCode())) {
			sr.setResult("常驻地区编码错误");
			return sr;
		}

		// 更新申请信息
		int updateNo = userDao.updateAgreementNoByApply(applyType, hotarea.getAreaCode(), hotarea.getAreaName(), userCode);
		if (updateNo == 1) {
			user.setApplyAgrNoType(applyType);
			user.setAreaCode(hotarea.getAreaCode());
			user.setAreaName(hotarea.getAreaName());
			user.setBanks(bankDao.findAllByUserCode(user.getUserCode(), user.getUserCodeBs()));
			sr.setResult(user);
			sr.setSuccess();
		}
		// 通知内容
		String contents = String.format(Constants.SMS_BY_APPLY_AGRNO, hotarea.getAreaName(), applyName, user.getUserName(), user.getMobile());
		if (StringUtils.isNotBlank(hotarea.getContactMobile()) && hotarea.getContactMobile().length() == 11) {
			// 短信通知
			smsService.smsToMobile(hotarea.getContactMobile(), contents);
		}
		if (StringUtils.isNotBlank(hotarea.getContactEmail()) && hotarea.getContactEmail().indexOf("@") > 0) {
			// 邮件通知
			String subject = "来自自营平台[申请业务关系代码]";// 标题
			String content = contents;// 内容
			List<String> mails = new ArrayList<String>();
			mails.add(hotarea.getContactEmail());
			emailService.sendEmail(subject, content, mails, null, null, new HashMap<String, byte[]>(), true);
		}
		return sr;
	}

	@Override
	public TQueryLmAgentResponseDto findAgreementNo(String agreementNo) {
		// 获取业务关系代码查询基本信息
		@SuppressWarnings("rawtypes")
		Map sysMap = (Map) redis.get(Constants.SYS_SERVICE_INFO);
		SysServiceInfo sysServiceInfo = (SysServiceInfo) sysMap.get(CoreServiceEnum.FindAgentInfoByAgreementCode_URL.getUrlKey());

		HttpHeaders headers = RequestUtils.genEncryptHttpHeaders(sysServiceInfo.getUserName(), sysServiceInfo.getPassword(),
				MediaType.APPLICATION_JSON);
		// 请求参数对象
		AgreementCodeRequestDto agreementCodeRequestDto = new AgreementCodeRequestDto();
		agreementCodeRequestDto.setAgreementCode(agreementNo);
		agreementCodeRequestDto.setFlowId(sysServiceInfo.getUserName() + UUID.getUUIDString());
		agreementCodeRequestDto.setOperatorDate(String.valueOf(new Date().getTime()));
		agreementCodeRequestDto.setPartnerAccountCode(sysServiceInfo.getUserName());
		// 请求体
		HttpEntity<AgreementCodeRequestDto> requestEntity = new HttpEntity<AgreementCodeRequestDto>(agreementCodeRequestDto, headers);
		// 请求接口
		ResponseEntity<TQueryLmAgentResponseDto> responseEntity = restTemplate.exchange(sysServiceInfo.getUrl(), HttpMethod.POST, requestEntity,
				TQueryLmAgentResponseDto.class);

		TbSpSaleLog saleLog = new TbSpSaleLog();
		saleLog.setOperation(TbSpSaleLog.UP_SHOP_AGRNO);
		saleLog.setMark("三方接口查询业务关系代码");
		saleLog.setUserCode(Current.userCode.get());
		saleLog.setRequestTime(new Date());
		saleLog.setRequestData("requestEntity: " + JSON.toJSONString(requestEntity));
		saleLog.setResponseData("responseEntity: " + JSON.toJSONString(responseEntity));
		spSaleLogDao.save(saleLog);

		return responseEntity.getBody();
	}

	@Override
	public ServiceResult findEYongjCountByUserCode(ServiceResult sr, String userCode) throws Exception {
		int num = storeConfigDao.findEYongjCountByUserCode(userCode);
		// 有设置专属佣金
		if (num > 0) {
			sr.setResult(num);
			sr.setSuccess();
		}
		return sr;
	}

	@Override
	public ServiceResult findByAgreementNo(HttpServletRequest request, ServiceResult sr, String agreementNo) throws Exception {
		sr.setResult("未配置业务关系代码");
		if (StringUtils.isBlank(agreementNo)) {
			sr.setResult("业务关系代码为空");
		}

		if (agreementNo.trim().length() < 5 || agreementNo.trim().length() > 20) {
			sr.setResult("关系代码长度为5-20位");
			return sr;
		}

		TQueryLmAgentResponseDto responseDto = findAgreementNo(agreementNo);
		// 业务关系代码列表为空，默认请使用第一条数据
		List<PrpLmAgent> prpLmAgentList = responseDto.getPrpLmAgentList();
		if (CollectionUtils.isNotEmpty(prpLmAgentList)) {
			sr.setSuccess();
			// 业务关系代码
			PrpLmAgent prpLmAgent = prpLmAgentList.get(0);
			// 零时存储，用于校验
			redis.setWithExpireTime(Constants.AGREEMEN_TINFO + request.getSession().getId(), prpLmAgent, Constants.SINGLE_REQUEST_TIMEOUT_SECONDS);

			// 业务关系代码所属机构代码,此处为8位,需截取4位
			if (StringUtils.isNotBlank(prpLmAgent.getBranch()) && prpLmAgent.getBranch().length() > 4) {
				prpLmAgent.setBranch(prpLmAgent.getBranch().substring(0, 4));
			}
			// 获取全部机构代码列表,判断该归属机构是否为四川和广东
			@SuppressWarnings("unchecked")
			Map<String, List<SysCodeNode>> map = (HashMap<String, List<SysCodeNode>>) redis.get(Constants.lIMIT_LIST_CODE);
			List<SysCodeNode> codeList = map.get("BranchCode");
			if (CollectionUtils.isNotEmpty(codeList)) {
				for (SysCodeNode code : codeList) {
					if (this.isSpecialArea(prpLmAgent, code)) {
						// 暂时只有四川和广东需要选择销售人员
						responseDto.setSelectSalePerson(true);
						break;
					}
				}
			}
			// 业务关系代码类型
			// 0=内部员工,1=个贷,2=专业代理,3=兼业代理,4=经纪业务,5=电销,6=网销,i=邮政代理,k=银保代理
			// 内部员工(0)
			// 直销(5,6)
			// 个人代理(1)和机构代理(2,3,4,i,k)
			sr.setResult(responseDto);
		}
		return sr;
	}

	/**Remarks: 是否为特殊地区<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月18日下午4:32:35<br>
	 * Project：liberty_sale_plat<br>
	 * @param responseDto
	 * @param prpLmAgent
	 * @param code
	 * @return
	 */
	private boolean isSpecialArea(PrpLmAgent prpLmAgent, SysCodeNode code) {
		String sysCode = code.getCode();// 机构编码
		String branch = prpLmAgent.getBranch();// 机构编码

		if (StringUtils.isNotBlank(sysCode) && StringUtils.isNotBlank(branch) && sysCode.length() >= 2 && branch.length() >= 2) {
			String c = sysCode.substring(0, 2);// 机构编码前2位
			String b = branch.substring(0, 2);// 机构编码前2位
			String cname = code.getCodeCname();// 地区名

			if (c.equals(b) && ("44".equals(b) || "51".equals(b) || cname.contains("广东") || cname.contains("四川"))) {
				return true;
			}
		}
		return false;
	}

	@Override
	public ServiceResult findAllHotArea() {
		ServiceResult sr = new ServiceResult();
		// sr.setResult(hotareaDao.findAllArea());
		sr.setResult(sysBranchDao.findAllBranch());
		return sr;
	}

	@Transactional(propagation = Propagation.REQUIRED) // 事务
	@Modifying(clearAutomatically = true) // @Modifying注解需要使用clearAutomatically=true，同一接口更新后立即查询获得更新后的数据,默认false查询还是更新前的数据
	@Override
	public ServiceResult updateStoreStateByArea(HttpServletRequest request, ServiceResult sr, String userCode, String agreementNo, String areaCode,
			String saleCode, String saleName) throws Exception {
		sr.setAppFail("操作失败");
		if (StringUtils.isBlank(userCode)) {
			sr.setResult("编码无效");
			return sr;
		}
		TbSpUser user = userDao.findByUserCode(userCode);
		if (null == user) {
			sr.setResult("用户不存在");
			return sr;
		}

		TbSysHotarea hotarea = hotareaDao.findByAgreementNoAndAreaCode(agreementNo, areaCode);
		if (null == hotarea) {
			sr.setResult("当前常驻地区不存在");
			return sr;
		}

		// 校验业务关系代码，并设置相关信息到账户
		sr = this.checkAgreementNo(request, sr, Constants.AGREEMENT_NO_SELECTAREA, null, user, agreementNo, saleCode, saleName, hotarea);
		if (sr.getState() == ServiceResult.STATE_SUCCESS) {
			user = (TbSpUser) sr.getResult();
		} else {
			return sr;// 验证失败
		}

		// 存储分享信息
		saveShare(user.getUserCode(), "店铺-自动生成", ":1");

		// 非开店状态的设置状态和时间
		if (!"1".equals(user.getStoreFlag())) {
			user.setStoreFlag("1");
			user.setStoreCreateTime(new Timestamp(new Date().getTime()));
		}

		// 有设置专属佣金配置的，清空配置
		if (storeConfigDao.findEYongjCountByUserCode(userCode) > 0) {
			sr = this.deleteStoreConfigByUserCode(sr, user.getUserCode());
		}
		// 更新用户信息
		userDao.save(user);

		user.setBanks(bankDao.findAllByUserCode(user.getUserCode(), user.getUserCodeBs()));
		sr.setResult(user);
		sr.setSuccess();

		TbSpSaleLog saleLog = new TbSpSaleLog();
		saleLog.setOperation(TbSpSaleLog.UP_SHOP_AREA);
		saleLog.setUserCode(userCode);
		saleLog.setMark("常驻地区开店");
		saleLog.setRequestTime(new Date());
		saleLog.setRequestData("userCode: " + userCode + "agreementNo: " + agreementNo + "areaCode: " + areaCode + "saleCode: " + saleCode
				+ "saleName: " + saleName);
		saleLog.setResponseData(JSON.toJSONString(user));
		spSaleLogDao.save(saleLog);

		return sr;
	}

	@Transactional(propagation = Propagation.REQUIRED) // 事务
	@Modifying(clearAutomatically = true) // @Modifying注解需要使用clearAutomatically=true，同一接口更新后立即查询获得更新后的数据,默认false查询还是更新前的数据
	@Override
	public ServiceResult updateStoreStateByAgreement(HttpServletRequest request, ServiceResult sr, String userCode, String agreementNo,
			String saleCode, String saleName) throws Exception {
		sr.setAppFail("操作失败");
		if (StringUtils.isBlank(userCode)) {
			sr.setAppFail("编码无效");
			return sr;
		}
		TbSpUser user = userDao.findByUserCode(userCode);
		if (null == user) {
			sr.setAppFail("用户不存在");
			return sr;
		}

		// 校验输入的关系代码信息是否被篡改
		Object obj = redis.get(Constants.AGREEMEN_TINFO + request.getSession().getId());
		if (null == obj) {
			sr.setAppFail("请重新输入业务关系代码");
			return sr;
		}
		// 获取缓存中的业务关系代码对象
		PrpLmAgent prpLmAgent = (PrpLmAgent) obj;

		// 校验业务关系代码，并设置相关信息到账户
		sr = this.checkAgreementNo(request, sr, Constants.AGREEMENT_NO_INPUT, prpLmAgent, user, agreementNo, saleCode, saleName, null);
		if (sr.getState() == ServiceResult.STATE_SUCCESS) {
			user = (TbSpUser) sr.getResult();
		} else {
			return sr;// 验证失败
		}

		// 存储分享信息
		saveShare(user.getUserCode(), "店铺-自动生成", ":1");

		// 非开店状态的设置状态和时间
		if (!"1".equals(user.getStoreFlag())) {
			user.setStoreFlag("1");
			user.setStoreCreateTime(new Timestamp(new Date().getTime()));
		}

		// 有设置专属佣金配置的，清空配置
		if (storeConfigDao.findEYongjCountByUserCode(userCode) > 0) {
			sr = this.deleteStoreConfigByUserCode(sr, user.getUserCode());
		}

		userDao.save(user);
		user.setBanks(bankDao.findAllByUserCode(user.getUserCode(), user.getUserCodeBs()));

		sr.setResult(user);
		sr.setSuccess();

		TbSpSaleLog saleLog = new TbSpSaleLog();
		saleLog.setOperation(TbSpSaleLog.UP_SHOP_AGRNO);
		saleLog.setUserCode(userCode);
		saleLog.setMark("店铺信息设置——业务关系代码");
		saleLog.setRequestTime(new Date());
		saleLog.setRequestData("userCode: " + userCode + "agreementNo: " + agreementNo + "saleCode: " + saleCode + "saleName: " + saleName);
		saleLog.setResponseData(JSON.toJSONString(user));
		spSaleLogDao.save(saleLog);

		return sr;
	}

	public ServiceResult checkAgreementNo(HttpServletRequest request, ServiceResult sr, String type, PrpLmAgent prpLmAgent, TbSpUser user,
			String agreementNo, String saleCode, String saleName, TbSysHotarea hotarea) throws Exception {
		if (StringUtils.isBlank(agreementNo)) {
			sr.setAppFail("业务关系代码不能为空");
			return sr;
		}
		if ("CHQ003514".equals(agreementNo)) {
			sr.setAppFail("业务关系代码异常，请联系销售人员");
			return sr;
		}
		switch (type) {
		case Constants.AGREEMENT_NO_INPUT:

			log.info("店铺信息设置——录入业务关系代码");
			String redisBranch = prpLmAgent.getBranch();// 业务关系代码所属机构编码
			String redisAgrNo = prpLmAgent.getAgreementNo();// 业务关系代码
			String redisAgrName = prpLmAgent.getAgentName();// 代理名称
			String orgCode = prpLmAgent.getAlternate6();// 组织机构代码
			String licenceNo = prpLmAgent.getAlternate7();// 营业执照号
			String customType = prpLmAgent.getAlternate8();// 关系代码客户类型
			String customName = prpLmAgent.getAlternate9();// 关系代码客户名称
			String redisChannel = prpLmAgent.getChannel();// 销售渠道编码
			String redisChannelName = prpLmAgent.getChannelName();// 销售渠道名称
			String accType = prpLmAgent.getAccType();// 佣金类型
			String codeType = prpLmAgent.getAlternate1();// 业务关系代码类型

			// 校验
			if (!redisAgrNo.equals(agreementNo)) {
				sr.setAppFail("请正确提交上一步信息");
				return sr;
			}

			// 客户单位类型
			customType = StringUtils.isNotBlank(customType) ? customType.toUpperCase() : customType;
			if (!Constants.AGREEMENT_NO_CUSTOMER_TYPE.contains(customType)) {
				sr.setAppFail("根据要求，微门店业务关系代码对应的客户单位类型必须设置为" + Constants.AGREEMENT_NO_CUSTOMER_TYPE + "，如有疑问请联系机构销管或电子商务部");
				return sr;
			}

			// 业务关系代码所属机构编码
			if (StringUtils.isNotBlank(redisBranch) && redisBranch.length() >= 4) {
				redisBranch = redisBranch.substring(0, 4);
			} else {
				sr.setAppFail("请配置业务关系代码所属机构");
				return sr;
			}

			if (StringUtils.isNotBlank(saleName) && saleName.length() < 2) {
				sr.setAppFail("销售人员名称过短");
				return sr;
			}
			if (StringUtils.isNotBlank(saleCode) && saleCode.length() < 3) {
				sr.setAppFail("销售人员编码过短");
				return sr;
			}
			// 有效的销售人员信息
			if (StringUtils.isNotBlank(saleName) && StringUtils.isNotBlank(saleCode)) {
				// 校验选择的销售人员信息是否被篡改
				Object obj = redis.get(Constants.SALE_PERSON_TINFO + request.getSession().getId());
				if (null == obj) {
					sr.setAppFail("请重新选择销售人员");
					return sr;
				}
				@SuppressWarnings("unchecked")
				HashMap<String, String> map = (HashMap<String, String>) obj;
				String sName = map.get(saleCode);// 根据销售编码获取销售名称进行校验
				// 校验
				if (!saleName.equals(sName)) {
					sr.setAppFail("销售人员代码和名称不匹配");
					return sr;
				}
				redis.deletelike(Constants.SALE_PERSON_TINFO);// 清除当前操作使用的内容

				// 设置销售人员信息
				user.setSaleCode(saleCode);// 销售人员编码
				user.setSaleName(saleName);// 销售人员名称
			}

			if (StringUtils.isBlank(accType) || StringUtils.isBlank(codeType)) {
				sr.setAppFail("业务关系代码配置不完整");
				return sr;
			}

			redis.deletelike(Constants.AGREEMEN_TINFO);// 清除当前操作使用的内容

			// 用户类型
			String userType = "2";// 默认普通账户 // 已废弃：this.getUserType(orgCode, licenceNo, accType, codeType);

			// 有代理人代码和名称的算中介协议，余下的算CA协议
			if (StringUtils.isNotBlank(redisAgrNo) && StringUtils.isNotBlank(redisAgrName)) {
				userType = "3";// 专业代理
			} else {
				// CA协议
				if (StringUtils.isNotBlank(orgCode) || StringUtils.isNotBlank(licenceNo)) {
					userType = "4";// 商户
				} else {
					userType = "5";// 销售人员
				}
			}
			// 设置用户类型
			user.setUserType(userType);

			user.setBranchCode(redisBranch);// 业务关系代码所属机构编码
			user.setAgrementNo(redisAgrNo);// 关系代码
			user.setAgrementName(redisAgrName);// 代理名称
			user.setChannelType(customType);// 客户单位类型
			user.setCustomName(customName);// 客户单位名称
			user.setChannelCode(redisChannel);// 销售渠道编码
			user.setChannelName(redisChannelName);// 销售渠道名称

			user.setAreaCode(null);// 置空常住地编码
			user.setAreaName(null);// 置空常住地名称
			break;
		case Constants.AGREEMENT_NO_SELECTAREA:

			log.info("店铺信息设置——常驻地区");

			// if ((StringUtils.isNotBlank(saleCode) &&
			// !hotarea.getSaleCode().equals(saleCode))
			// || (StringUtils.isNotBlank(saleName) &&
			// !hotarea.getSaleName().equals(saleName))) {
			// sr.setResult("请不要修改销售人员信息");
			// return sr;
			// }

			// 开店更新数据，
			// 用户类型:1游客,2普通客户,3专业代理,4商户,5销售人员
			// 是否店主 0：否 ; 1：是
			user.setUserType("2");
			user.setBranchCode(hotarea.getBranchCode());
			user.setAgrementNo(null);
			user.setAgrementName(null);
			user.setSaleCode(null);
			user.setSaleName(null);
			user.setChannelCode(null);
			user.setChannelName(null);
			user.setAreaCode(hotarea.getAreaCode());// 常驻地区编码
			user.setAreaName(hotarea.getAreaName());// 常驻地区名称
			user.setChannelType(null);
			user.setCustomName(null);
			break;

		default:
			break;
		}
		sr.setSuccess();
		sr.setResult(user);
		return sr;
	}

	@Transactional(propagation = Propagation.REQUIRED) // 事务
	@Override
	public ServiceResult deleteStoreConfigByUserCode(ServiceResult sr, String userCode) throws Exception {
		sr.setAppFail("清空专属佣金配置失败");

		List<TbSpStoreConfig> list = storeConfigDao.findByUserCode(userCode);
		storeConfigDao.deleteAll(list);

		sr.setSuccess();
		sr.setResult(list);
		return sr;
	}

	/**Remarks: 根据业务关系代码类型和业务关系代码佣金类型确认协议并分类用户<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月28日上午11:11:45<br>
	 * Project：liberty_sale_plat<br>
	 * @param orgCode
	 * @param licenceNo
	 * @param accType
	 * @param codeType
	 * @return
	 */
	private String getUserType(String orgCode, String licenceNo, String accType, String codeType) {
		/*
		 * 业务关系代码类型(codeType) 0=内部员工,1=个贷,2=专业代理,3=兼业代理,4=经纪业务,5=电销,6=网销,i=邮政代理,k=银保代理
		 * 
		 * ------------------------------------- 业务关系代码佣金类型(accType) 1=代理人,2=提佣,3=绩效
		 * 
		 * ------------------------------------- CA协议 accType=2&&codeType=0
		 * ------------------------------------- 中介协议 accType=1&&codeType=(1,2,3,4,i,k)
		 * 
		 * ------------------------------------- 是否店主 0：否 ; 1：是
		 * ------------------------------------- 用户类型:
		 * ------------------------------------- 1游客：
		 * ------------------------------------- 2普通客户：CA协议-->无业务关系代码
		 * ------------------------------------- 3专业代理：中介协议-->个贷+机构代理
		 * ------------------------------------- 4商户：CA协议-->组织机构代码或营业执照号任意不为空。
		 * ------------------------------------- 5销售人员：CA协议-->非商户
		 */
		String userType = "2";// 默认为普通客户
		if ("1".equals(accType)) {
			switch (codeType) {
			case "1":// 个贷
			case "2":// 机构代理
			case "3":// 机构代理
			case "4":// 机构代理
			case "i":// 机构代理
			case "k":// 机构代理
				// 中介协议
				userType = "3";// 专业代理
				break;
			default:
				// 中介协议其他
				break;
			}
		}
		// CA协议
		if ("2".equals(accType) && "0".equals(codeType)) {
			if (StringUtils.isNotBlank(orgCode) || StringUtils.isNotBlank(licenceNo)) {
				userType = "4";// 商户
			} else {
				userType = "5";// 销售人员
			}
		}
		return userType;
	}

	@Override
	public ServiceResult findShopInfo(String userCode, HttpServletRequest request, String type) {
		ServiceResult sr = new ServiceResult("查询异常", ServiceResult.STATE_APP_EXCEPTION);

		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("proxy-client-ip");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("wl-proxy-client-ip");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		// 如果用户分享ID不存在，则创建分享ID，新版用户注册时已调用该方法
		saveShare(userCode, "店铺-自动生成", ":1");

		Map<String, Object> userMap = new HashMap<String, Object>();
		TbSpUser shareUser = userDao.findByUserCodeBase(userCode);
		if (shareUser != null) {
			// 设置店铺名字
			if (StringUtils.isNotBlank(shareUser.getNickName()) && StringUtils.isBlank(shareUser.getShopName())) {
				shareUser.setShopName(shareUser.getNickName());
				userDao.save(shareUser);
			} else if (!StringUtils.isBlank(shareUser.getUserName()) && StringUtils.isBlank(shareUser.getShopName())) {
				shareUser.setShopName(shareUser.getUserName());
				userDao.save(shareUser);
			}
			if (shareUser.getPopularity() == null) {
				shareUser.setPopularity(0);
				userDao.save(shareUser);
			}
			String userCodeBs = "henghua";
			if (StringUtils.isNotBlank(shareUser.getUserCodeBs())) {
				userCodeBs = shareUser.getUserCodeBs();
			}
			userMap.put("volume", orderDao.findUserCount(userCode, userCodeBs));
			userMap.put("popularity", shareUser.getPopularity());
			userMap.put("shopState", shareUser.getTypeState());
			userMap.put("shopName", shareUser.getShopName());
			userMap.put("shopIntroduct", shareUser.getShopIntroduct());
			userMap.put("header", shareUser.getHeadUrl());
			userMap.put("state", shareUser.getState());
			userMap.put("userType", shareUser.getUserType());
			userMap.put("registerType", shareUser.getRegisterType());
			userMap.put("nickName", shareUser.getNickName());

			// resUser.setVolume(orderDao.findUserCount(tbSp.getUserCode()));
			// 人气+1 （type=acc表示非自己点击的查询）
			if ("acc".equals(type)) {
				Object ipIsAcc = redis.get(userCode + ip);
				if (ipIsAcc == null) {
					redis.setWithExpireTime(userCode + ip, true, 3600 * 24);
					Integer popularity = shareUser.getPopularity();
					popularity++;
					shareUser.setPopularity(popularity);
					userDao.save(shareUser);
				}
			}
			sr.setSuccess();
			sr.setResult(userMap);
		} else {
			sr.setAppFail("userCode=" + userCode + "的账户不存在");
		}
		return sr;
	}

	@Override
	public void saveShare(String userCode, String type, String sub) {
		Md5PwdEncoder md5PwdEncoder = new Md5PwdEncoder();
		// 加密密码
		String shareId = md5PwdEncoder.encodePassword(userCode + sub);
		List<TbSpShare> tsList = shareDao.findByShareId(shareId);
		if (CollectionUtils.isEmpty(tsList)) {
			TbSpShare addTbSp = new TbSpShare();
			addTbSp.setShareId(shareId);
			addTbSp.setChannelName("");
			addTbSp.setSaleCode("");
			addTbSp.setSaleName("");
			addTbSp.setLastShareType("");
			addTbSp.setState(TbSpShare.SHARE_TYPE_NO_VISIT);
			addTbSp.setUserCodeChain("");
			addTbSp.setUserCode(userCode);

			addTbSp.setProductId(type);
			addTbSp.setShareType(type);

			addTbSp.setSharinChain(0);
			addTbSp.setCreateTime(new Date());
			shareDao.save(addTbSp);
		}
	}

	@Override
	public ServiceResult shopRate(String userCode, String branchCode) {
		ServiceResult sr = new ServiceResult();
		if (StringUtils.isBlank(branchCode)) {
			sr.setResult("地区编码不能为空");
			return sr;
		}
		List<TbSpProductConfig> tbProList = productConfigDao.findByBranchCodeAndStatus(branchCode, "1");

		if (StringUtils.isBlank(userCode)) {
			sr.setResult("用户不能为空！");
			return sr;
		}
		List<TbSpStoreConfig> list = storeConfigDao.findByUserCode(userCode);
		Map<String, Object> map = new HashMap<String, Object>();
		if (CollectionUtils.isNotEmpty(list)) {
			for (TbSpStoreConfig storeConfig : list) {
				map.put(storeConfig.getProductId().toString(), storeConfig.getRate());
			}
		}
		List<TbSpScoreConfig> scoreconfig = null;
		scoreconfig = (List<TbSpScoreConfig>) redis.get(Constants.SCORE_CONFIG_INFO);
		if (CollectionUtils.isEmpty(scoreconfig)) {
			scoreconfig = scoreConfigDao.findAllConfig();
			redisUtils.set(Constants.SCORE_CONFIG_INFO, scoreconfig);
		}
		map.put("feeRate", scoreconfig.get(0).getFeeRate());
		map.put("tbProList", tbProList);
		sr.setResult(map);
		return sr;

	}
}
