package cn.com.libertymutual.sp.dto.car.dto;

import java.io.Serializable;

public class TPrptCarOwnerDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8837022798898386954L;
	private String name;
	private String natureOfRole;
	private String idType;
	private String gental;
	private String brithday;
	private String idNo;
	private String cellPhoneNo;

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	public String getCellPhoneNo() {
		return cellPhoneNo;
	}

	public void setCellPhoneNo(String cellPhoneNo) {
		this.cellPhoneNo = cellPhoneNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNatureOfRole() {
		return natureOfRole;
	}

	public void setNatureOfRole(String natureOfRole) {
		this.natureOfRole = natureOfRole;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getGental() {
		return gental;
	}

	public void setGental(String gental) {
		this.gental = gental;
	}

	public String getBrithday() {
		return brithday;
	}

	public void setBrithday(String brithday) {
		this.brithday = brithday;
	}

}
