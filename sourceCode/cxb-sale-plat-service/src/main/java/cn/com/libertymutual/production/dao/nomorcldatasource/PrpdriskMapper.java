package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdrisk;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskKey;
@Mapper
public interface PrpdriskMapper {
    int countByExample(PrpdriskExample example);

    int deleteByExample(PrpdriskExample example);

    int deleteByPrimaryKey(PrpdriskKey key);

    int insert(Prpdrisk record);

    int insertSelective(Prpdrisk record);

    List<Prpdrisk> selectByExample(PrpdriskExample example);

    Prpdrisk selectByPrimaryKey(PrpdriskKey key);

    int updateByExampleSelective(@Param("record") Prpdrisk record, @Param("example") PrpdriskExample example);

    int updateByExample(@Param("record") Prpdrisk record, @Param("example") PrpdriskExample example);

    int updateByPrimaryKeySelective(Prpdrisk record);

    int updateByPrimaryKey(Prpdrisk record);
}