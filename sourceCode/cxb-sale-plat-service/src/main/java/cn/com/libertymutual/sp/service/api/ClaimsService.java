package cn.com.libertymutual.sp.service.api;



import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import cn.com.libertymutual.core.base.dto.ResponseBaseDto;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.dto.ClaimCaseResponseDto;
import cn.com.libertymutual.sp.dto.QueryCommonResponseDto;
import cn.com.libertymutual.sp.dto.TQueryPolicyRequstDto;
import cn.com.libertymutual.sp.dto.TQueryPolicyResponseDto;
import cn.com.libertymutual.sp.dto.TReportClaimRequestDto;

public interface ClaimsService {


	ClaimCaseResponseDto queryClaims(TQueryPolicyRequstDto  reqeust,HttpServletRequest request);

	ResponseBaseDto reportClaims(TReportClaimRequestDto claimReques, MultipartFile[] imgFiles);

	TQueryPolicyResponseDto queryPolicys(TQueryPolicyRequstDto request);

	File uploadPhoto(String reportNo, MultipartFile[] imgFiles);
	
	ServiceResult queryReports(HttpServletRequest request,
			HttpServletResponse response, ServiceResult sr, TQueryPolicyRequstDto policyRequest);

	QueryCommonResponseDto queryPolicyStatus(TQueryPolicyRequstDto requstDto);

	TQueryPolicyResponseDto queryProposal(TQueryPolicyRequstDto requstDto);

	QueryCommonResponseDto queryProposalStatus(TQueryPolicyRequstDto requstDto);

}
