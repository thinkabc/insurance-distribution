package cn.com.libertymutual.sp.dto.axtx;

/**保单信息*/
public class GuaranteeInfo {

	/**车主姓名*/
	private String name;
	/**身份证号*/
	private String idNum;
	/**手机号*/
	private String phoneNum;
	/**电子邮箱*/
	private String mailAddress;
	/**车牌号*/
	private String carLicenseNum;
	/**车架号后三位*/
	private String carFrameNum;
	/**座位数*/
	private String seatNum;
	/**起保时间*/
	private String stringDate;
	/**是否没有车牌号*/
	private boolean noLicenseNum;
	private String planCode;
	private Double sumPremium;
	private String saleCode;
	private String salerName; // 销售人员名称 20171113 bob.kuang
	private String agreementCode;
	private String riskCode;
	private String endTime;
	private String channelName;
	private String sex;
	private String birth;
	private String identifyType;

	public String getIdentifyType() {
		return identifyType;
	}

	public void setIdentifyType(String identifyType) {
		this.identifyType = identifyType;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getSaleCode() {
		return saleCode;
	}

	public void setSaleCode(String saleCode) {
		this.saleCode = saleCode;
	}

	public String getSalerName() {
		return salerName;
	}

	public void setSalerName(String salerName) {
		this.salerName = salerName;
	}

	public String getAgreementCode() {
		return agreementCode;
	}

	public void setAgreementCode(String agreementCode) {
		this.agreementCode = agreementCode;
	}

	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public Double getSumPremium() {
		return sumPremium;
	}

	public void setSumPremium(Double sumPremium) {
		this.sumPremium = sumPremium;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdNum() {
		return idNum;
	}

	public void setIdNum(String idNum) {
		this.idNum = idNum;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public String getCarLicenseNum() {
		return carLicenseNum;
	}

	public void setCarLicenseNum(String carLicenseNum) {
		this.carLicenseNum = carLicenseNum;
	}

	public String getCarFrameNum() {
		return carFrameNum;
	}

	public void setCarFrameNum(String carFrameNum) {
		this.carFrameNum = carFrameNum;
	}

	public String getSeatNum() {
		return seatNum;
	}

	public void setSeatNum(String seatNum) {
		this.seatNum = seatNum;
	}

	public String getStringDate() {
		return stringDate;
	}

	public void setStringDate(String stringDate) {
		this.stringDate = stringDate;
	}

	public boolean isNoLicenseNum() {
		return noLicenseNum;
	}

	public void setNoLicenseNum(boolean noLicenseNum) {
		this.noLicenseNum = noLicenseNum;
	}

	@Override
	public String toString() {
		return "GuaranteeInfo [name=" + name + ", idNum=" + idNum + ", phoneNum=" + phoneNum + ", mailAddress=" + mailAddress + ", carLicenseNum="
				+ carLicenseNum + ", carFrameNum=" + carFrameNum + ", seatNum=" + seatNum + ", stringDate=" + stringDate + ", noLicenseNum="
				+ noLicenseNum + ", planCode=" + planCode + ", sumPremium=" + sumPremium + ", saleCode=" + saleCode + ", salerName=" + salerName
				+ ", agreementCode=" + agreementCode + ", riskCode=" + riskCode + "]";
	}

}
