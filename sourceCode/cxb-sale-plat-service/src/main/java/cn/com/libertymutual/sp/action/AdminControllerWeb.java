package cn.com.libertymutual.sp.action;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.AdminService;
import cn.com.libertymutual.sys.bean.SysUser;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/admin/admin")
public class AdminControllerWeb {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private AdminService adminService;

	/*
	 * 后台查询所有的管理员
	 */

	@ApiOperation(value = "后台查询所有的管理员", notes = "后台查询所有的管理员")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "userid", value = "用户ID", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "username", value = "用户名", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "roleid", value = "角色id", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "分页", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "分页", required = true, paramType = "query", dataType = "Long"), })
	@PostMapping(value = "/adminList")
	public ServiceResult adminList(String userid, String username,String roleid, int pageNumber, int pageSize) {

		if (StringUtils.isNotBlank(roleid)) {
			log.info("------roleid-----:{}",roleid);
			return adminService.adminList(userid, username,Integer.parseInt(roleid),pageNumber, pageSize);
		}else {
			return adminService.adminList(userid, username,null,pageNumber, pageSize);
		}
	}

	/*
	 * 添加用户
	 */

	@ApiOperation(value = "添加管理员", notes = "添加管理员")
	@PostMapping(value = "/addAdmin")
	public ServiceResult addAdmin(
			@RequestBody @ApiParam(name = "sysUser", value = "sysUser", required = true) SysUser sysUser) {

			return adminService.addAdmin(sysUser);
	}

	/*
	 * 删除用户
	 */

	@ApiOperation(value = "删除用户", notes = "删除用户")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "userid", value = "用户ID", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "type", value = "删除或者恢复", required = false, paramType = "query", dataType = "String"), })
	@PostMapping(value = "/updateAdminStauts")
	public ServiceResult updateAdminStauts(String userid, String type) {

		return adminService.updateAdminStauts(userid, type);
	}

	/*
	 * 获取Ldap用户信息
	 */
	@ApiOperation(value = "获取Ldap用户信息", notes = "获取Ldap用户信息")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "userid", value = "用户名", required = false, paramType = "query", dataType = "String"), })
	@PostMapping(value = "/LdapUser")
	public ServiceResult getUserInfoFromLdap(String userId) {

		return adminService.getUserInfoFromLdap(userId);
	}

	/*
	 * 配置用户权限
	 */

	@ApiOperation(value = "配置用户角色", notes = "配置用户角色")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "userid", value = "用户名", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "roleId", value = "角色名", required = false, paramType = "query", dataType = "Long"), })
	@PostMapping(value = "/setRoleToUser")
	public ServiceResult setRoleToUser(String userId, Integer roleId) {

		return adminService.setRoleToUser(userId, roleId);
	}

	@ApiOperation(value = "用户原有角色", notes = "用户原有角色")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "userId", value = "用户名", required = false, paramType = "query", dataType = "String"), })
	@PostMapping(value = "/getRoleOfUser")
	public ServiceResult getRoleOfUser(String userId) {

		return adminService.getRoleOfUser(userId);
	}

}
