package cn.com.libertymutual.sp.dto.savePlan.base;

/**
 * 销售平台条款基础Dto
 */
public class ItemKindBaseDto {


	
	// 险别名称代码
	private String kindCode;

	// 备案号代码
	private String efileCode;

	// 保险责任代码
	private String itemCode;

	// 职业代码
	private String modelName;

	// 险别名称
	private String kindName;

	// 备案名称
	private String efileName;

	// 保险责任
	private String itemName;

	// 职业
	private String model;

	// 职业类型
	private String occupationFlag;

	// 职业序号
	private String modelNo;

	// 币别
	private String currency;

	// 每人保额
	private String unitAmount;

	// 人数
	private String quantity;

	// 总保额
	private String amount;

	// 标准保费
	private String benchMarkPremium;

	// 折扣率
	private String discount;

	// 最终费率
	private String finalRate;

	// 险别版本
	private String kindVersion;

	// 计划代码
	private String planCode;

	// 实收保费
	private String premium;

	// 费率
	private String rate;

	// 序号
	private String itemNo;

	// 基础保费
	private String basePremium;

	// 险别序号
	private String itemKindNo;
	// 家庭人员序号
	private String familyNo;
	// 家庭人员名称
	private String familyName;
	// 免赔率
	private String deductibleRate;
	// 免赔额
	private String deductible;
	// 免赔期
	private String estimatedPremium;
	// 免赔备注
	private String deductibleRemark;
	
	private String shortRateFlag;
	private String deductibleModel;
	private String limitType;
	private String startNo;
	private String endNo;
	private String shortRate;
	private String costStart;
	private String costEnd;
	private String itemDetailName;
	private String value;
	private String adjustRate;
	// 限额类型
	private String modeName;
	private String calculateFlag;

	private String attribute4;
	public String getAttribute4() {
		return attribute4;
	}

	public void setAttribute4(String attribute4) {
		this.attribute4 = attribute4;
	}

	public String getKindCode() {
		return kindCode;
	}

	public void setKindCode(String kindCode) {
		this.kindCode = kindCode;
	}

	public String getEfileCode() {
		return efileCode;
	}

	public void setEfileCode(String efileCode) {
		this.efileCode = efileCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getKindName() {
		return kindName;
	}

	public void setKindName(String kindName) {
		this.kindName = kindName;
	}

	public String getEfileName() {
		return efileName;
	}

	public void setEfileName(String efileName) {
		this.efileName = efileName;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getOccupationFlag() {
		return occupationFlag;
	}

	public void setOccupationFlag(String occupationFlag) {
		this.occupationFlag = occupationFlag;
	}

	public String getModelNo() {
		return modelNo;
	}

	public void setModelNo(String modelNo) {
		this.modelNo = modelNo;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getUnitAmount() {
		return unitAmount;
	}

	public void setUnitAmount(String unitAmount) {
		this.unitAmount = unitAmount;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBenchMarkPremium() {
		return benchMarkPremium;
	}

	public void setBenchMarkPremium(String benchMarkPremium) {
		this.benchMarkPremium = benchMarkPremium;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getFinalRate() {
		return finalRate;
	}

	public void setFinalRate(String finalRate) {
		this.finalRate = finalRate;
	}

	public String getKindVersion() {
		return kindVersion;
	}

	public void setKindVersion(String kindVersion) {
		this.kindVersion = kindVersion;
	}

	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public String getBasePremium() {
		return basePremium;
	}

	public void setBasePremium(String basePremium) {
		this.basePremium = basePremium;
	}

	public String getItemKindNo() {
		return itemKindNo;
	}

	public void setItemKindNo(String itemKindNo) {
		this.itemKindNo = itemKindNo;
	}

	public String getFamilyNo() {
		return familyNo;
	}

	public void setFamilyNo(String familyNo) {
		this.familyNo = familyNo;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getDeductibleRate() {
		return deductibleRate;
	}

	public void setDeductibleRate(String deductibleRate) {
		this.deductibleRate = deductibleRate;
	}

	public String getDeductible() {
		return deductible;
	}

	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}

	public String getEstimatedPremium() {
		return estimatedPremium;
	}

	public void setEstimatedPremium(String estimatedPremium) {
		this.estimatedPremium = estimatedPremium;
	}

	public String getDeductibleRemark() {
		return deductibleRemark;
	}

	public void setDeductibleRemark(String deductibleRemark) {
		this.deductibleRemark = deductibleRemark;
	}

	public String getShortRateFlag() {
		return shortRateFlag;
	}

	public void setShortRateFlag(String shortRateFlag) {
		this.shortRateFlag = shortRateFlag;
	}

	public String getDeductibleModel() {
		return deductibleModel;
	}

	public void setDeductibleModel(String deductibleModel) {
		this.deductibleModel = deductibleModel;
	}

	public String getLimitType() {
		return limitType;
	}

	public void setLimitType(String limitType) {
		this.limitType = limitType;
	}

	public String getStartNo() {
		return startNo;
	}

	public void setStartNo(String startNo) {
		this.startNo = startNo;
	}

	public String getEndNo() {
		return endNo;
	}

	public void setEndNo(String endNo) {
		this.endNo = endNo;
	}

	public String getShortRate() {
		return shortRate;
	}

	public void setShortRate(String shortRate) {
		this.shortRate = shortRate;
	}

	public String getCostStart() {
		return costStart;
	}

	public void setCostStart(String costStart) {
		this.costStart = costStart;
	}

	public String getCostEnd() {
		return costEnd;
	}

	public void setCostEnd(String costEnd) {
		this.costEnd = costEnd;
	}

	public String getItemDetailName() {
		return itemDetailName;
	}

	public void setItemDetailName(String itemDetailName) {
		this.itemDetailName = itemDetailName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getAdjustRate() {
		return adjustRate;
	}

	public void setAdjustRate(String adjustRate) {
		this.adjustRate = adjustRate;
	}

	public String getModeName() {
		return modeName;
	}

	public void setModeName(String modeName) {
		this.modeName = modeName;
	}

	public String getCalculateFlag() {
		return calculateFlag;
	}

	public void setCalculateFlag(String calculateFlag) {
		this.calculateFlag = calculateFlag;
	}

}
