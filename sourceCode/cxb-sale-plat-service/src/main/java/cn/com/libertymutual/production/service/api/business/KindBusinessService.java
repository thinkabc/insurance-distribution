package cn.com.libertymutual.production.service.api.business;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.libertymutual.production.pojo.request.LinkItemRequest;
import cn.com.libertymutual.production.pojo.request.LinkRiskRequest;
import cn.com.libertymutual.production.pojo.request.PrpdItemLibraryRequest;
import cn.com.libertymutual.production.pojo.request.PrpdItemRequest;
import cn.com.libertymutual.production.pojo.request.PrpdKindLibraryRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.PrpdItemLibraryService;
import cn.com.libertymutual.production.service.api.PrpdItemService;
import cn.com.libertymutual.production.service.api.PrpdKindItemService;
import cn.com.libertymutual.production.service.api.PrpdKindLibraryService;
import cn.com.libertymutual.production.service.api.PrpdKindService;
import cn.com.libertymutual.production.service.api.PrpdRiskPlanService;
import cn.com.libertymutual.production.service.api.PrpdRiskService;
import cn.com.libertymutual.production.service.api.PrpdSerialNoService;
import cn.com.libertymutual.production.service.api.PrpdrationService;
import cn.com.libertymutual.production.service.api.PrpdshortratelibraryService;
import cn.com.libertymutual.production.service.impl.SystemLog;
import cn.com.libertymutual.production.utils.Constant;

/** 
 * @Description: 后台业务逻辑入口
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
public abstract class KindBusinessService {

	protected static final Logger log = Constant.log;
	
	@Autowired
	protected SystemLog systemLog;
	@Autowired
	protected PrpdKindLibraryService prpdKindLibraryService;
	@Autowired
	protected PrpdRiskService prpdRiskService;
	@Autowired
	protected PrpdKindService prpdKindService;
	@Autowired
	protected PrpdRiskPlanService prpdRiskPlanService;
	@Autowired
	protected PrpdItemLibraryService prpdItemLibraryService;
	@Autowired
	protected PrpdSerialNoService prpdSerialNoService;
	@Autowired
	protected PrpdKindItemService prpdKindItemService;
	@Autowired
	protected PrpdItemService prpdItemService;
	@Autowired
	protected PrpdrationService prpdrationService;
	@Autowired
	protected PrpdshortratelibraryService prpdshortratelibraryService;
	
	/**
	 * 获取条款基本信息
	 * @param prpdKindLibraryRequest
	 * @return
	 */
	public abstract Response findPrpdKindLibrary(PrpdKindLibraryRequest prpdKindLibraryRequest);
	
	/**
	 * 条款管理-获取已关联险种下的标的基本信息
	 * @param prpdItemLibraryRequest
	 * @return
	 */
	public abstract Response findPrpdItemLibrary(PrpdItemLibraryRequest prpdItemLibraryRequest);

	/**
	 * 新增条款
	 * @param prpdKindLibraryRequest
	 * @return
	 * @throws Exception
	 */
	public abstract Response insertPrpdKindLibrary(PrpdKindLibraryRequest prpdKindLibraryRequest) throws Exception;
	
	/**
	 * 修改条款
	 * @param prpdKindLibraryRequest
	 * @return
	 * @throws Exception
	 */
	public abstract Response updatePrpdKindLibrary(PrpdKindLibraryRequest prpdKindLibraryRequest) throws Exception;
	
	/**
	 * 校验条款当前条款是否最高版本
	 * @param prpdKindLibraryRequest
	 * @return
	 */
	public abstract Response isLastKindVersion(PrpdKindLibraryRequest prpdKindLibraryRequest);
	
	/**
	 * 批量注销条款
	 * @param list
	 * @return
	 * @throws Exception
	 */
	public abstract Response inActiveKindVersion(List<PrpdKindLibraryRequest> list) throws Exception;
	
	/**
	 * 生成条款代码
	 * @param ownerriskcode  归属险种
	 * @param kindType	主险/附加险
	 * @return
	 * @throws Exception
	 */
	public abstract Response getNewKindSerialno(String ownerriskcode, int kindType) throws Exception;

	/**
	 * 关联条款-险种
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public abstract Response linkRisk(LinkRiskRequest request) throws Exception;
	
	/**
	 * 关联条款-标的/责任
	 * @param linkItemRequest
	 * @return
	 * @throws Exception
	 */
	public abstract Response linkItem(LinkItemRequest linkItemRequest) throws Exception;
	
	/**
	 * 新增标的
	 * @param prpdItemLibraryRequest
	 * @return
	 */
	public abstract Response insertPrpdItemLibrary(PrpdItemLibraryRequest prpdItemLibraryRequest) throws Exception;
	
	/**
	 * 生成标的代码
	 * @return
	 */
	public abstract Response getNewItemCode();
	
	/**
	 * 查看已关联标的
	 * @param LinkItemRequest
	 * @return
	 */
	public abstract Response findItemLinked(LinkItemRequest linkItemRequest);
	
	/**
	 * 删除已关联的标的
	 * @param linkItemRequest
	 * @return
	 * @throws Exception
	 */
	public abstract Response delItemLinked(LinkItemRequest linkItemRequest) throws Exception;
	
	/**
	 * 查看已关联险种
	 * @param LinkRiskRequest
	 * @return
	 */
	public abstract Response findRiskLinked(LinkRiskRequest linkRiskRequest);
	
	/**
	 * 删除已关联的险种，级联删除险种下的所有标的
	 * @param linkRiskRequest
	 * @return
	 * @throws Exception
	 */
	public abstract Response delRiskLinked(LinkRiskRequest linkRiskRequest) throws Exception;
	
	/**
	 * 条款关联标的时，验证是否已关联险种
	 * @param prpdKindLibraryRequest
	 * @return
	 */
	public abstract Response checkRiskLinked(PrpdKindLibraryRequest prpdKindLibraryRequest);
	
	/**
	 * 条款关联标的时，验证已关联险种下是否有标的
	 * @param prpdKindLibraryRequest
	 * @return
	 */
	public abstract Response checkItemLinked(PrpdKindLibraryRequest prpdKindLibraryRequest);
	
	/**
	 * 获取还未关联的险种
	 * @param prpdItemRequest
	 * @return
	 */
	public abstract Response findItemNotRiskLinked(PrpdItemRequest prpdItemRequest);
	
	/**
	 * 关联险种-标的/责任
	 * @param prpdItemRequest
	 * @return
	 * @throws Exception
	 */
	public abstract Response linkItem2Risk(PrpdItemRequest prpdItemRequest) throws Exception;

	/**
	 * 条款管理-取消已关联险种时，校验条款/险种是否被方案使用
	 * @param linkRiskRequest
	 * @return
	 */
	public abstract Response checkRiskLinkedDeletable(LinkRiskRequest linkRiskRequest);
	
	/**
	 * 条款管理-校验条款是否被方案使用
	 * @param list
	 * @return
	 */
	public abstract Response checkKindDeletable(List<PrpdKindLibraryRequest> list);
	
	/**
	 * 条款管理-取消已关联责任时，校验条款/责任是否被方案使用
	 * @param request
	 * @return
	 */
	public abstract Response checkItemLinkedDeletable(LinkItemRequest request);
	
	/**
	 * 获取所有短期费率
	 * @return
	 */
	public abstract Response findAllShortRate();
	
	/**
	 * 根据归属险类查询对应的条款
	 * @param request
	 * @return
	 */
	public abstract Response findByOwnerRisk(PrpdKindLibraryRequest request);
	
}