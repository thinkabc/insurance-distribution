package cn.com.libertymutual.sp.bean.car;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 * Created by Ryan on 2016-09-09.
 */
public class PrpKindRelationPK implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3277306785212649690L;
	private String kindCode;
	private String riskCode;

	@Column(name = "KIND_CODE", nullable = false, length = 10)
	@Id
	public String getKindCode() {
		return kindCode;
	}

	public void setKindCode(String kindCode) {
		this.kindCode = kindCode;
	}

	@Column(name = "RISK_CODE", nullable = false, length = 4)
	@Id
	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		PrpKindRelationPK that = (PrpKindRelationPK) o;
		if (kindCode != null ? !kindCode.equals(that.kindCode) : that.kindCode != null)
			return false;
		if (riskCode != null ? !riskCode.equals(that.riskCode) : that.riskCode != null)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		int result = kindCode != null ? kindCode.hashCode() : 0;
		result = 31 * result + (riskCode != null ? riskCode.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "PrpKindRelationPK [kindCode=" + kindCode + ", riskCode=" + riskCode + "]";
	}
}