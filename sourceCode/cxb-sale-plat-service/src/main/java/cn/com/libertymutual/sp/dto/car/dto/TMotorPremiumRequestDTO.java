package cn.com.libertymutual.sp.dto.car.dto;

import java.io.Serializable;
import java.util.List;

import cn.com.libertymutual.core.base.dto.RequestBaseDto;

public class TMotorPremiumRequestDTO extends RequestBaseDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TPrptMainDto tprpTmainDto;
	private TPrptItemCarDto tprpTitemCarDto;
	private TPrptCarOwnerDto tprptCarOwnerDto;
	private TPrptApplicantDto tprptApplicantDto;
	private TPrptInsuredDto tprptInsuredDto;
	private TPrptCarshipTaxDto prpTcarshipTaxDto;
	private List<TPrptItemKindDto> tprpTitemKindListDto;

	public TPrptMainDto getTprpTmainDto() {
		return tprpTmainDto;
	}

	public void setTprpTmainDto(TPrptMainDto tprpTmainDto) {
		this.tprpTmainDto = tprpTmainDto;
	}

	public TPrptCarOwnerDto getTprptCarOwnerDto() {
		return tprptCarOwnerDto;
	}

	public void setTprptCarOwnerDto(TPrptCarOwnerDto tprptCarOwnerDto) {
		this.tprptCarOwnerDto = tprptCarOwnerDto;
	}

	public TPrptApplicantDto getTprptApplicantDto() {
		return tprptApplicantDto;
	}

	public void setTprptApplicantDto(TPrptApplicantDto tprptApplicantDto) {
		this.tprptApplicantDto = tprptApplicantDto;
	}

	public TPrptItemCarDto getTprpTitemCarDto() {
		return tprpTitemCarDto;
	}

	public void setTprpTitemCarDto(TPrptItemCarDto tprpTitemCarDto) {
		this.tprpTitemCarDto = tprpTitemCarDto;
	}

	public TPrptInsuredDto getTprptInsuredDto() {
		return tprptInsuredDto;
	}

	public void setTprptInsuredDto(TPrptInsuredDto tprptInsuredDto) {
		this.tprptInsuredDto = tprptInsuredDto;
	}

	public TPrptCarshipTaxDto getPrpTcarshipTaxDto() {
		return prpTcarshipTaxDto;
	}

	public void setPrpTcarshipTaxDto(TPrptCarshipTaxDto prpTcarshipTaxDto) {
		this.prpTcarshipTaxDto = prpTcarshipTaxDto;
	}

	public List<TPrptItemKindDto> getTprpTitemKindListDto() {
		return tprpTitemKindListDto;
	}

	public void setTprpTitemKindListDto(List<TPrptItemKindDto> tprpTitemKindListDto) {
		this.tprpTitemKindListDto = tprpTitemKindListDto;
	}

}
