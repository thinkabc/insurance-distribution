package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;
import java.util.Date;

public class Prpsagent {
    private String agentcode;

    private String agentname;

    private String agentename;

    private String agentshortname;

    private String postcode;

    private String agenttype;

    private String identifynumber;

    private String identifytype;

    private String permitflag;

    private String permittype;

    private String permitno;

    private String currentstate;

    private Date bargaindate;

    private Date terminatedate;

    private String classcode;

    private String comcode;

    private String handlercode;

    private String teamcode;

    private String upperagentcode;

    private String newagentcode;

    private String linkername;

    private String linkernumber;

    private String principalname;

    private String priphonenumber;

    private String prifaxnumber;

    private String primobile;

    private String priemail;

    private String prinetaddress;

    private String bank;

    private String depositbank;

    private String account;

    private String accountholder;

    private String lowerviewflag;

    private String businesslicense;

    private String orgcode;

    private String agentadmin;

    private String cooperationlevel;

    private String idno;

    private String sex;

    private Date birth;

    private String residenceaddr;

    private String maritalstatus;

    private String education;

    private String degree;

    private String profession;

    private String graduateschool;

    private String jobexperience;

    private String badremark;

    private String badremarkstatus;

    private String staffsequence;

    private String staffrank;

    private Date workingdate;

    private BigDecimal businessplan;

    private BigDecimal completedpremium;

    private String exhibitionno;

    private String contractno;

    private String fundaccount;

    private String fundaccount1;

    private String increasenum;

    private String increasename;

    private String honor;

    private String trainingplan;

    private String bigagenttype;

    private String credentialsno;

    private Date credentialsstartdate;

    private Date credentialsenddate;

    private String accountname;

    private Date permitstartdate;

    private Date permitenddate;

    private BigDecimal completedproportion;

    private BigDecimal prepremium;

    private String trano;

    private String checkoutstatus;

    private String creatorcode;

    private Date createdate;

    private String updatercode;

    private Date updatedate;

    private String validstatus;

    private String remark;

    private String flag;

    private String icflag;

    private String salechannelcode;

    private String businessnature;

    private String attribute4;

    private String attribute5;

    private String attribute6;

    private String attribute7;

    private String attribute8;

    private Date attribute9;

    private Date attribute10;

    private Date exhibitionstartdate;

    private Date exhibitionenddate;

    private Date contractstartdate;

    private Date contractenddate;

    private String accountadress;

    private String professionname;

    private String professionno;

    private Date professionstartdate;

    private String industry;

    private Date professionenddate;

    private String provincecode;

    private String provincename;

    private String citycode;

    private String cityname;

    private String countycode;

    private String countyname;

    private String controlflag;

    private Date organzationstartdate;

    private Date organzationenddate;

    private String taxpayertype;

    private String taxpayerno;

    private String agreementno;

    private String agreementname;

    private String agreementno1;

    private String agreementname1;

    public String getAgentcode() {
        return agentcode;
    }

    public void setAgentcode(String agentcode) {
        this.agentcode = agentcode == null ? null : agentcode.trim();
    }

    public String getAgentname() {
        return agentname;
    }

    public void setAgentname(String agentname) {
        this.agentname = agentname == null ? null : agentname.trim();
    }

    public String getAgentename() {
        return agentename;
    }

    public void setAgentename(String agentename) {
        this.agentename = agentename == null ? null : agentename.trim();
    }

    public String getAgentshortname() {
        return agentshortname;
    }

    public void setAgentshortname(String agentshortname) {
        this.agentshortname = agentshortname == null ? null : agentshortname.trim();
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode == null ? null : postcode.trim();
    }

    public String getAgenttype() {
        return agenttype;
    }

    public void setAgenttype(String agenttype) {
        this.agenttype = agenttype == null ? null : agenttype.trim();
    }

    public String getIdentifynumber() {
        return identifynumber;
    }

    public void setIdentifynumber(String identifynumber) {
        this.identifynumber = identifynumber == null ? null : identifynumber.trim();
    }

    public String getIdentifytype() {
        return identifytype;
    }

    public void setIdentifytype(String identifytype) {
        this.identifytype = identifytype == null ? null : identifytype.trim();
    }

    public String getPermitflag() {
        return permitflag;
    }

    public void setPermitflag(String permitflag) {
        this.permitflag = permitflag == null ? null : permitflag.trim();
    }

    public String getPermittype() {
        return permittype;
    }

    public void setPermittype(String permittype) {
        this.permittype = permittype == null ? null : permittype.trim();
    }

    public String getPermitno() {
        return permitno;
    }

    public void setPermitno(String permitno) {
        this.permitno = permitno == null ? null : permitno.trim();
    }

    public String getCurrentstate() {
        return currentstate;
    }

    public void setCurrentstate(String currentstate) {
        this.currentstate = currentstate == null ? null : currentstate.trim();
    }

    public Date getBargaindate() {
        return bargaindate;
    }

    public void setBargaindate(Date bargaindate) {
        this.bargaindate = bargaindate;
    }

    public Date getTerminatedate() {
        return terminatedate;
    }

    public void setTerminatedate(Date terminatedate) {
        this.terminatedate = terminatedate;
    }

    public String getClasscode() {
        return classcode;
    }

    public void setClasscode(String classcode) {
        this.classcode = classcode == null ? null : classcode.trim();
    }

    public String getComcode() {
        return comcode;
    }

    public void setComcode(String comcode) {
        this.comcode = comcode == null ? null : comcode.trim();
    }

    public String getHandlercode() {
        return handlercode;
    }

    public void setHandlercode(String handlercode) {
        this.handlercode = handlercode == null ? null : handlercode.trim();
    }

    public String getTeamcode() {
        return teamcode;
    }

    public void setTeamcode(String teamcode) {
        this.teamcode = teamcode == null ? null : teamcode.trim();
    }

    public String getUpperagentcode() {
        return upperagentcode;
    }

    public void setUpperagentcode(String upperagentcode) {
        this.upperagentcode = upperagentcode == null ? null : upperagentcode.trim();
    }

    public String getNewagentcode() {
        return newagentcode;
    }

    public void setNewagentcode(String newagentcode) {
        this.newagentcode = newagentcode == null ? null : newagentcode.trim();
    }

    public String getLinkername() {
        return linkername;
    }

    public void setLinkername(String linkername) {
        this.linkername = linkername == null ? null : linkername.trim();
    }

    public String getLinkernumber() {
        return linkernumber;
    }

    public void setLinkernumber(String linkernumber) {
        this.linkernumber = linkernumber == null ? null : linkernumber.trim();
    }

    public String getPrincipalname() {
        return principalname;
    }

    public void setPrincipalname(String principalname) {
        this.principalname = principalname == null ? null : principalname.trim();
    }

    public String getPriphonenumber() {
        return priphonenumber;
    }

    public void setPriphonenumber(String priphonenumber) {
        this.priphonenumber = priphonenumber == null ? null : priphonenumber.trim();
    }

    public String getPrifaxnumber() {
        return prifaxnumber;
    }

    public void setPrifaxnumber(String prifaxnumber) {
        this.prifaxnumber = prifaxnumber == null ? null : prifaxnumber.trim();
    }

    public String getPrimobile() {
        return primobile;
    }

    public void setPrimobile(String primobile) {
        this.primobile = primobile == null ? null : primobile.trim();
    }

    public String getPriemail() {
        return priemail;
    }

    public void setPriemail(String priemail) {
        this.priemail = priemail == null ? null : priemail.trim();
    }

    public String getPrinetaddress() {
        return prinetaddress;
    }

    public void setPrinetaddress(String prinetaddress) {
        this.prinetaddress = prinetaddress == null ? null : prinetaddress.trim();
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank == null ? null : bank.trim();
    }

    public String getDepositbank() {
        return depositbank;
    }

    public void setDepositbank(String depositbank) {
        this.depositbank = depositbank == null ? null : depositbank.trim();
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getAccountholder() {
        return accountholder;
    }

    public void setAccountholder(String accountholder) {
        this.accountholder = accountholder == null ? null : accountholder.trim();
    }

    public String getLowerviewflag() {
        return lowerviewflag;
    }

    public void setLowerviewflag(String lowerviewflag) {
        this.lowerviewflag = lowerviewflag == null ? null : lowerviewflag.trim();
    }

    public String getBusinesslicense() {
        return businesslicense;
    }

    public void setBusinesslicense(String businesslicense) {
        this.businesslicense = businesslicense == null ? null : businesslicense.trim();
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode == null ? null : orgcode.trim();
    }

    public String getAgentadmin() {
        return agentadmin;
    }

    public void setAgentadmin(String agentadmin) {
        this.agentadmin = agentadmin == null ? null : agentadmin.trim();
    }

    public String getCooperationlevel() {
        return cooperationlevel;
    }

    public void setCooperationlevel(String cooperationlevel) {
        this.cooperationlevel = cooperationlevel == null ? null : cooperationlevel.trim();
    }

    public String getIdno() {
        return idno;
    }

    public void setIdno(String idno) {
        this.idno = idno == null ? null : idno.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getResidenceaddr() {
        return residenceaddr;
    }

    public void setResidenceaddr(String residenceaddr) {
        this.residenceaddr = residenceaddr == null ? null : residenceaddr.trim();
    }

    public String getMaritalstatus() {
        return maritalstatus;
    }

    public void setMaritalstatus(String maritalstatus) {
        this.maritalstatus = maritalstatus == null ? null : maritalstatus.trim();
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education == null ? null : education.trim();
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree == null ? null : degree.trim();
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession == null ? null : profession.trim();
    }

    public String getGraduateschool() {
        return graduateschool;
    }

    public void setGraduateschool(String graduateschool) {
        this.graduateschool = graduateschool == null ? null : graduateschool.trim();
    }

    public String getJobexperience() {
        return jobexperience;
    }

    public void setJobexperience(String jobexperience) {
        this.jobexperience = jobexperience == null ? null : jobexperience.trim();
    }

    public String getBadremark() {
        return badremark;
    }

    public void setBadremark(String badremark) {
        this.badremark = badremark == null ? null : badremark.trim();
    }

    public String getBadremarkstatus() {
        return badremarkstatus;
    }

    public void setBadremarkstatus(String badremarkstatus) {
        this.badremarkstatus = badremarkstatus == null ? null : badremarkstatus.trim();
    }

    public String getStaffsequence() {
        return staffsequence;
    }

    public void setStaffsequence(String staffsequence) {
        this.staffsequence = staffsequence == null ? null : staffsequence.trim();
    }

    public String getStaffrank() {
        return staffrank;
    }

    public void setStaffrank(String staffrank) {
        this.staffrank = staffrank == null ? null : staffrank.trim();
    }

    public Date getWorkingdate() {
        return workingdate;
    }

    public void setWorkingdate(Date workingdate) {
        this.workingdate = workingdate;
    }

    public BigDecimal getBusinessplan() {
        return businessplan;
    }

    public void setBusinessplan(BigDecimal businessplan) {
        this.businessplan = businessplan;
    }

    public BigDecimal getCompletedpremium() {
        return completedpremium;
    }

    public void setCompletedpremium(BigDecimal completedpremium) {
        this.completedpremium = completedpremium;
    }

    public String getExhibitionno() {
        return exhibitionno;
    }

    public void setExhibitionno(String exhibitionno) {
        this.exhibitionno = exhibitionno == null ? null : exhibitionno.trim();
    }

    public String getContractno() {
        return contractno;
    }

    public void setContractno(String contractno) {
        this.contractno = contractno == null ? null : contractno.trim();
    }

    public String getFundaccount() {
        return fundaccount;
    }

    public void setFundaccount(String fundaccount) {
        this.fundaccount = fundaccount == null ? null : fundaccount.trim();
    }

    public String getFundaccount1() {
        return fundaccount1;
    }

    public void setFundaccount1(String fundaccount1) {
        this.fundaccount1 = fundaccount1 == null ? null : fundaccount1.trim();
    }

    public String getIncreasenum() {
        return increasenum;
    }

    public void setIncreasenum(String increasenum) {
        this.increasenum = increasenum == null ? null : increasenum.trim();
    }

    public String getIncreasename() {
        return increasename;
    }

    public void setIncreasename(String increasename) {
        this.increasename = increasename == null ? null : increasename.trim();
    }

    public String getHonor() {
        return honor;
    }

    public void setHonor(String honor) {
        this.honor = honor == null ? null : honor.trim();
    }

    public String getTrainingplan() {
        return trainingplan;
    }

    public void setTrainingplan(String trainingplan) {
        this.trainingplan = trainingplan == null ? null : trainingplan.trim();
    }

    public String getBigagenttype() {
        return bigagenttype;
    }

    public void setBigagenttype(String bigagenttype) {
        this.bigagenttype = bigagenttype == null ? null : bigagenttype.trim();
    }

    public String getCredentialsno() {
        return credentialsno;
    }

    public void setCredentialsno(String credentialsno) {
        this.credentialsno = credentialsno == null ? null : credentialsno.trim();
    }

    public Date getCredentialsstartdate() {
        return credentialsstartdate;
    }

    public void setCredentialsstartdate(Date credentialsstartdate) {
        this.credentialsstartdate = credentialsstartdate;
    }

    public Date getCredentialsenddate() {
        return credentialsenddate;
    }

    public void setCredentialsenddate(Date credentialsenddate) {
        this.credentialsenddate = credentialsenddate;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname == null ? null : accountname.trim();
    }

    public Date getPermitstartdate() {
        return permitstartdate;
    }

    public void setPermitstartdate(Date permitstartdate) {
        this.permitstartdate = permitstartdate;
    }

    public Date getPermitenddate() {
        return permitenddate;
    }

    public void setPermitenddate(Date permitenddate) {
        this.permitenddate = permitenddate;
    }

    public BigDecimal getCompletedproportion() {
        return completedproportion;
    }

    public void setCompletedproportion(BigDecimal completedproportion) {
        this.completedproportion = completedproportion;
    }

    public BigDecimal getPrepremium() {
        return prepremium;
    }

    public void setPrepremium(BigDecimal prepremium) {
        this.prepremium = prepremium;
    }

    public String getTrano() {
        return trano;
    }

    public void setTrano(String trano) {
        this.trano = trano == null ? null : trano.trim();
    }

    public String getCheckoutstatus() {
        return checkoutstatus;
    }

    public void setCheckoutstatus(String checkoutstatus) {
        this.checkoutstatus = checkoutstatus == null ? null : checkoutstatus.trim();
    }

    public String getCreatorcode() {
        return creatorcode;
    }

    public void setCreatorcode(String creatorcode) {
        this.creatorcode = creatorcode == null ? null : creatorcode.trim();
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getUpdatercode() {
        return updatercode;
    }

    public void setUpdatercode(String updatercode) {
        this.updatercode = updatercode == null ? null : updatercode.trim();
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public String getValidstatus() {
        return validstatus;
    }

    public void setValidstatus(String validstatus) {
        this.validstatus = validstatus == null ? null : validstatus.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getIcflag() {
        return icflag;
    }

    public void setIcflag(String icflag) {
        this.icflag = icflag == null ? null : icflag.trim();
    }

    public String getSalechannelcode() {
        return salechannelcode;
    }

    public void setSalechannelcode(String salechannelcode) {
        this.salechannelcode = salechannelcode == null ? null : salechannelcode.trim();
    }

    public String getBusinessnature() {
        return businessnature;
    }

    public void setBusinessnature(String businessnature) {
        this.businessnature = businessnature == null ? null : businessnature.trim();
    }

    public String getAttribute4() {
        return attribute4;
    }

    public void setAttribute4(String attribute4) {
        this.attribute4 = attribute4 == null ? null : attribute4.trim();
    }

    public String getAttribute5() {
        return attribute5;
    }

    public void setAttribute5(String attribute5) {
        this.attribute5 = attribute5 == null ? null : attribute5.trim();
    }

    public String getAttribute6() {
        return attribute6;
    }

    public void setAttribute6(String attribute6) {
        this.attribute6 = attribute6 == null ? null : attribute6.trim();
    }

    public String getAttribute7() {
        return attribute7;
    }

    public void setAttribute7(String attribute7) {
        this.attribute7 = attribute7 == null ? null : attribute7.trim();
    }

    public String getAttribute8() {
        return attribute8;
    }

    public void setAttribute8(String attribute8) {
        this.attribute8 = attribute8 == null ? null : attribute8.trim();
    }

    public Date getAttribute9() {
        return attribute9;
    }

    public void setAttribute9(Date attribute9) {
        this.attribute9 = attribute9;
    }

    public Date getAttribute10() {
        return attribute10;
    }

    public void setAttribute10(Date attribute10) {
        this.attribute10 = attribute10;
    }

    public Date getExhibitionstartdate() {
        return exhibitionstartdate;
    }

    public void setExhibitionstartdate(Date exhibitionstartdate) {
        this.exhibitionstartdate = exhibitionstartdate;
    }

    public Date getExhibitionenddate() {
        return exhibitionenddate;
    }

    public void setExhibitionenddate(Date exhibitionenddate) {
        this.exhibitionenddate = exhibitionenddate;
    }

    public Date getContractstartdate() {
        return contractstartdate;
    }

    public void setContractstartdate(Date contractstartdate) {
        this.contractstartdate = contractstartdate;
    }

    public Date getContractenddate() {
        return contractenddate;
    }

    public void setContractenddate(Date contractenddate) {
        this.contractenddate = contractenddate;
    }

    public String getAccountadress() {
        return accountadress;
    }

    public void setAccountadress(String accountadress) {
        this.accountadress = accountadress == null ? null : accountadress.trim();
    }

    public String getProfessionname() {
        return professionname;
    }

    public void setProfessionname(String professionname) {
        this.professionname = professionname == null ? null : professionname.trim();
    }

    public String getProfessionno() {
        return professionno;
    }

    public void setProfessionno(String professionno) {
        this.professionno = professionno == null ? null : professionno.trim();
    }

    public Date getProfessionstartdate() {
        return professionstartdate;
    }

    public void setProfessionstartdate(Date professionstartdate) {
        this.professionstartdate = professionstartdate;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry == null ? null : industry.trim();
    }

    public Date getProfessionenddate() {
        return professionenddate;
    }

    public void setProfessionenddate(Date professionenddate) {
        this.professionenddate = professionenddate;
    }

    public String getProvincecode() {
        return provincecode;
    }

    public void setProvincecode(String provincecode) {
        this.provincecode = provincecode == null ? null : provincecode.trim();
    }

    public String getProvincename() {
        return provincename;
    }

    public void setProvincename(String provincename) {
        this.provincename = provincename == null ? null : provincename.trim();
    }

    public String getCitycode() {
        return citycode;
    }

    public void setCitycode(String citycode) {
        this.citycode = citycode == null ? null : citycode.trim();
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname == null ? null : cityname.trim();
    }

    public String getCountycode() {
        return countycode;
    }

    public void setCountycode(String countycode) {
        this.countycode = countycode == null ? null : countycode.trim();
    }

    public String getCountyname() {
        return countyname;
    }

    public void setCountyname(String countyname) {
        this.countyname = countyname == null ? null : countyname.trim();
    }

    public String getControlflag() {
        return controlflag;
    }

    public void setControlflag(String controlflag) {
        this.controlflag = controlflag == null ? null : controlflag.trim();
    }

    public Date getOrganzationstartdate() {
        return organzationstartdate;
    }

    public void setOrganzationstartdate(Date organzationstartdate) {
        this.organzationstartdate = organzationstartdate;
    }

    public Date getOrganzationenddate() {
        return organzationenddate;
    }

    public void setOrganzationenddate(Date organzationenddate) {
        this.organzationenddate = organzationenddate;
    }

    public String getTaxpayertype() {
        return taxpayertype;
    }

    public void setTaxpayertype(String taxpayertype) {
        this.taxpayertype = taxpayertype == null ? null : taxpayertype.trim();
    }

    public String getTaxpayerno() {
        return taxpayerno;
    }

    public void setTaxpayerno(String taxpayerno) {
        this.taxpayerno = taxpayerno == null ? null : taxpayerno.trim();
    }

    public String getAgreementno() {
        return agreementno;
    }

    public void setAgreementno(String agreementno) {
        this.agreementno = agreementno == null ? null : agreementno.trim();
    }

    public String getAgreementname() {
        return agreementname;
    }

    public void setAgreementname(String agreementname) {
        this.agreementname = agreementname == null ? null : agreementname.trim();
    }

    public String getAgreementno1() {
        return agreementno1;
    }

    public void setAgreementno1(String agreementno1) {
        this.agreementno1 = agreementno1 == null ? null : agreementno1.trim();
    }

    public String getAgreementname1() {
        return agreementname1;
    }

    public void setAgreementname1(String agreementname1) {
        this.agreementname1 = agreementname1 == null ? null : agreementname1.trim();
    }
}