package cn.com.libertymutual.sp.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
@Entity
@Table(name = "tb_sp_product")
public class TbSpProduct implements Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -1322935554736001224L;

	@ApiModelProperty(name = "id", value = "id", notes = "唯一标识")
	private Integer id;

	@ApiModelProperty(name = "productType", value = "productType", notes = "产品大类")
	private String productType;// 产品大类

	@ApiModelProperty(name = "riskCode", value = "riskCode", notes = "风险编码")
	private String riskCode;

	@ApiModelProperty(name = "productCname", value = "productCname", notes = "产品中文名")
	private String productCname;// 产品中文名

	@ApiModelProperty(name = "productEname", value = "productEname", notes = "产品中文名")
	private String productEname;// 产品英文名

	@ApiModelProperty(name = "imgUrl", value = "imgUrl", notes = "列表图标地址")
	private String imgUrl;

	@ApiModelProperty(name = "imgThumbnailUrl", value = "imgThumbnailUrl", notes = "缩略图地址")
	private String imgThumbnailUrl;

	@ApiModelProperty(name = "isShow", value = "isShow", notes = "是否显示")
	private String isShow;

	@ApiModelProperty(name = "descrition", value = "descrition", notes = "描述")
	private String descrition;

	@ApiModelProperty(name = "serialNo", value = "serialNo", notes = "序号")
	private Integer serialNo;

	@ApiModelProperty(name = "minPrice", value = "minPrice", notes = "最低价格")
	private Double minPrice;

	@ApiModelProperty(name = "priPrice", value = "priPrice", notes = "原价")
	private Double priPrice;

	@ApiModelProperty(name = "discountPrice", value = "discountPrice", notes = "折扣价")
	private Double discountPrice;

	@ApiModelProperty(name = "quantity", value = "quantity", notes = "销量")
	private Integer quantity;

	@ApiModelProperty(name = "startDate", value = "startDate", notes = "有效起期")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@Temporal(TemporalType.DATE)
	private Date startDate;

	@ApiModelProperty(name = "endDate", value = "endDate", notes = "有效止期")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@Temporal(TemporalType.DATE)
	private Date endDate;

	@ApiModelProperty(name = "detail", value = "detail", notes = "详细介绍")
	private String detail;

	@ApiModelProperty(name = "introduceUrl", value = "introduceUrl", notes = "产品介绍")
	private String introduceUrl;

	@ApiModelProperty(name = "schemeUrl", value = "schemeUrl", notes = "保障方案")
	private String schemeUrl;

	@ApiModelProperty(name = "detailUrl", value = "detailUrl", notes = "服务详情")
	private String detailUrl;

	@ApiModelProperty(name = "remark", value = "remark", notes = "备注")
	private String remark;

	@ApiModelProperty(name = "minAge", value = "minAge", notes = "最小年龄")
	private String minAge;// 最小年龄

	@ApiModelProperty(name = "maxAge", value = "maxAge", notes = "最大年龄")
	private String maxAge;// 最大年龄

	@ApiModelProperty(name = "maxDeadLine", value = "deadLine", notes = "最大保障期限")
	private String maxDeadLine;// 保障期限

	@ApiModelProperty(name = "mixDeadLine", value = "deadLine", notes = "最小保障期限")
	private String minDeadLine;// 保障期限

	@ApiModelProperty(name = "status", value = "status", notes = "状态")
	private String status;

	@ApiModelProperty(name = "element", value = "status", notes = "显示的元素")
	private String element;

	private String modifyUser;
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date modifyTime;
	private String isExclusive;// 是否专属
	private String riskPrintName;
	private String advanceDay;
	private String baseStatus;
	private Integer minMonth;
	private String isHot;
	private String isQuota;
	private String isCar;
	@ApiModelProperty(name = "productCompany", value = "productCompany", notes = "产品所属公司")
	private Integer productCompany;

	@Column(name = "IS_HOT")
	public String getIsHot() {
		return isHot;
	}

	public void setIsHot(String isHot) {
		this.isHot = isHot;
	}
	@Column(name = "is_quota", length = 1)
	public String getIsQuota() {
		return isQuota;
	}

	public void setIsQuota(String isQuota) {
		this.isQuota = isQuota;
	}

	@Column(name = "IS_CAR")
	public String getIsCar() {
		return isCar;
	}

	public void setIsCar(String isCar) {
		this.isCar = isCar;
	}

	@Column(name = "MIN_MONTH")
	public Integer getMinMonth() {
		return minMonth;
	}

	public void setMinMonth(Integer minMonth) {
		this.minMonth = minMonth;
	}

	@Column(name = "MODIFY_USER", length = 20)
	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	@Column(name = "BASE_STATUS")
	public String getBaseStatus() {
		return baseStatus;
	}

	public void setBaseStatus(String baseStatus) {
		this.baseStatus = baseStatus;
	}

	@Column(name = "MODIFY_TIME")
	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	@Column(name = "IS_EXCLUSIVE", length = 2)
	public String getIsExclusive() {
		return isExclusive;
	}

	public void setIsExclusive(String isExclusive) {
		this.isExclusive = isExclusive;
	}

	@Column(name = "Advance_day", length = 10)
	public String getAdvanceDay() {
		return advanceDay;
	}

	public void setAdvanceDay(String advanceDay) {
		this.advanceDay = advanceDay;
	}

	/** default constructor */
	public TbSpProduct() {
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "MAX_DEADLINE", length = 100)
	public String getMaxDeadLine() {
		return maxDeadLine;
	}

	public void setMaxDeadLine(String maxDeadLine) {
		this.maxDeadLine = maxDeadLine;
	}

	@Column(name = "MIN_DEADLINE", length = 100)
	public String getMinDeadLine() {
		return minDeadLine;
	}

	public void setMinDeadLine(String minDeadLine) {
		this.minDeadLine = minDeadLine;
	}

	@Column(name = "PRODUCT_TYPE", length = 10)
	public String getProductType() {
		return this.productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	@Column(name = "ELEMENT", length = 10)
	public String getElement() {
		return element;
	}

	public void setElement(String element) {
		this.element = element;
	}

	@Column(name = "RISK_CODE", length = 10)
	public String getRiskCode() {
		return this.riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	@Column(name = "PRODUCT_ENAME", length = 100)
	public String getProductEname() {
		return this.productEname;
	}

	public void setProductEname(String codeEname) {
		this.productEname = codeEname;
	}

	@Column(name = "PRODUCT_CNAME", length = 100)
	public String getProductCname() {
		return this.productCname;
	}

	public void setProductCname(String codeCname) {
		this.productCname = codeCname;
	}

	@Column(name = "IMG_URL", length = 500)
	public String getImgUrl() {
		return this.imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Column(name = "IMG_THUMBNAIL_URL", length = 500)
	public String getImgThumbnailUrl() {
		return imgThumbnailUrl;
	}

	public void setImgThumbnailUrl(String imgThumbnailUrl) {
		this.imgThumbnailUrl = imgThumbnailUrl;
	}

	@Column(name = "IS_SHOW", length = 2)
	public String getIsShow() {
		return this.isShow;
	}

	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}

	@Column(name = "DESCRITION", length = 200)
	public String getDescrition() {
		return this.descrition;
	}

	public void setDescrition(String descrition) {
		this.descrition = descrition;
	}

	@Column(name = "SERIAL_NO", length = 10)
	public Integer getSerialNo() {
		return this.serialNo;
	}

	public void setSerialNo(Integer serialNo) {
		this.serialNo = serialNo;
	}

	@Column(name = "MIN_PRICE", precision = 22, scale = 0)
	public Double getMinPrice() {
		return this.minPrice;
	}

	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}

	@Column(name = "PRI_PRICE", precision = 22, scale = 0)
	public Double getPriPrice() {
		return this.priPrice;
	}

	public void setPriPrice(Double priPrice) {
		this.priPrice = priPrice;
	}

	@Column(name = "DISCOUNT_PRICE", precision = 22, scale = 0)
	public Double getDiscountPrice() {
		return this.discountPrice;
	}

	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	@Column(name = "QUANTITY")
	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@Column(name = "START_DATE", length = 10)
	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@Column(name = "END_DATE", length = 10)
	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "DETAIL", length = 400)
	public String getDetail() {
		return this.detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	@Column(name = "INTRODUCE_URL", length = 500)
	public String getIntroduceUrl() {
		return this.introduceUrl;
	}

	public void setIntroduceUrl(String introduceUrl) {
		this.introduceUrl = introduceUrl;
	}

	@Column(name = "SCHEME_URL", length = 500)
	public String getSchemeUrl() {
		return this.schemeUrl;
	}

	public void setSchemeUrl(String schemeUrl) {
		this.schemeUrl = schemeUrl;
	}

	@Column(name = "DETAIL_URL", length = 500)
	public String getDetailUrl() {
		return this.detailUrl;
	}

	public void setDetailUrl(String detailUrl) {
		this.detailUrl = detailUrl;
	}

	@Column(name = "REMARK", length = 100)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "MIN_AGE")
	public String getMinAge() {
		return minAge;
	}

	public void setMinAge(String minAge) {
		this.minAge = minAge;
	}

	@Column(name = "MAX_AGE")
	public String getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(String maxAge) {
		this.maxAge = maxAge;
	}

	@Column(name = "STATUS", length = 1)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "RISK_PRINT_NAME", length = 50)
	public String getRiskPrintName() {
		return riskPrintName;
	}

	public void setRiskPrintName(String riskPrintName) {
		this.riskPrintName = riskPrintName;
	}

	private int pageSize;
	private int pageNumber;
	private List<TbSpPlan> tbSpPlans;
	@Transient
	public boolean hotFlag;
	private Integer count;
	private List<TbSpProductConfig> productConfigs;
	private TbSpApprove tbSpApprove;
	private List<String> shopUsers;

	@Transient
	public List<String> getShopUsers() {
		return shopUsers;
	}

	public void setShopUsers(List<String> shopUsers) {
		this.shopUsers = shopUsers;
	}

	@Transient
	public TbSpApprove getTbSpApprove() {
		return tbSpApprove;
	}

	public void setTbSpApprove(TbSpApprove tbSpApprove) {
		this.tbSpApprove = tbSpApprove;
	}

	@Transient
	public List<TbSpProductConfig> getProductConfigs() {
		return productConfigs;
	}

	public void setProductConfigs(List<TbSpProductConfig> productConfigs) {
		this.productConfigs = productConfigs;
	}

	@Transient
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@Transient
	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	@Transient
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	@Transient
	public List<TbSpPlan> getTbSpPlans() {
		return tbSpPlans;
	}

	public void setTbSpPlans(List<TbSpPlan> tbSpPlans) {
		this.tbSpPlans = tbSpPlans;
	}

	@Transient
	public boolean getHotFlag() {
		return hotFlag;
	}

	public void setHotFlag(boolean hotFlag) {
		this.hotFlag = hotFlag;
	}

	private String approveAuth;

	@Transient
	public String getApproveAuth() {
		return approveAuth;
	}

	public void setApproveAuth(String approveAuth) {
		this.approveAuth = approveAuth;
	}
	
	@Column(name = "PRODUCT_COMPANY")
	public Integer getProductCompany() {
		return productCompany;
	}

	public void setProductCompany(Integer productCompany) {
		this.productCompany = productCompany;
	}

	@Override
	public String toString() {
		return "TbSpProduct [id=" + id + ", productType=" + productType + ", riskCode=" + riskCode + ", productCname=" + productCname
				+ ", productEname=" + productEname + ", imgUrl=" + imgUrl + ", imgThumbnailUrl=" + imgThumbnailUrl + ", isShow=" + isShow
				+ ", descrition=" + descrition + ", serialNo=" + serialNo + ", minPrice=" + minPrice + ", priPrice=" + priPrice + ", discountPrice="
				+ discountPrice + ", quantity=" + quantity + ", startDate=" + startDate + ", endDate=" + endDate + ", detail=" + detail
				+ ", introduceUrl=" + introduceUrl + ", schemeUrl=" + schemeUrl + ", detailUrl=" + detailUrl + ", remark=" + remark + ", minAge="
				+ minAge + ", maxAge=" + maxAge + ", maxDeadLine=" + maxDeadLine + ", minDeadLine=" + minDeadLine + ", status=" + status
				+ ", element=" + element + ", advanceDay=" + advanceDay + ", isQuota=" + isQuota + ", hotFlag=" + hotFlag + ", count=" + count 
				+ ", productCompany=" + productCompany + "]";
	}

	public boolean differ(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TbSpProduct other = (TbSpProduct) obj;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.toString().equals(other.endDate.toString()))
			return false;
		if (isShow == null) {
			if (other.isShow != null)
				return false;
		} else if (!isShow.equals(other.isShow))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (maxAge == null) {
			if (other.maxAge != null)
				return false;
		} else if (!maxAge.equals(other.maxAge))
			return false;
		if (maxDeadLine == null) {
			if (other.maxDeadLine != null)
				return false;
		} else if (!maxDeadLine.equals(other.maxDeadLine))
			return false;
		if (minAge == null) {
			if (other.minAge != null)
				return false;
		} else if (!minAge.equals(other.minAge))
			return false;
		if (minDeadLine == null) {
			if (other.minDeadLine != null)
				return false;
		} else if (!minDeadLine.equals(other.minDeadLine))
			return false;
		if (productCname == null) {
			if (other.productCname != null)
				return false;
		} else if (!productCname.equals(other.productCname))
			return false;
		if (minPrice == null) {
			if (other.minPrice != null)
				return false;
		} else if (!minPrice.equals(other.minPrice))
			return false;
		if (riskCode == null) {
			if (other.riskCode != null)
				return false;
		} else if (!riskCode.equals(other.riskCode))
			return false;
		if (serialNo == null) {
			if (other.serialNo != null)
				return false;
		} else if (!serialNo.equals(other.serialNo))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.toString().equals(other.startDate.toString()))
			return false;
		return true;
	}

}