package cn.com.libertymutual.sp.action;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import cn.com.libertymutual.core.base.dto.ResponseBaseDto;
import cn.com.libertymutual.core.email.IEmailService;
import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.util.StringUtil;
import cn.com.libertymutual.core.util.UtilTool;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.biz.ClaimsBiz;
import cn.com.libertymutual.sp.dto.ClaimCaseResponseDto;
import cn.com.libertymutual.sp.dto.TQueryPolicyRequstDto;
import cn.com.libertymutual.sp.dto.TReportClaimRequestDto;
import cn.com.libertymutual.sp.service.api.ClaimsService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/nol/claims")
public class ClaimsController {

	@Resource
	private ClaimsService claimsService;
	@Resource
	private ClaimsBiz claimsBiz;
	@Resource
	private IEmailService emailService;
	
	@Value("${app.interface.reportClaims.email}")
	private String reportEmail;
	
	private Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * 理赔查询 状态（1：新建 ，2 ：打开， 3：重开 ，0：关闭）
	 * @author wkf
	 * @param policyNo
	 * @return ClaimCaseResponseDto
	 * @throws Exception
	 * @date 2017-11-6下午01:48:55
	 */
	@ApiOperation(value = "查找理赔记录", notes = "查找理赔记录")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "policyNo", value = "保单号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "id", value = "身份证号", required = false, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/queryClaims")
	public ClaimCaseResponseDto queryClaims(HttpServletRequest requestHttp, HttpServletResponse response, String identCode,
			@RequestBody TQueryPolicyRequstDto reqeust) throws Exception {
		//ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);

		ClaimCaseResponseDto responseDto = new ClaimCaseResponseDto();
		responseDto = claimsService.queryClaims(reqeust, requestHttp);
		return responseDto;
	}

	/**
	 * 通过证件号码查询保单
	 * @param  用户code
	 * @param request
	 * @param response
	 * @param policyRequest
	 * @return
	 * @throws Exception
	 * 保单状态status
	 * 1：在期   
	 * 0 ：未生效
	 * 2：已撤单
	 * 3：已注销
	 * 4：全单退保
	 * 5:保单遗失
	 * 6：保险标志遗失
	 * 7：保单和保险标志遗失
	 * 9：失效
	 */
	@ApiOperation(value = "通过证件号码查询保单", notes = "通过证件号码查询保单")
	@RequestMapping(value = "/queryPolicys")
	public ServiceResult queryPolicys(HttpServletRequest request, HttpServletResponse response, String userCode,
			@RequestBody TQueryPolicyRequstDto policyRequest) throws Exception {
		return claimsBiz.queryPolicys(request, response, policyRequest, Current.userCode.get());
	}

	/**
	 * 通过证件号码查询投保单
	 * @param  用户code
	 * @param request
	 * @param response
	 * @param policyRequest
	 * @return
	 * @throws Exception
	 * 有效状态status
	 * 1：在期   
	 * 0 ：未生效
	 * 2：已撤单
	 * 3：已注销
	 * 4：全单退保
	 * 5:保单遗失
	 * 6：保险标志遗失
	 * 7：保单和保险标志遗失
	 * 9：失效
	 * 
	 * 投保单状态：underwriteflag;//投保单状态：8等待支付，9等待核保 ，1完成，2核保未通过 ，0投保中
	 */
	@ApiOperation(value = "通过证件号码查询投保单", notes = "通过证件号码查询投保单")
	@RequestMapping(value = "/queryProposal")
	public ServiceResult queryProposal(HttpServletRequest request, HttpServletResponse response, String userCode,
			@RequestBody TQueryPolicyRequstDto policyRequest) throws Exception {
		return claimsBiz.queryProposal(request, response, policyRequest, Current.userCode.get());
	}

	@ApiOperation(value = "查询保单认证", notes = "查询保单认证")
	@RequestMapping(value = "/queryPolicysAuthen")
	public ServiceResult queryPolicysAuthen(HttpServletRequest request, HttpServletResponse response, String userCode,
			@RequestBody TQueryPolicyRequstDto policyRequest) throws Exception {
		return claimsBiz.queryPolicysAuthen(request, response, policyRequest, Current.userCode.get());
	}

	@ApiOperation(value = "删除认证信息", notes = "查询保单认证")
	@RequestMapping(value = "/destroy")
	public void destroy(HttpServletRequest request) throws Exception {
		claimsBiz.destroy(request);
	}

	@ApiOperation(value = "查询保单", notes = "查询保单认证")
	@RequestMapping(value = "/findPolicys")
	public ServiceResult findPolicys(HttpServletRequest request, HttpServletResponse response, String userCode,
			@RequestBody TQueryPolicyRequstDto policyRequest) throws Exception {
		return claimsBiz.findPolicys(request, response, policyRequest, Current.userCode.get());
	}

	/**Remarks: 通过证件号码查询报案列表<br>理赔状态（1：新建 ，2 ：打开， 3：重开 ，0：关闭）
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年11月14日下午1:58:44<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param response
	 * @param policyRequest
	 * @return
	 * @throws Exception
	 */

	@ApiOperation(value = "通过证件号码查询报案列表", notes = "通过证件号码查询保单")
	@RequestMapping(value = "/queryReports")
	public ServiceResult queryReports(HttpServletRequest request, HttpServletResponse response, @RequestBody TQueryPolicyRequstDto policyRequest)
			throws Exception {
		return claimsBiz.queryReports(request, response, policyRequest);
	}

	/**
	 * 报案
	 * @author wkf
	 * @param policyNo
	 * @return ClaimCaseResponseDto
	 * @throws Exception
	 * @date 2017-11-6下午01:48:55
	 */
	@ApiOperation(value = "报案", notes = "报案")
	@RequestMapping(value = "/reportClaims")
	public ResponseBaseDto reportClaims(HttpServletRequest request, HttpServletResponse response, String identCode,
			TReportClaimRequestDto claimRequest, MultipartFile[] imgFiles) throws Exception {

		ResponseBaseDto responseDto = claimsService.reportClaims(claimRequest, imgFiles);
		if(null!=responseDto && responseDto.getStatus()){//报案成功
			log.info("开始发送邮箱HQwxbagroup@libertymutual.com.cn：");
			List<String> mails = new ArrayList<String>();
			if(StringUtil.isEmpty(reportEmail)){
				mails.add("HQwxbagroup@libertymutual.com.cn");
			}else{
				mails.add(reportEmail);
			}
			
			String subject="赔案："+responseDto.getResultMessage()+"已通过微信报案成功，请尽快跟进处理！";
			emailService.sendEmail(subject, "", mails);
			log.info(reportEmail+"发送邮箱内容"+subject);
		}
		return responseDto;
	}

	@ApiOperation(value = "资料上传", notes = "资料上传")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "reportNo", value = "报案号", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "email", value = "邮箱地址", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "identCode", value = "手机验证码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "imgFiles", value = "文件对象", required = true, paramType = "query", dataType = "MultipartFile[]") })
	@RequestMapping(value = "/uploadPhoto")
	public ResponseBaseDto uploadPhoto(HttpServletRequest request, HttpServletResponse response, String identCode, String reportNo, String email,
			MultipartFile[] imgFiles) throws Exception {

		ResponseBaseDto responseDto = new ResponseBaseDto();
		if (StringUtil.isEmpty(email) || StringUtil.isEmpty(reportNo)) {
			responseDto.setStatus(false);
			responseDto.setResultMessage("参数:报案号或邮箱为空");
			return responseDto;
		}
		File file = claimsService.uploadPhoto(reportNo, imgFiles);
		if (file == null) {
			responseDto.setStatus(false);
			responseDto.setResultMessage("not find file!");
			return responseDto;
		}
		if (!file.exists()) {
			responseDto.setStatus(false);
			responseDto.setResultMessage("not find folder!");
			return responseDto;
		}
		String subject = "赔案号" + reportNo + "-" + "保单理赔-资料上传-邮件发送照片";
		String content = "来自自营平台资料上传";
		List<String> mails = new ArrayList<String>();
		mails.add(email);
		emailService.sendEmail(subject, content, mails, null, null, file, true);
		// 清空临时文件夹
		UtilTool.deleteDirectory(file.getParent());
		responseDto.setStatus(true);
		return responseDto;
	}

}
