package cn.com.libertymutual.production.pojo.request;

import java.util.Date;

public class PrpdClauseRequest extends Request {
	private String clausename;

	private String language;

	private String titleflag;

	private String context;

	private String newclausecode;

	private String validstatus;

	private String flag;

	private String comcode;

	private String contflag;

	private String delflag;

	private String iniflag;

	private String clausecode;

	private String lineno;

	private String remark;

	private String agentcode;

	private String riskcode;

	private String defaultriskcode;

	private String plancode;

	private String defaultcomcode;

	private Date createdate;

	public String getClausename() {
		return clausename;
	}

	public void setClausename(String clausename) {
		this.clausename = clausename;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getTitleflag() {
		return titleflag;
	}

	public void setTitleflag(String titleflag) {
		this.titleflag = titleflag;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getNewclausecode() {
		return newclausecode;
	}

	public void setNewclausecode(String newclausecode) {
		this.newclausecode = newclausecode;
	}

	public String getValidstatus() {
		return validstatus;
	}

	public void setValidstatus(String validstatus) {
		this.validstatus = validstatus;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getComcode() {
		return comcode;
	}

	public void setComcode(String comcode) {
		this.comcode = comcode;
	}

	public String getContflag() {
		return contflag;
	}

	public void setContflag(String contflag) {
		this.contflag = contflag;
	}

	public String getDelflag() {
		return delflag;
	}

	public void setDelflag(String delflag) {
		this.delflag = delflag;
	}

	public String getIniflag() {
		return iniflag;
	}

	public void setIniflag(String iniflag) {
		this.iniflag = iniflag;
	}

	public String getClausecode() {
		return clausecode;
	}

	public void setClausecode(String clausecode) {
		this.clausecode = clausecode;
	}

	public String getLineno() {
		return lineno;
	}

	public void setLineno(String lineno) {
		this.lineno = lineno;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getAgentcode() {
		return agentcode;
	}

	public void setAgentcode(String agentcode) {
		this.agentcode = agentcode;
	}

	public String getRiskcode() {
		return riskcode;
	}

	public void setRiskcode(String riskcode) {
		this.riskcode = riskcode;
	}

	public String getDefaultriskcode() {
		return defaultriskcode;
	}

	public void setDefaultriskcode(String defaultriskcode) {
		this.defaultriskcode = defaultriskcode;
	}

	public String getPlancode() {
		return plancode;
	}

	public void setPlancode(String plancode) {
		this.plancode = plancode;
	}

	public String getDefaultcomcode() {
		return defaultcomcode;
	}

	public void setDefaultcomcode(String defaultcomcode) {
		this.defaultcomcode = defaultcomcode;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
}
