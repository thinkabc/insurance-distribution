package cn.com.libertymutual.sp.dto.axtx;

import java.io.Serializable;


public class ItemCarDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5987998481577266510L;

	 	private String carType;             //车辆类型1
	    private String nature;              //使用性质1
	    private String licenseNo;           //车牌号码
	    private String brandName;           //厂牌型号1
	    private String frameNo;             //车架号
	    private int itemCarSeatCount;       //核定座位5 7 
	    private int itemCarSeatCountInsured;//投保座位
		public String getCarType() {
			return carType;
		}
		public void setCarType(String carType) {
			this.carType = carType;
		}
		public String getNature() {
			return nature;
		}
		public void setNature(String nature) {
			this.nature = nature;
		}
		public String getLicenseNo() {
			return licenseNo;
		}
		public void setLicenseNo(String licenseNo) {
			this.licenseNo = licenseNo;
		}
		public String getBrandName() {
			return brandName;
		}
		public void setBrandName(String brandName) {
			this.brandName = brandName;
		}
		public String getFrameNo() {
			return frameNo;
		}
		public void setFrameNo(String frameNo) {
			this.frameNo = frameNo;
		}
		public int getItemCarSeatCount() {
			return itemCarSeatCount;
		}
		public void setItemCarSeatCount(int itemCarSeatCount) {
			this.itemCarSeatCount = itemCarSeatCount;
		}
		public int getItemCarSeatCountInsured() {
			return itemCarSeatCountInsured;
		}
		public void setItemCarSeatCountInsured(int itemCarSeatCountInsured) {
			this.itemCarSeatCountInsured = itemCarSeatCountInsured;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((brandName == null) ? 0 : brandName.hashCode());
			result = prime * result
					+ ((carType == null) ? 0 : carType.hashCode());
			result = prime * result
					+ ((frameNo == null) ? 0 : frameNo.hashCode());
			result = prime * result + itemCarSeatCount;
			result = prime * result + itemCarSeatCountInsured;
			result = prime * result
					+ ((licenseNo == null) ? 0 : licenseNo.hashCode());
			result = prime * result
					+ ((nature == null) ? 0 : nature.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ItemCarDto other = (ItemCarDto) obj;
			if (brandName == null) {
				if (other.brandName != null)
					return false;
			} else if (!brandName.equals(other.brandName))
				return false;
			if (carType == null) {
				if (other.carType != null)
					return false;
			} else if (!carType.equals(other.carType))
				return false;
			if (frameNo == null) {
				if (other.frameNo != null)
					return false;
			} else if (!frameNo.equals(other.frameNo))
				return false;
			if (itemCarSeatCount != other.itemCarSeatCount)
				return false;
			if (itemCarSeatCountInsured != other.itemCarSeatCountInsured)
				return false;
			if (licenseNo == null) {
				if (other.licenseNo != null)
					return false;
			} else if (!licenseNo.equals(other.licenseNo))
				return false;
			if (nature == null) {
				if (other.nature != null)
					return false;
			} else if (!nature.equals(other.nature))
				return false;
			return true;
		}
		@Override
		public String toString() {
			return "ItemCarDto [carType=" + carType + ", nature=" + nature
					+ ", licenseNo=" + licenseNo + ", brandName=" + brandName
					+ ", frameNo=" + frameNo + ", itemCarSeatCount="
					+ itemCarSeatCount + ", itemCarSeatCountInsured="
					+ itemCarSeatCountInsured + "]";
		}
}
