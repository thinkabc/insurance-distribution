package cn.com.libertymutual.production.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdkindMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkind;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkindExample;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkindlibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkindExample.Criteria;
import cn.com.libertymutual.production.service.api.PrpdKindService;

/**
 * @author Steven.Li
 * @date 2017年7月28日
 * 
 */
@Service
public class PrpdKindServiceImpl implements PrpdKindService {

	@Autowired
	private PrpdkindMapper prpdkindMapper;

	@Override
	public void insert(Prpdkind record) {

		prpdkindMapper.insertSelective(record);

	}

	@Override
	public void update(Prpdkind record) {
		PrpdkindExample example = new PrpdkindExample();
		Criteria criteria = example.createCriteria();
		if(!StringUtils.isEmpty(record.getKindcode())) {
			criteria.andKindcodeEqualTo(record.getKindcode());
		}
		if(!StringUtils.isEmpty(record.getKindversion())) {
			criteria.andKindversionEqualTo(record.getKindversion());
		}
		prpdkindMapper.updateByExampleSelective(record, example);
	}

	@Override
	public List<Prpdkind> check(Prpdkind prpdkind) {
		PrpdkindExample example = new PrpdkindExample();
		Criteria criteria = example.createCriteria();
		criteria.andKindcodeEqualTo(prpdkind.getKindcode());
		criteria.andKindversionEqualTo(prpdkind.getKindversion());
		criteria.andRiskcodeEqualTo(prpdkind.getRiskcode());
		criteria.andRiskversionEqualTo(prpdkind.getRiskversion());
		criteria.andPlancodeEqualTo(prpdkind.getPlancode());
		return prpdkindMapper.selectByExample(example);
	}

	@Override
	public List<Prpdkind> select(Prpdkind prpdkind) {
		PrpdkindExample example = new PrpdkindExample();
		Criteria criteria = example.createCriteria();
		String kindCode = prpdkind.getKindcode();
		String kindVersion = prpdkind.getKindversion();
		String riskCode = prpdkind.getRiskcode();
		String riskVersion = prpdkind.getRiskversion();
		String planCode = prpdkind.getPlancode();
		String ownerRiskCode = prpdkind.getOwnerriskcode();
		if (!StringUtils.isEmpty(kindCode)) {
			criteria.andKindcodeEqualTo(kindCode);
		}
		if (!StringUtils.isEmpty(kindVersion)) {
			criteria.andKindversionEqualTo(kindVersion);
		}
		if (!StringUtils.isEmpty(riskCode)) {
			criteria.andRiskcodeEqualTo(riskCode);
		}
		if (!StringUtils.isEmpty(riskVersion)) {
			criteria.andRiskversionEqualTo(riskVersion);
		}
		if (!StringUtils.isEmpty(planCode)) {
			criteria.andPlancodeEqualTo(planCode);
		}
		if (!StringUtils.isEmpty(ownerRiskCode)) {
			criteria.andOwnerriskcodeEqualTo(ownerRiskCode);
		}
		return prpdkindMapper.selectByExample(example);
	}

	@Override
	public void delete(Prpdkind prpdkind) {
		PrpdkindExample example = new PrpdkindExample();
		Criteria criteria = example.createCriteria();
		String kindCode = prpdkind.getKindcode();
		String kindVersion = prpdkind.getKindversion();
		String riskCode = prpdkind.getRiskcode();
		String riskVersion = prpdkind.getRiskversion();
		String planCode = prpdkind.getPlancode();
		if (!StringUtils.isEmpty(kindCode)) {
			criteria.andKindcodeEqualTo(kindCode);
		}
		if (!StringUtils.isEmpty(kindVersion)) {
			criteria.andKindversionEqualTo(kindVersion);
		}
		if (!StringUtils.isEmpty(riskCode)) {
			criteria.andRiskcodeEqualTo(riskCode);
		}
		if (!StringUtils.isEmpty(riskVersion)) {
			criteria.andRiskversionEqualTo(riskVersion);
		}
		if (!StringUtils.isEmpty(planCode)) {
			criteria.andPlancodeEqualTo(planCode);
		}
		prpdkindMapper.deleteByExample(example);
	}

	@Override
	public boolean checkItemLinked(String kindCode) {
		List<Object> list = prpdkindMapper.checkItemLinked(kindCode);
		if (list.isEmpty()) {
			return false;
		}
		return true;
	}

	@Override
	public List<Prpdkindlibrary> findKindsByRisk(String riskCode, String riskVersion) {
		return prpdkindMapper.findKindsByRisk(riskCode, riskVersion);
	}
}
