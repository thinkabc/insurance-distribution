
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prptItemKindDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptItemKindDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amountMain" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="benchMarkPremiumMain" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="currencyMain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="discountMain" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="endHour" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="finalRateMain" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="flag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insureArea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="itemCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="itemCodeMain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="itemDetailName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="itemKindNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="itemNameMain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="kindCodeMain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="kindNameMain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modelMain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modelNameMain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modelNoMain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="occupationFlagMain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="premiumMain" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="quantityMain" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="startHour" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unitAmountMain" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="eFileCodeMain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eFileNameMain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptItemKindDto", propOrder = {
    "amountMain",
    "benchMarkPremiumMain",
    "currencyMain",
    "discountMain",
    "endDate",
    "endHour",
    "finalRateMain",
    "flag",
    "insureArea",
    "itemCode",
    "itemCodeMain",
    "itemDetailName",
    "itemKindNo",
    "itemNameMain",
    "kindCodeMain",
    "kindNameMain",
    "modeCode",
    "modeName",
    "modelMain",
    "modelNameMain",
    "modelNoMain",
    "occupationFlagMain",
    "premiumMain",
    "quantityMain",
    "startDate",
    "startHour",
    "unitAmountMain",
    "eFileCodeMain",
    "eFileNameMain"
})
public class PrptItemKindDto implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -959509709508995001L;
	protected double amountMain;
    protected double benchMarkPremiumMain;
    protected String currencyMain;
    protected double discountMain;
    protected String endDate;
    protected String endHour;
    protected double finalRateMain;
    protected String flag;
    protected String insureArea;
    protected String itemCode;
    protected String itemCodeMain;
    protected String itemDetailName;
    protected String itemKindNo;
    protected String itemNameMain;
    protected String kindCodeMain;
    protected String kindNameMain;
    protected String modeCode;
    protected String modeName;
    protected String modelMain;
    protected String modelNameMain;
    protected String modelNoMain;
    protected String occupationFlagMain;
    protected double premiumMain;
    protected double quantityMain;
    protected String startDate;
    protected String startHour;
    protected double unitAmountMain;
    protected String eFileCodeMain;
    protected String eFileNameMain;

    /**
     * Gets the value of the amountMain property.
     * 
     */
    public double getAmountMain() {
        return amountMain;
    }

    /**
     * Sets the value of the amountMain property.
     * 
     */
    public void setAmountMain(double value) {
        this.amountMain = value;
    }

    /**
     * Gets the value of the benchMarkPremiumMain property.
     * 
     */
    public double getBenchMarkPremiumMain() {
        return benchMarkPremiumMain;
    }

    /**
     * Sets the value of the benchMarkPremiumMain property.
     * 
     */
    public void setBenchMarkPremiumMain(double value) {
        this.benchMarkPremiumMain = value;
    }

    /**
     * Gets the value of the currencyMain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyMain() {
        return currencyMain;
    }

    /**
     * Sets the value of the currencyMain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyMain(String value) {
        this.currencyMain = value;
    }

    /**
     * Gets the value of the discountMain property.
     * 
     */
    public double getDiscountMain() {
        return discountMain;
    }

    /**
     * Sets the value of the discountMain property.
     * 
     */
    public void setDiscountMain(double value) {
        this.discountMain = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndDate(String value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the endHour property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndHour() {
        return endHour;
    }

    /**
     * Sets the value of the endHour property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndHour(String value) {
        this.endHour = value;
    }

    /**
     * Gets the value of the finalRateMain property.
     * 
     */
    public double getFinalRateMain() {
        return finalRateMain;
    }

    /**
     * Sets the value of the finalRateMain property.
     * 
     */
    public void setFinalRateMain(double value) {
        this.finalRateMain = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlag(String value) {
        this.flag = value;
    }

    /**
     * Gets the value of the insureArea property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsureArea() {
        return insureArea;
    }

    /**
     * Sets the value of the insureArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsureArea(String value) {
        this.insureArea = value;
    }

    /**
     * Gets the value of the itemCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * Sets the value of the itemCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemCode(String value) {
        this.itemCode = value;
    }

    /**
     * Gets the value of the itemCodeMain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemCodeMain() {
        return itemCodeMain;
    }

    /**
     * Sets the value of the itemCodeMain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemCodeMain(String value) {
        this.itemCodeMain = value;
    }

    /**
     * Gets the value of the itemDetailName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemDetailName() {
        return itemDetailName;
    }

    /**
     * Sets the value of the itemDetailName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemDetailName(String value) {
        this.itemDetailName = value;
    }

    /**
     * Gets the value of the itemKindNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemKindNo() {
        return itemKindNo;
    }

    /**
     * Sets the value of the itemKindNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemKindNo(String value) {
        this.itemKindNo = value;
    }

    /**
     * Gets the value of the itemNameMain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemNameMain() {
        return itemNameMain;
    }

    /**
     * Sets the value of the itemNameMain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemNameMain(String value) {
        this.itemNameMain = value;
    }

    /**
     * Gets the value of the kindCodeMain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKindCodeMain() {
        return kindCodeMain;
    }

    /**
     * Sets the value of the kindCodeMain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKindCodeMain(String value) {
        this.kindCodeMain = value;
    }

    /**
     * Gets the value of the kindNameMain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKindNameMain() {
        return kindNameMain;
    }

    /**
     * Sets the value of the kindNameMain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKindNameMain(String value) {
        this.kindNameMain = value;
    }

    /**
     * Gets the value of the modeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModeCode() {
        return modeCode;
    }

    /**
     * Sets the value of the modeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModeCode(String value) {
        this.modeCode = value;
    }

    /**
     * Gets the value of the modeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModeName() {
        return modeName;
    }

    /**
     * Sets the value of the modeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModeName(String value) {
        this.modeName = value;
    }

    /**
     * Gets the value of the modelMain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelMain() {
        return modelMain;
    }

    /**
     * Sets the value of the modelMain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelMain(String value) {
        this.modelMain = value;
    }

    /**
     * Gets the value of the modelNameMain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelNameMain() {
        return modelNameMain;
    }

    /**
     * Sets the value of the modelNameMain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelNameMain(String value) {
        this.modelNameMain = value;
    }

    /**
     * Gets the value of the modelNoMain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelNoMain() {
        return modelNoMain;
    }

    /**
     * Sets the value of the modelNoMain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelNoMain(String value) {
        this.modelNoMain = value;
    }

    /**
     * Gets the value of the occupationFlagMain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupationFlagMain() {
        return occupationFlagMain;
    }

    /**
     * Sets the value of the occupationFlagMain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupationFlagMain(String value) {
        this.occupationFlagMain = value;
    }

    /**
     * Gets the value of the premiumMain property.
     * 
     */
    public double getPremiumMain() {
        return premiumMain;
    }

    /**
     * Sets the value of the premiumMain property.
     * 
     */
    public void setPremiumMain(double value) {
        this.premiumMain = value;
    }

    /**
     * Gets the value of the quantityMain property.
     * 
     */
    public double getQuantityMain() {
        return quantityMain;
    }

    /**
     * Sets the value of the quantityMain property.
     * 
     */
    public void setQuantityMain(double value) {
        this.quantityMain = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartDate(String value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the startHour property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartHour() {
        return startHour;
    }

    /**
     * Sets the value of the startHour property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartHour(String value) {
        this.startHour = value;
    }

    /**
     * Gets the value of the unitAmountMain property.
     * 
     */
    public double getUnitAmountMain() {
        return unitAmountMain;
    }

    /**
     * Sets the value of the unitAmountMain property.
     * 
     */
    public void setUnitAmountMain(double value) {
        this.unitAmountMain = value;
    }

    /**
     * Gets the value of the eFileCodeMain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEFileCodeMain() {
        return eFileCodeMain;
    }

    /**
     * Sets the value of the eFileCodeMain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEFileCodeMain(String value) {
        this.eFileCodeMain = value;
    }

    /**
     * Gets the value of the eFileNameMain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEFileNameMain() {
        return eFileNameMain;
    }

    /**
     * Sets the value of the eFileNameMain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEFileNameMain(String value) {
        this.eFileNameMain = value;
    }

}
