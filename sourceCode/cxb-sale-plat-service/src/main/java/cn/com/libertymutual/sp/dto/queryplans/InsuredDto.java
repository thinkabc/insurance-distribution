package cn.com.libertymutual.sp.dto.queryplans;

public class InsuredDto {
	private String serialNo;//序号
	private String insuredType;//被保险人类型
	private String insuredName;//被保险人姓名
	private String identifyType;//被保险人证件类型
	private String identifyNumber;//被保险人证件号码
	private String insuredidentity;//与投保人关系
	private String linkMobile;//被保险人手机
	private String birth;//出生日期
	private String sex;//性别
	private String email;//被保险人邮箱地址
	private String benefType;//受益方式
	private String occupationCode;
	private String occupationName;
	
	public String getOccupationName() {
		return occupationName;
	}
	public void setOccupationName(String occupationName) {
		this.occupationName = occupationName;
	}
	public String getOccupationCode() {
		return occupationCode;
	}
	public void setOccupationCode(String occupationCode) {
		this.occupationCode = occupationCode;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getInsuredType() {
		return insuredType;
	}
	public void setInsuredType(String insuredType) {
		this.insuredType = insuredType;
	}
	public String getInsuredName() {
		return insuredName;
	}
	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}
	public String getIdentifyType() {
		return identifyType;
	}
	public void setIdentifyType(String identifyType) {
		this.identifyType = identifyType;
	}
	public String getIdentifyNumber() {
		return identifyNumber;
	}
	public void setIdentifyNumber(String identifyNumber) {
		this.identifyNumber = identifyNumber;
	}
	public String getInsuredidentity() {
		return insuredidentity;
	}
	public void setInsuredidentity(String insuredidentity) {
		this.insuredidentity = insuredidentity;
	}
	public String getLinkMobile() {
		return linkMobile;
	}
	public void setLinkMobile(String linkMobile) {
		this.linkMobile = linkMobile;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getBenefType() {
		return benefType;
	}
	public void setBenefType(String benefType) {
		this.benefType = benefType;
	}
	
	
}
