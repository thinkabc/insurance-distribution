package cn.com.libertymutual.production.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdserialnoMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdserialno;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdserialnoExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdserialnoExample.Criteria;
import cn.com.libertymutual.production.service.api.PrpdSerialNoService;

@Service
public class PrpdSerialNoServiceImpl implements PrpdSerialNoService {

	@Autowired
	private PrpdserialnoMapper prpdserialnoMapper;

	@Override
	public List<Prpdserialno> findPrpdSerialno(String businessType, String classCode, String serialType) {
		PrpdserialnoExample example = new PrpdserialnoExample();
		Criteria criteria = example.createCriteria();
		if(!StringUtils.isEmpty(businessType)){
			criteria.andBusinesstypeEqualTo(businessType);
		}
		if(!StringUtils.isEmpty(classCode)){
			criteria.andClasscodeEqualTo(classCode);
		}
		if(!StringUtils.isEmpty(serialType)){
			criteria.andSerialtypeEqualTo(serialType);
		}
		List<Prpdserialno> list =  prpdserialnoMapper.selectByExample(example);
		return list;
	}

	@Override
	public void insertPrpdSerialno(String businessType, String classCode, String serialType, int serialNo) {
		Prpdserialno record = new Prpdserialno();
		record.setBusinesstype(businessType);
		record.setClasscode(classCode);
		record.setSerialtype(serialType);
		record.setSerialno(serialNo);
		prpdserialnoMapper.insertSelective(record);
	}

	@Override
	public void updatePrpdSerialno(String businessType, String classCode, String serialType, int serialNo) {
		Prpdserialno prpdserialno = new Prpdserialno();
		prpdserialno.setSerialno(serialNo);
		PrpdserialnoExample example = new PrpdserialnoExample();
		Criteria criteria = example.createCriteria();
		criteria.andBusinesstypeEqualTo(businessType);
		if(!StringUtils.isEmpty(classCode)) {
			criteria.andClasscodeEqualTo(classCode);
		}
		if(!StringUtils.isEmpty(serialType)) {
			criteria.andSerialtypeEqualTo(serialType);
		}
		prpdserialnoMapper.updateByExampleSelective(prpdserialno, example);
	}

}
