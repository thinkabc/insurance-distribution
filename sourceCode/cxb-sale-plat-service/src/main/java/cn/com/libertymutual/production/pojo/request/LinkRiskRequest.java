package cn.com.libertymutual.production.pojo.request;

import java.util.List;

public class LinkRiskRequest extends Request {

	private PrpdKindLibraryRequest kind;
	private List<PrpdRiskRequest> selectedRows;
	private String shortratetype;

	public PrpdKindLibraryRequest getKind() {
		return kind;
	}

	public void setKind(PrpdKindLibraryRequest kind) {
		this.kind = kind;
	}

	public List<PrpdRiskRequest> getSelectedRows() {
		return selectedRows;
	}

	public void setSelectedRows(List<PrpdRiskRequest> selectedRows) {
		this.selectedRows = selectedRows;
	}

	public String getShortratetype() {
		return shortratetype;
	}

	public void setShortratetype(String shortratetype) {
		this.shortratetype = shortratetype;
	}
}
