package cn.com.libertymutual.sp.dto.car.bean;

import java.io.Serializable;

public class TpTitemKind   implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -7799435726161623600L;
	private int id;
    private String flowId;
    private String itemKindNo;
    private String kindCodeMain;
    private String kindNameMain;
    private String unitAmountMain;
    private String quantityMain;
    private String amountMain;
    private String benchMarkPremiumMain;
    private String premiumMain;
    private String modeCode;
    private String modeName;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getFlowId() {
        return flowId;
    }
    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }
    public String getItemKindNo() {
        return itemKindNo;
    }
    public void setItemKindNo(String itemKindNo) {
        this.itemKindNo = itemKindNo;
    }
    public String getKindCodeMain() {
        return kindCodeMain;
    }
    public void setKindCodeMain(String kindCodeMain) {
        this.kindCodeMain = kindCodeMain;
    }
    public String getKindNameMain() {
        return kindNameMain;
    }
    public void setKindNameMain(String kindNameMain) {
        this.kindNameMain = kindNameMain;
    }
    public String getUnitAmountMain() {
        return unitAmountMain;
    }
    public void setUnitAmountMain(String unitAmountMain) {
        this.unitAmountMain = unitAmountMain;
    }
    public String getQuantityMain() {
        return quantityMain;
    }
    public void setQuantityMain(String quantityMain) {
        this.quantityMain = quantityMain;
    }
    public String getAmountMain() {
        return amountMain;
    }
    public void setAmountMain(String amountMain) {
        this.amountMain = amountMain;
    }
    public String getBenchMarkPremiumMain() {
        return benchMarkPremiumMain;
    }
    public void setBenchMarkPremiumMain(String benchMarkPremiumMain) {
        this.benchMarkPremiumMain = benchMarkPremiumMain;
    }
    public String getPremiumMain() {
        return premiumMain;
    }
    public void setPremiumMain(String premiumMain) {
        this.premiumMain = premiumMain;
    }
    public String getModeCode() {
        return modeCode;
    }
    public void setModeCode(String modeCode) {
        this.modeCode = modeCode;
    }
    public String getModeName() {
        return modeName;
    }
    public void setModeName(String modeName) {
        this.modeName = modeName;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TpTitemKind that = (TpTitemKind) o;
        if (id != that.id) return false;
        if (flowId != null ? !flowId.equals(that.flowId) : that.flowId != null) return false;
        if (itemKindNo != null ? !itemKindNo.equals(that.itemKindNo) : that.itemKindNo != null) return false;
        if (kindCodeMain != null ? !kindCodeMain.equals(that.kindCodeMain) : that.kindCodeMain != null) return false;
        if (kindNameMain != null ? !kindNameMain.equals(that.kindNameMain) : that.kindNameMain != null) return false;
        if (unitAmountMain != null ? !unitAmountMain.equals(that.unitAmountMain) : that.unitAmountMain != null)
            return false;
        if (quantityMain != null ? !quantityMain.equals(that.quantityMain) : that.quantityMain != null) return false;
        if (amountMain != null ? !amountMain.equals(that.amountMain) : that.amountMain != null) return false;
        if (benchMarkPremiumMain != null ? !benchMarkPremiumMain.equals(that.benchMarkPremiumMain) : that.benchMarkPremiumMain != null)
            return false;
        if (premiumMain != null ? !premiumMain.equals(that.premiumMain) : that.premiumMain != null) return false;
        if (modeCode != null ? !modeCode.equals(that.modeCode) : that.modeCode != null) return false;
        if (modeName != null ? !modeName.equals(that.modeName) : that.modeName != null) return false;
        return true;
    }
    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (flowId != null ? flowId.hashCode() : 0);
        result = 31 * result + (itemKindNo != null ? itemKindNo.hashCode() : 0);
        result = 31 * result + (kindCodeMain != null ? kindCodeMain.hashCode() : 0);
        result = 31 * result + (kindNameMain != null ? kindNameMain.hashCode() : 0);
        result = 31 * result + (unitAmountMain != null ? unitAmountMain.hashCode() : 0);
        result = 31 * result + (quantityMain != null ? quantityMain.hashCode() : 0);
        result = 31 * result + (amountMain != null ? amountMain.hashCode() : 0);
        result = 31 * result + (benchMarkPremiumMain != null ? benchMarkPremiumMain.hashCode() : 0);
        result = 31 * result + (premiumMain != null ? premiumMain.hashCode() : 0);
        result = 31 * result + (modeCode != null ? modeCode.hashCode() : 0);
        result = 31 * result + (modeName != null ? modeName.hashCode() : 0);
        return result;
    }
	@Override
	public String toString() {
		return "TpTitemKind [id=" + id + ", flowId=" + flowId + ", itemKindNo="
				+ itemKindNo + ", kindCodeMain=" + kindCodeMain
				+ ", kindNameMain=" + kindNameMain + ", unitAmountMain="
				+ unitAmountMain + ", quantityMain=" + quantityMain
				+ ", amountMain=" + amountMain + ", benchMarkPremiumMain="
				+ benchMarkPremiumMain + ", premiumMain=" + premiumMain
				+ ", modeCode=" + modeCode + ", modeName=" + modeName + "]";
	}
}