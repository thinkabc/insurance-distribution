package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import cn.com.libertymutual.production.model.nomorcldatasource.PrpdItemRiskLibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkinditem;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkinditemExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkinditemKey;
@Mapper
public interface PrpdkinditemMapper {
    int countByExample(PrpdkinditemExample example);

    int deleteByExample(PrpdkinditemExample example);

    int deleteByPrimaryKey(PrpdkinditemKey key);

    int insert(Prpdkinditem record);

    int insertSelective(Prpdkinditem record);

    List<Prpdkinditem> selectByExample(PrpdkinditemExample example);

    Prpdkinditem selectByPrimaryKey(PrpdkinditemKey key);

    int updateByExampleSelective(@Param("record") Prpdkinditem record, @Param("example") PrpdkinditemExample example);

    int updateByExample(@Param("record") Prpdkinditem record, @Param("example") PrpdkinditemExample example);

    int updateByPrimaryKeySelective(Prpdkinditem record);

    int updateByPrimaryKey(Prpdkinditem record);
    
    @Select({"<script>",
		 "SELECT c.*,d.riskcode,d.riskversion FROM prpditemlibrary c,",
		 "(SELECT distinct a.itemcode,a.riskcode,a.riskversion FROM prpditem a ",
		 "WHERE 1=1 ",
		 "<when test='itemCode!=null'>",
		 		"and a.itemcode like '%'||#{itemCode}||'%' ",
		 "</when>",
		 "<when test='itemCName!=null'>",
		 		"and a.itemcname like '%'||#{itemCName}||'%' ",
		 "</when>",
		 "and exists (SELECT 1 FROM prpdkind b where b.kindcode = #{kindCode} and b.kindversion=#{kindVersion} and b.riskcode = a.riskcode)) d ",
		 "WHERE c.itemcode = d.itemcode ",
		 "<when test='validstatus == \"0\"'>",
		 	"AND C.VALIDSTATUS = #{validstatus} ",
		 "</when>",
		 "<when test='validstatus == \"1\"'>",
		 	"AND C.VALIDSTATUS != '0' ",
		 "</when>",
		 "AND (C.ID = '1' OR C.ID = '2' OR C.ID IS NULL) ",
		 "AND NOT EXISTS (SELECT 1 FROM PRPDKINDITEM E WHERE E.RISKCODE = D.RISKCODE AND E.RISKVERSION = D.RISKVERSION AND E.ITEMCODE = C.ITEMCODE AND E.KINDCODE = #{kindCode} AND E.KINDVERSION=#{kindVersion})",
	 	 "</script>"})
List<PrpdItemRiskLibrary> findItemCodesWithLinked(@Param("kindCode") String kindCode,
												  @Param("kindVersion") String kindVersion,
												  @Param("itemCode") String itemCode,
												  @Param("itemCName") String itemCName,
												  @Param("validstatus") String validstatus,
												  @Param("pageNum") int pageNum,
												  @Param("pageSize") int pageSize);
}