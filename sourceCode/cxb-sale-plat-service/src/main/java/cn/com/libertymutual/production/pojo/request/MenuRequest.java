package cn.com.libertymutual.production.pojo.request;

public class MenuRequest extends Request {

	private Integer menuid;

	private Integer fmenuid;

	private String menuname;

	private String menunameen;

	private String menuurl;

	private String stdcode;

	private String icon;

	private String target;

	private String isleaf;

	private Integer orderid;

	private String loadflag;

	public Integer getMenuid() {
		return menuid;
	}

	public void setMenuid(Integer menuid) {
		this.menuid = menuid;
	}

	public Integer getFmenuid() {
		return fmenuid;
	}

	public void setFmenuid(Integer fmenuid) {
		this.fmenuid = fmenuid;
	}

	public String getMenuname() {
		return menuname;
	}

	public void setMenuname(String menuname) {
		this.menuname = menuname;
	}

	public String getMenunameen() {
		return menunameen;
	}

	public void setMenunameen(String menunameen) {
		this.menunameen = menunameen;
	}

	public String getMenuurl() {
		return menuurl;
	}

	public void setMenuurl(String menuurl) {
		this.menuurl = menuurl;
	}

	public String getStdcode() {
		return stdcode;
	}

	public void setStdcode(String stdcode) {
		this.stdcode = stdcode;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getIsleaf() {
		return isleaf;
	}

	public void setIsleaf(String isleaf) {
		this.isleaf = isleaf;
	}

	public Integer getOrderid() {
		return orderid;
	}

	public void setOrderid(Integer orderid) {
		this.orderid = orderid;
	}

	public String getLoadflag() {
		return loadflag;
	}

	public void setLoadflag(String loadflag) {
		this.loadflag = loadflag;
	}

}
