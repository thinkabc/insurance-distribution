package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "tb_sp_flowdetail")
public class TbSpFlowDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7474847352141387292L;
	private Integer id;
	private String flowNode;// 节点编号
	private String flowNo;
	private String flowName;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTIime;
	private String userCode;
	private String roleCode;
	private String updateFlag;
	private String parentNode;// 父节点
	private String flowCondition;// 条件转出脚本
	private List<TbSpFlowDetail> nextNodes;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "FLOW_NODE")
	public String getFlowNode() {
		return flowNode;
	}

	public void setFlowNode(String flowNode) {
		this.flowNode = flowNode;
	}

	@Column(name = "FLOW_NO")
	public String getFlowNo() {
		return flowNo;
	}

	public void setFlowNo(String flowNo) {
		this.flowNo = flowNo;
	}

	@Column(name = "FLOW_NAME")
	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	@Column(name = "CREATE_TIME")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "UPDATE_TIME")
	public Date getUpdateTIime() {
		return updateTIime;
	}

	public void setUpdateTIime(Date updateTIime) {
		this.updateTIime = updateTIime;
	}

	@Column(name = "USER_CODE")
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	@Column(name = "ROLE_CODE")
	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	@Column(name = "UPDATE_FLAG")
	public String getUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(String updateFlag) {
		this.updateFlag = updateFlag;
	}

	@Column(name = "PARENT_NODE")
	public String getParentNode() {
		return parentNode;
	}

	public void setParentNode(String parentNode) {
		this.parentNode = parentNode;
	}

	@Column(name = "FLOW_CONDITION")
	public String getFlowCondition() {
		return flowCondition;
	}

	public void setFlowCondition(String flowCondition) {
		this.flowCondition = flowCondition;
	}

	@Transient
	public List<TbSpFlowDetail> getNextNodes() {
		return nextNodes;
	}

	public void setNextNodes(List<TbSpFlowDetail> nextNodes) {
		this.nextNodes = nextNodes;
	}

	private String username;
	@Transient
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
	
	
}
