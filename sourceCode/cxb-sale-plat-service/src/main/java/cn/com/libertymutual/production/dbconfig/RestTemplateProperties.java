package cn.com.libertymutual.production.dbconfig;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

//@Configuration("restTemplateProperties2")
//@ConfigurationProperties(prefix="spring.http.rest")
public class RestTemplateProperties {

	
	private int connectTimeout = 30000;
	private int readTimeout = 60000;
	private RestPollingConnection pollingConnection;
	private boolean bufferRequestBody = true;
	

	public int getConnectTimeout() {
		return connectTimeout;
	}


	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}


	public int getReadTimeout() {
		return readTimeout;
	}


	public void setReadTimeout(int readTimeout) {
		this.readTimeout = readTimeout;
	}


	public boolean isBufferRequestBody() {
		return bufferRequestBody;
	}


	public void setBufferRequestBody(boolean bufferRequestBody) {
		this.bufferRequestBody = bufferRequestBody;
	}


	public RestPollingConnection getPollingConnection() {
		if( pollingConnection == null ) {
			pollingConnection = new RestPollingConnection(); 
		}
		return pollingConnection;
	}


	public void setPollingConnection(RestPollingConnection pollingConnection) {
		this.pollingConnection = pollingConnection;
	}
	
}
