package cn.com.libertymutual.production.pojo.request;


public class PrpdKindItemRequest extends Request {
	private String validstatus;
	
	private String flag;
	 
	private String kindcode;

    private String kindversion;

    private String riskcode;

    private String riskversion;

    private String plancode;

    private String itemcode;

	public String getValidstatus() {
		return validstatus;
	}

	public void setValidstatus(String validstatus) {
		this.validstatus = validstatus;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getKindcode() {
		return kindcode;
	}

	public void setKindcode(String kindcode) {
		this.kindcode = kindcode;
	}

	public String getKindversion() {
		return kindversion;
	}

	public void setKindversion(String kindversion) {
		this.kindversion = kindversion;
	}

	public String getRiskcode() {
		return riskcode;
	}

	public void setRiskcode(String riskcode) {
		this.riskcode = riskcode;
	}

	public String getRiskversion() {
		return riskversion;
	}

	public void setRiskversion(String riskversion) {
		this.riskversion = riskversion;
	}

	public String getPlancode() {
		return plancode;
	}

	public void setPlancode(String plancode) {
		this.plancode = plancode;
	}

	public String getItemcode() {
		return itemcode;
	}

	public void setItemcode(String itemcode) {
		this.itemcode = itemcode;
	}
    
}
