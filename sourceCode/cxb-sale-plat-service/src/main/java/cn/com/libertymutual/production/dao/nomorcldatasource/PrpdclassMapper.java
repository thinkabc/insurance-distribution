package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdclass;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdclassExample;
@Mapper
public interface PrpdclassMapper {
    int countByExample(PrpdclassExample example);

    int deleteByExample(PrpdclassExample example);

    int deleteByPrimaryKey(String classcode);

    int insert(Prpdclass record);

    int insertSelective(Prpdclass record);

    List<Prpdclass> selectByExample(PrpdclassExample example);

    Prpdclass selectByPrimaryKey(String classcode);

    int updateByExampleSelective(@Param("record") Prpdclass record, @Param("example") PrpdclassExample example);

    int updateByExample(@Param("record") Prpdclass record, @Param("example") PrpdclassExample example);

    int updateByPrimaryKeySelective(Prpdclass record);

    int updateByPrimaryKey(Prpdclass record);
}