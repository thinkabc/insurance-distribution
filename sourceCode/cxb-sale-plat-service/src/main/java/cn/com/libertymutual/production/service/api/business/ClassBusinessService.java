package cn.com.libertymutual.production.service.api.business;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.libertymutual.production.pojo.request.PrpdClassRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.PrpdClassService;
import cn.com.libertymutual.production.service.impl.SystemLog;
import cn.com.libertymutual.production.utils.Constant;

/** 
 * @Description: 后台业务逻辑入口
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
public abstract class ClassBusinessService {

	protected static final Logger log = Constant.log;
	
	@Autowired
	protected SystemLog systemLog;
	@Autowired
	protected PrpdClassService prpdClassService;
	
	/**
	 * 按分页查询险类信息
	 * @param request
	 * @return
	 */
	public abstract Response findPrpdClass(PrpdClassRequest request);
	
	/**
	 * 校验险类代码是否使用
	 * @param classcode
	 * @return
	 */
	public abstract Response checkClassCodeUsed(String classcode);
	
	/**
	 * 新增险别
	 * @return
	 * @throws Exception
	 */
	public abstract Response insert(PrpdClassRequest request) throws Exception;
	
	/**
	 * 修改险类
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public abstract Response update(PrpdClassRequest request) throws Exception;

}