package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sp.bean.TbSpHotproduct;

@Repository
public interface HotProductDao extends PagingAndSortingRepository<TbSpHotproduct, Integer>, JpaSpecificationExecutor<TbSpHotproduct>
{

	@Transactional
	@Modifying
	@Query("update TbSpHotproduct set status = 0 where branchCode = ?1 and productId = ?2")
	void updateStatus(String branchCode,int productId);

	
	TbSpHotproduct findByProductId(int productId);

	@Query("select distinct t.branchCode  from TbSpHotproduct t ")
	List<String> findBranchList();

	@Query("select t from TbSpHotproduct t where t.branchCode = ?1 and status = '1' ")
	List<TbSpHotproduct> findByBranchCodeAndStatus(String branchCode);


	TbSpHotproduct findByProductIdAndBranchCode(Integer productId, String branchCode);

	
	
}

