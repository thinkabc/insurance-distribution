package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.dto.UserDto;
import cn.com.libertymutual.sp.req.StoreRateReq;
import cn.com.libertymutual.sys.bean.SysBranch;

public interface LevelService {

	ServiceResult levelList(String branchNo, String branchCname);

	ServiceResult nextChannelList(String mobile,String istopUser,String branchNo,String channelId,String channelName,Integer pageSize,Integer pageNumber);

	ServiceResult createChannelUser(UserDto userDto);
	
	ServiceResult updateChannelUser(UserDto userDto);

	ServiceResult isMergeUser(String mobile,String comCode);

	ServiceResult treeNextChannelList(String businessNo,String type);

	ServiceResult thisChannel(String userCode);

	ServiceResult orderAuth(String userId,String type);

	ServiceResult authUserList(String userId, String mobile, String type, int pageNumber, int pageSize);

	ServiceResult updateOrderAuth(String[] list,String userCode);

	ServiceResult topChannelUser(String userCode);

	ServiceResult isHasIdnumber(String idNumber);

	ServiceResult setShopPro(StoreRateReq store);

	ServiceResult branchManage(SysBranch sysBranch);

	ServiceResult nextBranchList(String branchNo,String nextBranchNo,String nextBranchName,int pageNumber,int pageSize);

	ServiceResult treeNextBranchList(String branchNo);
}
