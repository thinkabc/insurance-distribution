package cn.com.libertymutual.sp.dao.car;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.com.libertymutual.sp.bean.car.PrpKindRelation;

public interface PrpKindRelationDao extends PagingAndSortingRepository<PrpKindRelation, Integer>, JpaSpecificationExecutor<PrpKindRelation> {
	@Query("from PrpKindRelation where isShow = '1' and riskCode=?1 order by serialNo asc")
	List<PrpKindRelation> findRelation(String riskCode);
}
