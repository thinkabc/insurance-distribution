package cn.com.libertymutual.sp.dto;

public class RequestPayDto {
	/**
	 * 
	 */

	private String documentNo;
	private String riskCode;
	private String partnerAccountCode;
	private String flowId;
	private String operatorDate;
	private String recordCode;
	private String 	agreementNo;
	private String access_token;
	
	public String getRecordCode() {
		return recordCode;
	}
	public void setRecordCode(String recordCode) {
		this.recordCode = recordCode;
	}
	public String getAgreementNo() {
		return agreementNo;
	}
	public void setAgreementNo(String agreementNo) {
		this.agreementNo = agreementNo;
	}
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	public String getDocumentNo() {
		return documentNo;
	}
	public String getPartnerAccountCode() {
		return partnerAccountCode;
	}
	public void setPartnerAccountCode(String partnerAccountCode) {
		this.partnerAccountCode = partnerAccountCode;
	}
	public String getFlowId() {
		return flowId;
	}
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}
	public String getOperatorDate() {
		return operatorDate;
	}
	public void setOperatorDate(String operatorDate) {
		this.operatorDate = operatorDate;
	}
	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}
	public String getRiskCode() {
		return riskCode;
	}
	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}
	@Override
	public String toString() {
		return "RequestPayDto [documentNo=" + documentNo + ", riskCode="
				+ riskCode + ", partnerAccountCode=" + partnerAccountCode
				+ ", flowId=" + flowId + ", operatorDate=" + operatorDate
				+ ", recordCode=" + recordCode + ", agreementNo=" + agreementNo
				+ ", access_token=" + access_token + "]";
	}
	
	
}
