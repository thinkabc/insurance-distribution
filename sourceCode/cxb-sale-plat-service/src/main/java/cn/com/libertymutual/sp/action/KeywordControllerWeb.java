package cn.com.libertymutual.sp.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpKeywordWeChat;
import cn.com.libertymutual.sp.service.api.KeywordService;
import cn.com.libertymutual.sp.service.api.KeywordWeChatService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/admin/keyword")
public class KeywordControllerWeb {

	@Autowired
	private KeywordService keywordService;

	@Autowired
	private KeywordWeChatService keywordWeChatService;

	/*
	 * 店铺不可输入的名字列表
	 */
	@ApiOperation(value = "店铺不可输入的名字列表", notes = "店铺不可输入的名字列表")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "type", value = "类型", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long") })
	@RequestMapping(value = "/allKeywords", method = RequestMethod.POST)
	public ServiceResult allKeywords(String type, Integer pageNumber, Integer pageSize) {

		return keywordService.allKeywords(type, pageNumber, pageSize);
	}

	/*
	 * 新增或者修改
	 */
	@ApiOperation(value = "新增或者修改", notes = "新增或者修改")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "id", value = "类型", required = false, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "type", value = "类型", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "content", value = "内容", required = true, paramType = "query", dataType = "String"), })
	@RequestMapping(value = "/addOrUpdateKeyword", method = RequestMethod.POST)
	public ServiceResult addOrUpdateKeyword(Integer id, String type, String content) {

		return keywordService.addOrUpdateKeyword(id, type, content);
	}

	@ApiOperation(value = "新增微信关键词", notes = "测试")
	@RequestMapping(value = "/addKeywordWeChat", method = RequestMethod.POST)
	public ServiceResult addKeywordWeChat(@RequestBody TbSpKeywordWeChat keywordWeChat) {
		try {
			return keywordWeChatService.insertKeyWordwechat(keywordWeChat);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ServiceResult("新增异常", ServiceResult.STATE_EXCEPTION);
	}

	@ApiOperation(value = "修改微信关键词", notes = "测试")
	@RequestMapping(value = "/updateKeywordWeChat", method = RequestMethod.POST)
	public ServiceResult updateKeywordWeChat(@RequestBody TbSpKeywordWeChat keywordWeChat) {
		try {
			return keywordWeChatService.updateKeyWordwechat(keywordWeChat);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ServiceResult("修改异常", ServiceResult.STATE_EXCEPTION);
	}
	
	
	/*
	 * 回复关键字列表
	 */
	@ApiOperation(value = "回复关键字列表", notes = "回复关键字列表")
	@ApiImplicitParams(value = { 
		@ApiImplicitParam(name = "keyName", value = "关键词标识", required = true, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "keyWord", value = "关键词", required = true, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "type", value = "类型", required = true, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
		@ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long") })
	@RequestMapping(value = "/weChatKeywords", method = RequestMethod.POST)
	public ServiceResult weChatKeywords(String keyName, String keyWord, String type, Integer pageNumber, Integer pageSize) {

		return keywordWeChatService.weChatKeywords(keyName,keyWord,type, pageNumber, pageSize);
	}


}
