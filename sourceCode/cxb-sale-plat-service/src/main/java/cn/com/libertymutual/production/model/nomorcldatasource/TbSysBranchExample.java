package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.ArrayList;
import java.util.List;

public class TbSysBranchExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TbSysBranchExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andBranchNoIsNull() {
            addCriterion("BRANCH_NO is null");
            return (Criteria) this;
        }

        public Criteria andBranchNoIsNotNull() {
            addCriterion("BRANCH_NO is not null");
            return (Criteria) this;
        }

        public Criteria andBranchNoEqualTo(String value) {
            addCriterion("BRANCH_NO =", value, "branchNo");
            return (Criteria) this;
        }

        public Criteria andBranchNoNotEqualTo(String value) {
            addCriterion("BRANCH_NO <>", value, "branchNo");
            return (Criteria) this;
        }

        public Criteria andBranchNoGreaterThan(String value) {
            addCriterion("BRANCH_NO >", value, "branchNo");
            return (Criteria) this;
        }

        public Criteria andBranchNoGreaterThanOrEqualTo(String value) {
            addCriterion("BRANCH_NO >=", value, "branchNo");
            return (Criteria) this;
        }

        public Criteria andBranchNoLessThan(String value) {
            addCriterion("BRANCH_NO <", value, "branchNo");
            return (Criteria) this;
        }

        public Criteria andBranchNoLessThanOrEqualTo(String value) {
            addCriterion("BRANCH_NO <=", value, "branchNo");
            return (Criteria) this;
        }

        public Criteria andBranchNoLike(String value) {
            addCriterion("BRANCH_NO like", value, "branchNo");
            return (Criteria) this;
        }

        public Criteria andBranchNoNotLike(String value) {
            addCriterion("BRANCH_NO not like", value, "branchNo");
            return (Criteria) this;
        }

        public Criteria andBranchNoIn(List<String> values) {
            addCriterion("BRANCH_NO in", values, "branchNo");
            return (Criteria) this;
        }

        public Criteria andBranchNoNotIn(List<String> values) {
            addCriterion("BRANCH_NO not in", values, "branchNo");
            return (Criteria) this;
        }

        public Criteria andBranchNoBetween(String value1, String value2) {
            addCriterion("BRANCH_NO between", value1, value2, "branchNo");
            return (Criteria) this;
        }

        public Criteria andBranchNoNotBetween(String value1, String value2) {
            addCriterion("BRANCH_NO not between", value1, value2, "branchNo");
            return (Criteria) this;
        }

        public Criteria andBranchCnameIsNull() {
            addCriterion("BRANCH_CNAME is null");
            return (Criteria) this;
        }

        public Criteria andBranchCnameIsNotNull() {
            addCriterion("BRANCH_CNAME is not null");
            return (Criteria) this;
        }

        public Criteria andBranchCnameEqualTo(String value) {
            addCriterion("BRANCH_CNAME =", value, "branchCname");
            return (Criteria) this;
        }

        public Criteria andBranchCnameNotEqualTo(String value) {
            addCriterion("BRANCH_CNAME <>", value, "branchCname");
            return (Criteria) this;
        }

        public Criteria andBranchCnameGreaterThan(String value) {
            addCriterion("BRANCH_CNAME >", value, "branchCname");
            return (Criteria) this;
        }

        public Criteria andBranchCnameGreaterThanOrEqualTo(String value) {
            addCriterion("BRANCH_CNAME >=", value, "branchCname");
            return (Criteria) this;
        }

        public Criteria andBranchCnameLessThan(String value) {
            addCriterion("BRANCH_CNAME <", value, "branchCname");
            return (Criteria) this;
        }

        public Criteria andBranchCnameLessThanOrEqualTo(String value) {
            addCriterion("BRANCH_CNAME <=", value, "branchCname");
            return (Criteria) this;
        }

        public Criteria andBranchCnameLike(String value) {
            addCriterion("BRANCH_CNAME like", value, "branchCname");
            return (Criteria) this;
        }

        public Criteria andBranchCnameNotLike(String value) {
            addCriterion("BRANCH_CNAME not like", value, "branchCname");
            return (Criteria) this;
        }

        public Criteria andBranchCnameIn(List<String> values) {
            addCriterion("BRANCH_CNAME in", values, "branchCname");
            return (Criteria) this;
        }

        public Criteria andBranchCnameNotIn(List<String> values) {
            addCriterion("BRANCH_CNAME not in", values, "branchCname");
            return (Criteria) this;
        }

        public Criteria andBranchCnameBetween(String value1, String value2) {
            addCriterion("BRANCH_CNAME between", value1, value2, "branchCname");
            return (Criteria) this;
        }

        public Criteria andBranchCnameNotBetween(String value1, String value2) {
            addCriterion("BRANCH_CNAME not between", value1, value2, "branchCname");
            return (Criteria) this;
        }

        public Criteria andBranchEnameIsNull() {
            addCriterion("BRANCH_ENAME is null");
            return (Criteria) this;
        }

        public Criteria andBranchEnameIsNotNull() {
            addCriterion("BRANCH_ENAME is not null");
            return (Criteria) this;
        }

        public Criteria andBranchEnameEqualTo(String value) {
            addCriterion("BRANCH_ENAME =", value, "branchEname");
            return (Criteria) this;
        }

        public Criteria andBranchEnameNotEqualTo(String value) {
            addCriterion("BRANCH_ENAME <>", value, "branchEname");
            return (Criteria) this;
        }

        public Criteria andBranchEnameGreaterThan(String value) {
            addCriterion("BRANCH_ENAME >", value, "branchEname");
            return (Criteria) this;
        }

        public Criteria andBranchEnameGreaterThanOrEqualTo(String value) {
            addCriterion("BRANCH_ENAME >=", value, "branchEname");
            return (Criteria) this;
        }

        public Criteria andBranchEnameLessThan(String value) {
            addCriterion("BRANCH_ENAME <", value, "branchEname");
            return (Criteria) this;
        }

        public Criteria andBranchEnameLessThanOrEqualTo(String value) {
            addCriterion("BRANCH_ENAME <=", value, "branchEname");
            return (Criteria) this;
        }

        public Criteria andBranchEnameLike(String value) {
            addCriterion("BRANCH_ENAME like", value, "branchEname");
            return (Criteria) this;
        }

        public Criteria andBranchEnameNotLike(String value) {
            addCriterion("BRANCH_ENAME not like", value, "branchEname");
            return (Criteria) this;
        }

        public Criteria andBranchEnameIn(List<String> values) {
            addCriterion("BRANCH_ENAME in", values, "branchEname");
            return (Criteria) this;
        }

        public Criteria andBranchEnameNotIn(List<String> values) {
            addCriterion("BRANCH_ENAME not in", values, "branchEname");
            return (Criteria) this;
        }

        public Criteria andBranchEnameBetween(String value1, String value2) {
            addCriterion("BRANCH_ENAME between", value1, value2, "branchEname");
            return (Criteria) this;
        }

        public Criteria andBranchEnameNotBetween(String value1, String value2) {
            addCriterion("BRANCH_ENAME not between", value1, value2, "branchEname");
            return (Criteria) this;
        }

        public Criteria andAddressCnameIsNull() {
            addCriterion("ADDRESS_CNAME is null");
            return (Criteria) this;
        }

        public Criteria andAddressCnameIsNotNull() {
            addCriterion("ADDRESS_CNAME is not null");
            return (Criteria) this;
        }

        public Criteria andAddressCnameEqualTo(String value) {
            addCriterion("ADDRESS_CNAME =", value, "addressCname");
            return (Criteria) this;
        }

        public Criteria andAddressCnameNotEqualTo(String value) {
            addCriterion("ADDRESS_CNAME <>", value, "addressCname");
            return (Criteria) this;
        }

        public Criteria andAddressCnameGreaterThan(String value) {
            addCriterion("ADDRESS_CNAME >", value, "addressCname");
            return (Criteria) this;
        }

        public Criteria andAddressCnameGreaterThanOrEqualTo(String value) {
            addCriterion("ADDRESS_CNAME >=", value, "addressCname");
            return (Criteria) this;
        }

        public Criteria andAddressCnameLessThan(String value) {
            addCriterion("ADDRESS_CNAME <", value, "addressCname");
            return (Criteria) this;
        }

        public Criteria andAddressCnameLessThanOrEqualTo(String value) {
            addCriterion("ADDRESS_CNAME <=", value, "addressCname");
            return (Criteria) this;
        }

        public Criteria andAddressCnameLike(String value) {
            addCriterion("ADDRESS_CNAME like", value, "addressCname");
            return (Criteria) this;
        }

        public Criteria andAddressCnameNotLike(String value) {
            addCriterion("ADDRESS_CNAME not like", value, "addressCname");
            return (Criteria) this;
        }

        public Criteria andAddressCnameIn(List<String> values) {
            addCriterion("ADDRESS_CNAME in", values, "addressCname");
            return (Criteria) this;
        }

        public Criteria andAddressCnameNotIn(List<String> values) {
            addCriterion("ADDRESS_CNAME not in", values, "addressCname");
            return (Criteria) this;
        }

        public Criteria andAddressCnameBetween(String value1, String value2) {
            addCriterion("ADDRESS_CNAME between", value1, value2, "addressCname");
            return (Criteria) this;
        }

        public Criteria andAddressCnameNotBetween(String value1, String value2) {
            addCriterion("ADDRESS_CNAME not between", value1, value2, "addressCname");
            return (Criteria) this;
        }

        public Criteria andAddressEnameIsNull() {
            addCriterion("ADDRESS_ENAME is null");
            return (Criteria) this;
        }

        public Criteria andAddressEnameIsNotNull() {
            addCriterion("ADDRESS_ENAME is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEnameEqualTo(String value) {
            addCriterion("ADDRESS_ENAME =", value, "addressEname");
            return (Criteria) this;
        }

        public Criteria andAddressEnameNotEqualTo(String value) {
            addCriterion("ADDRESS_ENAME <>", value, "addressEname");
            return (Criteria) this;
        }

        public Criteria andAddressEnameGreaterThan(String value) {
            addCriterion("ADDRESS_ENAME >", value, "addressEname");
            return (Criteria) this;
        }

        public Criteria andAddressEnameGreaterThanOrEqualTo(String value) {
            addCriterion("ADDRESS_ENAME >=", value, "addressEname");
            return (Criteria) this;
        }

        public Criteria andAddressEnameLessThan(String value) {
            addCriterion("ADDRESS_ENAME <", value, "addressEname");
            return (Criteria) this;
        }

        public Criteria andAddressEnameLessThanOrEqualTo(String value) {
            addCriterion("ADDRESS_ENAME <=", value, "addressEname");
            return (Criteria) this;
        }

        public Criteria andAddressEnameLike(String value) {
            addCriterion("ADDRESS_ENAME like", value, "addressEname");
            return (Criteria) this;
        }

        public Criteria andAddressEnameNotLike(String value) {
            addCriterion("ADDRESS_ENAME not like", value, "addressEname");
            return (Criteria) this;
        }

        public Criteria andAddressEnameIn(List<String> values) {
            addCriterion("ADDRESS_ENAME in", values, "addressEname");
            return (Criteria) this;
        }

        public Criteria andAddressEnameNotIn(List<String> values) {
            addCriterion("ADDRESS_ENAME not in", values, "addressEname");
            return (Criteria) this;
        }

        public Criteria andAddressEnameBetween(String value1, String value2) {
            addCriterion("ADDRESS_ENAME between", value1, value2, "addressEname");
            return (Criteria) this;
        }

        public Criteria andAddressEnameNotBetween(String value1, String value2) {
            addCriterion("ADDRESS_ENAME not between", value1, value2, "addressEname");
            return (Criteria) this;
        }

        public Criteria andPostcodeIsNull() {
            addCriterion("POSTCODE is null");
            return (Criteria) this;
        }

        public Criteria andPostcodeIsNotNull() {
            addCriterion("POSTCODE is not null");
            return (Criteria) this;
        }

        public Criteria andPostcodeEqualTo(String value) {
            addCriterion("POSTCODE =", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeNotEqualTo(String value) {
            addCriterion("POSTCODE <>", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeGreaterThan(String value) {
            addCriterion("POSTCODE >", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeGreaterThanOrEqualTo(String value) {
            addCriterion("POSTCODE >=", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeLessThan(String value) {
            addCriterion("POSTCODE <", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeLessThanOrEqualTo(String value) {
            addCriterion("POSTCODE <=", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeLike(String value) {
            addCriterion("POSTCODE like", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeNotLike(String value) {
            addCriterion("POSTCODE not like", value, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeIn(List<String> values) {
            addCriterion("POSTCODE in", values, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeNotIn(List<String> values) {
            addCriterion("POSTCODE not in", values, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeBetween(String value1, String value2) {
            addCriterion("POSTCODE between", value1, value2, "postcode");
            return (Criteria) this;
        }

        public Criteria andPostcodeNotBetween(String value1, String value2) {
            addCriterion("POSTCODE not between", value1, value2, "postcode");
            return (Criteria) this;
        }

        public Criteria andPhoneNumberIsNull() {
            addCriterion("PHONE_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andPhoneNumberIsNotNull() {
            addCriterion("PHONE_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneNumberEqualTo(String value) {
            addCriterion("PHONE_NUMBER =", value, "phoneNumber");
            return (Criteria) this;
        }

        public Criteria andPhoneNumberNotEqualTo(String value) {
            addCriterion("PHONE_NUMBER <>", value, "phoneNumber");
            return (Criteria) this;
        }

        public Criteria andPhoneNumberGreaterThan(String value) {
            addCriterion("PHONE_NUMBER >", value, "phoneNumber");
            return (Criteria) this;
        }

        public Criteria andPhoneNumberGreaterThanOrEqualTo(String value) {
            addCriterion("PHONE_NUMBER >=", value, "phoneNumber");
            return (Criteria) this;
        }

        public Criteria andPhoneNumberLessThan(String value) {
            addCriterion("PHONE_NUMBER <", value, "phoneNumber");
            return (Criteria) this;
        }

        public Criteria andPhoneNumberLessThanOrEqualTo(String value) {
            addCriterion("PHONE_NUMBER <=", value, "phoneNumber");
            return (Criteria) this;
        }

        public Criteria andPhoneNumberLike(String value) {
            addCriterion("PHONE_NUMBER like", value, "phoneNumber");
            return (Criteria) this;
        }

        public Criteria andPhoneNumberNotLike(String value) {
            addCriterion("PHONE_NUMBER not like", value, "phoneNumber");
            return (Criteria) this;
        }

        public Criteria andPhoneNumberIn(List<String> values) {
            addCriterion("PHONE_NUMBER in", values, "phoneNumber");
            return (Criteria) this;
        }

        public Criteria andPhoneNumberNotIn(List<String> values) {
            addCriterion("PHONE_NUMBER not in", values, "phoneNumber");
            return (Criteria) this;
        }

        public Criteria andPhoneNumberBetween(String value1, String value2) {
            addCriterion("PHONE_NUMBER between", value1, value2, "phoneNumber");
            return (Criteria) this;
        }

        public Criteria andPhoneNumberNotBetween(String value1, String value2) {
            addCriterion("PHONE_NUMBER not between", value1, value2, "phoneNumber");
            return (Criteria) this;
        }

        public Criteria andUpperBranchNoIsNull() {
            addCriterion("UPPER_BRANCH_NO is null");
            return (Criteria) this;
        }

        public Criteria andUpperBranchNoIsNotNull() {
            addCriterion("UPPER_BRANCH_NO is not null");
            return (Criteria) this;
        }

        public Criteria andUpperBranchNoEqualTo(String value) {
            addCriterion("UPPER_BRANCH_NO =", value, "upperBranchNo");
            return (Criteria) this;
        }

        public Criteria andUpperBranchNoNotEqualTo(String value) {
            addCriterion("UPPER_BRANCH_NO <>", value, "upperBranchNo");
            return (Criteria) this;
        }

        public Criteria andUpperBranchNoGreaterThan(String value) {
            addCriterion("UPPER_BRANCH_NO >", value, "upperBranchNo");
            return (Criteria) this;
        }

        public Criteria andUpperBranchNoGreaterThanOrEqualTo(String value) {
            addCriterion("UPPER_BRANCH_NO >=", value, "upperBranchNo");
            return (Criteria) this;
        }

        public Criteria andUpperBranchNoLessThan(String value) {
            addCriterion("UPPER_BRANCH_NO <", value, "upperBranchNo");
            return (Criteria) this;
        }

        public Criteria andUpperBranchNoLessThanOrEqualTo(String value) {
            addCriterion("UPPER_BRANCH_NO <=", value, "upperBranchNo");
            return (Criteria) this;
        }

        public Criteria andUpperBranchNoLike(String value) {
            addCriterion("UPPER_BRANCH_NO like", value, "upperBranchNo");
            return (Criteria) this;
        }

        public Criteria andUpperBranchNoNotLike(String value) {
            addCriterion("UPPER_BRANCH_NO not like", value, "upperBranchNo");
            return (Criteria) this;
        }

        public Criteria andUpperBranchNoIn(List<String> values) {
            addCriterion("UPPER_BRANCH_NO in", values, "upperBranchNo");
            return (Criteria) this;
        }

        public Criteria andUpperBranchNoNotIn(List<String> values) {
            addCriterion("UPPER_BRANCH_NO not in", values, "upperBranchNo");
            return (Criteria) this;
        }

        public Criteria andUpperBranchNoBetween(String value1, String value2) {
            addCriterion("UPPER_BRANCH_NO between", value1, value2, "upperBranchNo");
            return (Criteria) this;
        }

        public Criteria andUpperBranchNoNotBetween(String value1, String value2) {
            addCriterion("UPPER_BRANCH_NO not between", value1, value2, "upperBranchNo");
            return (Criteria) this;
        }

        public Criteria andBranchTypeIsNull() {
            addCriterion("BRANCH_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andBranchTypeIsNotNull() {
            addCriterion("BRANCH_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andBranchTypeEqualTo(String value) {
            addCriterion("BRANCH_TYPE =", value, "branchType");
            return (Criteria) this;
        }

        public Criteria andBranchTypeNotEqualTo(String value) {
            addCriterion("BRANCH_TYPE <>", value, "branchType");
            return (Criteria) this;
        }

        public Criteria andBranchTypeGreaterThan(String value) {
            addCriterion("BRANCH_TYPE >", value, "branchType");
            return (Criteria) this;
        }

        public Criteria andBranchTypeGreaterThanOrEqualTo(String value) {
            addCriterion("BRANCH_TYPE >=", value, "branchType");
            return (Criteria) this;
        }

        public Criteria andBranchTypeLessThan(String value) {
            addCriterion("BRANCH_TYPE <", value, "branchType");
            return (Criteria) this;
        }

        public Criteria andBranchTypeLessThanOrEqualTo(String value) {
            addCriterion("BRANCH_TYPE <=", value, "branchType");
            return (Criteria) this;
        }

        public Criteria andBranchTypeLike(String value) {
            addCriterion("BRANCH_TYPE like", value, "branchType");
            return (Criteria) this;
        }

        public Criteria andBranchTypeNotLike(String value) {
            addCriterion("BRANCH_TYPE not like", value, "branchType");
            return (Criteria) this;
        }

        public Criteria andBranchTypeIn(List<String> values) {
            addCriterion("BRANCH_TYPE in", values, "branchType");
            return (Criteria) this;
        }

        public Criteria andBranchTypeNotIn(List<String> values) {
            addCriterion("BRANCH_TYPE not in", values, "branchType");
            return (Criteria) this;
        }

        public Criteria andBranchTypeBetween(String value1, String value2) {
            addCriterion("BRANCH_TYPE between", value1, value2, "branchType");
            return (Criteria) this;
        }

        public Criteria andBranchTypeNotBetween(String value1, String value2) {
            addCriterion("BRANCH_TYPE not between", value1, value2, "branchType");
            return (Criteria) this;
        }

        public Criteria andBranchLevelIsNull() {
            addCriterion("BRANCH_LEVEL is null");
            return (Criteria) this;
        }

        public Criteria andBranchLevelIsNotNull() {
            addCriterion("BRANCH_LEVEL is not null");
            return (Criteria) this;
        }

        public Criteria andBranchLevelEqualTo(String value) {
            addCriterion("BRANCH_LEVEL =", value, "branchLevel");
            return (Criteria) this;
        }

        public Criteria andBranchLevelNotEqualTo(String value) {
            addCriterion("BRANCH_LEVEL <>", value, "branchLevel");
            return (Criteria) this;
        }

        public Criteria andBranchLevelGreaterThan(String value) {
            addCriterion("BRANCH_LEVEL >", value, "branchLevel");
            return (Criteria) this;
        }

        public Criteria andBranchLevelGreaterThanOrEqualTo(String value) {
            addCriterion("BRANCH_LEVEL >=", value, "branchLevel");
            return (Criteria) this;
        }

        public Criteria andBranchLevelLessThan(String value) {
            addCriterion("BRANCH_LEVEL <", value, "branchLevel");
            return (Criteria) this;
        }

        public Criteria andBranchLevelLessThanOrEqualTo(String value) {
            addCriterion("BRANCH_LEVEL <=", value, "branchLevel");
            return (Criteria) this;
        }

        public Criteria andBranchLevelLike(String value) {
            addCriterion("BRANCH_LEVEL like", value, "branchLevel");
            return (Criteria) this;
        }

        public Criteria andBranchLevelNotLike(String value) {
            addCriterion("BRANCH_LEVEL not like", value, "branchLevel");
            return (Criteria) this;
        }

        public Criteria andBranchLevelIn(List<String> values) {
            addCriterion("BRANCH_LEVEL in", values, "branchLevel");
            return (Criteria) this;
        }

        public Criteria andBranchLevelNotIn(List<String> values) {
            addCriterion("BRANCH_LEVEL not in", values, "branchLevel");
            return (Criteria) this;
        }

        public Criteria andBranchLevelBetween(String value1, String value2) {
            addCriterion("BRANCH_LEVEL between", value1, value2, "branchLevel");
            return (Criteria) this;
        }

        public Criteria andBranchLevelNotBetween(String value1, String value2) {
            addCriterion("BRANCH_LEVEL not between", value1, value2, "branchLevel");
            return (Criteria) this;
        }

        public Criteria andValidStatusIsNull() {
            addCriterion("VALID_STATUS is null");
            return (Criteria) this;
        }

        public Criteria andValidStatusIsNotNull() {
            addCriterion("VALID_STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andValidStatusEqualTo(String value) {
            addCriterion("VALID_STATUS =", value, "validStatus");
            return (Criteria) this;
        }

        public Criteria andValidStatusNotEqualTo(String value) {
            addCriterion("VALID_STATUS <>", value, "validStatus");
            return (Criteria) this;
        }

        public Criteria andValidStatusGreaterThan(String value) {
            addCriterion("VALID_STATUS >", value, "validStatus");
            return (Criteria) this;
        }

        public Criteria andValidStatusGreaterThanOrEqualTo(String value) {
            addCriterion("VALID_STATUS >=", value, "validStatus");
            return (Criteria) this;
        }

        public Criteria andValidStatusLessThan(String value) {
            addCriterion("VALID_STATUS <", value, "validStatus");
            return (Criteria) this;
        }

        public Criteria andValidStatusLessThanOrEqualTo(String value) {
            addCriterion("VALID_STATUS <=", value, "validStatus");
            return (Criteria) this;
        }

        public Criteria andValidStatusLike(String value) {
            addCriterion("VALID_STATUS like", value, "validStatus");
            return (Criteria) this;
        }

        public Criteria andValidStatusNotLike(String value) {
            addCriterion("VALID_STATUS not like", value, "validStatus");
            return (Criteria) this;
        }

        public Criteria andValidStatusIn(List<String> values) {
            addCriterion("VALID_STATUS in", values, "validStatus");
            return (Criteria) this;
        }

        public Criteria andValidStatusNotIn(List<String> values) {
            addCriterion("VALID_STATUS not in", values, "validStatus");
            return (Criteria) this;
        }

        public Criteria andValidStatusBetween(String value1, String value2) {
            addCriterion("VALID_STATUS between", value1, value2, "validStatus");
            return (Criteria) this;
        }

        public Criteria andValidStatusNotBetween(String value1, String value2) {
            addCriterion("VALID_STATUS not between", value1, value2, "validStatus");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRateIsNull() {
            addCriterion("RATE is null");
            return (Criteria) this;
        }

        public Criteria andRateIsNotNull() {
            addCriterion("RATE is not null");
            return (Criteria) this;
        }

        public Criteria andRateEqualTo(Double value) {
            addCriterion("RATE =", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotEqualTo(Double value) {
            addCriterion("RATE <>", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateGreaterThan(Double value) {
            addCriterion("RATE >", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateGreaterThanOrEqualTo(Double value) {
            addCriterion("RATE >=", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateLessThan(Double value) {
            addCriterion("RATE <", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateLessThanOrEqualTo(Double value) {
            addCriterion("RATE <=", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateIn(List<Double> values) {
            addCriterion("RATE in", values, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotIn(List<Double> values) {
            addCriterion("RATE not in", values, "rate");
            return (Criteria) this;
        }

        public Criteria andRateBetween(Double value1, Double value2) {
            addCriterion("RATE between", value1, value2, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotBetween(Double value1, Double value2) {
            addCriterion("RATE not between", value1, value2, "rate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}