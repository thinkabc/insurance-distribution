
package cn.com.libertymutual.sp.webService.policy;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.isoftstone.liberty.webservice.policy package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PolicyDetailInquiryResponse_QNAME = new QName("http://prpall.liberty.com/all/cb/policyDetailInquiry/intf", "policyDetailInquiryResponse");
    private final static QName _PolicyDetailInquiry_QNAME = new QName("http://prpall.liberty.com/all/cb/policyDetailInquiry/intf", "policyDetailInquiry");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.isoftstone.liberty.webservice.policy
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PolicyDetailInquiryResponse }
     * 
     */
    public PolicyDetailInquiryResponse createPolicyDetailInquiryResponse() {
        return new PolicyDetailInquiryResponse();
    }

    /**
     * Create an instance of {@link PolicyDetailInquiry }
     * 
     */
    public PolicyDetailInquiry createPolicyDetailInquiry() {
        return new PolicyDetailInquiry();
    }

    /**
     * Create an instance of {@link PolicyDetailInquiryResponseDto }
     * 
     */
    public PolicyDetailInquiryResponseDto createPolicyDetailInquiryResponseDto() {
        return new PolicyDetailInquiryResponseDto();
    }

    /**
     * Create an instance of {@link PolicyDetailInquiryRequestDto }
     * 
     */
    public PolicyDetailInquiryRequestDto createPolicyDetailInquiryRequestDto() {
        return new PolicyDetailInquiryRequestDto();
    }

    /**
     * Create an instance of {@link PrptApplicantDto }
     * 
     */
    public PrptApplicantDto createPrptApplicantDto() {
        return new PrptApplicantDto();
    }

    /**
     * Create an instance of {@link PrptCoinsDto }
     * 
     */
    public PrptCoinsDto createPrptCoinsDto() {
        return new PrptCoinsDto();
    }

    /**
     * Create an instance of {@link PrpTprofitDetailDto }
     * 
     */
    public PrpTprofitDetailDto createPrpTprofitDetailDto() {
        return new PrpTprofitDetailDto();
    }

    /**
     * Create an instance of {@link PrptMainPropDto }
     * 
     */
    public PrptMainPropDto createPrptMainPropDto() {
        return new PrptMainPropDto();
    }

    /**
     * Create an instance of {@link PrpTcarShipTaxDto }
     * 
     */
    public PrpTcarShipTaxDto createPrpTcarShipTaxDto() {
        return new PrpTcarShipTaxDto();
    }

    /**
     * Create an instance of {@link PrpTitemCarDto }
     * 
     */
    public PrpTitemCarDto createPrpTitemCarDto() {
        return new PrpTitemCarDto();
    }

    /**
     * Create an instance of {@link PrptcarDriverDto }
     * 
     */
    public PrptcarDriverDto createPrptcarDriverDto() {
        return new PrptcarDriverDto();
    }

    /**
     * Create an instance of {@link ResponseHeadDto }
     * 
     */
    public ResponseHeadDto createResponseHeadDto() {
        return new ResponseHeadDto();
    }

    /**
     * Create an instance of {@link MotorPolicyDto }
     * 
     */
    public MotorPolicyDto createMotorPolicyDto() {
        return new MotorPolicyDto();
    }

    /**
     * Create an instance of {@link PrpTItemShipDto }
     * 
     */
    public PrpTItemShipDto createPrpTItemShipDto() {
        return new PrpTItemShipDto();
    }

    /**
     * Create an instance of {@link PrptAdjustDto }
     * 
     */
    public PrptAdjustDto createPrptAdjustDto() {
        return new PrptAdjustDto();
    }

    /**
     * Create an instance of {@link PrpTcarDeviceDto }
     * 
     */
    public PrpTcarDeviceDto createPrpTcarDeviceDto() {
        return new PrpTcarDeviceDto();
    }

    /**
     * Create an instance of {@link MtplPolicyDto }
     * 
     */
    public MtplPolicyDto createMtplPolicyDto() {
        return new MtplPolicyDto();
    }

    /**
     * Create an instance of {@link PrptItemKindDto }
     * 
     */
    public PrptItemKindDto createPrptItemKindDto() {
        return new PrptItemKindDto();
    }

    /**
     * Create an instance of {@link PrpTMainCargoSubDto }
     * 
     */
    public PrpTMainCargoSubDto createPrpTMainCargoSubDto() {
        return new PrpTMainCargoSubDto();
    }

    /**
     * Create an instance of {@link PrptInsuredDto }
     * 
     */
    public PrptInsuredDto createPrptInsuredDto() {
        return new PrptInsuredDto();
    }

    /**
     * Create an instance of {@link NonMotorPolicyDto }
     * 
     */
    public NonMotorPolicyDto createNonMotorPolicyDto() {
        return new NonMotorPolicyDto();
    }

    /**
     * Create an instance of {@link PrptBeneficiaryDto }
     * 
     */
    public PrptBeneficiaryDto createPrptBeneficiaryDto() {
        return new PrptBeneficiaryDto();
    }

    /**
     * Create an instance of {@link PrpTmainSubDto }
     * 
     */
    public PrpTmainSubDto createPrpTmainSubDto() {
        return new PrpTmainSubDto();
    }

    /**
     * Create an instance of {@link PrptMainDto }
     * 
     */
    public PrptMainDto createPrptMainDto() {
        return new PrptMainDto();
    }

    /**
     * Create an instance of {@link PrpTvoyageDto }
     * 
     */
    public PrpTvoyageDto createPrpTvoyageDto() {
        return new PrpTvoyageDto();
    }

    /**
     * Create an instance of {@link PrptEngageDto }
     * 
     */
    public PrptEngageDto createPrptEngageDto() {
        return new PrptEngageDto();
    }

    /**
     * Create an instance of {@link PrptReinscededDto }
     * 
     */
    public PrptReinscededDto createPrptReinscededDto() {
        return new PrptReinscededDto();
    }

    /**
     * Create an instance of {@link PrptPlanDto }
     * 
     */
    public PrptPlanDto createPrptPlanDto() {
        return new PrptPlanDto();
    }

    /**
     * Create an instance of {@link RequestHeadDto }
     * 
     */
    public RequestHeadDto createRequestHeadDto() {
        return new RequestHeadDto();
    }

    /**
     * Create an instance of {@link PrptinsurednatureDto }
     * 
     */
    public PrptinsurednatureDto createPrptinsurednatureDto() {
        return new PrptinsurednatureDto();
    }

    /**
     * Create an instance of {@link PrpTcarOwnerDto }
     * 
     */
    public PrpTcarOwnerDto createPrpTcarOwnerDto() {
        return new PrpTcarOwnerDto();
    }

    /**
     * Create an instance of {@link PrptnameDto }
     * 
     */
    public PrptnameDto createPrptnameDto() {
        return new PrptnameDto();
    }

    /**
     * Create an instance of {@link PrptaddressDto }
     * 
     */
    public PrptaddressDto createPrptaddressDto() {
        return new PrptaddressDto();
    }

    /**
     * Create an instance of {@link PrptLimitDto }
     * 
     */
    public PrptLimitDto createPrptLimitDto() {
        return new PrptLimitDto();
    }

    /**
     * Create an instance of {@link PrpTitemCarExtDto }
     * 
     */
    public PrpTitemCarExtDto createPrpTitemCarExtDto() {
        return new PrpTitemCarExtDto();
    }

    /**
     * Create an instance of {@link PrptprofitDto }
     * 
     */
    public PrptprofitDto createPrptprofitDto() {
        return new PrptprofitDto();
    }

    /**
     * Create an instance of {@link PrptmaincargoDto }
     * 
     */
    public PrptmaincargoDto createPrptmaincargoDto() {
        return new PrptmaincargoDto();
    }

    /**
     * Create an instance of {@link PrptCommissionDto }
     * 
     */
    public PrptCommissionDto createPrptCommissionDto() {
        return new PrptCommissionDto();
    }

    /**
     * Create an instance of {@link PrptFeeDto }
     * 
     */
    public PrptFeeDto createPrptFeeDto() {
        return new PrptFeeDto();
    }

    /**
     * Create an instance of {@link PrptexpCarInfoDto }
     * 
     */
    public PrptexpCarInfoDto createPrptexpCarInfoDto() {
        return new PrptexpCarInfoDto();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PolicyDetailInquiryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://prpall.liberty.com/all/cb/policyDetailInquiry/intf", name = "policyDetailInquiryResponse")
    public JAXBElement<PolicyDetailInquiryResponse> createPolicyDetailInquiryResponse(PolicyDetailInquiryResponse value) {
        return new JAXBElement<PolicyDetailInquiryResponse>(_PolicyDetailInquiryResponse_QNAME, PolicyDetailInquiryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PolicyDetailInquiry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://prpall.liberty.com/all/cb/policyDetailInquiry/intf", name = "policyDetailInquiry")
    public JAXBElement<PolicyDetailInquiry> createPolicyDetailInquiry(PolicyDetailInquiry value) {
        return new JAXBElement<PolicyDetailInquiry>(_PolicyDetailInquiry_QNAME, PolicyDetailInquiry.class, null, value);
    }

}
