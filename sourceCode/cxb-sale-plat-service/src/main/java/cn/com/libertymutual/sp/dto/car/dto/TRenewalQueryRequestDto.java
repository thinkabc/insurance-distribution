package cn.com.libertymutual.sp.dto.car.dto;

public class TRenewalQueryRequestDto extends RequestBaseDto {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String engineNo;   //发动机号
	private String licenseNo;  //车牌号
	private String startDate;  //起保日期
	private String vinNo;      //车架号/VIN码
	
	public String getEngineNo() {
		return engineNo;
	}
	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getVinNo() {
		return vinNo;
	}
	public void setVinNo(String vinNo) {
		this.vinNo = vinNo;
	}
	@Override
	public String toString() {
		return "TRenewalQueryRequestDto [engineNo=" + engineNo + ", licenseNo=" + licenseNo + ", startDate=" + startDate
				+ ", vinNo=" + vinNo + "]";
	}
	
	
}
