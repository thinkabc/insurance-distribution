package cn.com.libertymutual.production.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.production.dao.nomorcldatasource.TbSysBranchMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.TbSysBranch;
import cn.com.libertymutual.production.model.nomorcldatasource.TbSysBranchExample;
import cn.com.libertymutual.production.model.nomorcldatasource.TbSysBranchExample.Criteria;
import cn.com.libertymutual.production.pojo.request.Request;
import cn.com.libertymutual.production.service.api.BranchService;
import cn.com.libertymutual.production.service.api.PrpdCompanyService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/** 
 * @author Steven.Li
 * @date 2017年5月19日
 *  
 */
@Service
public class BranchServiceImpl implements BranchService {

	@Autowired
	private TbSysBranchMapper branchMapper;


	@Override
	public PageInfo<TbSysBranch> findBranchs(Request request) {
		TbSysBranchExample example = new TbSysBranchExample();
		Criteria criteria = example.createCriteria();
		criteria.andBranchLevelGreaterThanOrEqualTo("1");
		criteria.andBranchLevelLessThanOrEqualTo("3");
		criteria.andValidStatusEqualTo("1");
		PageHelper.startPage(request.getCurrentPage(), request.getPageSize());
		PageInfo<TbSysBranch> pageInfo = new PageInfo<>(branchMapper.selectByExample(example));
		return pageInfo;
	}

	@Override
	public List<TbSysBranch> findNextBranchs() {
		TbSysBranchExample example = new TbSysBranchExample();
		Criteria criteria = example.createCriteria();
//		criteria.andCenterflagEqualTo("1");
		criteria.andBranchNoNotEqualTo("0000");
		criteria.andValidStatusEqualTo("1");
		return branchMapper.selectByExample(example);
	}

	@Override
	public List<TbSysBranch> findNextBranchsByCodes(List<String> comcodes) {
		TbSysBranchExample example = new TbSysBranchExample();
		Criteria criteria = example.createCriteria();
//		criteria.andCenterflagEqualTo("1");
		criteria.andBranchNoNotEqualTo("0000");
		criteria.andValidStatusEqualTo("1");
		criteria.andBranchNoIn(comcodes);
		return branchMapper.selectByExample(example);
	}


}
