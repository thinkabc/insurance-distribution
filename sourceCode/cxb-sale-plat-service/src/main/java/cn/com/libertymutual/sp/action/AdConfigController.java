package cn.com.libertymutual.sp.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.AdConfigService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/nol/advert")
public class AdConfigController {
	
	

		@Autowired
		private AdConfigService adConfigService;
		
		
		@ApiOperation(value = "前端广告配置接口", notes = "前端广告配置接口")
		@PostMapping(value = "/advertList")
		@ApiImplicitParams(value = {
	            @ApiImplicitParam(name = "sortfield", value = "排序字段", required = true, paramType = "query", dataType = "String"),
	            @ApiImplicitParam(name = "sorttype", value = "排序方式", required = true, paramType = "query", dataType = "String"),
	    })
		public ServiceResult advertList(String sortfield,String sorttype){
			ServiceResult sr = new ServiceResult();
			
			sr = adConfigService.advertList(sorttype);
			return sr;
			
		}
		
}
