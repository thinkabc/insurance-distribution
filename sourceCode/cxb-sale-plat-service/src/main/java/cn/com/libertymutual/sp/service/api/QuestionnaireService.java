package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpInputConfig;
import cn.com.libertymutual.sp.bean.TbSpQuestionnaire;

public interface QuestionnaireService {
	ServiceResult findQueId(String id);

	ServiceResult questionList(String title, String type);

	ServiceResult addOrUpdateQuestionnaire(TbSpQuestionnaire questionnaire);

	ServiceResult addPetConfig(TbSpInputConfig inputConfig);

	ServiceResult petConfigList();
}
