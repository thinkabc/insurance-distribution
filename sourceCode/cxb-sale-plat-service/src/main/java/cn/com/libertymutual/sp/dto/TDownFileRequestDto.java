package cn.com.libertymutual.sp.dto;

import java.io.Serializable;

import cn.com.libertymutual.core.base.dto.RequestBaseDto;


/**
 * 电子保单下载 
 * @author issuser
 *
 */
public class TDownFileRequestDto extends  RequestBaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// 业务单号
	private String businessNo;
	private String appIdNo;//投保人证件号
	private String printType;//打印类型
	private String uploadDate;//电子保单生成时间
	public String getBusinessNo() {
		return businessNo;
	}
	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}
	public String getAppIdNo() {
		return appIdNo;
	}
	public void setAppIdNo(String appIdNo) {
		this.appIdNo = appIdNo;
	}
	public String getPrintType() {
		return printType;
	}
	public void setPrintType(String printType) {
		this.printType = printType;
	}
	public String getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}
	
}
