package cn.com.libertymutual.production.pojo.request;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhexitemkind;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkindlibrary;

public class PrpdRiskRequest extends Request {

    private Prpdkindlibrary kind;
    private String riskcode;
    private String classcode;
    private String riskcname;
    private String riskename;
    private String riskversion;
    private String plancode;
    private String validstatus;
    private String riskflag;
    private String compositeflag;
    private String ownerriskcode;    // 条款关联险种时，组合险需指定归属险种
    private String ownerflag;        // 条款关联险种时，2724归属类型
    private String dangerSplitMode;
    private String dangerViewType;
    private String reinsTrialMode;
    private String riskEvaluateMode;
    private String riskPoolMode;
    private String inrateflag;
    private List<FhFhxDto> fhData;
    private List<FhFhxDto> fhxData;

    public String getRiskflag() {
        return riskflag;
    }

    public void setRiskflag(String riskflag) {
        this.riskflag = riskflag;
    }

    public List<FhFhxDto> getFhData() {
        return fhData;
    }

    public void setFhData(List<FhFhxDto> fhData) {
        this.fhData = fhData;
    }

    public List<FhFhxDto> getFhxData() {
        return fhxData;
    }

    public void setFhxData(List<FhFhxDto> fhxData) {
        this.fhxData = fhxData;
    }

    public String getInrateflag() {
        return inrateflag;
    }

    public void setInrateflag(String inrateflag) {
        this.inrateflag = inrateflag;
    }

    public String getDangerSplitMode() {
        return dangerSplitMode;
    }

    public void setDangerSplitMode(String dangerSplitMode) {
        this.dangerSplitMode = dangerSplitMode;
    }

    public String getDangerViewType() {
        return dangerViewType;
    }

    public void setDangerViewType(String dangerViewType) {
        this.dangerViewType = dangerViewType;
    }

    public String getReinsTrialMode() {
        return reinsTrialMode;
    }

    public void setReinsTrialMode(String reinsTrialMode) {
        this.reinsTrialMode = reinsTrialMode;
    }

    public String getRiskEvaluateMode() {
        return riskEvaluateMode;
    }

    public void setRiskEvaluateMode(String riskEvaluateMode) {
        this.riskEvaluateMode = riskEvaluateMode;
    }

    public String getRiskPoolMode() {
        return riskPoolMode;
    }

    public void setRiskPoolMode(String riskPoolMode) {
        this.riskPoolMode = riskPoolMode;
    }

    public String getOwnerflag() {
        return ownerflag;
    }

    public void setOwnerflag(String ownerflag) {
        this.ownerflag = ownerflag;
    }

    public String getClasscode() {
        return classcode;
    }

    public void setClasscode(String classcode) {
        this.classcode = classcode;
    }

    public String getRiskename() {
        return riskename;
    }

    public void setRiskename(String riskename) {
        this.riskename = riskename;
    }

    public String getRiskcode() {
        return riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode;
    }

    public String getRiskversion() {
        return riskversion;
    }

    public void setRiskversion(String riskversion) {
        this.riskversion = riskversion;
    }

    public String getValidstatus() {
        return validstatus;
    }

    public void setValidstatus(String validstatus) {
        this.validstatus = validstatus;
    }

    public String getRiskcname() {
        return riskcname;
    }

    public void setRiskcname(String riskcname) {
        this.riskcname = riskcname;
    }

    public Prpdkindlibrary getKind() {
        return kind;
    }

    public void setKind(Prpdkindlibrary kind) {
        this.kind = kind;
    }

    public String getCompositeflag() {
        return compositeflag;
    }

    public void setCompositeflag(String compositeflag) {
        this.compositeflag = compositeflag;
    }

    public String getOwnerriskcode() {
        return ownerriskcode;
    }

    public void setOwnerriskcode(String ownerriskcode) {
        this.ownerriskcode = ownerriskcode;
    }

    public String getPlancode() {
        return plancode;
    }

    public void setPlancode(String plancode) {
        this.plancode = plancode;
    }

}
