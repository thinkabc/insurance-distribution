package cn.com.libertymutual.sp.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbWebRequestLog;
import cn.com.libertymutual.sp.dao.ServiceMenuDao;
import cn.com.libertymutual.sp.dao.TbWebRequestLogDao;
import cn.com.libertymutual.sp.service.api.TbWebRequestLogService;
@Service("TbWebRequestLogService")
public class TbWebRequestLogServiceImpl implements TbWebRequestLogService {
	@Autowired
	private TbWebRequestLogDao tbWebRequestLogDao;
	private Logger log = LoggerFactory.getLogger(getClass());
	@Override
	public ServiceResult saveWebRequestLog(TbWebRequestLog tequestLog) {
		ServiceResult sr = new ServiceResult();
		try{
			tbWebRequestLogDao.save(tequestLog);
			sr.setSuccess();;
		}catch(Exception e){
			sr.setFail();
			log.error(e.toString());
		}
		return sr;
	}



}
