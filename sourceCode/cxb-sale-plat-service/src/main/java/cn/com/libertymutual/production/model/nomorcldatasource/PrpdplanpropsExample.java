package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.ArrayList;
import java.util.List;

public class PrpdplanpropsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PrpdplanpropsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPlancodeIsNull() {
            addCriterion("PLANCODE is null");
            return (Criteria) this;
        }

        public Criteria andPlancodeIsNotNull() {
            addCriterion("PLANCODE is not null");
            return (Criteria) this;
        }

        public Criteria andPlancodeEqualTo(String value) {
            addCriterion("PLANCODE =", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotEqualTo(String value) {
            addCriterion("PLANCODE <>", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeGreaterThan(String value) {
            addCriterion("PLANCODE >", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeGreaterThanOrEqualTo(String value) {
            addCriterion("PLANCODE >=", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeLessThan(String value) {
            addCriterion("PLANCODE <", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeLessThanOrEqualTo(String value) {
            addCriterion("PLANCODE <=", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeLike(String value) {
            addCriterion("PLANCODE like", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotLike(String value) {
            addCriterion("PLANCODE not like", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeIn(List<String> values) {
            addCriterion("PLANCODE in", values, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotIn(List<String> values) {
            addCriterion("PLANCODE not in", values, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeBetween(String value1, String value2) {
            addCriterion("PLANCODE between", value1, value2, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotBetween(String value1, String value2) {
            addCriterion("PLANCODE not between", value1, value2, "plancode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIsNull() {
            addCriterion("RISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIsNotNull() {
            addCriterion("RISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andRiskcodeEqualTo(String value) {
            addCriterion("RISKCODE =", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotEqualTo(String value) {
            addCriterion("RISKCODE <>", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeGreaterThan(String value) {
            addCriterion("RISKCODE >", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeGreaterThanOrEqualTo(String value) {
            addCriterion("RISKCODE >=", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLessThan(String value) {
            addCriterion("RISKCODE <", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLessThanOrEqualTo(String value) {
            addCriterion("RISKCODE <=", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLike(String value) {
            addCriterion("RISKCODE like", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotLike(String value) {
            addCriterion("RISKCODE not like", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIn(List<String> values) {
            addCriterion("RISKCODE in", values, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotIn(List<String> values) {
            addCriterion("RISKCODE not in", values, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeBetween(String value1, String value2) {
            addCriterion("RISKCODE between", value1, value2, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotBetween(String value1, String value2) {
            addCriterion("RISKCODE not between", value1, value2, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskversionIsNull() {
            addCriterion("RISKVERSION is null");
            return (Criteria) this;
        }

        public Criteria andRiskversionIsNotNull() {
            addCriterion("RISKVERSION is not null");
            return (Criteria) this;
        }

        public Criteria andRiskversionEqualTo(String value) {
            addCriterion("RISKVERSION =", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotEqualTo(String value) {
            addCriterion("RISKVERSION <>", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionGreaterThan(String value) {
            addCriterion("RISKVERSION >", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionGreaterThanOrEqualTo(String value) {
            addCriterion("RISKVERSION >=", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLessThan(String value) {
            addCriterion("RISKVERSION <", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLessThanOrEqualTo(String value) {
            addCriterion("RISKVERSION <=", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLike(String value) {
            addCriterion("RISKVERSION like", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotLike(String value) {
            addCriterion("RISKVERSION not like", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionIn(List<String> values) {
            addCriterion("RISKVERSION in", values, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotIn(List<String> values) {
            addCriterion("RISKVERSION not in", values, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionBetween(String value1, String value2) {
            addCriterion("RISKVERSION between", value1, value2, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotBetween(String value1, String value2) {
            addCriterion("RISKVERSION not between", value1, value2, "riskversion");
            return (Criteria) this;
        }

        public Criteria andPropscodeIsNull() {
            addCriterion("PROPSCODE is null");
            return (Criteria) this;
        }

        public Criteria andPropscodeIsNotNull() {
            addCriterion("PROPSCODE is not null");
            return (Criteria) this;
        }

        public Criteria andPropscodeEqualTo(String value) {
            addCriterion("PROPSCODE =", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeNotEqualTo(String value) {
            addCriterion("PROPSCODE <>", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeGreaterThan(String value) {
            addCriterion("PROPSCODE >", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeGreaterThanOrEqualTo(String value) {
            addCriterion("PROPSCODE >=", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeLessThan(String value) {
            addCriterion("PROPSCODE <", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeLessThanOrEqualTo(String value) {
            addCriterion("PROPSCODE <=", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeLike(String value) {
            addCriterion("PROPSCODE like", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeNotLike(String value) {
            addCriterion("PROPSCODE not like", value, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeIn(List<String> values) {
            addCriterion("PROPSCODE in", values, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeNotIn(List<String> values) {
            addCriterion("PROPSCODE not in", values, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeBetween(String value1, String value2) {
            addCriterion("PROPSCODE between", value1, value2, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropscodeNotBetween(String value1, String value2) {
            addCriterion("PROPSCODE not between", value1, value2, "propscode");
            return (Criteria) this;
        }

        public Criteria andPropstypeIsNull() {
            addCriterion("PROPSTYPE is null");
            return (Criteria) this;
        }

        public Criteria andPropstypeIsNotNull() {
            addCriterion("PROPSTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andPropstypeEqualTo(String value) {
            addCriterion("PROPSTYPE =", value, "propstype");
            return (Criteria) this;
        }

        public Criteria andPropstypeNotEqualTo(String value) {
            addCriterion("PROPSTYPE <>", value, "propstype");
            return (Criteria) this;
        }

        public Criteria andPropstypeGreaterThan(String value) {
            addCriterion("PROPSTYPE >", value, "propstype");
            return (Criteria) this;
        }

        public Criteria andPropstypeGreaterThanOrEqualTo(String value) {
            addCriterion("PROPSTYPE >=", value, "propstype");
            return (Criteria) this;
        }

        public Criteria andPropstypeLessThan(String value) {
            addCriterion("PROPSTYPE <", value, "propstype");
            return (Criteria) this;
        }

        public Criteria andPropstypeLessThanOrEqualTo(String value) {
            addCriterion("PROPSTYPE <=", value, "propstype");
            return (Criteria) this;
        }

        public Criteria andPropstypeLike(String value) {
            addCriterion("PROPSTYPE like", value, "propstype");
            return (Criteria) this;
        }

        public Criteria andPropstypeNotLike(String value) {
            addCriterion("PROPSTYPE not like", value, "propstype");
            return (Criteria) this;
        }

        public Criteria andPropstypeIn(List<String> values) {
            addCriterion("PROPSTYPE in", values, "propstype");
            return (Criteria) this;
        }

        public Criteria andPropstypeNotIn(List<String> values) {
            addCriterion("PROPSTYPE not in", values, "propstype");
            return (Criteria) this;
        }

        public Criteria andPropstypeBetween(String value1, String value2) {
            addCriterion("PROPSTYPE between", value1, value2, "propstype");
            return (Criteria) this;
        }

        public Criteria andPropstypeNotBetween(String value1, String value2) {
            addCriterion("PROPSTYPE not between", value1, value2, "propstype");
            return (Criteria) this;
        }

        public Criteria andLinenoIsNull() {
            addCriterion("LINENO is null");
            return (Criteria) this;
        }

        public Criteria andLinenoIsNotNull() {
            addCriterion("LINENO is not null");
            return (Criteria) this;
        }

        public Criteria andLinenoEqualTo(String value) {
            addCriterion("LINENO =", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoNotEqualTo(String value) {
            addCriterion("LINENO <>", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoGreaterThan(String value) {
            addCriterion("LINENO >", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoGreaterThanOrEqualTo(String value) {
            addCriterion("LINENO >=", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoLessThan(String value) {
            addCriterion("LINENO <", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoLessThanOrEqualTo(String value) {
            addCriterion("LINENO <=", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoLike(String value) {
            addCriterion("LINENO like", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoNotLike(String value) {
            addCriterion("LINENO not like", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoIn(List<String> values) {
            addCriterion("LINENO in", values, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoNotIn(List<String> values) {
            addCriterion("LINENO not in", values, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoBetween(String value1, String value2) {
            addCriterion("LINENO between", value1, value2, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoNotBetween(String value1, String value2) {
            addCriterion("LINENO not between", value1, value2, "lineno");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNull() {
            addCriterion("VALIDSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNotNull() {
            addCriterion("VALIDSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andValidstatusEqualTo(String value) {
            addCriterion("VALIDSTATUS =", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotEqualTo(String value) {
            addCriterion("VALIDSTATUS <>", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThan(String value) {
            addCriterion("VALIDSTATUS >", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS >=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThan(String value) {
            addCriterion("VALIDSTATUS <", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS <=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLike(String value) {
            addCriterion("VALIDSTATUS like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotLike(String value) {
            addCriterion("VALIDSTATUS not like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusIn(List<String> values) {
            addCriterion("VALIDSTATUS in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotIn(List<String> values) {
            addCriterion("VALIDSTATUS not in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS not between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andStartvalueIsNull() {
            addCriterion("STARTVALUE is null");
            return (Criteria) this;
        }

        public Criteria andStartvalueIsNotNull() {
            addCriterion("STARTVALUE is not null");
            return (Criteria) this;
        }

        public Criteria andStartvalueEqualTo(String value) {
            addCriterion("STARTVALUE =", value, "startvalue");
            return (Criteria) this;
        }

        public Criteria andStartvalueNotEqualTo(String value) {
            addCriterion("STARTVALUE <>", value, "startvalue");
            return (Criteria) this;
        }

        public Criteria andStartvalueGreaterThan(String value) {
            addCriterion("STARTVALUE >", value, "startvalue");
            return (Criteria) this;
        }

        public Criteria andStartvalueGreaterThanOrEqualTo(String value) {
            addCriterion("STARTVALUE >=", value, "startvalue");
            return (Criteria) this;
        }

        public Criteria andStartvalueLessThan(String value) {
            addCriterion("STARTVALUE <", value, "startvalue");
            return (Criteria) this;
        }

        public Criteria andStartvalueLessThanOrEqualTo(String value) {
            addCriterion("STARTVALUE <=", value, "startvalue");
            return (Criteria) this;
        }

        public Criteria andStartvalueLike(String value) {
            addCriterion("STARTVALUE like", value, "startvalue");
            return (Criteria) this;
        }

        public Criteria andStartvalueNotLike(String value) {
            addCriterion("STARTVALUE not like", value, "startvalue");
            return (Criteria) this;
        }

        public Criteria andStartvalueIn(List<String> values) {
            addCriterion("STARTVALUE in", values, "startvalue");
            return (Criteria) this;
        }

        public Criteria andStartvalueNotIn(List<String> values) {
            addCriterion("STARTVALUE not in", values, "startvalue");
            return (Criteria) this;
        }

        public Criteria andStartvalueBetween(String value1, String value2) {
            addCriterion("STARTVALUE between", value1, value2, "startvalue");
            return (Criteria) this;
        }

        public Criteria andStartvalueNotBetween(String value1, String value2) {
            addCriterion("STARTVALUE not between", value1, value2, "startvalue");
            return (Criteria) this;
        }

        public Criteria andEndvalueIsNull() {
            addCriterion("ENDVALUE is null");
            return (Criteria) this;
        }

        public Criteria andEndvalueIsNotNull() {
            addCriterion("ENDVALUE is not null");
            return (Criteria) this;
        }

        public Criteria andEndvalueEqualTo(String value) {
            addCriterion("ENDVALUE =", value, "endvalue");
            return (Criteria) this;
        }

        public Criteria andEndvalueNotEqualTo(String value) {
            addCriterion("ENDVALUE <>", value, "endvalue");
            return (Criteria) this;
        }

        public Criteria andEndvalueGreaterThan(String value) {
            addCriterion("ENDVALUE >", value, "endvalue");
            return (Criteria) this;
        }

        public Criteria andEndvalueGreaterThanOrEqualTo(String value) {
            addCriterion("ENDVALUE >=", value, "endvalue");
            return (Criteria) this;
        }

        public Criteria andEndvalueLessThan(String value) {
            addCriterion("ENDVALUE <", value, "endvalue");
            return (Criteria) this;
        }

        public Criteria andEndvalueLessThanOrEqualTo(String value) {
            addCriterion("ENDVALUE <=", value, "endvalue");
            return (Criteria) this;
        }

        public Criteria andEndvalueLike(String value) {
            addCriterion("ENDVALUE like", value, "endvalue");
            return (Criteria) this;
        }

        public Criteria andEndvalueNotLike(String value) {
            addCriterion("ENDVALUE not like", value, "endvalue");
            return (Criteria) this;
        }

        public Criteria andEndvalueIn(List<String> values) {
            addCriterion("ENDVALUE in", values, "endvalue");
            return (Criteria) this;
        }

        public Criteria andEndvalueNotIn(List<String> values) {
            addCriterion("ENDVALUE not in", values, "endvalue");
            return (Criteria) this;
        }

        public Criteria andEndvalueBetween(String value1, String value2) {
            addCriterion("ENDVALUE between", value1, value2, "endvalue");
            return (Criteria) this;
        }

        public Criteria andEndvalueNotBetween(String value1, String value2) {
            addCriterion("ENDVALUE not between", value1, value2, "endvalue");
            return (Criteria) this;
        }

        public Criteria andExt1IsNull() {
            addCriterion("EXT1 is null");
            return (Criteria) this;
        }

        public Criteria andExt1IsNotNull() {
            addCriterion("EXT1 is not null");
            return (Criteria) this;
        }

        public Criteria andExt1EqualTo(String value) {
            addCriterion("EXT1 =", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotEqualTo(String value) {
            addCriterion("EXT1 <>", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThan(String value) {
            addCriterion("EXT1 >", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThanOrEqualTo(String value) {
            addCriterion("EXT1 >=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThan(String value) {
            addCriterion("EXT1 <", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThanOrEqualTo(String value) {
            addCriterion("EXT1 <=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Like(String value) {
            addCriterion("EXT1 like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotLike(String value) {
            addCriterion("EXT1 not like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1In(List<String> values) {
            addCriterion("EXT1 in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotIn(List<String> values) {
            addCriterion("EXT1 not in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Between(String value1, String value2) {
            addCriterion("EXT1 between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotBetween(String value1, String value2) {
            addCriterion("EXT1 not between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt2IsNull() {
            addCriterion("EXT2 is null");
            return (Criteria) this;
        }

        public Criteria andExt2IsNotNull() {
            addCriterion("EXT2 is not null");
            return (Criteria) this;
        }

        public Criteria andExt2EqualTo(String value) {
            addCriterion("EXT2 =", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotEqualTo(String value) {
            addCriterion("EXT2 <>", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThan(String value) {
            addCriterion("EXT2 >", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThanOrEqualTo(String value) {
            addCriterion("EXT2 >=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThan(String value) {
            addCriterion("EXT2 <", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThanOrEqualTo(String value) {
            addCriterion("EXT2 <=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Like(String value) {
            addCriterion("EXT2 like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotLike(String value) {
            addCriterion("EXT2 not like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2In(List<String> values) {
            addCriterion("EXT2 in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotIn(List<String> values) {
            addCriterion("EXT2 not in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Between(String value1, String value2) {
            addCriterion("EXT2 between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotBetween(String value1, String value2) {
            addCriterion("EXT2 not between", value1, value2, "ext2");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}