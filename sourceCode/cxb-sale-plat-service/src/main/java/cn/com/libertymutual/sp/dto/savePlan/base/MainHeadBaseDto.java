package cn.com.libertymutual.sp.dto.savePlan.base;

public class MainHeadBaseDto {

	private String trdSalesCode;
	private String riskCode;// 险种代码
	private String bussFromNo;// 外部系统传入单号
	private String language;
	private String currency;// 币别
	// 合同争议解决方式代码
	private String argueSolution;
	private String tradeQuantity;// 申请数量
	private String departureCountry; // 启程国家代码
	private String departureCountryName; // 启程国家名称
	private String arrivalCountry; // 目的地国家代码
	private String arrivalCountryName; // 目的地国家名称
	private String licenseno; // 车牌号
	private String vinno; // 车架号
	private String callBackUrl;
	private String othEngage;

	public String getOthEngage() {
		return othEngage;
	}

	public void setOthEngage(String othEngage) {
		this.othEngage = othEngage;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	public String getLicenseno() {
		return licenseno;
	}

	public void setLicenseno(String licenseno) {
		this.licenseno = licenseno;
	}

	public String getVinno() {
		return vinno;
	}

	public void setVinno(String vinno) {
		this.vinno = vinno;
	}

	public String getTrdSalesCode() {
		return trdSalesCode;
	}

	public void setTrdSalesCode(String trdSalesCode) {
		this.trdSalesCode = trdSalesCode;
	}

	public String getDepartureCountry() {
		return departureCountry;
	}

	public void setDepartureCountry(String departureCountry) {
		this.departureCountry = departureCountry;
	}

	public String getDepartureCountryName() {
		return departureCountryName;
	}

	public void setDepartureCountryName(String departureCountryName) {
		this.departureCountryName = departureCountryName;
	}

	public String getArrivalCountry() {
		return arrivalCountry;
	}

	public void setArrivalCountry(String arrivalCountry) {
		this.arrivalCountry = arrivalCountry;
	}

	public String getArrivalCountryName() {
		return arrivalCountryName;
	}

	public void setArrivalCountryName(String arrivalCountryName) {
		this.arrivalCountryName = arrivalCountryName;
	}

	// 保单种类-预留
	private String policySort;
	// 保单类型-预留
	private String policyType;
	// 市外/统括业务 一般业务-预留
	private String policyNatureFlag;
	// 分入标志 -预留
	private String businessFlag;
	// 联共保标志-预留
	private String coinsFlag;
	// 险种版本-预留
	private String riskVersion;

	public String getPolicySort() {
		return policySort;
	}

	public void setPolicySort(String policySort) {
		this.policySort = policySort;
	}

	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public String getPolicyNatureFlag() {
		return policyNatureFlag;
	}

	public void setPolicyNatureFlag(String policyNatureFlag) {
		this.policyNatureFlag = policyNatureFlag;
	}

	public String getBusinessFlag() {
		return businessFlag;
	}

	public void setBusinessFlag(String businessFlag) {
		this.businessFlag = businessFlag;
	}

	public String getCoinsFlag() {
		return coinsFlag;
	}

	public void setCoinsFlag(String coinsFlag) {
		this.coinsFlag = coinsFlag;
	}

	public String getRiskVersion() {
		return riskVersion;
	}

	public void setRiskVersion(String riskVersion) {
		this.riskVersion = riskVersion;
	}

	public String getRiskCode() {
		return riskCode;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	public String getBussFromNo() {
		return bussFromNo;
	}

	public void setBussFromNo(String bussFromNo) {
		this.bussFromNo = bussFromNo;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getArgueSolution() {
		return argueSolution;
	}

	public void setArgueSolution(String argueSolution) {
		this.argueSolution = argueSolution;
	}

	public String getTradeQuantity() {
		return tradeQuantity;
	}

	public void setTradeQuantity(String tradeQuantity) {
		this.tradeQuantity = tradeQuantity;
	}

	@Override
	public String toString() {
		return "MainHeadBaseDto [riskCode=" + riskCode + ", bussFromNo=" + bussFromNo + ", language=" + language + ", currency=" + currency
				+ ", argueSolution=" + argueSolution + ", tradeQuantity=" + tradeQuantity + ", policySort=" + policySort + ", policyType="
				+ policyType + ", policyNatureFlag=" + policyNatureFlag + ", businessFlag=" + businessFlag + ", coinsFlag=" + coinsFlag
				+ ", riskVersion=" + riskVersion + "]";
	}

	public MainHeadBaseDto(String riskCode, String bussFromNo, String language, String currency, String argueSolution, String tradeQuantity,
			String policySort, String policyType, String policyNatureFlag, String businessFlag, String coinsFlag, String riskVersion) {
		super();
		this.riskCode = riskCode;
		this.bussFromNo = bussFromNo;
		this.language = language;
		this.currency = currency;
		this.argueSolution = argueSolution;
		this.tradeQuantity = tradeQuantity;
		this.policySort = policySort;
		this.policyType = policyType;
		this.policyNatureFlag = policyNatureFlag;
		this.businessFlag = businessFlag;
		this.coinsFlag = coinsFlag;
		this.riskVersion = riskVersion;
	}

	public MainHeadBaseDto() {
		super();
	}

}
