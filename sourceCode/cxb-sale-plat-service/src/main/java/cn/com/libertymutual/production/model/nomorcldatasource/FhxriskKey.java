package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;

public class FhxriskKey {
    private String treatyno;

    private BigDecimal layerno;

    private String riskcode;

    private String sectionno;

    public String getTreatyno() {
        return treatyno;
    }

    public void setTreatyno(String treatyno) {
        this.treatyno = treatyno == null ? null : treatyno.trim();
    }

    public BigDecimal getLayerno() {
        return layerno;
    }

    public void setLayerno(BigDecimal layerno) {
        this.layerno = layerno;
    }

    public String getRiskcode() {
        return riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode == null ? null : riskcode.trim();
    }

    public String getSectionno() {
        return sectionno;
    }

    public void setSectionno(String sectionno) {
        this.sectionno = sectionno == null ? null : sectionno.trim();
    }
}