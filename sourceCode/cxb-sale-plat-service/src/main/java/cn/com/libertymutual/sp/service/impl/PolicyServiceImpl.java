package cn.com.libertymutual.sp.service.impl;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.CommonUtil;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.util.DateUtil;
import cn.com.libertymutual.core.util.ObjectPojo;
import cn.com.libertymutual.core.util.SpringContextUtil;
import cn.com.libertymutual.core.util.StringUtil;
import cn.com.libertymutual.core.util.UtilTool;
import cn.com.libertymutual.core.util.XmlUtil;
import cn.com.libertymutual.core.util.enums.CoreServiceEnum;
import cn.com.libertymutual.core.web.util.RequestUtils;
import cn.com.libertymutual.sp.dto.CommonQueryRequestDTO;
import cn.com.libertymutual.sp.dto.TQueryPolicyRequstDto;
import cn.com.libertymutual.sp.dto.TQueryPolicyResponseDto;
import cn.com.libertymutual.sp.service.api.PolicyService;
import cn.com.libertymutual.sp.webService.policy.MotorPolicyDto;
import cn.com.libertymutual.sp.webService.policy.MtplPolicyDto;
import cn.com.libertymutual.sp.webService.policy.NonMotorPolicyDto;
import cn.com.libertymutual.sp.webService.policy.PolicyDetailInquiryRequestDto;
import cn.com.libertymutual.sp.webService.policy.PolicyDetailInquiryResponseDto;
import cn.com.libertymutual.sp.webService.policy.PolicyDetailInquiryService;
import cn.com.libertymutual.sp.webService.policy.PolicyDetailInquiryService_Service;
import cn.com.libertymutual.sp.webService.policy.PrpTcarShipTaxDto;
import cn.com.libertymutual.sp.webService.policy.PrpTitemCarDto;
import cn.com.libertymutual.sp.webService.policy.PrptApplicantDto;
import cn.com.libertymutual.sp.webService.policy.PrptInsuredDto;
import cn.com.libertymutual.sp.webService.policy.PrptItemKindDto;
import cn.com.libertymutual.sp.webService.policy.PrptMainDto;
import cn.com.libertymutual.sp.webService.policy.RequestHeadDto;
import cn.com.libertymutual.sys.bean.SysServiceInfo;

/**
 * 
 * @author xrxianga
 * @date 2014-6-17
 */
@Service(value="policyService")
public class PolicyServiceImpl implements PolicyService{
	private static final Logger logger = LoggerFactory.getLogger(PolicyServiceImpl.class);
	@Resource
	private RedisUtils redis;
	@Resource
	private RestTemplate restTemplate;
	/**
	 * 解析接口返回报文，获取并封装数据
	 * @author xrxianga
	 * @param sb 接口报文
	 * @return
	 * @throws Exception
	 * @date 2014-7-1下午02:02:27
	 */
	public List<Map<String, Object>> doQueryAllPolicy(String sb)throws Exception{
		List<Map<String, Object>> listMap = null;
		try {
			logger.info("---------------解析接口返回报文----开始---------------");
			Map<String, Object> map = (Map<String, Object>) XmlUtil.doElementMapData(sb.toString());
			if(null != map && !map.isEmpty()){
				listMap = new ArrayList<Map<String,Object>>();
				String code = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_CODE));//代码
				String message = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_MESSAGE));//提示信息
				logger.info("---返回代码："+code+"--------------提示信息："+message);
				List<Map<String, Object>> carList = (List<Map<String, Object>>)map.get(ObjectPojo.POLICY_ALL_CAR);//车险
				List<Map<String, Object>> familyList = (List<Map<String, Object>>)map.get(ObjectPojo.POLICY_ALL_FAMILY);//家财
				List<Map<String, Object>> personList = (List<Map<String, Object>>)map.get(ObjectPojo.POLICY_ALL_PERSON);//个人
				if(null != carList && !carList.isEmpty() && carList.size()>0){
					logger.info("----------车险信息有"+carList.size()+"条-----------");
					for (int i = 0; i < carList.size(); i++) {
						Map<String, Object> carMap = doAllPolicyMap(carList.get(i),ObjectPojo.LIBERTY_TYPE_AUTOMOBILE);
						if(null!=carMap && !carMap.isEmpty()){
							listMap.add(carMap);
						}
					}
				}else{
					Map<String, Object> carMap = doAllPolicyMap(map,ObjectPojo.LIBERTY_TYPE_AUTOMOBILE);
					if(null!=carMap && !carMap.isEmpty()){
						logger.info("----------车险信息有1条-----------");
						listMap.add(carMap);
					}
				}
				if(null != familyList && !familyList.isEmpty() && familyList.size()>0){
					logger.info("----------家财险信息有"+familyList.size()+"条-----------");
					for (int j = 0; j < familyList.size(); j++) {
						Map<String, Object> familyMap = doAllPolicyMap(familyList.get(j),ObjectPojo.LIBERTY_TYPE_FAMILY);
						if(null!=familyMap && !familyMap.isEmpty()){
							listMap.add(familyMap);
						}
					}
				}else{
					Map<String, Object> familyMap = doAllPolicyMap(map,ObjectPojo.LIBERTY_TYPE_FAMILY);
					if(null!=familyMap && !familyMap.isEmpty()){
						logger.info("----------家财险信息有1条-----------");
						listMap.add(familyMap);
					}
				}
				if(null != personList && !personList.isEmpty() && personList.size()>0){
					logger.info("----------个人意外险信息有"+personList.size()+"条-----------");
					for (int k = 0; k < personList.size(); k++) {
						Map<String, Object> personMap = doAllPolicyMap(personList.get(k),ObjectPojo.LIBERTY_TYPE_PERSONAL_ACCIDENT);
						if(null!=personMap && !personMap.isEmpty()){
							listMap.add(personMap);
						}
					}
				}else{
					Map<String, Object> personMap = doAllPolicyMap(map,ObjectPojo.LIBERTY_TYPE_PERSONAL_ACCIDENT);
					if(null!=personMap && !personMap.isEmpty()){
						logger.info("----------个人意外险信息有1条-----------");
						listMap.add(personMap);
					}
				}
			}
			logger.info("---------------解析接口返回报文----结束---------------");
		} catch (Exception e) {
			logger.error("--------保单查询：获取查询所有保单接口返回报文解析时出现异常,异常原因："+e.getMessage());
			throw new Exception(e.getMessage(),e);
		}
		return listMap;
	}
	/**
	 * 封装接口返回的参数
	 * @author xrxianga
	 * @param map 参数集合对象
	 * @param type 数据集合类型
	 * @return
	 * @throws Exception
	 * @date 2014-7-1下午05:05:24
	 */
	private Map<String, Object> doAllPolicyMap(Map<String, Object> map,String type)throws Exception{
		Map<String, Object> result = null;
		try {
			logger.info("--------------封装接口返回的参数----开始---------------");
			if(null != map && !map.isEmpty()){
				result = new HashMap<String, Object>();
				if(ObjectPojo.LIBERTY_TYPE_AUTOMOBILE.equals(type)){//车险
					logger.info("------获取车险信息--------");
					result.put("policyCode",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_CAR_A)));//车牌号
					result.put("commercial_insurance_num",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_CAR_B)));//商业险保单号
					result.put("commercial_user",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_CAR_C)));//被保人
					result.put("beginDate",CommonUtil.toDate(CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_CAR_D)),"yyyy-MM-dd"));//保单开始时间
					result.put("endDate",CommonUtil.toDate(CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_CAR_E)),"yyyy-MM-dd"));//保单结束时间
					result.put("compulsory_insurance_num",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_CAR_F)));//交强险保单号
					result.put("compulsory_user",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_CAR_G)));//被保险人
					result.put("cps_begin_date",CommonUtil.toDate(CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_CAR_H)),"yyyy-MM-dd"));//交强险保单开始时间
					String endDate = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_CAR_I));
					result.put("cps_end_date",CommonUtil.toDate(endDate,"yyyy-MM-dd"));//交强险保单结束时间
					result.put("isOverdue", doIsOverdue(endDate, "yyyy-MM-dd"));
					
					result.put("insuranceType", ObjectPojo.LIBERTY_TYPE_AUTOMOBILE);//车险
					result.put("policyRequest", ObjectPojo.POLICY_REQUEST_AUTOMOBILE);//请求地址
				}else if(ObjectPojo.LIBERTY_TYPE_FAMILY.equals(type)){//家财
					logger.info("------获取家财险信息--------");
					result.put("policyCode",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_FAMILY_A)));//保单号
					result.put("user",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_FAMILY_B)));//被保人
					result.put("beginDate",CommonUtil.toDate(CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_FAMILY_C)),"yyyy-MM-dd"));//保单开始时间
					String endDate = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_FAMILY_D));
					result.put("endDate",CommonUtil.toDate(endDate,"yyyy-MM-dd"));//保单结束时间
					result.put("isOverdue", doIsOverdue(endDate, "yyyy-MM-dd"));
					
					result.put("insuranceType", ObjectPojo.LIBERTY_TYPE_FAMILY);//家庭财产险
					result.put("policyRequest", ObjectPojo.POLICY_REQUEST_FAMILY);//请求地址
				}else if(ObjectPojo.LIBERTY_TYPE_PERSONAL_ACCIDENT.equals(type)){//个人
					logger.info("------获取个人意外险信息--------");
					result.put("policyCode",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_PERSON_A)));//保单号
					result.put("user",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_PERSON_B)));//被保人
					result.put("beginDate",CommonUtil.toDate(CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_PERSON_C)),"yyyy-MM-dd"));//保单开始时间
					String endDate = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_ALL_PERSON_D));
					result.put("endDate",CommonUtil.toDate(endDate,"yyyy-MM-dd"));//保单结束时间
					result.put("isOverdue", doIsOverdue(endDate, "yyyy-MM-dd"));
					
					result.put("insuranceType", ObjectPojo.LIBERTY_TYPE_PERSONAL_ACCIDENT);//个人意外险
					result.put("policyRequest", ObjectPojo.POLICY_REQUEST_PERSONAL_ACCIDENT);//请求地址
				}
			}
			logger.info("--------------封装接口返回的参数----结束---------------");
		} catch (Exception e) {
			logger.error("--------保单查询：封装查询所有保单接口返回的数据时异常,异常原因："+e.getMessage());
			throw new Exception(e.getMessage(),e);
		}
		return result;
	}
	/**
	 * 解析车险接口返回报文
	 * @author xrxianga
	 * @param sb
	 * @return
	 * @throws Exception
	 * @date 2014-7-1下午05:23:40
	 */
	public Map<String, Object> doQueryAutomobile(String sb)throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		List<Map<String, Object>> listMap = null;
		Map<String, Object> valMap = null;
		try {
			logger.info("-----------------解析车险接口返回报文--开始-----------------");
			Map<String, Object> map = (Map<String, Object>) XmlUtil.doElementMapData(sb.toString());
			if(null!=map && !map.isEmpty()){
				//车险基本信息
				String code = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_CODE));//代码
				String message = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_MESSAGE));//提示信息
				logger.info("---返回代码："+code+"--------------提示信息："+message);
				//商业险
				String comNumCmc = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_A));
				if(!"".equals(comNumCmc)){
					logger.info("--------封装车险中的【商业险】信息-----");
					result.put("comNumCmc",comNumCmc);//保单号
					result.put("comPlatesCmc",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_B)));//车牌号
					result.put("comInsuredCmc",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_C)));//被保人
					result.put("comPolicyholderCmc",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_D)));//投保人
					String beginDate = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_E));
					String endDate = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_F));
					result.put("comBeginDateCmc",CommonUtil.toDate(beginDate,"yyyy-MM-dd"));//保单开始时间
					result.put("comEndDateCmc",CommonUtil.toDate(endDate,"yyyy-MM-dd"));//单结束时间
					result.put("engineNumCmc",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_G)));//发动机号
					result.put("frameNumCmc",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_H)));//车架号
					result.put("ratifiedCmc", CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_I)));//核定载质量
					result.put("modelCmc",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_J)));//厂牌车型
					result.put("carTypeCmc",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_K)));//车辆种类
					result.put("seatsCmc",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_L)));//座位数
					result.put("tonnageCmc",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_M)));//吨位
					result.put("premiumCmc",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_N)));//保费
					result.put("isDisplay",this.doValidationDate(beginDate, endDate,"yyyy-MM-dd"));
					//商业险承保信息
					logger.info("--------封装车险中的【商业险承保】信息-----");
					listMap = new ArrayList<Map<String,Object>>();
					List<Map<String, Object>> cmcList = (List<Map<String, Object>>) map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_O);//承保详情
					if(null!=cmcList && !cmcList.isEmpty() && cmcList.size()>0){
						for (Map<String, Object> cmcMap : cmcList) {
							valMap = new HashMap<String, Object>();
							valMap.put("type",CommonUtil.doEmpty(cmcMap.get(ObjectPojo.POLICY_CAR_COMMERCIAL_P)));//承保险别
							valMap.put("insuredAmount",CommonUtil.doEmpty(cmcMap.get(ObjectPojo.POLICY_CAR_COMMERCIAL_Q)));//保险金额
							valMap.put("premiumAmount",CommonUtil.doEmpty(cmcMap.get(ObjectPojo.POLICY_CAR_COMMERCIAL_R)));//保费金额
							listMap.add(valMap);
						}
					}else{
						valMap = new HashMap<String, Object>();
						String type = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_P));
						String insuredAmount = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_Q));
						String premiumAmount = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMMERCIAL_R));
						if(!"".equals(type) || !"".equals(insuredAmount) || !"".equals(premiumAmount)){
							valMap.put("type",type);//承保险别
							valMap.put("insuredAmount",insuredAmount);//保险金额
							valMap.put("premiumAmount",premiumAmount);//保费金额
							listMap.add(valMap);
						}
					}
					if(null!=listMap && !listMap.isEmpty() && listMap.size()>0){
						result.put("underwritingDetailsCmc", listMap);
					}
				}
				
				//交强险
				String comNumCpsr = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_A));
				if(!"".equals(comNumCpsr)){
					logger.info("--------封装车险中的【交强险】信息-----");
					result.put("comNumCpsr",comNumCpsr);//保单号
					result.put("comPlatesCpsr",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_B)));//车牌号
					result.put("comInsuredCpsr",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_C)));//被保人
					result.put("comPolicyholderCpsr",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_D)));//投保人
					result.put("comBeginDateCpsr",CommonUtil.toDate(CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_E)),"yyyy-MM-dd"));//保单开始时间
					result.put("comEndDateCpsr",CommonUtil.toDate(CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_F)),"yyyy-MM-dd"));//单结束时间
					result.put("engineNumCpsr",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_G)));//发动机号
					result.put("frameNumCpsr",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_H)));//车架号
					result.put("ratifiedCpsr", CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_I)));//核定载质量
					result.put("modelCpsr",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_J)));//厂牌车型
					result.put("carTypeCpsr",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_K)));//车辆种类
					result.put("seatsCpsr",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_L)));//座位数
					result.put("tonnageCpsr",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_M)));//吨位
					//交强险承保信息
					logger.info("--------封装车险中的【交强险承保】信息-----");
					listMap = new ArrayList<Map<String,Object>>();
					List<Map<String, Object>> cpsrList = (List<Map<String, Object>>) map.get(ObjectPojo.POLICY_CAR_COMPULSORY_N);//承保详情
					if(null!=cpsrList && !cpsrList.isEmpty() && cpsrList.size()>0){
						for (Map<String, Object> cpsrMap : cpsrList) {
							valMap = new HashMap<String, Object>();
							valMap.put("type",CommonUtil.doEmpty(cpsrMap.get(ObjectPojo.POLICY_CAR_COMPULSORY_O)));//承保险别
							valMap.put("insuredAmount",CommonUtil.doEmpty(cpsrMap.get(ObjectPojo.POLICY_CAR_COMPULSORY_P)));//保险金额
							valMap.put("premiumAmount",CommonUtil.doEmpty(cpsrMap.get(ObjectPojo.POLICY_CAR_COMPULSORY_Q)));//保费金额
							valMap.put("vesselTaxAmount",CommonUtil.doEmpty(cpsrMap.get(ObjectPojo.POLICY_CAR_COMPULSORY_R)));//车船税金额
							listMap.add(valMap);
						}
					}else{
						valMap = new HashMap<String, Object>();
						String type = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_O));
						String insuredAmount = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_P));
						String premiumAmount = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_Q));
						String vesselTaxAmount = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_CAR_COMPULSORY_R));
						if(!"".equals(type) ||!"".equals(insuredAmount) 
								||!"".equals(premiumAmount) || !"".equals(vesselTaxAmount)){
							valMap.put("type",type);//承保险别
							valMap.put("insuredAmount",insuredAmount);//保险金额
							valMap.put("premiumAmount",premiumAmount);//保费金额
							valMap.put("vesselTaxAmount",vesselTaxAmount);//车船税金额
							listMap.add(valMap);
						}
					}
					if(null!=listMap && !listMap.isEmpty() && listMap.size()>0){
						result.put("underwritingDetailsCpsr", listMap);
					}
				}
			}
			logger.info("-----------------解析车险接口返回报文--结束-----------------");
		} catch (Exception e) {
			logger.error("--------车险保单：获取车险保单接口返回报文解析时出现异常,异常原因："+e.getMessage());
			throw new Exception(e.getMessage(),e);
		}
		return result;
	}
	/**
	 * 验证保单日期（如果此保单是30天以内和已经过期的保单，页面下面就多个按钮“我要续保”）
	 * @author xrxianga
	 * @param beginDate
	 * @param endDate
	 * @return
	 * @throws Exception
	 * @date 2014-7-3下午05:00:04
	 */
	private String doValidationDate(String beginDate,String endDate,String format)throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Calendar cd = Calendar.getInstance();
		String str = UtilTool.getCurrentSysDate(format);//系统当前日期
		logger.info("--------保险起期："+beginDate+"---------保险止期："+endDate+"---------系统当前日期："+str);
		cd.setTime(sdf.parse(str));
		long date = cd.getTimeInMillis();
		cd.setTime(sdf.parse(beginDate));
//		long begin = cd.getTimeInMillis();
		cd.setTime(sdf.parse(endDate));
		long end = cd.getTimeInMillis();
		if(end <= date){//已过期
			return ObjectPojo.POLICY_BUTTON_YES;
		}
		long day = (end - date)/(1000*3600*24);
		if(day <= 30){//距离过期日期小于30天
			return ObjectPojo.POLICY_BUTTON_YES;
		}
		return ObjectPojo.POLICY_BUTTON_NO;
	}
	/**
	 * 判断保险止期是否大于当前日期
	 * @author xrxianga
	 * @param endDate
	 * @param format
	 * @return
	 * @throws Exception
	 * @date 2014-7-4下午02:12:25
	 */
	private String doIsOverdue(String endDate,String format)throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Calendar cd = Calendar.getInstance();
		String str = UtilTool.getCurrentSysDate(format);//系统当前日期
//		logger.info("---------保险止期："+endDate+"---------系统当前日期："+str);
		if(!"".equals(endDate)){
			cd.setTime(sdf.parse(str));
			long date = cd.getTimeInMillis();
			cd.setTime(sdf.parse(endDate));
			long end = cd.getTimeInMillis();
			if(end >= date){
				return ObjectPojo.POLICY_YES;
			}
		}
		return ObjectPojo.POLICY_NO;
	}
	/**
	 * 家财险接口报文解析
	 * @author xrxianga
	 * @param sb
	 * @return
	 * @throws Exception
	 * @date 2014-7-4上午09:55:58
	 */
	public Map<String, Object> doQueryFamily(String sb)throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		List<Map<String, Object>> listMap = null;
		Map<String, Object> mp = null;
		try {
			logger.info("----------家财险接口报文解析----开始-----------");
			Map<String, Object> map = (Map<String, Object>) XmlUtil.doElementMapData(sb.toString());
			if(null!=map && !map.isEmpty()){
				logger.info("---------有报文内容---开始解析------------");
				String code = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_FAMILY_CODE));//代码
				String message = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_FAMILY_MESSAGE));//提示信息
				logger.info("---返回代码："+code+"--------------提示信息："+message);
				String num = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_FAMILY_NUM));//保单号
				if(!"".equals(num)){
					listMap = new ArrayList<Map<String,Object>>();
					logger.info("-----------保单存在----继续解析----------");
					result.put("num",num);
					result.put("insured",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_FAMILY_INSURED)));//被保人
					result.put("policyholder",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_FAMILY_POLICYHOLDER)));//投保人
					result.put("beginDate",CommonUtil.toDate(CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_FAMILY_BEGINDATE)),"yyyy-MM-dd"));//保单开始时间
					result.put("endDate",CommonUtil.toDate(CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_FAMILY_ENDDATE)),"yyyy-MM-dd"));//保单结束时间
					result.put("premiumAmount",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_FAMILY_PREMIUMAMOUNT)));//保费金额
					List<Map<String, Object>> valList = (List<Map<String, Object>>) map.get(ObjectPojo.POLICY_FAMILY_DETAIL);//详情
					if(null!=valList && !valList.isEmpty() && valList.size()>0){
						for (int i = 0; i < valList.size(); i++) {
							mp = new HashMap<String, Object>();
							Map<String, Object> valMap = valList.get(i);
							mp.put("name",CommonUtil.doEmpty(valMap.get(ObjectPojo.POLICY_FAMILY_DETAIL_NAME)));//责任名称
							mp.put("amount",CommonUtil.doEmpty(valMap.get(ObjectPojo.POLICY_FAMILY_DETAIL_AMOUNT)));//保险金额
							listMap.add(mp);
						}
					}else{
						String name = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_FAMILY_DETAIL_NAME));//责任名称
						String amount = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_FAMILY_DETAIL_AMOUNT));//保险金额
						if(!"".equals(name) || !"".equals(amount)){
							mp = new HashMap<String, Object>();
							mp.put("name",name);
							mp.put("amount",amount);
							listMap.add(mp);
						}
					}
					if(null!=listMap && !listMap.isEmpty() && listMap.size()>0){
						logger.info("--------存在保单责任内容-------------");
						result.put("underwritingDetails", listMap);
					}
					logger.info("---------报文解析结束------------");
				}
			}
			logger.info("------------家财险接口报文解析----结束---------");
		} catch (Exception e) {
			logger.error("--------家财险保单：获取家财险保单接口返回报文解析时出现异常,异常原因："+e.getMessage());
			throw new Exception(e.getMessage(),e);
		}
		return result;
	}
	/**
	 * 个人意外险接口报文解析
	 * @author xrxianga
	 * @param sb
	 * @return
	 * @throws Exception
	 * @date 2014-7-4上午11:58:58
	 */
	public Map<String, Object> doQueryPersonalAccident(String sb)throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		List<Map<String, Object>> listMap = null;
		Map<String, Object> mp = null;
		try {
			logger.info("----------个人意外险接口报文解析----开始-----------");
			Map<String, Object> map = (Map<String, Object>) XmlUtil.doElementMapData(sb.toString());
			if(null!=map && !map.isEmpty()){
				logger.info("---------有报文内容---开始解析------------");
				String code = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_PERSON_CODE));//代码
				String message = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_PERSON_MESSAGE));//提示信息
				logger.info("---返回代码："+code+"--------------提示信息："+message);
				String num = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_PERSON_COMNUM));//保单号
				if(!"".equals(num)){
					listMap = new ArrayList<Map<String,Object>>();
					logger.info("-----------保单存在----继续解析----------");
					result.put("num",num);
					result.put("insured",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_PERSON_COMINSURED)));//被保人
					result.put("policyholder",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_PERSON_COMPOLICYHOLDER)));//投保人
					result.put("beginDate",CommonUtil.toDate(CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_PERSON_COMBEGINDATE)),"yyyy-MM-dd"));//保单开始时间
					result.put("endDate",CommonUtil.toDate(CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_PERSON_COMENDDATE)),"yyyy-MM-dd"));//保单结束时间
					result.put("premiumAmount",CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_PERSON_PREMIUMAMOUNT)));//保费金额
					List<Map<String, Object>> valList = (List<Map<String, Object>>) map.get(ObjectPojo.POLICY_PERSON_DETAIL);//详情
					if(null!=valList && !valList.isEmpty() && valList.size()>0){
						for (int i = 0; i < valList.size(); i++) {
							mp = new HashMap<String, Object>();
							Map<String, Object> valMap = valList.get(i);
							mp.put("name",CommonUtil.doEmpty(valMap.get(ObjectPojo.POLICY_PERSON_DETAIL_NAME)));//责任名称
							mp.put("amount",CommonUtil.doEmpty(valMap.get(ObjectPojo.POLICY_PERSON_DETAIL_AMOUNT)));//保险金额
							listMap.add(mp);
						}
					}else{
						String name = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_PERSON_DETAIL_NAME));//责任名称
						String amount = CommonUtil.doEmpty(map.get(ObjectPojo.POLICY_PERSON_DETAIL_AMOUNT));//保险金额
						if(!"".equals(name) || !"".equals(amount)){
							mp = new HashMap<String, Object>();
							mp.put("name",name);
							mp.put("amount",amount);
							listMap.add(mp);
						}
					}
					if(null!=listMap && !listMap.isEmpty() && listMap.size()>0){
						logger.info("--------存在保单责任内容-------------");
						result.put("underwritingDetails", listMap);
					}
					logger.info("---------报文解析结束------------");
				}
			}
			logger.info("------------个人意外险接口报文解析----结束---------");
		} catch (Exception e) {
			logger.error("--------个人意外险保单：获取个人意外险保单接口返回报文解析时出现异常,异常原因："+e.getMessage());
			throw new Exception(e.getMessage(),e);
		}
		return result;
	}
	
	//===========================================webservices cxf client===========================================================================
	
	/**
	 * 保单查询--保单列表查询
	 * @author xrxianga
	 * @param idCard 证件号码
	 * @param phoneNum 电话号码
	 * @return
	 * @throws Exception
	 * @date 2014-8-7上午15:19:58
	 */
	/*public List<Map<String, Object>> doCxfQueryAllPolicy(String idCard,String phoneNum)throws Exception{
		List<Map<String,Object>> listMap = null;
		Date d1 = Calendar.getInstance().getTime();
		
		QName SERVICE_NAME = new QName("http://prpall.liberty.com/all/cb/policyListQuery/intf","PolicyListQueryService");
//		URL wsdlURL = PolicyListQueryService_Service.WSDL_LOCATION;
		URL wsdlURL = new URL(SpringContextUtil.getProperty("liberty.query.all.policy.url_b"));
		PolicyListQueryService_Service ss = new PolicyListQueryService_Service(wsdlURL, SERVICE_NAME);
		PolicyListQueryService port = ss.getPolicyListQueryServicePort();
		
		RequestHeadDto requestHeadDto = new RequestHeadDto(); 
		requestHeadDto.setPassword(SpringContextUtil.getProperty("liberty.webservice.policy.requestHeadDto.setPassword"));//密码
		requestHeadDto.setConsumerId(SpringContextUtil.getProperty("liberty.webservice.policy.requestHeadDto.setConsumerId"));//使用Id
		requestHeadDto.setUsercode(SpringContextUtil.getProperty("liberty.webservice.policy.requestHeadDto.setUsercode"));//用户编码
		
		PolicyListQueryRequestDto dto = new PolicyListQueryRequestDto();
		dto.setRequestHeadDto(requestHeadDto);
		dto.setInsuredIdentityNumber(idCard);//证件号码
		dto.setPhoneNumber(phoneNum);//手机或座机号码	
				
		PolicyListQueryResponseDto result = port.policyListQuery(dto);
//		ObjectCache.writeObj(result, "policyListQueryResponseDto_"+dto.getInsuredIdentityNumber().toString());
//		PolicyListQueryResponseDto result = (PolicyListQueryResponseDto) ObjectCache.readObj("policyListQueryResponseDto_"+dto.getInsuredIdentityNumber().toString());
		Date d2 = Calendar.getInstance().getTime();
		long runTime =d2.getTime() - d1.getTime();
		logger.info("保单webservice接口：查询所有保单接口用时为："+(runTime/1000.0));
		
		if(null != result){
			Map<String, Object> resultMap = null;
			listMap = new ArrayList<Map<String,Object>>();
			//车险
			List<MotorPolicyDto> motorPolicyDtoList = result.getMotorPolicyDtoList();
			for (MotorPolicyDto carDto : motorPolicyDtoList) {
				resultMap = new HashMap<String, Object>();
				PrpTitemCarDto prpTitemCarDto = carDto.getPrpTitemCarDto();//车辆信息
				if(null != prpTitemCarDto){
					resultMap.put("licnesNo", prpTitemCarDto.getLicenseNo());//车牌号
					resultMap.put("carBrand", prpTitemCarDto.getCarBrand());//品牌
				}
				PrptMainDto prptMainDto = carDto.getPrptMainDto();//保单基本信息
				if(null != prptMainDto){
					Date begin = prptMainDto.getStartDate().toGregorianCalendar().getTime();
					Date end = prptMainDto.getEndDate().toGregorianCalendar().getTime();
					String isOverdue = doIsOverdue(CommonUtil.toString(end, "yyyy-MM-dd"), "yyyy-MM-dd");
					resultMap.put("policyType", prptMainDto.getRiskCode());//险种类型
					resultMap.put("policyCode", prptMainDto.getPolicyNo());//保单号
					resultMap.put("beginDate", begin);//保单开始时间
					resultMap.put("endDate", end);//保单结束时间
					resultMap.put("isOverdue", isOverdue);//是否过期
				}
				resultMap.put("insuranceType", ObjectPojo.LIBERTY_TYPE_AUTOMOBILE);//车险
				resultMap.put("policyRequest", ObjectPojo.POLICY_REQUEST_AUTOMOBILE);//请求地址
				listMap.add(resultMap);
			}
			//非车险
			List<NonMotorPolicyDto> nonMotorPolicyDtoList = result.getNonMotorPolicyDtoList();
			for (NonMotorPolicyDto nonCarDto : nonMotorPolicyDtoList) {
				PrptMainDto prptMainDto = nonCarDto.getPrptMainDto();
				if(null != prptMainDto){
					resultMap = new HashMap<String, Object>();
					List<com.isoftstone.liberty.webservice.allpolicy.PrptInsuredDto> prptInsuredDtoList = nonCarDto.getPrptInsuredDtoList();
					if(null != prptInsuredDtoList && prptInsuredDtoList.size()>0){
						String com_insured = "";
						for (com.isoftstone.liberty.webservice.allpolicy.PrptInsuredDto prptInsuredDto : prptInsuredDtoList) {
							if(null!=com_insured && !"".equals(com_insured)){
								com_insured += "，";//使用符号 "，" 间隔开
							}
							com_insured += prptInsuredDto.getPrpInsuredInsuredName();//被保人
						}
						resultMap.put("insured",com_insured);//被保人
					}
					String riskCode = CommonUtil.doEmpty(prptMainDto.getRiskCode());//险种
					Date startDate = prptMainDto.getStartDate().toGregorianCalendar().getTime();//起始日期
					Date endDate = prptMainDto.getEndDate().toGregorianCalendar().getTime();//终止日期
					String isOverdue = doIsOverdue(CommonUtil.toString(endDate,"yyyy-MM-dd"), "yyyy-MM-dd");//是否过期
					if(ObjectPojo.POLICY_NONCAR_FAMILY.equals(riskCode)){//家庭财产险
						resultMap.put("insuranceType", ObjectPojo.LIBERTY_TYPE_FAMILY);//家庭财产险
						resultMap.put("policyRequest", ObjectPojo.POLICY_REQUEST_FAMILY);//请求地址
					}else if(ObjectPojo.POLICY_NONCAR_PERSON.equals(riskCode)){//个人意外险
						resultMap.put("insuranceType", ObjectPojo.LIBERTY_TYPE_PERSONAL_ACCIDENT);//个人意外险
						resultMap.put("policyRequest", ObjectPojo.POLICY_REQUEST_PERSONAL_ACCIDENT);//请求地址
					}
					resultMap.put("policyCode", prptMainDto.getPolicyNo());//保单号
					resultMap.put("beginDate", startDate);
					resultMap.put("endDate", endDate);
					resultMap.put("isOverdue", isOverdue);
					listMap.add(resultMap);
				}
			}
		}
		logger.info("【保单查询--保单列表查询】请求响应集合："+listMap.toString());
		return listMap;
	}*/
	/**
	 * 保单查询--非车险详情查询（家财、个人）
	 * @author xrxianga
	 * @param policyCode 保单号
	 * @return
	 * @throws Exception
	 * @date 2014-8-7下午04:42:13
	 */
	@Override
	public Map<String, Object> doCxfQueryNonCarPolicy(String policyCode)throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		List<Map<String, Object>> listMap = null;
		
		QName SERVICE_NAME = new QName("http://prpall.liberty.com/all/cb/policyDetailInquiry/intf", "PolicyDetailInquiryService");
//		URL wsdlURL = PolicyDetailInquiryService_Service.WSDL_LOCATION;
		Map map = (Map) redis.get(Constants.SYS_SERVICE_INFO);
		SysServiceInfo sysServiceInfo = (SysServiceInfo) map.get(CoreServiceEnum.WX_Policy_Detail.getUrlKey());
		
		URL wsdlURL = new URL(sysServiceInfo.getUrl());
		PolicyDetailInquiryService_Service ss = new PolicyDetailInquiryService_Service(wsdlURL, SERVICE_NAME);
		PolicyDetailInquiryService port = ss.getPolicyDetailInquiryServicePort();
		
		RequestHeadDto requestHeadDto = new RequestHeadDto();
		requestHeadDto.setPassword(sysServiceInfo.getPassword());//密码
		requestHeadDto.setConsumerId(sysServiceInfo.getTocken());//使用Id
		requestHeadDto.setUsercode(sysServiceInfo.getUserName());//用户编码
		
		PolicyDetailInquiryRequestDto dto = new PolicyDetailInquiryRequestDto();
		dto.setRequestHeadDto(requestHeadDto);
        dto.setPolicyNo(policyCode);
        
        Date d1 = Calendar.getInstance().getTime();
        PolicyDetailInquiryResponseDto resultDto = port.policyDetailInquiry(dto);
//        ObjectCache.writeObj(result, "policyDetailInquiryResponseDto_"+dto.getPolicyNo().toString());
//        PolicyDetailInquiryResponseDto resultDto = (PolicyDetailInquiryResponseDto) ObjectCache.readObj("policyDetailInquiryResponseDto_"+dto.getPolicyNo().toString());
        Date d2 = Calendar.getInstance().getTime();
		long runTime =d2.getTime() - d1.getTime();
		
		logger.info("保单查询--非车险详情查询（家财or个人）：非车险webservice接口请求用时："+(runTime/1000.0)+"秒");
		
		//非车险
		NonMotorPolicyDto  dtoa = resultDto.getNonMotorPolicyDto();
		if(null!=dtoa){
			String com_insured = "";
			List<PrptInsuredDto> prptInsuredDtoList = dtoa.getPrptInsuredDtoList();
			String insuredAddress = "";
			for (PrptInsuredDto prptInsuredDto: prptInsuredDtoList) {
				String address = CommonUtil.doEmpty(prptInsuredDto.getInsuredAddress());
				if(!"".equals(address)){
//					insuredAddress += insuredAddress == ""?address:"<br>"+address;
					if("".equals(insuredAddress)){
						insuredAddress = address;
					}else{
						insuredAddress += "<br>"+address;
					}
				}
			}
			result.put("insuredAddress",insuredAddress);//被保险人地址
			PrptMainDto prptMainDto = dtoa.getPrptMainDto();
			if(null != prptMainDto){
				Date beginDate = prptMainDto.getStartDate().toGregorianCalendar().getTime();//保单开始时间
				Date endDate = prptMainDto.getEndDate().toGregorianCalendar().getTime();//保单结束时间
				result.put("policyType",prptMainDto.getRiskCode());//保单险别
			    result.put("num",prptMainDto.getPolicyNo());//保单号
			    result.put("beginDate",beginDate);//保单开始时间
				result.put("endDate",endDate);//保单结束时间
				result.put("premiumAmount",prptMainDto.getSumPremium());//保费金额
			}
			PrptApplicantDto prptApplicantDto = dtoa.getPrptApplicantDto();
			if(null != prptApplicantDto){
				result.put("policyholder",prptApplicantDto.getAppliPrpInsuredInsuredName());//投保人
			}
			List<PrptInsuredDto> prptInsuredDto = dtoa.getPrptInsuredDtoList();
			if(null != prptInsuredDto && prptInsuredDto.size()>0){
				for (PrptInsuredDto insuredDto : prptInsuredDto) {
					if(null!=com_insured && !"".equals(com_insured)){
						com_insured += "，";//使用符号 "，" 间隔开
					}
					com_insured += insuredDto.getPrpInsuredInsuredName();//被保人
				}
				result.put("insured",com_insured);//被保人
			}
			List<PrptItemKindDto>  PrptItemKindDto = dtoa.getPrptItemKindDtoList();
			if(null!=PrptItemKindDto && PrptItemKindDto.size()>0){
				Map<String,Object> mp = null;
				listMap = new ArrayList<Map<String,Object>>();
			    for (PrptItemKindDto daow : PrptItemKindDto) {
			    	mp = new HashMap<String, Object>();
					mp.put("name",StringUtil.isEmpty(daow.getItemDetailName())?daow.getKindNameMain():daow.getItemDetailName());//责任名称
					mp.put("amountMain",daow.getAmountMain());//保险金额
					mp.put("premiumMain",daow.getPremiumMain());//保费金额
					
					listMap.add(mp);
			    }
			    result.put("underwritingDetails", listMap);
			}
		}
		logger.info("【保单查询--非车险详情查询（家财、个人）】请求响应集合："+result.toString());
		return result;
	}
	/**
	 * 保单查询--查询车险详情（商业or交强）
	 * @author xrxianga
	 * @param policyCode 保单号
	 * @return
	 * @throws Exception
	 * @date 2014-8-8下午02:56:03
	 */
	@Override
	public Map<String, Object> doCxfQueryCarPolicy(String policyCode)throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		List<Map<String, Object>> listMap = null;
		Map<String, Object> valMap = null;
		String insuredName = "";
		
		Date d1 = Calendar.getInstance().getTime();
		QName SERVICE_NAME = new QName("http://prpall.liberty.com/all/cb/policyDetailInquiry/intf", "PolicyDetailInquiryService");
//		URL wsdlURL = PolicyDetailInquiryService_Service.WSDL_LOCATION;
		Map map = (Map) redis.get(Constants.SYS_SERVICE_INFO);
		SysServiceInfo sysServiceInfo = (SysServiceInfo) map.get(CoreServiceEnum.WX_Policy_Detail.getUrlKey());
		
		URL wsdlURL = new URL(sysServiceInfo.getUrl());
		PolicyDetailInquiryService_Service ss = new PolicyDetailInquiryService_Service(wsdlURL, SERVICE_NAME);
		PolicyDetailInquiryService port = ss.getPolicyDetailInquiryServicePort();
		
		RequestHeadDto requestHeadDto = new RequestHeadDto();
		requestHeadDto.setPassword(sysServiceInfo.getPassword());//密码
		requestHeadDto.setConsumerId(sysServiceInfo.getTocken());//使用Id
		requestHeadDto.setUsercode(sysServiceInfo.getUserName());//用户编码
		
		PolicyDetailInquiryRequestDto dto = new PolicyDetailInquiryRequestDto();
		dto.setRequestHeadDto(requestHeadDto);
        dto.setPolicyNo(policyCode);
        
        PolicyDetailInquiryResponseDto resultDto = port.policyDetailInquiry(dto);
//        ObjectCache.writeObj(result, "policyDetailInquiryResponseDto_"+dto.getPolicyNo().toString());
//        PolicyDetailInquiryResponseDto resultDto = (PolicyDetailInquiryResponseDto) ObjectCache.readObj("policyDetailInquiryResponseDto_"+dto.getPolicyNo().toString());
        Date d2 = Calendar.getInstance().getTime();
		long runTime =d2.getTime() - d1.getTime();
		
		logger.info("保单查询--查询车险详情（商业or交强）：车险webservice接口请求用时："+(runTime/1000.0)+"秒");
		
//		String riskCode = "";//车险保单险种
//		MotorPolicyDto motorPolicyDto = resultDto.getMotorPolicyDto();
//		MtplPolicyDto dtocs = resultDto.getMTPLPolicyDto();
//		if(null != motorPolicyDto){
//			PrptMainDto prptMainDto = motorPolicyDto.getPrptMainDto();
//			if(null != prptMainDto){
//				riskCode = prptMainDto.getRiskCode();
//			}
//		}
//		if(ObjectPojo.POLICY_CAR_COMMERCIAL.equals(riskCode)){//商业险
			MotorPolicyDto dtob = resultDto.getMotorPolicyDto();
			if (dtob != null) {
				PrptApplicantDto prptApplicantDto = dtob.getPrptApplicantDto();
				if(null != prptApplicantDto){
					result.put("comPolicyholderCmc",prptApplicantDto.getAppliPrpInsuredInsuredName());//投保人
				}
				List<PrptInsuredDto> prptInsuredDto = dtob.getPrptInsuredDtoList();
				if(null != prptInsuredDto && prptInsuredDto.size()>0){
					for (PrptInsuredDto insuredDto : prptInsuredDto) {
						if(null != insuredName && !"".equals(insuredName)){
							insuredName += "，";
						}
						insuredName += insuredDto.getPrpInsuredInsuredName();//被保人
					}
					result.put("comInsuredCmc",insuredName);//被保人
				}
				PrptMainDto prptMainDto = dtob.getPrptMainDto();
				if(null != prptMainDto){
					Date startDate = prptMainDto.getStartDate().toGregorianCalendar().getTime();//保单开始时间
					Date endDate = prptMainDto.getEndDate().toGregorianCalendar().getTime();//单结束时间
					result.put("premiumCmc",prptMainDto.getSumPremium());//保费
					result.put("comNumCmc",prptMainDto.getPolicyNo());//保单号
					result.put("comBeginDateCmc",startDate);//保单开始时间
					result.put("comEndDateCmc",endDate);//保单结束时间
					result.put("isDisplay",this.doValidationDate(CommonUtil.toString(startDate,"yyyy-MM-dd"),CommonUtil.toString(endDate,"yyyy-MM-dd"),"yyyy-MM-dd"));//
				}
				PrpTitemCarDto prpTitemCarDto = dtob.getPrpTitemCarDto();
				if(null != prpTitemCarDto){
					String engineNo = CommonUtil.doEmpty(prpTitemCarDto.getEngineNo());//发动机号
					String vINNo = CommonUtil.doEmpty(prpTitemCarDto.getVINNo());//车架号
					
					engineNo = CommonUtil.doReplaceStr(engineNo, "****", 1, 5);//发动机号隐藏位数
					vINNo = CommonUtil.doReplaceStr(vINNo, "*******", 6, 13);//车架号隐藏位数
					
				    result.put("comPlatesCmc",prpTitemCarDto.getLicenseNo());//车牌号
				    result.put("engineNumCmc",engineNo);//发动机号
					result.put("frameNumCmc",vINNo);//车架号
					result.put("ratifiedCmc", prpTitemCarDto.getTonCount());//核定载质量
					result.put("modelCmc",prpTitemCarDto.getBrandName());//厂牌车型
					result.put("carTypeCmc",prpTitemCarDto.getCarKindCode());//车辆种类
					result.put("seatsCmc",prpTitemCarDto.getSeatCount());//座位数
//					result.put("tonnageCmc",prpTitemCarDto.getTonCount());//吨位
				}
				List<PrptItemKindDto>  PrptItemKindDto = dtob.getPrptItemKindDtoList();
			    if(null!=PrptItemKindDto && PrptItemKindDto.size()>0){
				    listMap = new ArrayList<Map<String,Object>>();
				    for (PrptItemKindDto dao : PrptItemKindDto) {
				    	valMap = new HashMap<String, Object>();
						valMap.put("type",dao.getKindNameMain());//承保险别
						valMap.put("insuredAmount",dao.getAmountMain());//保险金额
						valMap.put("premiumAmount",dao.getPremiumMain());//保费金额
						listMap.add(valMap);
				    }
				    result.put("underwritingDetailsCmc", listMap);
			    }
			}
//		}else if(ObjectPojo.POLICY_CAR_COMPULSORY.equals(riskCode)){//交强险
			MtplPolicyDto dtoc = resultDto.getMTPLPolicyDto();
			if (dtoc != null) {
				PrptApplicantDto prptApplicantDto = dtoc.getPrptApplicantDto();
				if(null != prptApplicantDto){
					result.put("comPolicyholderCpsr",prptApplicantDto.getAppliPrpInsuredInsuredName());//投保人
				}
				List<PrptInsuredDto> prptInsuredDto = dtoc.getPrptInsuredDtoList();
				if(null != prptInsuredDto && prptInsuredDto.size()>0){
					insuredName="";
					for (PrptInsuredDto insuredDto : prptInsuredDto) {
						if(null != insuredName && !"".equals(insuredName)){
							insuredName += "，";
						}
						insuredName += insuredDto.getPrpInsuredInsuredName();//被保人
					}
					result.put("comInsuredCpsr",insuredName);//被保人
				}
				PrptMainDto prptMainDto = dtoc.getPrptMainDto();
				if(null != prptMainDto){
					Date startDate = prptMainDto.getStartDate().toGregorianCalendar().getTime();//保单开始时间
					Date endDate = prptMainDto.getEndDate().toGregorianCalendar().getTime();//单结束时间
					result.put("comNumCpsr",prptMainDto.getPolicyNo());//保单号
					result.put("comBeginDateCpsr",startDate);//保单开始时间
					result.put("comEndDateCpsr",endDate);//保单结束时间
					result.put("isDisplay",this.doValidationDate(CommonUtil.toString(startDate,"yyyy-MM-dd"),CommonUtil.toString(endDate,"yyyy-MM-dd"),"yyyy-MM-dd"));//
				}
				PrpTitemCarDto prpTitemCarDto = dtoc.getPrpTitemCarDto();
				if(null != prpTitemCarDto){
					String engineNo = CommonUtil.doEmpty(prpTitemCarDto.getEngineNo());//发动机号
					String vINNo = CommonUtil.doEmpty(prpTitemCarDto.getVINNo());//车架号
					
					engineNo = CommonUtil.doReplaceStr(engineNo, "****", 1, 5);//发动机号隐藏位数
					vINNo = CommonUtil.doReplaceStr(vINNo, "*******", 6, 13);//车架号隐藏位数
					
				    result.put("comPlatesCpsr",prpTitemCarDto.getLicenseNo());//车牌号
				    result.put("engineNumCpsr",engineNo);//发动机号
					result.put("frameNumCpsr",vINNo);//车架号
					result.put("ratifiedCpsr", prpTitemCarDto.getTonCount());//核定载质量
					result.put("modelCpsr",prpTitemCarDto.getBrandName());//厂牌车型
					result.put("carTypeCpsr",prpTitemCarDto.getCarKindCode());//车辆种类
					result.put("seatsCpsr",prpTitemCarDto.getSeatCount());//座位数
//					result.put("tonnageCpsr",prpTitemCarDto.getTonCount());//吨位
				}
				List<PrptItemKindDto>  prptItemKindDto = dtoc.getPrptItemKindDtoList();
				if(null != prptItemKindDto && prptItemKindDto.size()>0){
					listMap = new ArrayList<Map<String,Object>>();
					PrpTcarShipTaxDto prpTcarShipTaxDto = dtoc.getPrpTcarShipTaxDto();
					for (PrptItemKindDto dsto : prptItemKindDto) {
						valMap = new HashMap<String, Object>();
						valMap.put("type",dsto.getKindNameMain());//承保险别
						valMap.put("insuredAmount",dsto.getAmountMain());//保险金额
						valMap.put("premiumAmount",dsto.getPremiumMain());//保费金额
						valMap.put("vesselTaxAmount",prpTcarShipTaxDto.getSumTax());//车船税金额
						listMap.add(valMap);
					}
					result.put("underwritingDetailsCpsr", listMap);
				}
			}
//		}
		logger.info("【保单查询--查询车险详情（商业or交强）】请求响应集合："+result.toString());
		return result;
	}
	@Override
	public TQueryPolicyResponseDto queryDetail(String policyNo) {
		CommonQueryRequestDTO requstDto=new CommonQueryRequestDTO();
		Map map = (Map) redis.get(Constants.SYS_SERVICE_INFO);
		SysServiceInfo sysServiceInfo = (SysServiceInfo) map.get(CoreServiceEnum.QUERY_DETAIL_URL.getUrlKey());
		requstDto.setDocumentNo(policyNo);
		requstDto.setPartnerAccountCode(sysServiceInfo.getUserName());
		requstDto.setFlowId(sysServiceInfo.getUserName() + DateUtil.dateFromat(new Date(), DateUtil.DATE_TIME_PATTERN3) + new Date().getTime());
		HttpEntity<CommonQueryRequestDTO> requestEntity = new HttpEntity<CommonQueryRequestDTO>(requstDto, RequestUtils.genEncryptHttpHeaders(sysServiceInfo.getUserName(), sysServiceInfo.getPassword()));
		//sysServiceInfo.setUrl("http://10.132.30.148:8850/queryPolicys.do");
		TQueryPolicyResponseDto response = restTemplate.postForObject(sysServiceInfo.getUrl(), requestEntity,
				TQueryPolicyResponseDto.class);
		return response;
	}
	
	
	
	
	
}
