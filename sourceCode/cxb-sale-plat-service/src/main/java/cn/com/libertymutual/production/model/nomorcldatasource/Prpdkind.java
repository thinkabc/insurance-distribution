package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.Date;

public class Prpdkind extends PrpdkindKey {
    private String kindcname;

    private String kindename;

    private Date startdate;

    private Date enddate;

    private String clausecode;

    private String relyonkindcode;

    private Date relyonstartdate;

    private Date relyonenddate;

    private String calculateflag;

    private String newkindcode;

    private String validstatus;

    private String flag;

    private String ownerriskcode;

    private String kindwidth;

    private String kindheight;

    private String shortratetype;

    private String ext1;

    private String ext2;

    private String ext3;

    public String getKindcname() {
        return kindcname;
    }

    public void setKindcname(String kindcname) {
        this.kindcname = kindcname == null ? null : kindcname.trim();
    }

    public String getKindename() {
        return kindename;
    }

    public void setKindename(String kindename) {
        this.kindename = kindename == null ? null : kindename.trim();
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getClausecode() {
        return clausecode;
    }

    public void setClausecode(String clausecode) {
        this.clausecode = clausecode == null ? null : clausecode.trim();
    }

    public String getRelyonkindcode() {
        return relyonkindcode;
    }

    public void setRelyonkindcode(String relyonkindcode) {
        this.relyonkindcode = relyonkindcode == null ? null : relyonkindcode.trim();
    }

    public Date getRelyonstartdate() {
        return relyonstartdate;
    }

    public void setRelyonstartdate(Date relyonstartdate) {
        this.relyonstartdate = relyonstartdate;
    }

    public Date getRelyonenddate() {
        return relyonenddate;
    }

    public void setRelyonenddate(Date relyonenddate) {
        this.relyonenddate = relyonenddate;
    }

    public String getCalculateflag() {
        return calculateflag;
    }

    public void setCalculateflag(String calculateflag) {
        this.calculateflag = calculateflag == null ? null : calculateflag.trim();
    }

    public String getNewkindcode() {
        return newkindcode;
    }

    public void setNewkindcode(String newkindcode) {
        this.newkindcode = newkindcode == null ? null : newkindcode.trim();
    }

    public String getValidstatus() {
        return validstatus;
    }

    public void setValidstatus(String validstatus) {
        this.validstatus = validstatus == null ? null : validstatus.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getOwnerriskcode() {
        return ownerriskcode;
    }

    public void setOwnerriskcode(String ownerriskcode) {
        this.ownerriskcode = ownerriskcode == null ? null : ownerriskcode.trim();
    }

    public String getKindwidth() {
        return kindwidth;
    }

    public void setKindwidth(String kindwidth) {
        this.kindwidth = kindwidth == null ? null : kindwidth.trim();
    }

    public String getKindheight() {
        return kindheight;
    }

    public void setKindheight(String kindheight) {
        this.kindheight = kindheight == null ? null : kindheight.trim();
    }

    public String getShortratetype() {
        return shortratetype;
    }

    public void setShortratetype(String shortratetype) {
        this.shortratetype = shortratetype == null ? null : shortratetype.trim();
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }
}