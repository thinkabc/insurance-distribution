package cn.com.libertymutual.production.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdkinditemMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkinditem;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkinditemExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkinditemExample.Criteria;
import cn.com.libertymutual.production.service.api.PrpdKindItemService;

/** 
 * @author GuoYue
 * @date 2017年8月14日
 *  
 */
@Service
public class PrpdKindItemServiceImpl implements PrpdKindItemService {

	@Autowired
	private PrpdkinditemMapper prpdkinditemMapper;
	
	@Override
	public void insert(Prpdkinditem record) {
		prpdkinditemMapper.insertSelective(record);
	}

	@Override
	public List<Prpdkinditem> check(Prpdkinditem record) {
		PrpdkinditemExample example = new PrpdkinditemExample();
		Criteria criteria = example.createCriteria();
		criteria.andRiskcodeEqualTo(record.getRiskcode());
		criteria.andRiskversionEqualTo(record.getRiskversion());
		criteria.andPlancodeEqualTo(record.getPlancode());
		criteria.andKindcodeEqualTo(record.getKindcode());
		criteria.andKindversionEqualTo(record.getKindversion());
		criteria.andItemcodeEqualTo(record.getItemcode());
		return prpdkinditemMapper.selectByExample(example);
	}

	@Override
	public List<Prpdkinditem> select(Prpdkinditem prpdkinditem) {
		PrpdkinditemExample example = new PrpdkinditemExample();
		Criteria criteria = example.createCriteria();
		String riskCode = prpdkinditem.getRiskcode();
		String riskVersion = prpdkinditem.getRiskversion();
		String planCode = prpdkinditem.getPlancode();
		String kindCode = prpdkinditem.getKindcode();
		String kindVersion = prpdkinditem.getKindversion();
		String itemCode = prpdkinditem.getItemcode();
		if(!StringUtils.isEmpty(riskCode)) {
			criteria.andRiskcodeEqualTo(riskCode);
		}
		if(!StringUtils.isEmpty(riskVersion)) {
			criteria.andRiskversionEqualTo(riskVersion);
		}
		if(!StringUtils.isEmpty(planCode)) {
			criteria.andPlancodeEqualTo(planCode);
		}
		if(!StringUtils.isEmpty(kindCode)) {
			criteria.andKindcodeEqualTo(kindCode);
		}
		if(!StringUtils.isEmpty(kindVersion)) {
			criteria.andKindversionEqualTo(kindVersion);
		}
		if(!StringUtils.isEmpty(itemCode)) {
			criteria.andItemcodeEqualTo(itemCode);
		}
		criteria.andValidstatusEqualTo("1");
		return prpdkinditemMapper.selectByExample(example);
	}

	@Override
	public void delete(Prpdkinditem record) {
		PrpdkinditemExample example = new PrpdkinditemExample();
		Criteria criteria = example.createCriteria();
		String riskCode = record.getRiskcode();
		String riskVersion = record.getRiskversion();
		String planCode = record.getPlancode();
		String kindCode = record.getKindcode();
		String kindVersion = record.getKindversion();
		String itemCode = record.getItemcode();
		if(!StringUtils.isEmpty(riskCode)) {
			criteria.andRiskcodeEqualTo(riskCode);
		}
		if(!StringUtils.isEmpty(riskVersion)) {
			criteria.andRiskversionEqualTo(riskVersion);
		}
		if(!StringUtils.isEmpty(planCode)) {
			criteria.andPlancodeEqualTo(planCode);
		}
		if(!StringUtils.isEmpty(kindCode)) {
			criteria.andKindcodeEqualTo(kindCode);
		}
		if(!StringUtils.isEmpty(kindVersion)) {
			criteria.andKindversionEqualTo(kindVersion);
		}
		if(!StringUtils.isEmpty(itemCode)) {
			criteria.andItemcodeEqualTo(itemCode);
		}
		prpdkinditemMapper.deleteByExample(example);
	}
	
}
