package cn.com.libertymutual.sp.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.com.libertymutual.sp.bean.TbSpKeyword;

public interface KeywordDao extends PagingAndSortingRepository<TbSpKeyword, Integer>, JpaSpecificationExecutor<TbSpKeyword> {


	
}
