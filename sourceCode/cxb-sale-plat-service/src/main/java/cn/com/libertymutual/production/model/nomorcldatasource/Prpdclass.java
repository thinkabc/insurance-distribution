package cn.com.libertymutual.production.model.nomorcldatasource;

public class Prpdclass {
    private String classcode;

    private String classname;

    private String classename;

    private String acccode;

    private String newclasscode;

    private String validstatus;

    private String flag;

    private String riskcategory;

    private String compositeflag;

    public String getClasscode() {
        return classcode;
    }

    public void setClasscode(String classcode) {
        this.classcode = classcode == null ? null : classcode.trim();
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname == null ? null : classname.trim();
    }

    public String getClassename() {
        return classename;
    }

    public void setClassename(String classename) {
        this.classename = classename == null ? null : classename.trim();
    }

    public String getAcccode() {
        return acccode;
    }

    public void setAcccode(String acccode) {
        this.acccode = acccode == null ? null : acccode.trim();
    }

    public String getNewclasscode() {
        return newclasscode;
    }

    public void setNewclasscode(String newclasscode) {
        this.newclasscode = newclasscode == null ? null : newclasscode.trim();
    }

    public String getValidstatus() {
        return validstatus;
    }

    public void setValidstatus(String validstatus) {
        this.validstatus = validstatus == null ? null : validstatus.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getRiskcategory() {
        return riskcategory;
    }

    public void setRiskcategory(String riskcategory) {
        this.riskcategory = riskcategory == null ? null : riskcategory.trim();
    }

    public String getCompositeflag() {
        return compositeflag;
    }

    public void setCompositeflag(String compositeflag) {
        this.compositeflag = compositeflag == null ? null : compositeflag.trim();
    }
}