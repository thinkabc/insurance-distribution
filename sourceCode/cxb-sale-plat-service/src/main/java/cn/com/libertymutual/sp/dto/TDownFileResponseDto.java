package cn.com.libertymutual.sp.dto;


import cn.com.libertymutual.core.base.dto.ResponseBaseDto;


public class TDownFileResponseDto extends ResponseBaseDto{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fileName;
	private byte[] pdfFileByte;

	public byte[] getPdfFileByte() {
		return pdfFileByte;
	}

	public void setPdfFileByte(byte[] pdfFileByte) {
		this.pdfFileByte = pdfFileByte;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
