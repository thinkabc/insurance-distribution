package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PrpdrationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PrpdrationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRiskcodeIsNull() {
            addCriterion("RISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIsNotNull() {
            addCriterion("RISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andRiskcodeEqualTo(String value) {
            addCriterion("RISKCODE =", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotEqualTo(String value) {
            addCriterion("RISKCODE <>", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeGreaterThan(String value) {
            addCriterion("RISKCODE >", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeGreaterThanOrEqualTo(String value) {
            addCriterion("RISKCODE >=", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLessThan(String value) {
            addCriterion("RISKCODE <", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLessThanOrEqualTo(String value) {
            addCriterion("RISKCODE <=", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLike(String value) {
            addCriterion("RISKCODE like", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotLike(String value) {
            addCriterion("RISKCODE not like", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIn(List<String> values) {
            addCriterion("RISKCODE in", values, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotIn(List<String> values) {
            addCriterion("RISKCODE not in", values, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeBetween(String value1, String value2) {
            addCriterion("RISKCODE between", value1, value2, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotBetween(String value1, String value2) {
            addCriterion("RISKCODE not between", value1, value2, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskversionIsNull() {
            addCriterion("RISKVERSION is null");
            return (Criteria) this;
        }

        public Criteria andRiskversionIsNotNull() {
            addCriterion("RISKVERSION is not null");
            return (Criteria) this;
        }

        public Criteria andRiskversionEqualTo(String value) {
            addCriterion("RISKVERSION =", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotEqualTo(String value) {
            addCriterion("RISKVERSION <>", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionGreaterThan(String value) {
            addCriterion("RISKVERSION >", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionGreaterThanOrEqualTo(String value) {
            addCriterion("RISKVERSION >=", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLessThan(String value) {
            addCriterion("RISKVERSION <", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLessThanOrEqualTo(String value) {
            addCriterion("RISKVERSION <=", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLike(String value) {
            addCriterion("RISKVERSION like", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotLike(String value) {
            addCriterion("RISKVERSION not like", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionIn(List<String> values) {
            addCriterion("RISKVERSION in", values, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotIn(List<String> values) {
            addCriterion("RISKVERSION not in", values, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionBetween(String value1, String value2) {
            addCriterion("RISKVERSION between", value1, value2, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotBetween(String value1, String value2) {
            addCriterion("RISKVERSION not between", value1, value2, "riskversion");
            return (Criteria) this;
        }

        public Criteria andPlancodeIsNull() {
            addCriterion("PLANCODE is null");
            return (Criteria) this;
        }

        public Criteria andPlancodeIsNotNull() {
            addCriterion("PLANCODE is not null");
            return (Criteria) this;
        }

        public Criteria andPlancodeEqualTo(String value) {
            addCriterion("PLANCODE =", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotEqualTo(String value) {
            addCriterion("PLANCODE <>", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeGreaterThan(String value) {
            addCriterion("PLANCODE >", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeGreaterThanOrEqualTo(String value) {
            addCriterion("PLANCODE >=", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeLessThan(String value) {
            addCriterion("PLANCODE <", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeLessThanOrEqualTo(String value) {
            addCriterion("PLANCODE <=", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeLike(String value) {
            addCriterion("PLANCODE like", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotLike(String value) {
            addCriterion("PLANCODE not like", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeIn(List<String> values) {
            addCriterion("PLANCODE in", values, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotIn(List<String> values) {
            addCriterion("PLANCODE not in", values, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeBetween(String value1, String value2) {
            addCriterion("PLANCODE between", value1, value2, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotBetween(String value1, String value2) {
            addCriterion("PLANCODE not between", value1, value2, "plancode");
            return (Criteria) this;
        }

        public Criteria andSerialnoIsNull() {
            addCriterion("SERIALNO is null");
            return (Criteria) this;
        }

        public Criteria andSerialnoIsNotNull() {
            addCriterion("SERIALNO is not null");
            return (Criteria) this;
        }

        public Criteria andSerialnoEqualTo(Long value) {
            addCriterion("SERIALNO =", value, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoNotEqualTo(Long value) {
            addCriterion("SERIALNO <>", value, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoGreaterThan(Long value) {
            addCriterion("SERIALNO >", value, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoGreaterThanOrEqualTo(Long value) {
            addCriterion("SERIALNO >=", value, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoLessThan(Long value) {
            addCriterion("SERIALNO <", value, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoLessThanOrEqualTo(Long value) {
            addCriterion("SERIALNO <=", value, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoIn(List<Long> values) {
            addCriterion("SERIALNO in", values, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoNotIn(List<Long> values) {
            addCriterion("SERIALNO not in", values, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoBetween(Long value1, Long value2) {
            addCriterion("SERIALNO between", value1, value2, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoNotBetween(Long value1, Long value2) {
            addCriterion("SERIALNO not between", value1, value2, "serialno");
            return (Criteria) this;
        }

        public Criteria andRationtypeIsNull() {
            addCriterion("RATIONTYPE is null");
            return (Criteria) this;
        }

        public Criteria andRationtypeIsNotNull() {
            addCriterion("RATIONTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andRationtypeEqualTo(String value) {
            addCriterion("RATIONTYPE =", value, "rationtype");
            return (Criteria) this;
        }

        public Criteria andRationtypeNotEqualTo(String value) {
            addCriterion("RATIONTYPE <>", value, "rationtype");
            return (Criteria) this;
        }

        public Criteria andRationtypeGreaterThan(String value) {
            addCriterion("RATIONTYPE >", value, "rationtype");
            return (Criteria) this;
        }

        public Criteria andRationtypeGreaterThanOrEqualTo(String value) {
            addCriterion("RATIONTYPE >=", value, "rationtype");
            return (Criteria) this;
        }

        public Criteria andRationtypeLessThan(String value) {
            addCriterion("RATIONTYPE <", value, "rationtype");
            return (Criteria) this;
        }

        public Criteria andRationtypeLessThanOrEqualTo(String value) {
            addCriterion("RATIONTYPE <=", value, "rationtype");
            return (Criteria) this;
        }

        public Criteria andRationtypeLike(String value) {
            addCriterion("RATIONTYPE like", value, "rationtype");
            return (Criteria) this;
        }

        public Criteria andRationtypeNotLike(String value) {
            addCriterion("RATIONTYPE not like", value, "rationtype");
            return (Criteria) this;
        }

        public Criteria andRationtypeIn(List<String> values) {
            addCriterion("RATIONTYPE in", values, "rationtype");
            return (Criteria) this;
        }

        public Criteria andRationtypeNotIn(List<String> values) {
            addCriterion("RATIONTYPE not in", values, "rationtype");
            return (Criteria) this;
        }

        public Criteria andRationtypeBetween(String value1, String value2) {
            addCriterion("RATIONTYPE between", value1, value2, "rationtype");
            return (Criteria) this;
        }

        public Criteria andRationtypeNotBetween(String value1, String value2) {
            addCriterion("RATIONTYPE not between", value1, value2, "rationtype");
            return (Criteria) this;
        }

        public Criteria andPlancnameIsNull() {
            addCriterion("PLANCNAME is null");
            return (Criteria) this;
        }

        public Criteria andPlancnameIsNotNull() {
            addCriterion("PLANCNAME is not null");
            return (Criteria) this;
        }

        public Criteria andPlancnameEqualTo(String value) {
            addCriterion("PLANCNAME =", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameNotEqualTo(String value) {
            addCriterion("PLANCNAME <>", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameGreaterThan(String value) {
            addCriterion("PLANCNAME >", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameGreaterThanOrEqualTo(String value) {
            addCriterion("PLANCNAME >=", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameLessThan(String value) {
            addCriterion("PLANCNAME <", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameLessThanOrEqualTo(String value) {
            addCriterion("PLANCNAME <=", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameLike(String value) {
            addCriterion("PLANCNAME like", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameNotLike(String value) {
            addCriterion("PLANCNAME not like", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameIn(List<String> values) {
            addCriterion("PLANCNAME in", values, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameNotIn(List<String> values) {
            addCriterion("PLANCNAME not in", values, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameBetween(String value1, String value2) {
            addCriterion("PLANCNAME between", value1, value2, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameNotBetween(String value1, String value2) {
            addCriterion("PLANCNAME not between", value1, value2, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlanenameIsNull() {
            addCriterion("PLANENAME is null");
            return (Criteria) this;
        }

        public Criteria andPlanenameIsNotNull() {
            addCriterion("PLANENAME is not null");
            return (Criteria) this;
        }

        public Criteria andPlanenameEqualTo(String value) {
            addCriterion("PLANENAME =", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameNotEqualTo(String value) {
            addCriterion("PLANENAME <>", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameGreaterThan(String value) {
            addCriterion("PLANENAME >", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameGreaterThanOrEqualTo(String value) {
            addCriterion("PLANENAME >=", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameLessThan(String value) {
            addCriterion("PLANENAME <", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameLessThanOrEqualTo(String value) {
            addCriterion("PLANENAME <=", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameLike(String value) {
            addCriterion("PLANENAME like", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameNotLike(String value) {
            addCriterion("PLANENAME not like", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameIn(List<String> values) {
            addCriterion("PLANENAME in", values, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameNotIn(List<String> values) {
            addCriterion("PLANENAME not in", values, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameBetween(String value1, String value2) {
            addCriterion("PLANENAME between", value1, value2, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameNotBetween(String value1, String value2) {
            addCriterion("PLANENAME not between", value1, value2, "planename");
            return (Criteria) this;
        }

        public Criteria andKindcodeIsNull() {
            addCriterion("KINDCODE is null");
            return (Criteria) this;
        }

        public Criteria andKindcodeIsNotNull() {
            addCriterion("KINDCODE is not null");
            return (Criteria) this;
        }

        public Criteria andKindcodeEqualTo(String value) {
            addCriterion("KINDCODE =", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeNotEqualTo(String value) {
            addCriterion("KINDCODE <>", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeGreaterThan(String value) {
            addCriterion("KINDCODE >", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeGreaterThanOrEqualTo(String value) {
            addCriterion("KINDCODE >=", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeLessThan(String value) {
            addCriterion("KINDCODE <", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeLessThanOrEqualTo(String value) {
            addCriterion("KINDCODE <=", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeLike(String value) {
            addCriterion("KINDCODE like", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeNotLike(String value) {
            addCriterion("KINDCODE not like", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeIn(List<String> values) {
            addCriterion("KINDCODE in", values, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeNotIn(List<String> values) {
            addCriterion("KINDCODE not in", values, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeBetween(String value1, String value2) {
            addCriterion("KINDCODE between", value1, value2, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeNotBetween(String value1, String value2) {
            addCriterion("KINDCODE not between", value1, value2, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindversionIsNull() {
            addCriterion("KINDVERSION is null");
            return (Criteria) this;
        }

        public Criteria andKindversionIsNotNull() {
            addCriterion("KINDVERSION is not null");
            return (Criteria) this;
        }

        public Criteria andKindversionEqualTo(String value) {
            addCriterion("KINDVERSION =", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionNotEqualTo(String value) {
            addCriterion("KINDVERSION <>", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionGreaterThan(String value) {
            addCriterion("KINDVERSION >", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionGreaterThanOrEqualTo(String value) {
            addCriterion("KINDVERSION >=", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionLessThan(String value) {
            addCriterion("KINDVERSION <", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionLessThanOrEqualTo(String value) {
            addCriterion("KINDVERSION <=", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionLike(String value) {
            addCriterion("KINDVERSION like", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionNotLike(String value) {
            addCriterion("KINDVERSION not like", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionIn(List<String> values) {
            addCriterion("KINDVERSION in", values, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionNotIn(List<String> values) {
            addCriterion("KINDVERSION not in", values, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionBetween(String value1, String value2) {
            addCriterion("KINDVERSION between", value1, value2, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionNotBetween(String value1, String value2) {
            addCriterion("KINDVERSION not between", value1, value2, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindcnameIsNull() {
            addCriterion("KINDCNAME is null");
            return (Criteria) this;
        }

        public Criteria andKindcnameIsNotNull() {
            addCriterion("KINDCNAME is not null");
            return (Criteria) this;
        }

        public Criteria andKindcnameEqualTo(String value) {
            addCriterion("KINDCNAME =", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameNotEqualTo(String value) {
            addCriterion("KINDCNAME <>", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameGreaterThan(String value) {
            addCriterion("KINDCNAME >", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameGreaterThanOrEqualTo(String value) {
            addCriterion("KINDCNAME >=", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameLessThan(String value) {
            addCriterion("KINDCNAME <", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameLessThanOrEqualTo(String value) {
            addCriterion("KINDCNAME <=", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameLike(String value) {
            addCriterion("KINDCNAME like", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameNotLike(String value) {
            addCriterion("KINDCNAME not like", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameIn(List<String> values) {
            addCriterion("KINDCNAME in", values, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameNotIn(List<String> values) {
            addCriterion("KINDCNAME not in", values, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameBetween(String value1, String value2) {
            addCriterion("KINDCNAME between", value1, value2, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameNotBetween(String value1, String value2) {
            addCriterion("KINDCNAME not between", value1, value2, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindenameIsNull() {
            addCriterion("KINDENAME is null");
            return (Criteria) this;
        }

        public Criteria andKindenameIsNotNull() {
            addCriterion("KINDENAME is not null");
            return (Criteria) this;
        }

        public Criteria andKindenameEqualTo(String value) {
            addCriterion("KINDENAME =", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameNotEqualTo(String value) {
            addCriterion("KINDENAME <>", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameGreaterThan(String value) {
            addCriterion("KINDENAME >", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameGreaterThanOrEqualTo(String value) {
            addCriterion("KINDENAME >=", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameLessThan(String value) {
            addCriterion("KINDENAME <", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameLessThanOrEqualTo(String value) {
            addCriterion("KINDENAME <=", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameLike(String value) {
            addCriterion("KINDENAME like", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameNotLike(String value) {
            addCriterion("KINDENAME not like", value, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameIn(List<String> values) {
            addCriterion("KINDENAME in", values, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameNotIn(List<String> values) {
            addCriterion("KINDENAME not in", values, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameBetween(String value1, String value2) {
            addCriterion("KINDENAME between", value1, value2, "kindename");
            return (Criteria) this;
        }

        public Criteria andKindenameNotBetween(String value1, String value2) {
            addCriterion("KINDENAME not between", value1, value2, "kindename");
            return (Criteria) this;
        }

        public Criteria andStartdateIsNull() {
            addCriterion("STARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andStartdateIsNotNull() {
            addCriterion("STARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andStartdateEqualTo(Date value) {
            addCriterion("STARTDATE =", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotEqualTo(Date value) {
            addCriterion("STARTDATE <>", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateGreaterThan(Date value) {
            addCriterion("STARTDATE >", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("STARTDATE >=", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateLessThan(Date value) {
            addCriterion("STARTDATE <", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateLessThanOrEqualTo(Date value) {
            addCriterion("STARTDATE <=", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateIn(List<Date> values) {
            addCriterion("STARTDATE in", values, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotIn(List<Date> values) {
            addCriterion("STARTDATE not in", values, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateBetween(Date value1, Date value2) {
            addCriterion("STARTDATE between", value1, value2, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotBetween(Date value1, Date value2) {
            addCriterion("STARTDATE not between", value1, value2, "startdate");
            return (Criteria) this;
        }

        public Criteria andEnddateIsNull() {
            addCriterion("ENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andEnddateIsNotNull() {
            addCriterion("ENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andEnddateEqualTo(Date value) {
            addCriterion("ENDDATE =", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotEqualTo(Date value) {
            addCriterion("ENDDATE <>", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateGreaterThan(Date value) {
            addCriterion("ENDDATE >", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateGreaterThanOrEqualTo(Date value) {
            addCriterion("ENDDATE >=", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateLessThan(Date value) {
            addCriterion("ENDDATE <", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateLessThanOrEqualTo(Date value) {
            addCriterion("ENDDATE <=", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateIn(List<Date> values) {
            addCriterion("ENDDATE in", values, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotIn(List<Date> values) {
            addCriterion("ENDDATE not in", values, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateBetween(Date value1, Date value2) {
            addCriterion("ENDDATE between", value1, value2, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotBetween(Date value1, Date value2) {
            addCriterion("ENDDATE not between", value1, value2, "enddate");
            return (Criteria) this;
        }

        public Criteria andClausecodeIsNull() {
            addCriterion("CLAUSECODE is null");
            return (Criteria) this;
        }

        public Criteria andClausecodeIsNotNull() {
            addCriterion("CLAUSECODE is not null");
            return (Criteria) this;
        }

        public Criteria andClausecodeEqualTo(String value) {
            addCriterion("CLAUSECODE =", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeNotEqualTo(String value) {
            addCriterion("CLAUSECODE <>", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeGreaterThan(String value) {
            addCriterion("CLAUSECODE >", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeGreaterThanOrEqualTo(String value) {
            addCriterion("CLAUSECODE >=", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeLessThan(String value) {
            addCriterion("CLAUSECODE <", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeLessThanOrEqualTo(String value) {
            addCriterion("CLAUSECODE <=", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeLike(String value) {
            addCriterion("CLAUSECODE like", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeNotLike(String value) {
            addCriterion("CLAUSECODE not like", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeIn(List<String> values) {
            addCriterion("CLAUSECODE in", values, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeNotIn(List<String> values) {
            addCriterion("CLAUSECODE not in", values, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeBetween(String value1, String value2) {
            addCriterion("CLAUSECODE between", value1, value2, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeNotBetween(String value1, String value2) {
            addCriterion("CLAUSECODE not between", value1, value2, "clausecode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeIsNull() {
            addCriterion("RELYONKINDCODE is null");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeIsNotNull() {
            addCriterion("RELYONKINDCODE is not null");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeEqualTo(String value) {
            addCriterion("RELYONKINDCODE =", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeNotEqualTo(String value) {
            addCriterion("RELYONKINDCODE <>", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeGreaterThan(String value) {
            addCriterion("RELYONKINDCODE >", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeGreaterThanOrEqualTo(String value) {
            addCriterion("RELYONKINDCODE >=", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeLessThan(String value) {
            addCriterion("RELYONKINDCODE <", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeLessThanOrEqualTo(String value) {
            addCriterion("RELYONKINDCODE <=", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeLike(String value) {
            addCriterion("RELYONKINDCODE like", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeNotLike(String value) {
            addCriterion("RELYONKINDCODE not like", value, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeIn(List<String> values) {
            addCriterion("RELYONKINDCODE in", values, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeNotIn(List<String> values) {
            addCriterion("RELYONKINDCODE not in", values, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeBetween(String value1, String value2) {
            addCriterion("RELYONKINDCODE between", value1, value2, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonkindcodeNotBetween(String value1, String value2) {
            addCriterion("RELYONKINDCODE not between", value1, value2, "relyonkindcode");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateIsNull() {
            addCriterion("RELYONSTARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateIsNotNull() {
            addCriterion("RELYONSTARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateEqualTo(Date value) {
            addCriterion("RELYONSTARTDATE =", value, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateNotEqualTo(Date value) {
            addCriterion("RELYONSTARTDATE <>", value, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateGreaterThan(Date value) {
            addCriterion("RELYONSTARTDATE >", value, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("RELYONSTARTDATE >=", value, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateLessThan(Date value) {
            addCriterion("RELYONSTARTDATE <", value, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateLessThanOrEqualTo(Date value) {
            addCriterion("RELYONSTARTDATE <=", value, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateIn(List<Date> values) {
            addCriterion("RELYONSTARTDATE in", values, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateNotIn(List<Date> values) {
            addCriterion("RELYONSTARTDATE not in", values, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateBetween(Date value1, Date value2) {
            addCriterion("RELYONSTARTDATE between", value1, value2, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonstartdateNotBetween(Date value1, Date value2) {
            addCriterion("RELYONSTARTDATE not between", value1, value2, "relyonstartdate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateIsNull() {
            addCriterion("RELYONENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateIsNotNull() {
            addCriterion("RELYONENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateEqualTo(Date value) {
            addCriterion("RELYONENDDATE =", value, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateNotEqualTo(Date value) {
            addCriterion("RELYONENDDATE <>", value, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateGreaterThan(Date value) {
            addCriterion("RELYONENDDATE >", value, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateGreaterThanOrEqualTo(Date value) {
            addCriterion("RELYONENDDATE >=", value, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateLessThan(Date value) {
            addCriterion("RELYONENDDATE <", value, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateLessThanOrEqualTo(Date value) {
            addCriterion("RELYONENDDATE <=", value, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateIn(List<Date> values) {
            addCriterion("RELYONENDDATE in", values, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateNotIn(List<Date> values) {
            addCriterion("RELYONENDDATE not in", values, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateBetween(Date value1, Date value2) {
            addCriterion("RELYONENDDATE between", value1, value2, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andRelyonenddateNotBetween(Date value1, Date value2) {
            addCriterion("RELYONENDDATE not between", value1, value2, "relyonenddate");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeIsNull() {
            addCriterion("OWNERRISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeIsNotNull() {
            addCriterion("OWNERRISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeEqualTo(String value) {
            addCriterion("OWNERRISKCODE =", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeNotEqualTo(String value) {
            addCriterion("OWNERRISKCODE <>", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeGreaterThan(String value) {
            addCriterion("OWNERRISKCODE >", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeGreaterThanOrEqualTo(String value) {
            addCriterion("OWNERRISKCODE >=", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeLessThan(String value) {
            addCriterion("OWNERRISKCODE <", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeLessThanOrEqualTo(String value) {
            addCriterion("OWNERRISKCODE <=", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeLike(String value) {
            addCriterion("OWNERRISKCODE like", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeNotLike(String value) {
            addCriterion("OWNERRISKCODE not like", value, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeIn(List<String> values) {
            addCriterion("OWNERRISKCODE in", values, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeNotIn(List<String> values) {
            addCriterion("OWNERRISKCODE not in", values, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeBetween(String value1, String value2) {
            addCriterion("OWNERRISKCODE between", value1, value2, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andOwnerriskcodeNotBetween(String value1, String value2) {
            addCriterion("OWNERRISKCODE not between", value1, value2, "ownerriskcode");
            return (Criteria) this;
        }

        public Criteria andCalculateflagIsNull() {
            addCriterion("CALCULATEFLAG is null");
            return (Criteria) this;
        }

        public Criteria andCalculateflagIsNotNull() {
            addCriterion("CALCULATEFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andCalculateflagEqualTo(String value) {
            addCriterion("CALCULATEFLAG =", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagNotEqualTo(String value) {
            addCriterion("CALCULATEFLAG <>", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagGreaterThan(String value) {
            addCriterion("CALCULATEFLAG >", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagGreaterThanOrEqualTo(String value) {
            addCriterion("CALCULATEFLAG >=", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagLessThan(String value) {
            addCriterion("CALCULATEFLAG <", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagLessThanOrEqualTo(String value) {
            addCriterion("CALCULATEFLAG <=", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagLike(String value) {
            addCriterion("CALCULATEFLAG like", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagNotLike(String value) {
            addCriterion("CALCULATEFLAG not like", value, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagIn(List<String> values) {
            addCriterion("CALCULATEFLAG in", values, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagNotIn(List<String> values) {
            addCriterion("CALCULATEFLAG not in", values, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagBetween(String value1, String value2) {
            addCriterion("CALCULATEFLAG between", value1, value2, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andCalculateflagNotBetween(String value1, String value2) {
            addCriterion("CALCULATEFLAG not between", value1, value2, "calculateflag");
            return (Criteria) this;
        }

        public Criteria andItemcodeIsNull() {
            addCriterion("ITEMCODE is null");
            return (Criteria) this;
        }

        public Criteria andItemcodeIsNotNull() {
            addCriterion("ITEMCODE is not null");
            return (Criteria) this;
        }

        public Criteria andItemcodeEqualTo(String value) {
            addCriterion("ITEMCODE =", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeNotEqualTo(String value) {
            addCriterion("ITEMCODE <>", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeGreaterThan(String value) {
            addCriterion("ITEMCODE >", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeGreaterThanOrEqualTo(String value) {
            addCriterion("ITEMCODE >=", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeLessThan(String value) {
            addCriterion("ITEMCODE <", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeLessThanOrEqualTo(String value) {
            addCriterion("ITEMCODE <=", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeLike(String value) {
            addCriterion("ITEMCODE like", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeNotLike(String value) {
            addCriterion("ITEMCODE not like", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeIn(List<String> values) {
            addCriterion("ITEMCODE in", values, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeNotIn(List<String> values) {
            addCriterion("ITEMCODE not in", values, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeBetween(String value1, String value2) {
            addCriterion("ITEMCODE between", value1, value2, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeNotBetween(String value1, String value2) {
            addCriterion("ITEMCODE not between", value1, value2, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcnameIsNull() {
            addCriterion("ITEMCNAME is null");
            return (Criteria) this;
        }

        public Criteria andItemcnameIsNotNull() {
            addCriterion("ITEMCNAME is not null");
            return (Criteria) this;
        }

        public Criteria andItemcnameEqualTo(String value) {
            addCriterion("ITEMCNAME =", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameNotEqualTo(String value) {
            addCriterion("ITEMCNAME <>", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameGreaterThan(String value) {
            addCriterion("ITEMCNAME >", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameGreaterThanOrEqualTo(String value) {
            addCriterion("ITEMCNAME >=", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameLessThan(String value) {
            addCriterion("ITEMCNAME <", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameLessThanOrEqualTo(String value) {
            addCriterion("ITEMCNAME <=", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameLike(String value) {
            addCriterion("ITEMCNAME like", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameNotLike(String value) {
            addCriterion("ITEMCNAME not like", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameIn(List<String> values) {
            addCriterion("ITEMCNAME in", values, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameNotIn(List<String> values) {
            addCriterion("ITEMCNAME not in", values, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameBetween(String value1, String value2) {
            addCriterion("ITEMCNAME between", value1, value2, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameNotBetween(String value1, String value2) {
            addCriterion("ITEMCNAME not between", value1, value2, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemenameIsNull() {
            addCriterion("ITEMENAME is null");
            return (Criteria) this;
        }

        public Criteria andItemenameIsNotNull() {
            addCriterion("ITEMENAME is not null");
            return (Criteria) this;
        }

        public Criteria andItemenameEqualTo(String value) {
            addCriterion("ITEMENAME =", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameNotEqualTo(String value) {
            addCriterion("ITEMENAME <>", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameGreaterThan(String value) {
            addCriterion("ITEMENAME >", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameGreaterThanOrEqualTo(String value) {
            addCriterion("ITEMENAME >=", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameLessThan(String value) {
            addCriterion("ITEMENAME <", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameLessThanOrEqualTo(String value) {
            addCriterion("ITEMENAME <=", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameLike(String value) {
            addCriterion("ITEMENAME like", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameNotLike(String value) {
            addCriterion("ITEMENAME not like", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameIn(List<String> values) {
            addCriterion("ITEMENAME in", values, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameNotIn(List<String> values) {
            addCriterion("ITEMENAME not in", values, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameBetween(String value1, String value2) {
            addCriterion("ITEMENAME between", value1, value2, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameNotBetween(String value1, String value2) {
            addCriterion("ITEMENAME not between", value1, value2, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemflagIsNull() {
            addCriterion("ITEMFLAG is null");
            return (Criteria) this;
        }

        public Criteria andItemflagIsNotNull() {
            addCriterion("ITEMFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andItemflagEqualTo(String value) {
            addCriterion("ITEMFLAG =", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagNotEqualTo(String value) {
            addCriterion("ITEMFLAG <>", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagGreaterThan(String value) {
            addCriterion("ITEMFLAG >", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagGreaterThanOrEqualTo(String value) {
            addCriterion("ITEMFLAG >=", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagLessThan(String value) {
            addCriterion("ITEMFLAG <", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagLessThanOrEqualTo(String value) {
            addCriterion("ITEMFLAG <=", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagLike(String value) {
            addCriterion("ITEMFLAG like", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagNotLike(String value) {
            addCriterion("ITEMFLAG not like", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagIn(List<String> values) {
            addCriterion("ITEMFLAG in", values, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagNotIn(List<String> values) {
            addCriterion("ITEMFLAG not in", values, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagBetween(String value1, String value2) {
            addCriterion("ITEMFLAG between", value1, value2, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagNotBetween(String value1, String value2) {
            addCriterion("ITEMFLAG not between", value1, value2, "itemflag");
            return (Criteria) this;
        }

        public Criteria andFreeitem1IsNull() {
            addCriterion("FREEITEM1 is null");
            return (Criteria) this;
        }

        public Criteria andFreeitem1IsNotNull() {
            addCriterion("FREEITEM1 is not null");
            return (Criteria) this;
        }

        public Criteria andFreeitem1EqualTo(String value) {
            addCriterion("FREEITEM1 =", value, "freeitem1");
            return (Criteria) this;
        }

        public Criteria andFreeitem1NotEqualTo(String value) {
            addCriterion("FREEITEM1 <>", value, "freeitem1");
            return (Criteria) this;
        }

        public Criteria andFreeitem1GreaterThan(String value) {
            addCriterion("FREEITEM1 >", value, "freeitem1");
            return (Criteria) this;
        }

        public Criteria andFreeitem1GreaterThanOrEqualTo(String value) {
            addCriterion("FREEITEM1 >=", value, "freeitem1");
            return (Criteria) this;
        }

        public Criteria andFreeitem1LessThan(String value) {
            addCriterion("FREEITEM1 <", value, "freeitem1");
            return (Criteria) this;
        }

        public Criteria andFreeitem1LessThanOrEqualTo(String value) {
            addCriterion("FREEITEM1 <=", value, "freeitem1");
            return (Criteria) this;
        }

        public Criteria andFreeitem1Like(String value) {
            addCriterion("FREEITEM1 like", value, "freeitem1");
            return (Criteria) this;
        }

        public Criteria andFreeitem1NotLike(String value) {
            addCriterion("FREEITEM1 not like", value, "freeitem1");
            return (Criteria) this;
        }

        public Criteria andFreeitem1In(List<String> values) {
            addCriterion("FREEITEM1 in", values, "freeitem1");
            return (Criteria) this;
        }

        public Criteria andFreeitem1NotIn(List<String> values) {
            addCriterion("FREEITEM1 not in", values, "freeitem1");
            return (Criteria) this;
        }

        public Criteria andFreeitem1Between(String value1, String value2) {
            addCriterion("FREEITEM1 between", value1, value2, "freeitem1");
            return (Criteria) this;
        }

        public Criteria andFreeitem1NotBetween(String value1, String value2) {
            addCriterion("FREEITEM1 not between", value1, value2, "freeitem1");
            return (Criteria) this;
        }

        public Criteria andFreeitem2IsNull() {
            addCriterion("FREEITEM2 is null");
            return (Criteria) this;
        }

        public Criteria andFreeitem2IsNotNull() {
            addCriterion("FREEITEM2 is not null");
            return (Criteria) this;
        }

        public Criteria andFreeitem2EqualTo(String value) {
            addCriterion("FREEITEM2 =", value, "freeitem2");
            return (Criteria) this;
        }

        public Criteria andFreeitem2NotEqualTo(String value) {
            addCriterion("FREEITEM2 <>", value, "freeitem2");
            return (Criteria) this;
        }

        public Criteria andFreeitem2GreaterThan(String value) {
            addCriterion("FREEITEM2 >", value, "freeitem2");
            return (Criteria) this;
        }

        public Criteria andFreeitem2GreaterThanOrEqualTo(String value) {
            addCriterion("FREEITEM2 >=", value, "freeitem2");
            return (Criteria) this;
        }

        public Criteria andFreeitem2LessThan(String value) {
            addCriterion("FREEITEM2 <", value, "freeitem2");
            return (Criteria) this;
        }

        public Criteria andFreeitem2LessThanOrEqualTo(String value) {
            addCriterion("FREEITEM2 <=", value, "freeitem2");
            return (Criteria) this;
        }

        public Criteria andFreeitem2Like(String value) {
            addCriterion("FREEITEM2 like", value, "freeitem2");
            return (Criteria) this;
        }

        public Criteria andFreeitem2NotLike(String value) {
            addCriterion("FREEITEM2 not like", value, "freeitem2");
            return (Criteria) this;
        }

        public Criteria andFreeitem2In(List<String> values) {
            addCriterion("FREEITEM2 in", values, "freeitem2");
            return (Criteria) this;
        }

        public Criteria andFreeitem2NotIn(List<String> values) {
            addCriterion("FREEITEM2 not in", values, "freeitem2");
            return (Criteria) this;
        }

        public Criteria andFreeitem2Between(String value1, String value2) {
            addCriterion("FREEITEM2 between", value1, value2, "freeitem2");
            return (Criteria) this;
        }

        public Criteria andFreeitem2NotBetween(String value1, String value2) {
            addCriterion("FREEITEM2 not between", value1, value2, "freeitem2");
            return (Criteria) this;
        }

        public Criteria andFreeitem3IsNull() {
            addCriterion("FREEITEM3 is null");
            return (Criteria) this;
        }

        public Criteria andFreeitem3IsNotNull() {
            addCriterion("FREEITEM3 is not null");
            return (Criteria) this;
        }

        public Criteria andFreeitem3EqualTo(String value) {
            addCriterion("FREEITEM3 =", value, "freeitem3");
            return (Criteria) this;
        }

        public Criteria andFreeitem3NotEqualTo(String value) {
            addCriterion("FREEITEM3 <>", value, "freeitem3");
            return (Criteria) this;
        }

        public Criteria andFreeitem3GreaterThan(String value) {
            addCriterion("FREEITEM3 >", value, "freeitem3");
            return (Criteria) this;
        }

        public Criteria andFreeitem3GreaterThanOrEqualTo(String value) {
            addCriterion("FREEITEM3 >=", value, "freeitem3");
            return (Criteria) this;
        }

        public Criteria andFreeitem3LessThan(String value) {
            addCriterion("FREEITEM3 <", value, "freeitem3");
            return (Criteria) this;
        }

        public Criteria andFreeitem3LessThanOrEqualTo(String value) {
            addCriterion("FREEITEM3 <=", value, "freeitem3");
            return (Criteria) this;
        }

        public Criteria andFreeitem3Like(String value) {
            addCriterion("FREEITEM3 like", value, "freeitem3");
            return (Criteria) this;
        }

        public Criteria andFreeitem3NotLike(String value) {
            addCriterion("FREEITEM3 not like", value, "freeitem3");
            return (Criteria) this;
        }

        public Criteria andFreeitem3In(List<String> values) {
            addCriterion("FREEITEM3 in", values, "freeitem3");
            return (Criteria) this;
        }

        public Criteria andFreeitem3NotIn(List<String> values) {
            addCriterion("FREEITEM3 not in", values, "freeitem3");
            return (Criteria) this;
        }

        public Criteria andFreeitem3Between(String value1, String value2) {
            addCriterion("FREEITEM3 between", value1, value2, "freeitem3");
            return (Criteria) this;
        }

        public Criteria andFreeitem3NotBetween(String value1, String value2) {
            addCriterion("FREEITEM3 not between", value1, value2, "freeitem3");
            return (Criteria) this;
        }

        public Criteria andFreeitem4IsNull() {
            addCriterion("FREEITEM4 is null");
            return (Criteria) this;
        }

        public Criteria andFreeitem4IsNotNull() {
            addCriterion("FREEITEM4 is not null");
            return (Criteria) this;
        }

        public Criteria andFreeitem4EqualTo(String value) {
            addCriterion("FREEITEM4 =", value, "freeitem4");
            return (Criteria) this;
        }

        public Criteria andFreeitem4NotEqualTo(String value) {
            addCriterion("FREEITEM4 <>", value, "freeitem4");
            return (Criteria) this;
        }

        public Criteria andFreeitem4GreaterThan(String value) {
            addCriterion("FREEITEM4 >", value, "freeitem4");
            return (Criteria) this;
        }

        public Criteria andFreeitem4GreaterThanOrEqualTo(String value) {
            addCriterion("FREEITEM4 >=", value, "freeitem4");
            return (Criteria) this;
        }

        public Criteria andFreeitem4LessThan(String value) {
            addCriterion("FREEITEM4 <", value, "freeitem4");
            return (Criteria) this;
        }

        public Criteria andFreeitem4LessThanOrEqualTo(String value) {
            addCriterion("FREEITEM4 <=", value, "freeitem4");
            return (Criteria) this;
        }

        public Criteria andFreeitem4Like(String value) {
            addCriterion("FREEITEM4 like", value, "freeitem4");
            return (Criteria) this;
        }

        public Criteria andFreeitem4NotLike(String value) {
            addCriterion("FREEITEM4 not like", value, "freeitem4");
            return (Criteria) this;
        }

        public Criteria andFreeitem4In(List<String> values) {
            addCriterion("FREEITEM4 in", values, "freeitem4");
            return (Criteria) this;
        }

        public Criteria andFreeitem4NotIn(List<String> values) {
            addCriterion("FREEITEM4 not in", values, "freeitem4");
            return (Criteria) this;
        }

        public Criteria andFreeitem4Between(String value1, String value2) {
            addCriterion("FREEITEM4 between", value1, value2, "freeitem4");
            return (Criteria) this;
        }

        public Criteria andFreeitem4NotBetween(String value1, String value2) {
            addCriterion("FREEITEM4 not between", value1, value2, "freeitem4");
            return (Criteria) this;
        }

        public Criteria andFreeitem5IsNull() {
            addCriterion("FREEITEM5 is null");
            return (Criteria) this;
        }

        public Criteria andFreeitem5IsNotNull() {
            addCriterion("FREEITEM5 is not null");
            return (Criteria) this;
        }

        public Criteria andFreeitem5EqualTo(String value) {
            addCriterion("FREEITEM5 =", value, "freeitem5");
            return (Criteria) this;
        }

        public Criteria andFreeitem5NotEqualTo(String value) {
            addCriterion("FREEITEM5 <>", value, "freeitem5");
            return (Criteria) this;
        }

        public Criteria andFreeitem5GreaterThan(String value) {
            addCriterion("FREEITEM5 >", value, "freeitem5");
            return (Criteria) this;
        }

        public Criteria andFreeitem5GreaterThanOrEqualTo(String value) {
            addCriterion("FREEITEM5 >=", value, "freeitem5");
            return (Criteria) this;
        }

        public Criteria andFreeitem5LessThan(String value) {
            addCriterion("FREEITEM5 <", value, "freeitem5");
            return (Criteria) this;
        }

        public Criteria andFreeitem5LessThanOrEqualTo(String value) {
            addCriterion("FREEITEM5 <=", value, "freeitem5");
            return (Criteria) this;
        }

        public Criteria andFreeitem5Like(String value) {
            addCriterion("FREEITEM5 like", value, "freeitem5");
            return (Criteria) this;
        }

        public Criteria andFreeitem5NotLike(String value) {
            addCriterion("FREEITEM5 not like", value, "freeitem5");
            return (Criteria) this;
        }

        public Criteria andFreeitem5In(List<String> values) {
            addCriterion("FREEITEM5 in", values, "freeitem5");
            return (Criteria) this;
        }

        public Criteria andFreeitem5NotIn(List<String> values) {
            addCriterion("FREEITEM5 not in", values, "freeitem5");
            return (Criteria) this;
        }

        public Criteria andFreeitem5Between(String value1, String value2) {
            addCriterion("FREEITEM5 between", value1, value2, "freeitem5");
            return (Criteria) this;
        }

        public Criteria andFreeitem5NotBetween(String value1, String value2) {
            addCriterion("FREEITEM5 not between", value1, value2, "freeitem5");
            return (Criteria) this;
        }

        public Criteria andFreeitem6IsNull() {
            addCriterion("FREEITEM6 is null");
            return (Criteria) this;
        }

        public Criteria andFreeitem6IsNotNull() {
            addCriterion("FREEITEM6 is not null");
            return (Criteria) this;
        }

        public Criteria andFreeitem6EqualTo(String value) {
            addCriterion("FREEITEM6 =", value, "freeitem6");
            return (Criteria) this;
        }

        public Criteria andFreeitem6NotEqualTo(String value) {
            addCriterion("FREEITEM6 <>", value, "freeitem6");
            return (Criteria) this;
        }

        public Criteria andFreeitem6GreaterThan(String value) {
            addCriterion("FREEITEM6 >", value, "freeitem6");
            return (Criteria) this;
        }

        public Criteria andFreeitem6GreaterThanOrEqualTo(String value) {
            addCriterion("FREEITEM6 >=", value, "freeitem6");
            return (Criteria) this;
        }

        public Criteria andFreeitem6LessThan(String value) {
            addCriterion("FREEITEM6 <", value, "freeitem6");
            return (Criteria) this;
        }

        public Criteria andFreeitem6LessThanOrEqualTo(String value) {
            addCriterion("FREEITEM6 <=", value, "freeitem6");
            return (Criteria) this;
        }

        public Criteria andFreeitem6Like(String value) {
            addCriterion("FREEITEM6 like", value, "freeitem6");
            return (Criteria) this;
        }

        public Criteria andFreeitem6NotLike(String value) {
            addCriterion("FREEITEM6 not like", value, "freeitem6");
            return (Criteria) this;
        }

        public Criteria andFreeitem6In(List<String> values) {
            addCriterion("FREEITEM6 in", values, "freeitem6");
            return (Criteria) this;
        }

        public Criteria andFreeitem6NotIn(List<String> values) {
            addCriterion("FREEITEM6 not in", values, "freeitem6");
            return (Criteria) this;
        }

        public Criteria andFreeitem6Between(String value1, String value2) {
            addCriterion("FREEITEM6 between", value1, value2, "freeitem6");
            return (Criteria) this;
        }

        public Criteria andFreeitem6NotBetween(String value1, String value2) {
            addCriterion("FREEITEM6 not between", value1, value2, "freeitem6");
            return (Criteria) this;
        }

        public Criteria andQuantityIsNull() {
            addCriterion("QUANTITY is null");
            return (Criteria) this;
        }

        public Criteria andQuantityIsNotNull() {
            addCriterion("QUANTITY is not null");
            return (Criteria) this;
        }

        public Criteria andQuantityEqualTo(Long value) {
            addCriterion("QUANTITY =", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityNotEqualTo(Long value) {
            addCriterion("QUANTITY <>", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityGreaterThan(Long value) {
            addCriterion("QUANTITY >", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityGreaterThanOrEqualTo(Long value) {
            addCriterion("QUANTITY >=", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityLessThan(Long value) {
            addCriterion("QUANTITY <", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityLessThanOrEqualTo(Long value) {
            addCriterion("QUANTITY <=", value, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityIn(List<Long> values) {
            addCriterion("QUANTITY in", values, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityNotIn(List<Long> values) {
            addCriterion("QUANTITY not in", values, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityBetween(Long value1, Long value2) {
            addCriterion("QUANTITY between", value1, value2, "quantity");
            return (Criteria) this;
        }

        public Criteria andQuantityNotBetween(Long value1, Long value2) {
            addCriterion("QUANTITY not between", value1, value2, "quantity");
            return (Criteria) this;
        }

        public Criteria andCurrencyIsNull() {
            addCriterion("CURRENCY is null");
            return (Criteria) this;
        }

        public Criteria andCurrencyIsNotNull() {
            addCriterion("CURRENCY is not null");
            return (Criteria) this;
        }

        public Criteria andCurrencyEqualTo(String value) {
            addCriterion("CURRENCY =", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotEqualTo(String value) {
            addCriterion("CURRENCY <>", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyGreaterThan(String value) {
            addCriterion("CURRENCY >", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyGreaterThanOrEqualTo(String value) {
            addCriterion("CURRENCY >=", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLessThan(String value) {
            addCriterion("CURRENCY <", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLessThanOrEqualTo(String value) {
            addCriterion("CURRENCY <=", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLike(String value) {
            addCriterion("CURRENCY like", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotLike(String value) {
            addCriterion("CURRENCY not like", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyIn(List<String> values) {
            addCriterion("CURRENCY in", values, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotIn(List<String> values) {
            addCriterion("CURRENCY not in", values, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyBetween(String value1, String value2) {
            addCriterion("CURRENCY between", value1, value2, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotBetween(String value1, String value2) {
            addCriterion("CURRENCY not between", value1, value2, "currency");
            return (Criteria) this;
        }

        public Criteria andAmountIsNull() {
            addCriterion("AMOUNT is null");
            return (Criteria) this;
        }

        public Criteria andAmountIsNotNull() {
            addCriterion("AMOUNT is not null");
            return (Criteria) this;
        }

        public Criteria andAmountEqualTo(BigDecimal value) {
            addCriterion("AMOUNT =", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotEqualTo(BigDecimal value) {
            addCriterion("AMOUNT <>", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountGreaterThan(BigDecimal value) {
            addCriterion("AMOUNT >", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("AMOUNT >=", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountLessThan(BigDecimal value) {
            addCriterion("AMOUNT <", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("AMOUNT <=", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountIn(List<BigDecimal> values) {
            addCriterion("AMOUNT in", values, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotIn(List<BigDecimal> values) {
            addCriterion("AMOUNT not in", values, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("AMOUNT between", value1, value2, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("AMOUNT not between", value1, value2, "amount");
            return (Criteria) this;
        }

        public Criteria andUnitamountIsNull() {
            addCriterion("UNITAMOUNT is null");
            return (Criteria) this;
        }

        public Criteria andUnitamountIsNotNull() {
            addCriterion("UNITAMOUNT is not null");
            return (Criteria) this;
        }

        public Criteria andUnitamountEqualTo(BigDecimal value) {
            addCriterion("UNITAMOUNT =", value, "unitamount");
            return (Criteria) this;
        }

        public Criteria andUnitamountNotEqualTo(BigDecimal value) {
            addCriterion("UNITAMOUNT <>", value, "unitamount");
            return (Criteria) this;
        }

        public Criteria andUnitamountGreaterThan(BigDecimal value) {
            addCriterion("UNITAMOUNT >", value, "unitamount");
            return (Criteria) this;
        }

        public Criteria andUnitamountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("UNITAMOUNT >=", value, "unitamount");
            return (Criteria) this;
        }

        public Criteria andUnitamountLessThan(BigDecimal value) {
            addCriterion("UNITAMOUNT <", value, "unitamount");
            return (Criteria) this;
        }

        public Criteria andUnitamountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("UNITAMOUNT <=", value, "unitamount");
            return (Criteria) this;
        }

        public Criteria andUnitamountIn(List<BigDecimal> values) {
            addCriterion("UNITAMOUNT in", values, "unitamount");
            return (Criteria) this;
        }

        public Criteria andUnitamountNotIn(List<BigDecimal> values) {
            addCriterion("UNITAMOUNT not in", values, "unitamount");
            return (Criteria) this;
        }

        public Criteria andUnitamountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("UNITAMOUNT between", value1, value2, "unitamount");
            return (Criteria) this;
        }

        public Criteria andUnitamountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("UNITAMOUNT not between", value1, value2, "unitamount");
            return (Criteria) this;
        }

        public Criteria andRateIsNull() {
            addCriterion("RATE is null");
            return (Criteria) this;
        }

        public Criteria andRateIsNotNull() {
            addCriterion("RATE is not null");
            return (Criteria) this;
        }

        public Criteria andRateEqualTo(BigDecimal value) {
            addCriterion("RATE =", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotEqualTo(BigDecimal value) {
            addCriterion("RATE <>", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateGreaterThan(BigDecimal value) {
            addCriterion("RATE >", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE >=", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateLessThan(BigDecimal value) {
            addCriterion("RATE <", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE <=", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateIn(List<BigDecimal> values) {
            addCriterion("RATE in", values, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotIn(List<BigDecimal> values) {
            addCriterion("RATE not in", values, "rate");
            return (Criteria) this;
        }

        public Criteria andRateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE between", value1, value2, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE not between", value1, value2, "rate");
            return (Criteria) this;
        }

        public Criteria andDiscountIsNull() {
            addCriterion("DISCOUNT is null");
            return (Criteria) this;
        }

        public Criteria andDiscountIsNotNull() {
            addCriterion("DISCOUNT is not null");
            return (Criteria) this;
        }

        public Criteria andDiscountEqualTo(BigDecimal value) {
            addCriterion("DISCOUNT =", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountNotEqualTo(BigDecimal value) {
            addCriterion("DISCOUNT <>", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountGreaterThan(BigDecimal value) {
            addCriterion("DISCOUNT >", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("DISCOUNT >=", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountLessThan(BigDecimal value) {
            addCriterion("DISCOUNT <", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("DISCOUNT <=", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountIn(List<BigDecimal> values) {
            addCriterion("DISCOUNT in", values, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountNotIn(List<BigDecimal> values) {
            addCriterion("DISCOUNT not in", values, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("DISCOUNT between", value1, value2, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("DISCOUNT not between", value1, value2, "discount");
            return (Criteria) this;
        }

        public Criteria andPremiumIsNull() {
            addCriterion("PREMIUM is null");
            return (Criteria) this;
        }

        public Criteria andPremiumIsNotNull() {
            addCriterion("PREMIUM is not null");
            return (Criteria) this;
        }

        public Criteria andPremiumEqualTo(BigDecimal value) {
            addCriterion("PREMIUM =", value, "premium");
            return (Criteria) this;
        }

        public Criteria andPremiumNotEqualTo(BigDecimal value) {
            addCriterion("PREMIUM <>", value, "premium");
            return (Criteria) this;
        }

        public Criteria andPremiumGreaterThan(BigDecimal value) {
            addCriterion("PREMIUM >", value, "premium");
            return (Criteria) this;
        }

        public Criteria andPremiumGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("PREMIUM >=", value, "premium");
            return (Criteria) this;
        }

        public Criteria andPremiumLessThan(BigDecimal value) {
            addCriterion("PREMIUM <", value, "premium");
            return (Criteria) this;
        }

        public Criteria andPremiumLessThanOrEqualTo(BigDecimal value) {
            addCriterion("PREMIUM <=", value, "premium");
            return (Criteria) this;
        }

        public Criteria andPremiumIn(List<BigDecimal> values) {
            addCriterion("PREMIUM in", values, "premium");
            return (Criteria) this;
        }

        public Criteria andPremiumNotIn(List<BigDecimal> values) {
            addCriterion("PREMIUM not in", values, "premium");
            return (Criteria) this;
        }

        public Criteria andPremiumBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PREMIUM between", value1, value2, "premium");
            return (Criteria) this;
        }

        public Criteria andPremiumNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PREMIUM not between", value1, value2, "premium");
            return (Criteria) this;
        }

        public Criteria andUnitpremiumIsNull() {
            addCriterion("UNITPREMIUM is null");
            return (Criteria) this;
        }

        public Criteria andUnitpremiumIsNotNull() {
            addCriterion("UNITPREMIUM is not null");
            return (Criteria) this;
        }

        public Criteria andUnitpremiumEqualTo(BigDecimal value) {
            addCriterion("UNITPREMIUM =", value, "unitpremium");
            return (Criteria) this;
        }

        public Criteria andUnitpremiumNotEqualTo(BigDecimal value) {
            addCriterion("UNITPREMIUM <>", value, "unitpremium");
            return (Criteria) this;
        }

        public Criteria andUnitpremiumGreaterThan(BigDecimal value) {
            addCriterion("UNITPREMIUM >", value, "unitpremium");
            return (Criteria) this;
        }

        public Criteria andUnitpremiumGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("UNITPREMIUM >=", value, "unitpremium");
            return (Criteria) this;
        }

        public Criteria andUnitpremiumLessThan(BigDecimal value) {
            addCriterion("UNITPREMIUM <", value, "unitpremium");
            return (Criteria) this;
        }

        public Criteria andUnitpremiumLessThanOrEqualTo(BigDecimal value) {
            addCriterion("UNITPREMIUM <=", value, "unitpremium");
            return (Criteria) this;
        }

        public Criteria andUnitpremiumIn(List<BigDecimal> values) {
            addCriterion("UNITPREMIUM in", values, "unitpremium");
            return (Criteria) this;
        }

        public Criteria andUnitpremiumNotIn(List<BigDecimal> values) {
            addCriterion("UNITPREMIUM not in", values, "unitpremium");
            return (Criteria) this;
        }

        public Criteria andUnitpremiumBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("UNITPREMIUM between", value1, value2, "unitpremium");
            return (Criteria) this;
        }

        public Criteria andUnitpremiumNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("UNITPREMIUM not between", value1, value2, "unitpremium");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNull() {
            addCriterion("VALIDSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNotNull() {
            addCriterion("VALIDSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andValidstatusEqualTo(String value) {
            addCriterion("VALIDSTATUS =", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotEqualTo(String value) {
            addCriterion("VALIDSTATUS <>", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThan(String value) {
            addCriterion("VALIDSTATUS >", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS >=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThan(String value) {
            addCriterion("VALIDSTATUS <", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS <=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLike(String value) {
            addCriterion("VALIDSTATUS like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotLike(String value) {
            addCriterion("VALIDSTATUS not like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusIn(List<String> values) {
            addCriterion("VALIDSTATUS in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotIn(List<String> values) {
            addCriterion("VALIDSTATUS not in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS not between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("FLAG is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("FLAG =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("FLAG <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("FLAG >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("FLAG <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("FLAG <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("FLAG like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("FLAG not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("FLAG in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("FLAG not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("FLAG between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("FLAG not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andWaysofcalcIsNull() {
            addCriterion("WAYSOFCALC is null");
            return (Criteria) this;
        }

        public Criteria andWaysofcalcIsNotNull() {
            addCriterion("WAYSOFCALC is not null");
            return (Criteria) this;
        }

        public Criteria andWaysofcalcEqualTo(String value) {
            addCriterion("WAYSOFCALC =", value, "waysofcalc");
            return (Criteria) this;
        }

        public Criteria andWaysofcalcNotEqualTo(String value) {
            addCriterion("WAYSOFCALC <>", value, "waysofcalc");
            return (Criteria) this;
        }

        public Criteria andWaysofcalcGreaterThan(String value) {
            addCriterion("WAYSOFCALC >", value, "waysofcalc");
            return (Criteria) this;
        }

        public Criteria andWaysofcalcGreaterThanOrEqualTo(String value) {
            addCriterion("WAYSOFCALC >=", value, "waysofcalc");
            return (Criteria) this;
        }

        public Criteria andWaysofcalcLessThan(String value) {
            addCriterion("WAYSOFCALC <", value, "waysofcalc");
            return (Criteria) this;
        }

        public Criteria andWaysofcalcLessThanOrEqualTo(String value) {
            addCriterion("WAYSOFCALC <=", value, "waysofcalc");
            return (Criteria) this;
        }

        public Criteria andWaysofcalcLike(String value) {
            addCriterion("WAYSOFCALC like", value, "waysofcalc");
            return (Criteria) this;
        }

        public Criteria andWaysofcalcNotLike(String value) {
            addCriterion("WAYSOFCALC not like", value, "waysofcalc");
            return (Criteria) this;
        }

        public Criteria andWaysofcalcIn(List<String> values) {
            addCriterion("WAYSOFCALC in", values, "waysofcalc");
            return (Criteria) this;
        }

        public Criteria andWaysofcalcNotIn(List<String> values) {
            addCriterion("WAYSOFCALC not in", values, "waysofcalc");
            return (Criteria) this;
        }

        public Criteria andWaysofcalcBetween(String value1, String value2) {
            addCriterion("WAYSOFCALC between", value1, value2, "waysofcalc");
            return (Criteria) this;
        }

        public Criteria andWaysofcalcNotBetween(String value1, String value2) {
            addCriterion("WAYSOFCALC not between", value1, value2, "waysofcalc");
            return (Criteria) this;
        }

        public Criteria andDelflagIsNull() {
            addCriterion("DELFLAG is null");
            return (Criteria) this;
        }

        public Criteria andDelflagIsNotNull() {
            addCriterion("DELFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andDelflagEqualTo(String value) {
            addCriterion("DELFLAG =", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagNotEqualTo(String value) {
            addCriterion("DELFLAG <>", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagGreaterThan(String value) {
            addCriterion("DELFLAG >", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagGreaterThanOrEqualTo(String value) {
            addCriterion("DELFLAG >=", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagLessThan(String value) {
            addCriterion("DELFLAG <", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagLessThanOrEqualTo(String value) {
            addCriterion("DELFLAG <=", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagLike(String value) {
            addCriterion("DELFLAG like", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagNotLike(String value) {
            addCriterion("DELFLAG not like", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagIn(List<String> values) {
            addCriterion("DELFLAG in", values, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagNotIn(List<String> values) {
            addCriterion("DELFLAG not in", values, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagBetween(String value1, String value2) {
            addCriterion("DELFLAG between", value1, value2, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagNotBetween(String value1, String value2) {
            addCriterion("DELFLAG not between", value1, value2, "delflag");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}