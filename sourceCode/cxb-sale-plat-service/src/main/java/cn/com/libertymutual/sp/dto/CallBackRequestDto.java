package cn.com.libertymutual.sp.dto;

public class CallBackRequestDto {
	/**
	 * 投保单号
	 * */
	private String proposalNo;
	/**
	 * 保单号
	 * */
	private String policyNo;
	/**
	 * 车牌号
	 * */
	private String licenseNo;
	/**
	 * 发动机号
	 * */
	private String engineNo;
	/**
	 * 车架号
	 * */
	private String vinNo;
	/**
	 * 当前保费
	 * */
	private String currentPremium;
	/**
	 * 当前车船税
	 * */
	private String currentCarshipPremium;
	/**
	 * 批改类型
	 * pg: 批改,
	 * --pt:批退    修改为   21:批退  退保
	 * 03:保单遗失       
	 * 19:保单注销      退保
	 * */
	private String endorType;
	/**
	 * 批单号
	 * */
	private String endorseNo;
	/**
	 * 原始保费
	 * */
	private String oldPremium;
	/**
	 * 原始车船税
	 * */
	private String oldCarshipPremium;
	/**
	 * 当前状态
	 *  0:新保,1:生成保单,2:下发修改or拒保,3:自动核保,9:等待核保,4:主动撤回,
	 * 8:等待支付,7:查勘岗,6:单证审核不通过,5:待单证审核,A:待绩效审核,
	 * B:绩效审核不通过,
	 * */
	private String status;
	/**
	 * 核保未通过原因/支付失败原因
	 * */
	private String underWriteFailReason;
	/**
	 * 支付方式
	 * 1:刷卡
	 * 2:网银转账,3:支票,4:现金,5:内部转账,7在线支付
	 * */
	private String payWay;
	/**
	 * 投保人
	 * */
	private String appliName;
	/**
	 * 被保人
	 * */
	private String insuredName;
	/**
	 * 起保日期
	 * */
	private String startDate;
	/**
	 * 终保日期
	 * */
	private String endDate;
	/**
	 * 总保额
	 * */
	private String sumAmount;

	public String getProposalNo() {
		return proposalNo;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public String getLicenseNo() {
		return licenseNo;
	}

	public String getEngineNo() {
		return engineNo;
	}

	public String getVinNo() {
		return vinNo;
	}

	public String getCurrentPremium() {
		return currentPremium;
	}

	public String getCurrentCarshipPremium() {
		return currentCarshipPremium;
	}

	public String getEndorType() {
		return endorType;
	}

	public String getEndorseNo() {
		return endorseNo;
	}

	public String getOldPremium() {
		return oldPremium;
	}

	public String getOldCarshipPremium() {
		return oldCarshipPremium;
	}

	public String getStatus() {
		return status;
	}

	public String getUnderWriteFailReason() {
		return underWriteFailReason;
	}

	public String getPayWay() {
		return payWay;
	}

	public String getAppliName() {
		return appliName;
	}

	public String getInsuredName() {
		return insuredName;
	}

	public String getStartDate() {
		return startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public String getSumAmount() {
		return sumAmount;
	}

	public void setProposalNo(String proposalNo) {
		this.proposalNo = proposalNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}

	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}

	public void setVinNo(String vinNo) {
		this.vinNo = vinNo;
	}

	public void setCurrentPremium(String currentPremium) {
		this.currentPremium = currentPremium;
	}

	public void setCurrentCarshipPremium(String currentCarshipPremium) {
		this.currentCarshipPremium = currentCarshipPremium;
	}

	public void setEndorType(String endorType) {
		this.endorType = endorType;
	}

	public void setEndorseNo(String endorseNo) {
		this.endorseNo = endorseNo;
	}

	public void setOldPremium(String oldPremium) {
		this.oldPremium = oldPremium;
	}

	public void setOldCarshipPremium(String oldCarshipPremium) {
		this.oldCarshipPremium = oldCarshipPremium;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setUnderWriteFailReason(String underWriteFailReason) {
		this.underWriteFailReason = underWriteFailReason;
	}

	public void setPayWay(String payWay) {
		this.payWay = payWay;
	}

	public void setAppliName(String appliName) {
		this.appliName = appliName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public void setSumAmount(String sumAmount) {
		this.sumAmount = sumAmount;
	}
}
