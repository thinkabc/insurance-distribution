package cn.com.libertymutual.core.security.jwt.auth;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}