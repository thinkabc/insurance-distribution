package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Fdriskconfig;

/**
 * Created by steven.li on 2017/12/18.
 */
public interface FdriskconfigService {

    /**
     * 新增Fdriskconfig
     * @param record
     */
    void insert(Fdriskconfig record);

    List<Fdriskconfig> findAll();

    List<Fdriskconfig> findByRisk(String riskcode);
}
