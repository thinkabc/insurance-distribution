package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.FhxriskExample;
import cn.com.libertymutual.production.model.nomorcldatasource.FhxriskKey;
@Mapper
public interface FhxriskMapper {
    int countByExample(FhxriskExample example);

    int deleteByExample(FhxriskExample example);

    int deleteByPrimaryKey(FhxriskKey key);

    int insert(FhxriskKey record);

    int insertSelective(FhxriskKey record);

    List<FhxriskKey> selectByExample(FhxriskExample example);

    int updateByExampleSelective(@Param("record") FhxriskKey record, @Param("example") FhxriskExample example);

    int updateByExample(@Param("record") FhxriskKey record, @Param("example") FhxriskExample example);
}