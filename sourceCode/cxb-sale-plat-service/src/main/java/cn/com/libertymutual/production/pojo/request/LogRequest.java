package cn.com.libertymutual.production.pojo.request;


public class LogRequest extends Request {
	
	private String businessKeyValue;

	public String getBusinessKeyValue() {
		return businessKeyValue;
	}

	public void setBusinessKeyValue(String businessKeyValue) {
		this.businessKeyValue = businessKeyValue;
	}
}
