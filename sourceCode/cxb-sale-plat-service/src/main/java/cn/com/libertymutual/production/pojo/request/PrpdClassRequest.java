package cn.com.libertymutual.production.pojo.request;

public class PrpdClassRequest extends Request {
	private String classcode;

    private String classname;

    private String classename;

    private String acccode;

    private String newclasscode;

    private String validstatus;

    private String flag;

    private String riskcategory;

    private String compositeflag;

	public String getClasscode() {
		return classcode;
	}

	public void setClasscode(String classcode) {
		this.classcode = classcode;
	}

	public String getClassname() {
		return classname;
	}

	public void setClassname(String classname) {
		this.classname = classname;
	}

	public String getClassename() {
		return classename;
	}

	public void setClassename(String classename) {
		this.classename = classename;
	}

	public String getAcccode() {
		return acccode;
	}

	public void setAcccode(String acccode) {
		this.acccode = acccode;
	}

	public String getNewclasscode() {
		return newclasscode;
	}

	public void setNewclasscode(String newclasscode) {
		this.newclasscode = newclasscode;
	}

	public String getValidstatus() {
		return validstatus;
	}

	public void setValidstatus(String validstatus) {
		this.validstatus = validstatus;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getRiskcategory() {
		return riskcategory;
	}

	public void setRiskcategory(String riskcategory) {
		this.riskcategory = riskcategory;
	}

	public String getCompositeflag() {
		return compositeflag;
	}

	public void setCompositeflag(String compositeflag) {
		this.compositeflag = compositeflag;
	}
    
}
