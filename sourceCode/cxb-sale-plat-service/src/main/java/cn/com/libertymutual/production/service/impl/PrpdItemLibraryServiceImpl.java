package cn.com.libertymutual.production.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpditemlibraryMapper;
import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdkinditemMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdItemRiskLibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpditemlibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpditemlibraryExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpditemlibraryWithBLOBs;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpditemlibraryExample.Criteria;
import cn.com.libertymutual.production.pojo.request.LinkItemRequest;
import cn.com.libertymutual.production.pojo.request.PrpdItemLibraryRequest;
import cn.com.libertymutual.production.pojo.request.PrpdItemRequest;
import cn.com.libertymutual.production.pojo.response.ItemKind;
import cn.com.libertymutual.production.service.api.PrpdItemLibraryService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/** 
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
@Service
public class PrpdItemLibraryServiceImpl implements PrpdItemLibraryService {
	
	@Autowired
	private PrpditemlibraryMapper prpditemlibraryMapper;
	@Autowired
	private PrpdkinditemMapper prpdkinditemMapper;
	
	@Override
	public PageInfo<PrpditemlibraryWithBLOBs> select(PrpdItemRequest prpdItemRequest) {
		String itemCode = prpdItemRequest.getItemcode();
		String itemCname = prpdItemRequest.getItemcname();
		String riskCode = prpdItemRequest.getRisk().getRiskcode();
		String riskVersion = prpdItemRequest.getRisk().getRiskversion();
		int pageNum = prpdItemRequest.getCurrentPage();
		int pageSize = prpdItemRequest.getPageSize();
		PageInfo<PrpditemlibraryWithBLOBs> pageInfo = new PageInfo<>(prpditemlibraryMapper.selectNotRiskLinked(riskCode, 
																									  riskVersion, 
																									  "defaultPlan",
																									  "".equals(itemCode)?null:itemCode,
																									  "".equals(itemCname)?null:itemCname,
																									  pageNum,
																									  pageSize));
		return pageInfo;
	}

	@Override
	public PageInfo<PrpdItemRiskLibrary> findPrpdItemWithLinked(PrpdItemLibraryRequest prpdItemLibraryRequest) {
		String kindCode = prpdItemLibraryRequest.getKind().getKindcode();
		String kindVersion = prpdItemLibraryRequest.getKind().getKindversion();
		String itemCode = prpdItemLibraryRequest.getItemcode();
		String itemCName = prpdItemLibraryRequest.getItemcname();
		String validstatus = prpdItemLibraryRequest.getValidstatus();
		int pageNum = prpdItemLibraryRequest.getCurrentPage();
		int pageSize = prpdItemLibraryRequest.getPageSize();
		PageInfo<PrpdItemRiskLibrary> pageInfo = new PageInfo<>(prpdkinditemMapper.findItemCodesWithLinked(kindCode,
																										   kindVersion,
																										   "".equals(itemCode)?null:itemCode,
																										   "".equals(itemCName)?null:itemCName,
																											validstatus,
																											pageNum,
																											pageSize));
		return pageInfo;
	}

	@Override
	public void insert(PrpditemlibraryWithBLOBs prpditemlibrary) {
		prpditemlibraryMapper.insertSelective(prpditemlibrary);
	}

	@Override
	public PageInfo<PrpditemlibraryWithBLOBs> selectByPage(List<String> itemCodes, LinkItemRequest linkItemRequest) {
		PrpditemlibraryExample example = new PrpditemlibraryExample();
		Criteria criteria = example.createCriteria();
		criteria.andItemcodeIn(itemCodes);
		PageHelper.startPage(linkItemRequest.getCurrentPage(), linkItemRequest.getPageSize());
		PageInfo<PrpditemlibraryWithBLOBs> pageInfo = new PageInfo<>(prpditemlibraryMapper.selectByExampleWithBLOBs(example));
		return pageInfo;
	}

	@Override
	public List<ItemKind> findItemByRiskKind(String riskCode,
			String riskVersion, String kindCode, String kindVersion) {
		return prpditemlibraryMapper.selectByRiskKindLinked(riskCode, riskVersion, kindCode, kindVersion);
	}

	@Override
	public PageInfo<PrpditemlibraryWithBLOBs> selectItem(PrpdItemLibraryRequest request) {
		PrpditemlibraryExample example = new PrpditemlibraryExample();
		Criteria criteria = example.createCriteria();
		Date createDate = request.getCreatedate();
		
		example.setOrderByClause("createdate desc");
		if(!StringUtils.isEmpty(request.getItemcode())) {
			criteria.andItemcodeLike("%" + request.getItemcode() + "%");
		}
		if(!StringUtils.isEmpty(request.getItemcname())) {
			criteria.andItemcnameLike("%" + request.getItemcname() + "%");
		}
		if (createDate != null) {
			criteria.andCreatedateGreaterThanOrEqualTo(createDate);
			Calendar c = Calendar.getInstance();  
	        c.setTime(createDate);  
	        c.add(Calendar.DAY_OF_MONTH, 1);// 今天+1天  
	        Date tomorrow = c.getTime();
	        criteria.andCreatedateLessThanOrEqualTo(tomorrow);
		}
		if(!StringUtils.isEmpty(request.getValidstatus())) {
			criteria.andValidstatusEqualTo(request.getValidstatus());
		}
		if (!StringUtils.isEmpty(request.getOrderBy()) && !"undefined".equals(request.getOrderBy())) {
			example.setOrderByClause(request.getOrderBy() + " " + request.getOrder());
		}
		PageHelper.startPage(request.getCurrentPage(), request.getPageSize());
		PageInfo<PrpditemlibraryWithBLOBs> pageInfo = new PageInfo<>(prpditemlibraryMapper.selectByExampleWithBLOBs(example));
		return pageInfo;
	}

	@Override
	public void update(PrpditemlibraryWithBLOBs record) {
		PrpditemlibraryExample example = new PrpditemlibraryExample();
		Criteria criteria = example.createCriteria();
		criteria.andItemcodeEqualTo(record.getItemcode());
		prpditemlibraryMapper.updateByExampleSelective(record, example);
	}

	@Override
	public List<PrpditemlibraryWithBLOBs> selectTopItems() {
		PrpditemlibraryExample example = new PrpditemlibraryExample();
		Criteria criteria = example.createCriteria();
		criteria.andIdEqualTo("0");
		return prpditemlibraryMapper.selectByExampleWithBLOBs(example);
	}

	@Override
	public PageInfo<ItemKind> selectWithItemKind(LinkItemRequest linkItemRequest) {
		String kindCode = linkItemRequest.getKind().getKindcode();
		String kindVersion = linkItemRequest.getKind().getKindversion();
		int pageNum = linkItemRequest.getCurrentPage();
		int pageSize = linkItemRequest.getPageSize();
		return new PageInfo<ItemKind>(prpditemlibraryMapper.selectWithItemKind(kindCode, 
																			  kindVersion, 
																			  pageNum,
																			  pageSize));
	}
}
