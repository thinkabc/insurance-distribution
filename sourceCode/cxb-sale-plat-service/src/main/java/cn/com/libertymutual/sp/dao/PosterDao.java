package cn.com.libertymutual.sp.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sp.bean.TbSpPoster;
@Repository
public interface PosterDao extends PagingAndSortingRepository<TbSpPoster, Integer>, JpaSpecificationExecutor<TbSpPoster> {

	@Transactional
	@Modifying
	@Query("update TbSpPoster set status = ?1 where id = ?2")
	void updateStatus(String status,Integer id);

}
