
package cn.com.libertymutual.sp.webService.allpolicy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for prpTvoyageDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prpTvoyageDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="checkAgentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="endSiteCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="endSiteName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="makeYearMonth" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="shipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="startSiteCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="startSiteName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="viaSiteCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="viaSiteName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="voyageType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prpTvoyageDto", propOrder = {
    "checkAgentCode",
    "endSiteCode",
    "endSiteName",
    "makeYearMonth",
    "shipCode",
    "shipName",
    "startSiteCode",
    "startSiteName",
    "viaSiteCode",
    "viaSiteName",
    "voyageType"
})
public class PrpTvoyageDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String checkAgentCode;
    protected String endSiteCode;
    protected String endSiteName;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar makeYearMonth;
    protected String shipCode;
    protected String shipName;
    protected String startSiteCode;
    protected String startSiteName;
    protected String viaSiteCode;
    protected String viaSiteName;
    protected String voyageType;

    /**
     * Gets the value of the checkAgentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckAgentCode() {
        return checkAgentCode;
    }

    /**
     * Sets the value of the checkAgentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckAgentCode(String value) {
        this.checkAgentCode = value;
    }

    /**
     * Gets the value of the endSiteCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndSiteCode() {
        return endSiteCode;
    }

    /**
     * Sets the value of the endSiteCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndSiteCode(String value) {
        this.endSiteCode = value;
    }

    /**
     * Gets the value of the endSiteName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndSiteName() {
        return endSiteName;
    }

    /**
     * Sets the value of the endSiteName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndSiteName(String value) {
        this.endSiteName = value;
    }

    /**
     * Gets the value of the makeYearMonth property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMakeYearMonth() {
        return makeYearMonth;
    }

    /**
     * Sets the value of the makeYearMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMakeYearMonth(XMLGregorianCalendar value) {
        this.makeYearMonth = value;
    }

    /**
     * Gets the value of the shipCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipCode() {
        return shipCode;
    }

    /**
     * Sets the value of the shipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipCode(String value) {
        this.shipCode = value;
    }

    /**
     * Gets the value of the shipName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipName() {
        return shipName;
    }

    /**
     * Sets the value of the shipName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipName(String value) {
        this.shipName = value;
    }

    /**
     * Gets the value of the startSiteCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartSiteCode() {
        return startSiteCode;
    }

    /**
     * Sets the value of the startSiteCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartSiteCode(String value) {
        this.startSiteCode = value;
    }

    /**
     * Gets the value of the startSiteName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartSiteName() {
        return startSiteName;
    }

    /**
     * Sets the value of the startSiteName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartSiteName(String value) {
        this.startSiteName = value;
    }

    /**
     * Gets the value of the viaSiteCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getViaSiteCode() {
        return viaSiteCode;
    }

    /**
     * Sets the value of the viaSiteCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setViaSiteCode(String value) {
        this.viaSiteCode = value;
    }

    /**
     * Gets the value of the viaSiteName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getViaSiteName() {
        return viaSiteName;
    }

    /**
     * Sets the value of the viaSiteName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setViaSiteName(String value) {
        this.viaSiteName = value;
    }

    /**
     * Gets the value of the voyageType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoyageType() {
        return voyageType;
    }

    /**
     * Sets the value of the voyageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoyageType(String value) {
        this.voyageType = value;
    }

}
