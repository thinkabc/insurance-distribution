package cn.com.libertymutual.production.service.api.business;

import cn.com.libertymutual.production.pojo.request.PrpdPolicyPayRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.PrpdPolicyPayService;
import cn.com.libertymutual.production.service.api.PrpsAgentService;
import cn.com.libertymutual.production.utils.Constant;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/** 
 * @Description: 见费出单后台业务逻辑入口
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
public abstract class PolicyPayBusinessService {

	protected static final Logger log = Constant.log;

	@Autowired
	protected CommonBusinessService commonBusinessService;
	@Autowired
	protected PrpsAgentService prpsAgentService;
	@Autowired
	protected PrpdPolicyPayService prpdPolicyPayService;

	/**
	 * 查询见费出单
	 */
	public abstract Response findPolicyPays(PrpdPolicyPayRequest prpdPolicyPayRequest);

	/**
	 * 新增见费出单配置
	 * @param prpdPolicyPayRequest
	 * @return
     */
	public abstract Response insert(PrpdPolicyPayRequest prpdPolicyPayRequest) throws Exception;

	/**
	 * 修改见费出单配置
	 * @param prpdPolicyPayRequest
	 * @return
     */
	public abstract Response update(PrpdPolicyPayRequest prpdPolicyPayRequest) throws Exception ;

	/**
	 * 删除见费出单配置
	 * @param prpdPolicyPayRequest
	 * @return
     */
	public abstract Response delete(PrpdPolicyPayRequest prpdPolicyPayRequest);
}