package cn.com.libertymutual.production.pojo.request;

import cn.com.libertymutual.production.model.nomorcldatasource.*;
import cn.com.libertymutual.production.pojo.response.ItemKind;

import java.util.List;

public class PlanAddRequest2 extends Request {
    private String plantype;
    private Prpdrisk risk;
    private String plancode;
    private String plancname;
    private String planename;
    private String validstatus;
    private List<List<Prpdplansub>> propsGroup;
    private List<String> kindcodesGroup;
    private List<List<Prpdkindlibrary>> kindsGroup;
    private List<String> itemcodesGroup;
    private List<List<ItemKind>> itemsGroup;
    private List<List<AmountPremium>> kinditemsGroup;
    private String ilogflag;
    private String delflag;
    private String autoundwrt;
    private String autodoc;
    private String traveldestination;
    private String periodtype;
    private String period;
    private String applycomcode;
    private String applychannel;
    private String occupationcode;
    private Prpdriskplan originalPlan;

    public String getAutodoc() {
        return autodoc;
    }

    public void setAutodoc(String autodoc) {
        this.autodoc = autodoc;
    }

    public List<List<Prpdplansub>> getPropsGroup() {
        return propsGroup;
    }

    public void setPropsGroup(List<List<Prpdplansub>> propsGroup) {
        this.propsGroup = propsGroup;
    }

    public List<String> getKindcodesGroup() {
        return kindcodesGroup;
    }

    public void setKindcodesGroup(List<String> kindcodesGroup) {
        this.kindcodesGroup = kindcodesGroup;
    }

    public List<List<Prpdkindlibrary>> getKindsGroup() {
        return kindsGroup;
    }

    public void setKindsGroup(List<List<Prpdkindlibrary>> kindsGroup) {
        this.kindsGroup = kindsGroup;
    }

    public List<String> getItemcodesGroup() {
        return itemcodesGroup;
    }

    public void setItemcodesGroup(List<String> itemcodesGroup) {
        this.itemcodesGroup = itemcodesGroup;
    }

    public List<List<ItemKind>> getItemsGroup() {
        return itemsGroup;
    }

    public void setItemsGroup(List<List<ItemKind>> itemsGroup) {
        this.itemsGroup = itemsGroup;
    }

    public List<List<AmountPremium>> getKinditemsGroup() {
        return kinditemsGroup;
    }

    public void setKinditemsGroup(List<List<AmountPremium>> kinditemsGroup) {
        this.kinditemsGroup = kinditemsGroup;
    }

    public String getIlogflag() {
        return ilogflag;
    }

    public void setIlogflag(String ilogflag) {
        this.ilogflag = ilogflag;
    }

    public String getPlantype() {
        return plantype;
    }

    public void setPlantype(String plantype) {
        this.plantype = plantype;
    }

    public Prpdrisk getRisk() {
        return risk;
    }

    public void setRisk(Prpdrisk risk) {
        this.risk = risk;
    }

    public String getPlancode() {
        return plancode;
    }

    public void setPlancode(String plancode) {
        this.plancode = plancode;
    }

    public String getPlancname() {
        return plancname;
    }

    public void setPlancname(String plancname) {
        this.plancname = plancname;
    }

    public String getPlanename() {
        return planename;
    }

    public void setPlanename(String planename) {
        this.planename = planename;
    }

    public String getValidstatus() {
        return validstatus;
    }

    public void setValidstatus(String validstatus) {
        this.validstatus = validstatus;
    }

    public String getDelflag() {
        return delflag;
    }

    public void setDelflag(String delflag) {
        this.delflag = delflag;
    }

    public String getAutoundwrt() {
        return autoundwrt;
    }

    public void setAutoundwrt(String autoundwrt) {
        this.autoundwrt = autoundwrt;
    }

    public String getTraveldestination() {
        return traveldestination;
    }

    public void setTraveldestination(String traveldestination) {
        this.traveldestination = traveldestination;
    }

    public String getPeriodtype() {
        return periodtype;
    }

    public void setPeriodtype(String periodtype) {
        this.periodtype = periodtype;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getOccupationcode() {
        return occupationcode;
    }

    public void setOccupationcode(String occupationcode) {
        this.occupationcode = occupationcode;
    }

    public String getApplycomcode() {
        return applycomcode;
    }

    public void setApplycomcode(String applycomcode) {
        this.applycomcode = applycomcode;
    }

    public String getApplychannel() {
        return applychannel;
    }

    public void setApplychannel(String applychannel) {
        this.applychannel = applychannel;
    }

    public Prpdriskplan getOriginalPlan() {
        return originalPlan;
    }

    public void setOriginalPlan(Prpdriskplan originalPlan) {
        this.originalPlan = originalPlan;
    }

}
