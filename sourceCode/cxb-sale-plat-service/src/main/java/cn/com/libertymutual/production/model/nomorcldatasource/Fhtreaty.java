package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.Date;

public class Fhtreaty {
    private String treatyno;

    private String extreatyno;

    private String conntreatyno;

    private String opttype;

    private String refno;

    private String treatyname;

    private String treatyename;

    private Date closedate;

    private String treatytype;

    private String uwyear;

    private Date startdate;

    private Date enddate;

    private Date extenddate;

    private Date logoutdate;

    private String repremiumbase;

    private String calculatebase;

    private String currency;

    private Integer accdate;

    private Integer duedate;

    private String accperiod;

    private String extendflag;

    private String inreinscode;

    private String brokercode;

    private String paycode;

    private String inreinsname;

    private String brokername;

    private String payname;

    private String exchangeflag;

    private String stateflag;

    private String remarks;

    private String creatercode;

    private Date createdate;

    private String updatercode;

    private Date updatedate;

    private String underwritecode;

    private Date underwriteenddate;

    private String flag;

    private String fhtreatyremarks;

    public String getTreatyno() {
        return treatyno;
    }

    public void setTreatyno(String treatyno) {
        this.treatyno = treatyno == null ? null : treatyno.trim();
    }

    public String getExtreatyno() {
        return extreatyno;
    }

    public void setExtreatyno(String extreatyno) {
        this.extreatyno = extreatyno == null ? null : extreatyno.trim();
    }

    public String getConntreatyno() {
        return conntreatyno;
    }

    public void setConntreatyno(String conntreatyno) {
        this.conntreatyno = conntreatyno == null ? null : conntreatyno.trim();
    }

    public String getOpttype() {
        return opttype;
    }

    public void setOpttype(String opttype) {
        this.opttype = opttype == null ? null : opttype.trim();
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno == null ? null : refno.trim();
    }

    public String getTreatyname() {
        return treatyname;
    }

    public void setTreatyname(String treatyname) {
        this.treatyname = treatyname == null ? null : treatyname.trim();
    }

    public String getTreatyename() {
        return treatyename;
    }

    public void setTreatyename(String treatyename) {
        this.treatyename = treatyename == null ? null : treatyename.trim();
    }

    public Date getClosedate() {
        return closedate;
    }

    public void setClosedate(Date closedate) {
        this.closedate = closedate;
    }

    public String getTreatytype() {
        return treatytype;
    }

    public void setTreatytype(String treatytype) {
        this.treatytype = treatytype == null ? null : treatytype.trim();
    }

    public String getUwyear() {
        return uwyear;
    }

    public void setUwyear(String uwyear) {
        this.uwyear = uwyear == null ? null : uwyear.trim();
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public Date getExtenddate() {
        return extenddate;
    }

    public void setExtenddate(Date extenddate) {
        this.extenddate = extenddate;
    }

    public Date getLogoutdate() {
        return logoutdate;
    }

    public void setLogoutdate(Date logoutdate) {
        this.logoutdate = logoutdate;
    }

    public String getRepremiumbase() {
        return repremiumbase;
    }

    public void setRepremiumbase(String repremiumbase) {
        this.repremiumbase = repremiumbase == null ? null : repremiumbase.trim();
    }

    public String getCalculatebase() {
        return calculatebase;
    }

    public void setCalculatebase(String calculatebase) {
        this.calculatebase = calculatebase == null ? null : calculatebase.trim();
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency == null ? null : currency.trim();
    }

    public Integer getAccdate() {
        return accdate;
    }

    public void setAccdate(Integer accdate) {
        this.accdate = accdate;
    }

    public Integer getDuedate() {
        return duedate;
    }

    public void setDuedate(Integer duedate) {
        this.duedate = duedate;
    }

    public String getAccperiod() {
        return accperiod;
    }

    public void setAccperiod(String accperiod) {
        this.accperiod = accperiod == null ? null : accperiod.trim();
    }

    public String getExtendflag() {
        return extendflag;
    }

    public void setExtendflag(String extendflag) {
        this.extendflag = extendflag == null ? null : extendflag.trim();
    }

    public String getInreinscode() {
        return inreinscode;
    }

    public void setInreinscode(String inreinscode) {
        this.inreinscode = inreinscode == null ? null : inreinscode.trim();
    }

    public String getBrokercode() {
        return brokercode;
    }

    public void setBrokercode(String brokercode) {
        this.brokercode = brokercode == null ? null : brokercode.trim();
    }

    public String getPaycode() {
        return paycode;
    }

    public void setPaycode(String paycode) {
        this.paycode = paycode == null ? null : paycode.trim();
    }

    public String getInreinsname() {
        return inreinsname;
    }

    public void setInreinsname(String inreinsname) {
        this.inreinsname = inreinsname == null ? null : inreinsname.trim();
    }

    public String getBrokername() {
        return brokername;
    }

    public void setBrokername(String brokername) {
        this.brokername = brokername == null ? null : brokername.trim();
    }

    public String getPayname() {
        return payname;
    }

    public void setPayname(String payname) {
        this.payname = payname == null ? null : payname.trim();
    }

    public String getExchangeflag() {
        return exchangeflag;
    }

    public void setExchangeflag(String exchangeflag) {
        this.exchangeflag = exchangeflag == null ? null : exchangeflag.trim();
    }

    public String getStateflag() {
        return stateflag;
    }

    public void setStateflag(String stateflag) {
        this.stateflag = stateflag == null ? null : stateflag.trim();
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    public String getCreatercode() {
        return creatercode;
    }

    public void setCreatercode(String creatercode) {
        this.creatercode = creatercode == null ? null : creatercode.trim();
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getUpdatercode() {
        return updatercode;
    }

    public void setUpdatercode(String updatercode) {
        this.updatercode = updatercode == null ? null : updatercode.trim();
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public String getUnderwritecode() {
        return underwritecode;
    }

    public void setUnderwritecode(String underwritecode) {
        this.underwritecode = underwritecode == null ? null : underwritecode.trim();
    }

    public Date getUnderwriteenddate() {
        return underwriteenddate;
    }

    public void setUnderwriteenddate(Date underwriteenddate) {
        this.underwriteenddate = underwriteenddate;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getFhtreatyremarks() {
        return fhtreatyremarks;
    }

    public void setFhtreatyremarks(String fhtreatyremarks) {
        this.fhtreatyremarks = fhtreatyremarks == null ? null : fhtreatyremarks.trim();
    }
}