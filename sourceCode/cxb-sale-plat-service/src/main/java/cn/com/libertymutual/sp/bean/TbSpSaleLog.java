package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "tb_sp_sale_log")
public class TbSpSaleLog implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7998113431044589212L;

	private Integer id;
	private String requestData;
	private String responseData;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date requestTime;
	private String userCode;
	private String mark;
	private String operation;
	public static final String WECHAT_OAUTH2 = "微信授权";
	public static final String WECHAT_GET_GROUP_ID = "微信获取用户所在分组";
	public static final String WECHAT_GET_TICKET = "微信获取Ticket";
	public static final String WECHAT_KEYWORDS = "微信关键词";
	public static final String WECHAT_ACCESS_TOKEN = "微信accesstoken";
	public static final String SMS_MSG = "发送短信";
	public static final String OPERATION_INSURE = "投保"; // 允许
	public static final String OPERATION_PAY = "支付"; // 允许
	public static final String CALLBACK = "修改订单状态"; // 回调
	public static final String TB_INSURE = "投保"; // 允许
	public static final String UP_MERGE = "合并账户-H5";
	public static final String UP_MOBILE = "修改手机号-H5";
	public static final String UP_REALNAME = "实名认证-H5";
	public static final String UP_BINDINGBANK = "绑定银行卡-H5";
	public static final String UP_UNBINDINGBANK = "解绑银行卡-H5";
	public static final String UP_SETDEFAULTBANK = "设置默认银行卡-H5";
	public static final String UP_PASSWORD = "密码-H5";
	public static final String UP_LOGIN = "登录-H5";
	public static final String UP_SHOP_AGRNO = "查询业务关系代码";
	public static final String UP_SHOP_AREA = "查询业务关系代码";
	public static final String WECHAT_MSG = "微信消息推送";
	public static final String UP_SHARE = "分享";
	public static final String CHANNEL_SCORE = "渠道积分";
	public static final String CHANNEL_REGISTER = "渠道注册";

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "RequestData")
	public String getRequestData() {
		return requestData;
	}

	public void setRequestData(String requestData) {
		this.requestData = requestData;
	}

	@Column(name = "ResponseData")
	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}

	@Column(name = "RequestTime")
	public Date getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}

	@Column(name = "UserCode", length = 15)
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	@Column(name = "Mark", length = 50)
	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	@Column(name = "Operation", length = 20)
	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

}
