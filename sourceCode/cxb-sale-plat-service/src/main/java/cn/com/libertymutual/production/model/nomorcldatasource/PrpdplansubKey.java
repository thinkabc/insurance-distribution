package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpdplansubKey {
    private String plancode;

    private String riskcode;

    private String riskversion;

    private String propscode;

    private String propstype;

    private String lineno;

    public String getPlancode() {
        return plancode;
    }

    public void setPlancode(String plancode) {
        this.plancode = plancode == null ? null : plancode.trim();
    }

    public String getRiskcode() {
        return riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode == null ? null : riskcode.trim();
    }

    public String getRiskversion() {
        return riskversion;
    }

    public void setRiskversion(String riskversion) {
        this.riskversion = riskversion == null ? null : riskversion.trim();
    }

    public String getPropscode() {
        return propscode;
    }

    public void setPropscode(String propscode) {
        this.propscode = propscode == null ? null : propscode.trim();
    }

    public String getPropstype() {
        return propstype;
    }

    public void setPropstype(String propstype) {
        this.propstype = propstype == null ? null : propstype.trim();
    }

    public String getLineno() {
        return lineno;
    }

    public void setLineno(String lineno) {
        this.lineno = lineno == null ? null : lineno.trim();
    }
}