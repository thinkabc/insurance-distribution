package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.ArrayList;
import java.util.List;

public class PrpdlogoperationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PrpdlogoperationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andLogidIsNull() {
            addCriterion("LOGID is null");
            return (Criteria) this;
        }

        public Criteria andLogidIsNotNull() {
            addCriterion("LOGID is not null");
            return (Criteria) this;
        }

        public Criteria andLogidEqualTo(Long value) {
            addCriterion("LOGID =", value, "logid");
            return (Criteria) this;
        }

        public Criteria andLogidNotEqualTo(Long value) {
            addCriterion("LOGID <>", value, "logid");
            return (Criteria) this;
        }

        public Criteria andLogidGreaterThan(Long value) {
            addCriterion("LOGID >", value, "logid");
            return (Criteria) this;
        }

        public Criteria andLogidGreaterThanOrEqualTo(Long value) {
            addCriterion("LOGID >=", value, "logid");
            return (Criteria) this;
        }

        public Criteria andLogidLessThan(Long value) {
            addCriterion("LOGID <", value, "logid");
            return (Criteria) this;
        }

        public Criteria andLogidLessThanOrEqualTo(Long value) {
            addCriterion("LOGID <=", value, "logid");
            return (Criteria) this;
        }

        public Criteria andLogidIn(List<Long> values) {
            addCriterion("LOGID in", values, "logid");
            return (Criteria) this;
        }

        public Criteria andLogidNotIn(List<Long> values) {
            addCriterion("LOGID not in", values, "logid");
            return (Criteria) this;
        }

        public Criteria andLogidBetween(Long value1, Long value2) {
            addCriterion("LOGID between", value1, value2, "logid");
            return (Criteria) this;
        }

        public Criteria andLogidNotBetween(Long value1, Long value2) {
            addCriterion("LOGID not between", value1, value2, "logid");
            return (Criteria) this;
        }

        public Criteria andBusinesskeyvalueIsNull() {
            addCriterion("BUSINESSKEYVALUE is null");
            return (Criteria) this;
        }

        public Criteria andBusinesskeyvalueIsNotNull() {
            addCriterion("BUSINESSKEYVALUE is not null");
            return (Criteria) this;
        }

        public Criteria andBusinesskeyvalueEqualTo(String value) {
            addCriterion("BUSINESSKEYVALUE =", value, "businesskeyvalue");
            return (Criteria) this;
        }

        public Criteria andBusinesskeyvalueNotEqualTo(String value) {
            addCriterion("BUSINESSKEYVALUE <>", value, "businesskeyvalue");
            return (Criteria) this;
        }

        public Criteria andBusinesskeyvalueGreaterThan(String value) {
            addCriterion("BUSINESSKEYVALUE >", value, "businesskeyvalue");
            return (Criteria) this;
        }

        public Criteria andBusinesskeyvalueGreaterThanOrEqualTo(String value) {
            addCriterion("BUSINESSKEYVALUE >=", value, "businesskeyvalue");
            return (Criteria) this;
        }

        public Criteria andBusinesskeyvalueLessThan(String value) {
            addCriterion("BUSINESSKEYVALUE <", value, "businesskeyvalue");
            return (Criteria) this;
        }

        public Criteria andBusinesskeyvalueLessThanOrEqualTo(String value) {
            addCriterion("BUSINESSKEYVALUE <=", value, "businesskeyvalue");
            return (Criteria) this;
        }

        public Criteria andBusinesskeyvalueLike(String value) {
            addCriterion("BUSINESSKEYVALUE like", value, "businesskeyvalue");
            return (Criteria) this;
        }

        public Criteria andBusinesskeyvalueNotLike(String value) {
            addCriterion("BUSINESSKEYVALUE not like", value, "businesskeyvalue");
            return (Criteria) this;
        }

        public Criteria andBusinesskeyvalueIn(List<String> values) {
            addCriterion("BUSINESSKEYVALUE in", values, "businesskeyvalue");
            return (Criteria) this;
        }

        public Criteria andBusinesskeyvalueNotIn(List<String> values) {
            addCriterion("BUSINESSKEYVALUE not in", values, "businesskeyvalue");
            return (Criteria) this;
        }

        public Criteria andBusinesskeyvalueBetween(String value1, String value2) {
            addCriterion("BUSINESSKEYVALUE between", value1, value2, "businesskeyvalue");
            return (Criteria) this;
        }

        public Criteria andBusinesskeyvalueNotBetween(String value1, String value2) {
            addCriterion("BUSINESSKEYVALUE not between", value1, value2, "businesskeyvalue");
            return (Criteria) this;
        }

        public Criteria andOperationtypeIsNull() {
            addCriterion("OPERATIONTYPE is null");
            return (Criteria) this;
        }

        public Criteria andOperationtypeIsNotNull() {
            addCriterion("OPERATIONTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andOperationtypeEqualTo(String value) {
            addCriterion("OPERATIONTYPE =", value, "operationtype");
            return (Criteria) this;
        }

        public Criteria andOperationtypeNotEqualTo(String value) {
            addCriterion("OPERATIONTYPE <>", value, "operationtype");
            return (Criteria) this;
        }

        public Criteria andOperationtypeGreaterThan(String value) {
            addCriterion("OPERATIONTYPE >", value, "operationtype");
            return (Criteria) this;
        }

        public Criteria andOperationtypeGreaterThanOrEqualTo(String value) {
            addCriterion("OPERATIONTYPE >=", value, "operationtype");
            return (Criteria) this;
        }

        public Criteria andOperationtypeLessThan(String value) {
            addCriterion("OPERATIONTYPE <", value, "operationtype");
            return (Criteria) this;
        }

        public Criteria andOperationtypeLessThanOrEqualTo(String value) {
            addCriterion("OPERATIONTYPE <=", value, "operationtype");
            return (Criteria) this;
        }

        public Criteria andOperationtypeLike(String value) {
            addCriterion("OPERATIONTYPE like", value, "operationtype");
            return (Criteria) this;
        }

        public Criteria andOperationtypeNotLike(String value) {
            addCriterion("OPERATIONTYPE not like", value, "operationtype");
            return (Criteria) this;
        }

        public Criteria andOperationtypeIn(List<String> values) {
            addCriterion("OPERATIONTYPE in", values, "operationtype");
            return (Criteria) this;
        }

        public Criteria andOperationtypeNotIn(List<String> values) {
            addCriterion("OPERATIONTYPE not in", values, "operationtype");
            return (Criteria) this;
        }

        public Criteria andOperationtypeBetween(String value1, String value2) {
            addCriterion("OPERATIONTYPE between", value1, value2, "operationtype");
            return (Criteria) this;
        }

        public Criteria andOperationtypeNotBetween(String value1, String value2) {
            addCriterion("OPERATIONTYPE not between", value1, value2, "operationtype");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}