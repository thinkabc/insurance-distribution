package cn.com.libertymutual.core.base.dto;

/**
 * 个性化批量发送短信接收请求参数实体
* @ClassName: MultiMessageRequestDto 
* @Description: TODO
* @author pengmin
* @date 2017年7月31日 上午11:16:58 
*
 */
public class MultiMessageRequestDto extends RequestBaseDto
{

	private static final long serialVersionUID = 1L;
	
	// 时间戳,格式为:MMDDHHMMSS,即月日时分秒,定长10位,月日时分秒不足2位时左补0.时间戳请获取您真实的服务器时间,不要填写固定的时间,否则pwd参数起不到加密作用
	private String	timestamp;
	
	// 个性化信息详情：multimt中最多可支持100个手机号的个性化信息
	private String multimt;

	// 分公司编码
	private String branch;
	
	// 登录账号
	private String loginNo;
	
	// 系统来源
	private String systemSource;
	
	// 备用
	// private List<MultiMt> multiMts;
	
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getLoginNo() {
		return loginNo;
	}

	public void setLoginNo(String loginNo) {
		this.loginNo = loginNo;
	}

	public String getSystemSource() {
		return systemSource;
	}

	public void setSystemSource(String systemSource) {
		this.systemSource = systemSource;
	}

	public String getMultimt() {
		return multimt;
	}

	public void setMultimt(String multimt) {
		this.multimt = multimt;
	}

	// public List<MultiMt> getMultiMts() {
	// return multiMts;
	// }
	//
	// public void setMultiMts(List<MultiMt> multiMts) {
	// this.multiMts = multiMts;
	// }
}
