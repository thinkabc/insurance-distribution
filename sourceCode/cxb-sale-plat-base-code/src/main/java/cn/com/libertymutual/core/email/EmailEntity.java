package cn.com.libertymutual.core.email;

public class EmailEntity implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9118788637489924018L;
	// 邮箱服务器的登录用户名
	private String username = "UserIDManager";
	// 邮箱服务器的密码
	private String password = "VXNlcklEMTIz"; // "VXNlcklEMTIz"
	// 邮箱服务器smtp host
	private String smtpHost = "smtp.libertymutual.com.cn";
	// 发送方的邮箱（必须为邮箱服务器的登录用户名）
	private String fromEmail = "UserIDManager@libertymutual.com.cn";

	private String displayFromUserName = "利宝保险";

	// 发送方姓名
	// private String fromUsername;
	private String timeout = "30000";

	private boolean isAuth = true;
	private boolean multipart = false;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public String getFromEmail() {
		return fromEmail;
	}

	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}

	public String getDisplayFromUserName() {
		return displayFromUserName;
	}

	public void setDisplayFromUserName(String displayFromUserName) {
		this.displayFromUserName = displayFromUserName;
	}

	public String getTimeout() {
		return timeout;
	}

	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}

	public boolean isAuth() {
		return isAuth;
	}

	public void setAuth(boolean isAuth) {
		this.isAuth = isAuth;
	}

	public boolean isMultipart() {
		return multipart;
	}

	public void setMultipart(boolean multipart) {
		this.multipart = multipart;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((displayFromUserName == null) ? 0 : displayFromUserName.hashCode());
		result = prime * result + ((fromEmail == null) ? 0 : fromEmail.hashCode());
		result = prime * result + (isAuth ? 1231 : 1237);
		result = prime * result + (multipart ? 1231 : 1237);
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((smtpHost == null) ? 0 : smtpHost.hashCode());
		result = prime * result + ((timeout == null) ? 0 : timeout.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof EmailEntity))
			return false;
		EmailEntity other = (EmailEntity) obj;
		if (displayFromUserName == null) {
			if (other.displayFromUserName != null)
				return false;
		} else if (!displayFromUserName.equals(other.displayFromUserName))
			return false;
		if (fromEmail == null) {
			if (other.fromEmail != null)
				return false;
		} else if (!fromEmail.equals(other.fromEmail))
			return false;
		if (isAuth != other.isAuth)
			return false;
		if (multipart != other.multipart)
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (smtpHost == null) {
			if (other.smtpHost != null)
				return false;
		} else if (!smtpHost.equals(other.smtpHost))
			return false;
		if (timeout == null) {
			if (other.timeout != null)
				return false;
		} else if (!timeout.equals(other.timeout))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmailEntity [username=").append(username).append(", password=").append(password).append(", smtpHost=").append(smtpHost)
				.append(", fromEmail=").append(fromEmail).append(", displayFromUserName=").append(displayFromUserName).append(", timeout=")
				.append(timeout).append(", isAuth=").append(isAuth).append(", multipart=").append(multipart).append("]");
		return builder.toString();
	}

}
