/*
 *	Copyright (c) liberty 2016
 *	cn.com.libertymutual.core.exception
 *  Create Date 2016年4月28日
 *	@author tracy.liao
 */
package cn.com.libertymutual.core.exception;

import com.google.common.base.Strings;


/**
 * 业务异常
 * @author tracy.liao
 * @date 2016年4月28日
 */
public enum BusinessExceptionEnums implements ExceptionEnums{
	PARAMETER_NOT_NULL(3001,"参数不能为空!"),
	PARAMETER_TOO_LONG(3002,"参数值过长!"),
	PARAMETER_TYPE_ERROR(3003,"参数类型不正确!"),
	DOCUMENTNO_ISNULL_EXCEPTION(3009,"保单号或投保单号为空!"),
	DOCUMENTNO_EXCEPTION(3010,"保单或者投保单号不是正确的格式!"),
	NOT_FOUNT_VERIFY_FLAG(3011,"未获取到转保标识!"),
	NOT_MODIFY_DATA(3012,"未修改数据无须重复计算!"),
	/**计算保额失败，原因是折旧系数为0*/
	DEPRECIATION_IS_ZERO(3013,"计算保额失败，原因是折旧系数为0!"),
	/**计算保额失败，原因是折旧系数未能正确获取*/
	DEPRECIATION_NOT_FOUND(3014,"数据不正确，请确保车辆种类,车辆使用性质,座位数,车辆初登年月,商业险起保日期的数据正确!"),
	/**上传文件出错*/
	UPLOAD_DATA_EXCEPTION(3015,"上传文件出错"),
	/**获取上传文件出错*/
	GET_UPLOAD_FILE_EXCEPTION(3016,"获取上传文件出错"),
	/**上传文件不能为空*/
	UPLOAD_FILE_NUMBER_EXCEPTION(3017,"上传文件不能为空"),
	/**套餐配置错误,无法进行投保操作！*/
	PACK_ERROR_1(3018,"套餐配置错误,无法进行投保操作！"),
	/**"套餐配置不足三个或套餐查询错误！*/
	PACK_ERROR_2(3019,"套餐配置不足三个或套餐查询错误！"),
	/**套餐已存在*/
	PACK_ERROR_3(3020,"套餐已存在!"),
	/**查询套餐列表失败！*/
	PACK_ERROR_4(3021,"查询套餐列表失败！"),
	/**查询险种失败！*/
	QUERY_RISK_ERROR(3022,"查询险种失败！"),
	/**没有此套餐配置！*/
	PACK_ERROR_5(3023,"没有此套餐配置！"),
	/**套餐不存在，不能进行修改*/
	PACK_ERROR_6(3024,"套餐不存在，不能进行修改!"),
	CAR_TIEM_ERROR_1(3025,"平台查询失败,平台查询失败,军车不允许投保交强险!"),
	CAR_TIEM_EXT_ERROR_2(3026,"平台查询失败,转移登记日期不能为空!"),
	CAR_TIEM_EXT_ERROR_3(3027,"平台查询失败,转移登记日期格式不正确!"),
	CAR_TIEM_ERROR_4(3028,"平台查询失败,企业性质不能为空!"),
	CAR_TIEM_ERROR_5(3029,"平台查询失败,外地车不能为本地车车牌号!"),
	CAR_TIEM_ERROR_6(3030,"平台查询失败,本地车不能为外地车车牌号!"),
	CAR_TIEM_ERROR_7(3031,"平台查询失败,购买日期不能为空!"),
	CAR_TIEM_ERROR_8(3032,"平台查询失败,购买日期格式不正确!"),
	CAR_TIEM_EXT_ERROR_9(3033,"平台查询失败,转移登记日期不能大于当前日期"),
	CAR_TIEM_ERROR_10(3034,"平台查询失败,购买日期不能大于当前日期"),
	CAR_TIEM_ERROR_11(3035,"平台查询失败,车辆初登日期不能为空!"),
	CAR_TIEM_ERROR_12(3036,"平台查询失败,车辆初登日期不能大于当前日期"),
	CAR_TIEM_ERROR_13(3037,"平台查询失败,核定排量不正确"),
	CAR_TIEM_ERROR_14(3038,"平台查询失败,核定排量必须大于0"),
	CAR_TIEM_ERROR_15(3039,"平台查询失败,核定排量必须小于100000"),
	CAR_TIEM_ERROR_16(3040,"平台查询失败,吨位数不正确"),
	CAR_TIEM_ERROR_17A(3041,"平台查询失败,承保车辆为客车时，吨位数必须为0"),
	CAR_TIEM_ERROR_17(3041,"平台查询失败,承保车辆为货车时，吨位数必须大于0"),
	CAR_TIEM_ERROR_18(3042,"平台查询失败,承保车辆为货车时，吨位数必须小于100000"),
	CAR_TIEM_ERROR_19(3043,"平台查询失败,座位数不正确"),
	CAR_TIEM_ERROR_20(3044,"平台查询失败,座位数不能小于0"),
	CAR_TIEM_ERROR_21(3045,"平台查询失败,发动机号不能为空"),
	CAR_TIEM_ERROR_22(3046,"平台查询失败,发动机号不能大于50个字符"),
	CAR_TIEM_ERROR_23(3047,"平台查询失败,VIN码长度不为17"),
	CAR_TIEM_ERROR_24(3048,"平台查询失败,VIN码由数字和英文字母组成，但英文字母I、O和Q不能使用"),
	CAR_TIEM_ERROR_26(3049,"平台查询失败,商业险起保时间必须大于当前日期"),
	CAR_TIEM_ERROR_27(3050,"平台查询失败,交强险起保时间必须大于当前日期"),	
	CAR_TIEM_ERROR_28(3051,"平台查询失败,商业险起保日期格式不正确"),
	CAR_TIEM_ERROR_29(3052,"平台查询失败,交强险起保时间格式不正确"),
	CAR_TIEM_ERROR_30(3053,"平台查询失败,商业险终保时间必须大于等于起保日期"),
	CAR_TIEM_ERROR_31(3054,"平台查询失败,交强险终保时间必须大于等于起保日期"),
	CAR_TIEM_EXT_ERROR_32(3055,"平台询价查询失败,整备质量不允许为空"),
	CAR_TIEM_EXT_ERROR_33(3056,"平台询价查询失败:整备质量(千克)应该为整数"),
	CAR_TIEM_EXT_ERROR_34(3057,"平台询价查询失败:整备质量小于0或大于150000"),
	CAR_OWEN_ERROR_35(3058,"平台询价查询失败:车主名称不允许为空。"),
	CAR_PRP_TCAR_SHIP_TAX_1(3059,"平台询价查询失败:本年纳税信息减免信息中减免税原因代码不允许为空"),
	CAR_PRP_TCAR_SHIP_TAX_2(3060,"平台询价查询失败:纳税类型代码不允许为空"),
	CAR_PRP_TCAR_SHIP_TAX_3(3061,"平台询价查询失败,本年纳税信息完税信息中开具完税凭证的税务机关名称不允许为空"),
	CAR_PRP_TCAR_SHIP_TAX_4(3062,"平台询价查询失败,金额减免不能填写减免比例字段"),
	CAR_PRP_TCAR_SHIP_TAX_5(3063,"平台询价查询失败,比例减免不能填写减免金额字段"),
	CAR_PRP_TCAR_SHIP_TAX_6(3064,"平台询价查询失败,选择了免税，不能填写减免比例"),
	CAR_PRP_TCAR_SHIP_TAX_7(3065,"平台询价查询失败,投保查询时纳税类型不允许为拒缴"),
	CAR_PRP_TCAR_SHIP_TAX_8(3066,"平台询价查询失败,减免税凭证号不能为空"),
	CAR_PRP_TCAR_SHIP_TAX_9(3067,"平台询价查询失败,减税金额不能为空"),
	CAR_PRP_TCAR_SHIP_TAX_10(3068,"平台询价失败,减税金额不是一个有效的数字"),
	CAR_PRP_TCAR_SHIP_TAX_11(3069,"平台询价失败,减税金额不能小于0"),
	CAR_PRP_TCAR_SHIP_TAX_12(3067,"平台询价查询失败,减免比例不能为空"),
	
	CAR_TIEM_ERROR_32(3070,"平台询价失败,车牌号不能为空或长度太短"),
	CAR_TIEM_ERROR_33(3071,"平台询价失败,未上牌车不能录入车牌号"),
	
	COMMON_ERROR_3072(3072,"缺少主要信息，请联系系统管理员"),
	COMMON_ERROR_3073(3073,"缺少联合投保标识，联系系统管理员"),
	
	CALULATION_ERROR_3074(3074,"险别代码不能为空"),
	CALULATION_ERROR_3075(3075," 保额必须大于0"),
	CALULATION_ERROR_3076(3076," 单位保额必须大于0"),
	
	COMMON_ERROR_DATEFORMATE_3077(3077,"日期格式不正确"),
	COMMON_ERROR_STARTDATE_3079(3079,"起保时间不允许为空,且格式必须为yyyy-MM-dd"),	
	PARAMETER_DATA_ERROR(3080,"请求参数有误!"),
	
	CALULATION_KIND_ERROR_3081(3081,"险别信息校验未通过!"),
	COMMON_ERROR_IDCARD_3078(3078,"身份证格式不正确")
	;

	
	
	public int code;
	public String message;

	BusinessExceptionEnums(int code, String message) {
		this.code = code;
		this.message = message;
	}

	@Override
	public int getCode() {
		// TODO Auto-generated method stub
		return code;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return message;
	}
	
	public String getCodeAndMessage(){
		
		//return String.valueOf(code)+"-"+message;
		return String.format("%d-%s", code, Strings.isNullOrEmpty(message) ? "系统异常，如果多次出现，请联系管理员" : message );
		
	}
	

}
