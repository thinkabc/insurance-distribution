package cn.com.libertymutual.core.base.dto;

import java.util.List;

public class TQueryLmAgentResponseDto extends ResponseBaseDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5410513158299945211L;
	// 純粹的業務關係代碼
	private List<PrpLmAgent> prpLmAgentList;

	/**是否需要选择销售人员，仅用于业务关系代码认证时判断*/
	private boolean selectSalePerson = false;

	public boolean isSelectSalePerson() {
		return selectSalePerson;
	}

	public void setSelectSalePerson(boolean selectSalePerson) {
		this.selectSalePerson = selectSalePerson;
	}

	public List<PrpLmAgent> getPrpLmAgentList() {
		return prpLmAgentList;
	}

	public void setPrpLmAgentList(List<PrpLmAgent> prpLmAgentList) {
		this.prpLmAgentList = prpLmAgentList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((prpLmAgentList == null) ? 0 : prpLmAgentList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TQueryLmAgentResponseDto other = (TQueryLmAgentResponseDto) obj;
		if (prpLmAgentList == null) {
			if (other.prpLmAgentList != null)
				return false;
		} else if (!prpLmAgentList.equals(other.prpLmAgentList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TQueryLmAgentResponseDto [prpLmAgentList=" + prpLmAgentList + "]";
	}

}
