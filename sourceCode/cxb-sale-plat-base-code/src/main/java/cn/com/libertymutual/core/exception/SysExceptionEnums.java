/*
 *	Copyright (c) liberty 2016
 *	cn.com.libertymutual.core.exception
 *  Create Date 2016年4月29日
 *	@author tracy.liao
 */
package cn.com.libertymutual.core.exception;

/**
 * 系统异常 (7****)
 * @author tracy.liao
 * @date 2016年4月29日
 */
public enum SysExceptionEnums implements ExceptionEnums {
	
	
	SYS_USER_ERROR_EXCEPTION(7001,"用户信息校验失败,未查询到用户信息!" ),
	SYS_SOA_SHORTURL_EXCEPTION(7002,"短地址服务参数无效!" ),
	SYS_SOA_SHORTURL_URL_EXCEPTION(7003,"请求原地址不存在!" ),
	SYS_SOA_SHORTURL_POST_ACTION_NOPARAM_EXCEPTION(7004,"POST方式,请提交参数!" ),
	SYS_SOA_FROM_SYSTEM_EXCEPTION(7005,"系统来源参数不正确" ),
	
	SYS_EXCEPTION(7000,"系统异常")
	;

	public int code;
	public String message;

	SysExceptionEnums(int code, String message) {
		this.code = code;
		this.message = message;
	}

	@Override
	public int getCode() {
		// TODO Auto-generated method stub
		return code;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return message;
	}

}
