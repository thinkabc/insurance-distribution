package cn.com.libertymutual.core.query;

import org.hibernate.internal.util.StringHelper;

/**
 * 原生SQL查询组装
 * 
 * @author LuoGang
 *
 */
public class SqlQuery extends SqlWhere implements Query {

	public SqlQuery() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 仅使用select,from构造对象
	 * 
	 * @param select
	 * @param from
	 */
	public SqlQuery(String select, String from) {
		this.select = select;
		this.from = from;
	}

	// public SqlQuery(String where, List<Object> parameters) {
	// super(where, parameters);
	// }

	// public SqlQuery(String select, String from, String where, List<Object> parameters) {
	// super(where, parameters);
	// this.select = select;
	// this.from = from;
	// }

	/** 要查询的Sql的select语句,如SELECT * FROM TABLE A */
	private String select;

	/** 查询的from语句后的语句 */
	private String from;

	/** GROUP BY 语句。一般的，groupBy与orderBy不会同时存在 */
	private String groupBy;

	/** ORDER BY 排序语句 */
	private String orderBy;

	/** 查询总数的SQL语句,可以通过外部注入 */
	private String countSql;

	/**
	 * 返回查询SQL语句，格式如：SELECT * FROM TABLE WHERE xxx ORDER BY yyy
	 * 
	 * @return
	 */
	public String toSql() {
		StringBuilder sql = new StringBuilder("SELECT ");
		sql.append(select).append(" FROM ").append(from);
		String where = this.where();
		if (!where.isEmpty()) {
			sql.append(" WHERE ").append(where);
		}
		if (StringHelper.isNotEmpty(groupBy)) {
			sql.append(" GROUP BY ").append(groupBy);
		}
		if (StringHelper.isNotEmpty(orderBy)) {
			sql.append(" ORDER BY ").append(orderBy);
		}

		return sql.toString();
	}

	/**
	 * 返回查询总语句的SQL
	 * 
	 * @return
	 */
	public String toCountSql() {
		if (StringHelper.isNotEmpty(countSql)) return this.getCountSql();

		StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM (");
		sql.append(" select ").append(select).append(" from ");
		sql.append(from);
		String where = this.where();
		if (!where.isEmpty()) {
			sql.append(" WHERE ").append(where);
		}
		if (StringHelper.isNotEmpty(groupBy)) {
			sql.append(" GROUP BY ").append(groupBy);
		}
		sql.append(") t");
		return sql.toString();
	}

	protected String getCountSql() {
		return countSql;
	}

	public void setCountSql(String countSql) {
		this.countSql = countSql;
	}

	public String getSelect() {
		return select;
	}

	public void setSelect(String select) {
		this.select = select;
	}

	public String getGroupBy() {
		return groupBy;
	}

	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	@Override
	public String toString() {
		return this.toSql();
	}
}
