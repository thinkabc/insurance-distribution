package cn.com.libertymutual.core.common.logback;

import org.slf4j.MDC;
import org.springframework.core.env.ConfigurableEnvironment;

import cn.com.libertymutual.core.web.util.RequestUtils;


public class MDCParameters {

	public void init( ConfigurableEnvironment env ) {
		AppPlatInfo api = new AppPlatInfo();
		
		String[] hostinfos = RequestUtils.getLocalHostLANAddress();
		
		
		api.setLocalHostIP(hostinfos[0]);
		api.setLocalHostName(hostinfos[1]);
		api.setServiceName( env.getProperty("spring.application.name", ""));
		
		
	}
	
	public static void setSNMDC() {
		//MDC.put("ip", AppPlatInfo.getLocalHostIP());
		MDC.put("sn", AppPlatInfo.getServiceName());
	}
}
