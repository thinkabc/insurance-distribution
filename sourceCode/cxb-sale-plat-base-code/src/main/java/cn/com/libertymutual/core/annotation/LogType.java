package cn.com.libertymutual.core.annotation;

public enum LogType {
	OPER,
	SYSTEM,
	CONTROLLER,
	SERVICE,
	LOGIN,
	LOGOUT,
	ADD,
	UPDATE,
	DELETE,
	QUERY,
	
	OTHER
}
