package cn.com.libertymutual.core.common.json.databind.annotation;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;



@Component("webContextFilter")
public class WebContextFilter implements Filter {
	private  Logger log = LoggerFactory.getLogger(this.getClass());  
	@Override
	public void destroy() {
		log.info("destroy ");
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        ServletContext servletContext = request.getSession().getServletContext();
        WebContext.create(request, response, servletContext);

        chain.doFilter(request, response);

        WebContext.clear();
	}

	@Override
	public void init(FilterConfig arg0) {
		/*try {
			ApplicationContextUtil.getInstance().init( arg0.getServletContext() );
		} catch (Exception e) {

			log.error(e.getMessage()); 
			throw new CustomLangException(e);
		}*/
	}
}