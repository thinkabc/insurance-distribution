package cn.com.libertymutual.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SystemLog {

	/**
	 * 类型
	 * @return
	 */
	LogType type() default LogType.SERVICE;

	/**
	 * 描述
	 * @return
	 */
	public String description() default "";

	/**
	 * 是否记录日志表
	 * @return
	 */
	public boolean dbLog() default false;

	/**
	 * 是否推送日志
	 * @return
	 */
	public boolean pushLog() default true;
}
