import { MENU_URL } from '../api/api';
export default {
    logout(_this) {
        this.$confirm('确认退出吗?', '提示', {
            //type: 'warning'
        }).then(() => {
            sessionStorage.removeItem('user');
            _this.$router.push('/login');
        }).catch(() => {
        });
    },
    lookupUrl(_this, chilmenu) {
        for (let j = 0; j < chilmenu.length; j++) {
            if (chilmenu[j].child != null) {
                _this.lookupUrl(chilmenu[j].child);
            }
            if (chilmenu[j].loadflag == 1) {
                let componentOpen = {
                    path: chilmenu[j].menuurl + "open",
                    component: require("." + chilmenu[j].menuurl + ".vue")

                };
                _this.routeArray.push(componentOpen);
            }
            if (chilmenu[j].menuurl != null && chilmenu[j].menuurl != "") {
                let component = {
                    path: chilmenu[j].menuurl,
                    // component:resolve => require(["." + chilmenu[j].menuurl+".vue"], resolve)
                    // component:TestAAAAA
                    component: require("." + chilmenu[j].menuurl + ".vue")

                };
                _this.menuinfo.push(component);
            }
        }
    },
    getMenuInfo(_this) {
        var menuParams = { appFlag: "1", fmenuId: "1" };
        $.ajax({
            type: "POST",
            url: MENU_URL,
            async: false,
            data: menuParams,
            success: function (data, status, xhr) {
                if (data.success == false) {
                    _this.$message({
                        message: data.result,
                        type: 'error'
                    });
                    sessionStorage.removeItem('user');
                    if (sessionStorage.getItem('loginStatus') != '0') {
                        _this.$router.push('/login');
                    }
                } else {
                    _this.menu = data.result.menu;
                }
            }
        });
    }

}