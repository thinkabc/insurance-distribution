import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations'
import actions from './action'


Vue.use(Vuex)

const state = {
  user: {},
  count: 0,
  riskCodelist: [],//险种
  branchCodelist: [],//机构
  productList: [],//产品
  skipUrl: '',
  IsFirstURl: true,
  winWidth: 0,//浏览器宽度
  winHeight: 0,//浏览器高度
  editorPicture: '',//文本编辑器上传图片保存文件
  echartObj: '',//图表obj
  mainLeft: '200px',//首页left
  hotAreaList: [],//常驻地区列表
  productCompanylist: [],//产品所属公司
}



export default new Vuex.Store({
  state,
  actions,
  mutations
})