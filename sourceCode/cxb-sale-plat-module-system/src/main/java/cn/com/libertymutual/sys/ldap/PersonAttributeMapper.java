package cn.com.libertymutual.sys.ldap;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

import org.springframework.ldap.core.AttributesMapper;

public class PersonAttributeMapper implements AttributesMapper{  


	@Override
	public Object mapFromAttributes(Attributes attr) throws NamingException {
		Person person = new Person();
		person.setCn((String)attr.get("cn").get());
		
		if (attr.get("company") != null) {
			person.setCompany((String)attr.get("company").get());
		}
		if (attr.get("department") != null) {
			person.setDepartment((String)attr.get("department").get());
		}
		if (attr.get("displayName") != null) {
			person.setDisplayName((String)attr.get("displayName").get());
		}
		if (attr.get("homephone") != null) {
			person.setHomephone((String)attr.get("homephone").get());
		}
		if (attr.get("mail") != null) {
			person.setMail((String)attr.get("mail").get());
		}
		if (attr.get("manager") != null) {
			person.setManager((String)attr.get("manager").get());
		}
		if (attr.get("mobile") != null) {
			person.setMobile((String)attr.get("mobile").get());
		}
		if (attr.get("sAMAccountName") != null) {
			person.setsAMAccountName((String)attr.get("sAMAccountName").get());
		}
		if (attr.get("streetAddress") != null) {
			person.setStreetAddress((String)attr.get("streetAddress").get());
		}
		 
		if (attr.get("title") != null) {
			person.setTitle((String)attr.get("title").get());
		}
		
		if (attr.get("telephoneNumber") != null) {
			person.setTelephoneNumber((String)attr.get("telephoneNumber").get());
		}
		if (attr.get("description") != null) {
			person.setDescription((String)attr.get("description").get());
		}
		return person;
	}
	
}
