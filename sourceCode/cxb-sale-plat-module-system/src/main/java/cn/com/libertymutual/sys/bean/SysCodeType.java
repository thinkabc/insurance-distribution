package cn.com.libertymutual.sys.bean;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_sys_code_type",  catalog = "")
public class SysCodeType  implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -1499402525753170868L;
	private String codeType;
    private String codeTypeName;
    private String type;

    @Id
    @Column(name = "CODE_TYPE", nullable = false, length = 50)
    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    @Basic
    @Column(name = "CODE_TYPE_NAME", nullable = false, length = 100)
    public String getCodeTypeName() {
        return codeTypeName;
    }

    public void setCodeTypeName(String codeTypeName) {
        this.codeTypeName = codeTypeName;
    }

    @Basic
    @Column(name = "TYPE", nullable = true, length = 1)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysCodeType that = (SysCodeType) o;

        if (codeType != null ? !codeType.equals(that.codeType) : that.codeType != null) return false;
        if (codeTypeName != null ? !codeTypeName.equals(that.codeTypeName) : that.codeTypeName != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = codeType != null ? codeType.hashCode() : 0;
        result = 31 * result + (codeTypeName != null ? codeTypeName.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

	@Override
	public String toString() {
		return "SysCodeType [codeType=" + codeType + ", codeTypeName="
				+ codeTypeName + ", type=" + type + "]";
	}
}
