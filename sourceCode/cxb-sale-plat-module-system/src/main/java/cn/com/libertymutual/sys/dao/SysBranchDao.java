package cn.com.libertymutual.sys.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sys.bean.SysBranch;

@Repository
public interface SysBranchDao extends PagingAndSortingRepository<SysBranch, String>, JpaSpecificationExecutor<SysBranch>{

	@Query("from SysBranch where branchLevel = ?1")
	List<SysBranch> findByLevel(String level);

	@Query("from SysBranch where upperBranchNo = ?1")
	List<SysBranch> findNextBranch(String branchNo);

	
	@Query("from SysBranch where branchNo = ?1")
	SysBranch findByBranchNo(String branchNo);

	@Query(value="select MAX(BRANCH_NO) from tb_sys_branch where UPPER_BRANCH_NO=?1 ",nativeQuery=true)
	String findMaxBranchNo(String branchNo);
	
	@Query("from SysBranch where validStatus = '1'")
	List<SysBranch> findAllBranch();
}
