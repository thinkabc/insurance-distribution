package cn.com.libertymutual.sys.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sys.bean.SysUserMenuRes;
import cn.com.libertymutual.sys.bean.SysUserMenuResPK;


@Repository
public interface ISysUserMenuResDao  extends JpaRepository<SysUserMenuRes, SysUserMenuResPK> 
{
	
	
	//@Query( "SELECT DISTINCT t1.menuid FROM tb_sys_role_menu t1 INNER JOIN tb_sys_menu t2 ON t1.menuid=t2.menuid WHERE t1.roleid IN (?1) GROUP BY t1.menuid ORDER BY t2.orderid")
	//List<Integer> findRoleMenu(List<String> roleList, Integer fmenuId) ;
	
	
	List<SysUserMenuRes> findByUserid(String userId) ;
}
