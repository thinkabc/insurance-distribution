package cn.com.libertymutual.sys.bean;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Ryan on 2016-09-09.
 */
public class SysRoleMenuResPK implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4579639899021895605L;
	private String roleid;
    private int menuid;
    private String resid;

    @Column(name = "ROLEID", nullable = false, length = 16)
    @Id
    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    @Column(name = "MENUID", nullable = false)
    @Id
    public int getMenuid() {
        return menuid;
    }

    public void setMenuid(int menuid) {
        this.menuid = menuid;
    }

    @Column(name = "RESID", nullable = false, length = 32)
    @Id
    public String getResid() {
        return resid;
    }

    public void setResid(String resid) {
        this.resid = resid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysRoleMenuResPK that = (SysRoleMenuResPK) o;

        if (menuid != that.menuid) return false;
        if (roleid != null ? !roleid.equals(that.roleid) : that.roleid != null) return false;
        if (resid != null ? !resid.equals(that.resid) : that.resid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleid != null ? roleid.hashCode() : 0;
        result = 31 * result + menuid;
        result = 31 * result + (resid != null ? resid.hashCode() : 0);
        return result;
    }

	@Override
	public String toString() {
		return "SysRoleMenuResPK [roleid=" + roleid + ", menuid=" + menuid
				+ ", resid=" + resid + "]";
	}
}
