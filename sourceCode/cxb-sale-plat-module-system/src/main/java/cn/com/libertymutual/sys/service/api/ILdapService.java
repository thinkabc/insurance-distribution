package cn.com.libertymutual.sys.service.api;

import javax.naming.directory.DirContext;

import cn.com.libertymutual.core.web.ServiceResult;

public interface ILdapService {

/*	public ServiceResult modifyLdapMobile(CommonQueryRequestDTO commonQueryRequestDTO);

	public ServiceResult pwdExpireWarning(CommonQueryRequestDTO commonQueryRequestDTO);

	ServiceResult modifyLdapPwd(CommonQueryRequestDTO commonQueryRequestDTO);

	ServiceResult modifyLdapUserInfo(String uid, Map<String, String> map);*/

	public ServiceResult login(String userId, String userPwd);

	DirContext getDirContext(String userId, String userPwd);

	ServiceResult getUserInfoFromLdap(String userId); 
	
}
