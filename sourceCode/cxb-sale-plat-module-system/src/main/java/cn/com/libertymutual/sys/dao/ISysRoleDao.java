package cn.com.libertymutual.sys.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sys.bean.SysRole;

@Repository
public interface ISysRoleDao extends PagingAndSortingRepository<SysRole, String>, JpaSpecificationExecutor<SysRole> {
	@Query(value = " select r.roleid from SysRole  r ,SysRoleUser u where r.roleid=u.roleid and u.userid=?1 and r.status = '1' ")
	List<Integer> findRolesByUserId(String userId);
	// @Query( "SELECT DISTINCT t1.menuid FROM tb_sys_role_menu t1 INNER JOIN
	// tb_sys_menu t2 ON t1.menuid=t2.menuid WHERE t1.roleid IN (?1) GROUP BY
	// t1.menuid ORDER BY t2.orderid")
	// List<Integer> findRoleMenu(List<String> roleList, Integer fmenuId) ;

//	@Query("select max(sr.roleid) from SysRole as sr where sr.status = '0'  ")
//	int findMax();

	SysRole findByRoleid(int roleid);

	@Modifying
	@Transactional
	@Query(value = "update SysRole set status = :status where roleid = :roleId  ")
	void updateStatus(@Param("status") String status,@Param("roleId") Integer roleId);

	// @Query("select menuid from tb_sys_role_menu where roleid=:roleId order by
	// menuid")
	// List<Integer> findMenuIdList( String roleId );
}
