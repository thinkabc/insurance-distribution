package cn.com.libertymutual.sys.bean;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_sys_role_url",  catalog = "")
@IdClass(SysRoleUrlPK.class)
public class SysRoleUrl  implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1830535183561407906L;
	private String roleid;
    private String url;

    @Id
    @Column(name = "ROLEID", nullable = false, length = 16)
    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    @Id
    @Column(name = "URL", nullable = false, length = 64)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysRoleUrl that = (SysRoleUrl) o;

        if (roleid != null ? !roleid.equals(that.roleid) : that.roleid != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleid != null ? roleid.hashCode() : 0;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        return result;
    }

	@Override
	public String toString() {
		return "SysRoleUrl [roleid=" + roleid + ", url=" + url + "]";
	}
}
