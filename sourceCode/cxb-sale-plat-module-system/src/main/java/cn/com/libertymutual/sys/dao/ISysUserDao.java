package cn.com.libertymutual.sys.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sys.bean.SysUser;

public interface ISysUserDao  extends PagingAndSortingRepository<SysUser, String>, JpaSpecificationExecutor<SysUser>{

	@Query("select t from SysUser t where t.userid = ?1 and t.status = '1' ")
	SysUser findByUserid(String userid);
	
	
	@Modifying
	@Transactional
	@Query("update SysUser set status = ?1 where userid = ?2  ")
	int updateStatus(String status,String userid);

	@Query("select t from SysUser t where t.mobileno = ?1 and t.status = '1' ")
	List<SysUser> findByMobile(String mobile);

	@Query("select t from SysUser t where t.userCode = ?1 and t.status = '1' ")
	SysUser findByUserCode(String userCode);

	@Query(value = "select count(*) from tb_sys_user ", nativeQuery = true)
	Integer findTotalUser();

	@Query(value="select * from tb_sys_user u where u.USERID in (select ru.USERID from tb_sys_role_user ru where ru.ROLEID in (select r.ROLEID from tb_sys_role r where r.APPROVEAUTH=1))",nativeQuery=true)
	List<SysUser> findApproveUser();
}
