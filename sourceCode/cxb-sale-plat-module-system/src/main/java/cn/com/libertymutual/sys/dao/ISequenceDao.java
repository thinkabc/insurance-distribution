/*
 *	Copyright (c) liberty 2016
 *	cn.com.libertymutual.insure.dao.api
 *  Create Date 2016年4月7日
 *	@author tracy.liao
 */
package cn.com.libertymutual.sys.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import cn.com.libertymutual.sys.bean.SysSequence;

/**
 * 系统序列dao层接口
 * @author tracy.liao
 * @date 2016年4月7日
 */
public interface ISequenceDao  extends JpaRepository<SysSequence, Integer>{
	/**
	 * 根据序列名称获取序列下一个值
	 * @param sequenceName 序列名称
	 * @return 序列值
	 */
	
	@Query(value="select nextval( ?1 ) nextValue ",nativeQuery=true)
	public int  getSequenceNumByName(String sequenceName);
}
