package cn.com.libertymutual.sys.bean;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Ryan on 2016-09-09.
 */
public class SysRoleMenuPK implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 6237968357181863199L;
	private String roleid;
    private int menuid;

    @Column(name = "ROLEID", nullable = false, length = 16)
    @Id
    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    @Column(name = "MENUID", nullable = false)
    @Id
    public int getMenuid() {
        return menuid;
    }

    public void setMenuid(int menuid) {
        this.menuid = menuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysRoleMenuPK that = (SysRoleMenuPK) o;

        if (menuid != that.menuid) return false;
        if (roleid != null ? !roleid.equals(that.roleid) : that.roleid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleid != null ? roleid.hashCode() : 0;
        result = 31 * result + menuid;
        return result;
    }

	@Override
	public String toString() {
		return "SysRoleMenuPK [roleid=" + roleid + ", menuid=" + menuid + "]";
	}
}
