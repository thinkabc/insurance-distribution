package cn.com.libertymutual.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.sys.bean.SysCodeType;
import cn.com.libertymutual.sys.dao.ISysCodeTypeDao;
import cn.com.libertymutual.sys.service.api.ISysCodeTypeService;

import com.google.common.collect.Lists;

@Service("codeTypeService")
public class SysCodeTypeService implements ISysCodeTypeService {
	@Autowired
	 ISysCodeTypeDao sysCodeTypeDao;
	@Override
	public List<SysCodeType> listAll() {
		Iterable<SysCodeType> sysCodeTypes= sysCodeTypeDao.findAll();
		List<SysCodeType> relist = Lists.newArrayList();
		for( SysCodeType type :sysCodeTypes ){
			relist.add(type);
		}
		return relist;
	}

	

}
