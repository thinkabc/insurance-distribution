package cn.com.libertymutual.core.database.druid;

/**
 * 本地线程，数据源上下文 
 * @author AoYi
 *
 */
public class DataSourceContextHolder {
	// 线程本地环境
	private static final ThreadLocal<String> local = new ThreadLocal<String>();

	public static ThreadLocal<String> getLocal() {
        return local;
    }

    /**
     * 读可能是多个库
     */
    public static void read() {
        local.set(DataSourceType.read.getType());
    }

    /**
     * 写只有一个库
     */
    public static void write() {
        local.set(DataSourceType.write.getType());
    }

    public static String getJdbcType() {
        return local.get();
    }
}
